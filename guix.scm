;; To use this file to build HEAD of vcflib:
;;
;;   guix build -f guix.scm
;;
;; To get a development container (emacs shell will work)
;;
;;   guix shell -C -D -f guix.scm
;;

(use-modules
  ((guix licenses) #:prefix license:)
  (guix gexp)
  (guix packages)
  (guix git-download)
  (guix build-system gnu)
  (gnu packages algebra)
  (gnu packages autotools)
  (gnu packages base)
  (gnu packages compression)
  (gnu packages build-tools)
  (gnu packages check)
  (gnu packages curl)
  (gnu packages gcc)
  (gnu packages gdb)
  (gnu packages guile)
  (gnu packages haskell-xyz) ; pandoc for help files
  (gnu packages llvm)
  (gnu packages parallel)
  (gnu packages pkg-config)
  (gnu packages tls)
  (gnu packages zig)
  (srfi srfi-1)
  (ice-9 popen)
  (ice-9 rdelim))

(define %source-dir (dirname (current-filename)))

(define %git-commit
    (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f 2" OPEN_READ)))

(define-public guile-zig-git
  (package
    (name "guile-zig-git")
    (version (git-version "0.0.1" "HEAD" %git-commit))
    (source (local-file %source-dir #:recursive? #t))
    (build-system gnu-build-system)
    (inputs
     `(
       ("gdb" ,gdb)
       ("guile" ,guile-3.0-latest)
       ("guile-debug" ,guile-3.0-latest "debug")
       ("zig" ,zig) ;; note we use zig-0.9.1
       ))
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (home-page "https://github.com/pjotrp/guile-zig")
    (synopsis "Example for binding guile agains zig")
    (description "Example for binding guile agains zig.")
    (license license:expat)))

guile-zig-git
