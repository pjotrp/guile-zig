pub const __builtin_bswap16 = @import("std").zig.c_builtins.__builtin_bswap16;
pub const __builtin_bswap32 = @import("std").zig.c_builtins.__builtin_bswap32;
pub const __builtin_bswap64 = @import("std").zig.c_builtins.__builtin_bswap64;
pub const __builtin_signbit = @import("std").zig.c_builtins.__builtin_signbit;
pub const __builtin_signbitf = @import("std").zig.c_builtins.__builtin_signbitf;
pub const __builtin_popcount = @import("std").zig.c_builtins.__builtin_popcount;
pub const __builtin_ctz = @import("std").zig.c_builtins.__builtin_ctz;
pub const __builtin_clz = @import("std").zig.c_builtins.__builtin_clz;
pub const __builtin_sqrt = @import("std").zig.c_builtins.__builtin_sqrt;
pub const __builtin_sqrtf = @import("std").zig.c_builtins.__builtin_sqrtf;
pub const __builtin_sin = @import("std").zig.c_builtins.__builtin_sin;
pub const __builtin_sinf = @import("std").zig.c_builtins.__builtin_sinf;
pub const __builtin_cos = @import("std").zig.c_builtins.__builtin_cos;
pub const __builtin_cosf = @import("std").zig.c_builtins.__builtin_cosf;
pub const __builtin_exp = @import("std").zig.c_builtins.__builtin_exp;
pub const __builtin_expf = @import("std").zig.c_builtins.__builtin_expf;
pub const __builtin_exp2 = @import("std").zig.c_builtins.__builtin_exp2;
pub const __builtin_exp2f = @import("std").zig.c_builtins.__builtin_exp2f;
pub const __builtin_log = @import("std").zig.c_builtins.__builtin_log;
pub const __builtin_logf = @import("std").zig.c_builtins.__builtin_logf;
pub const __builtin_log2 = @import("std").zig.c_builtins.__builtin_log2;
pub const __builtin_log2f = @import("std").zig.c_builtins.__builtin_log2f;
pub const __builtin_log10 = @import("std").zig.c_builtins.__builtin_log10;
pub const __builtin_log10f = @import("std").zig.c_builtins.__builtin_log10f;
pub const __builtin_abs = @import("std").zig.c_builtins.__builtin_abs;
pub const __builtin_fabs = @import("std").zig.c_builtins.__builtin_fabs;
pub const __builtin_fabsf = @import("std").zig.c_builtins.__builtin_fabsf;
pub const __builtin_floor = @import("std").zig.c_builtins.__builtin_floor;
pub const __builtin_floorf = @import("std").zig.c_builtins.__builtin_floorf;
pub const __builtin_ceil = @import("std").zig.c_builtins.__builtin_ceil;
pub const __builtin_ceilf = @import("std").zig.c_builtins.__builtin_ceilf;
pub const __builtin_trunc = @import("std").zig.c_builtins.__builtin_trunc;
pub const __builtin_truncf = @import("std").zig.c_builtins.__builtin_truncf;
pub const __builtin_round = @import("std").zig.c_builtins.__builtin_round;
pub const __builtin_roundf = @import("std").zig.c_builtins.__builtin_roundf;
pub const __builtin_strlen = @import("std").zig.c_builtins.__builtin_strlen;
pub const __builtin_strcmp = @import("std").zig.c_builtins.__builtin_strcmp;
pub const __builtin_object_size = @import("std").zig.c_builtins.__builtin_object_size;
pub const __builtin___memset_chk = @import("std").zig.c_builtins.__builtin___memset_chk;
pub const __builtin_memset = @import("std").zig.c_builtins.__builtin_memset;
pub const __builtin___memcpy_chk = @import("std").zig.c_builtins.__builtin___memcpy_chk;
pub const __builtin_memcpy = @import("std").zig.c_builtins.__builtin_memcpy;
pub const __builtin_expect = @import("std").zig.c_builtins.__builtin_expect;
pub const __builtin_nanf = @import("std").zig.c_builtins.__builtin_nanf;
pub const __builtin_huge_valf = @import("std").zig.c_builtins.__builtin_huge_valf;
pub const __builtin_inff = @import("std").zig.c_builtins.__builtin_inff;
pub const __builtin_isnan = @import("std").zig.c_builtins.__builtin_isnan;
pub const __builtin_isinf = @import("std").zig.c_builtins.__builtin_isinf;
pub const __builtin_isinf_sign = @import("std").zig.c_builtins.__builtin_isinf_sign;
pub const __u_char = u8;
pub const __u_short = c_ushort;
pub const __u_int = c_uint;
pub const __u_long = c_ulong;
pub const __int8_t = i8;
pub const __uint8_t = u8;
pub const __int16_t = c_short;
pub const __uint16_t = c_ushort;
pub const __int32_t = c_int;
pub const __uint32_t = c_uint;
pub const __int64_t = c_long;
pub const __uint64_t = c_ulong;
pub const __int_least8_t = __int8_t;
pub const __uint_least8_t = __uint8_t;
pub const __int_least16_t = __int16_t;
pub const __uint_least16_t = __uint16_t;
pub const __int_least32_t = __int32_t;
pub const __uint_least32_t = __uint32_t;
pub const __int_least64_t = __int64_t;
pub const __uint_least64_t = __uint64_t;
pub const __quad_t = c_long;
pub const __u_quad_t = c_ulong;
pub const __intmax_t = c_long;
pub const __uintmax_t = c_ulong;
pub const __dev_t = c_ulong;
pub const __uid_t = c_uint;
pub const __gid_t = c_uint;
pub const __ino_t = c_ulong;
pub const __ino64_t = c_ulong;
pub const __mode_t = c_uint;
pub const __nlink_t = c_ulong;
pub const __off_t = c_long;
pub const __off64_t = c_long;
pub const __pid_t = c_int;
pub const __fsid_t = extern struct {
    __val: [2]c_int,
};
pub const __clock_t = c_long;
pub const __rlim_t = c_ulong;
pub const __rlim64_t = c_ulong;
pub const __id_t = c_uint;
pub const __time_t = c_long;
pub const __useconds_t = c_uint;
pub const __suseconds_t = c_long;
pub const __suseconds64_t = c_long;
pub const __daddr_t = c_int;
pub const __key_t = c_int;
pub const __clockid_t = c_int;
pub const __timer_t = ?*anyopaque;
pub const __blksize_t = c_long;
pub const __blkcnt_t = c_long;
pub const __blkcnt64_t = c_long;
pub const __fsblkcnt_t = c_ulong;
pub const __fsblkcnt64_t = c_ulong;
pub const __fsfilcnt_t = c_ulong;
pub const __fsfilcnt64_t = c_ulong;
pub const __fsword_t = c_long;
pub const __ssize_t = c_long;
pub const __syscall_slong_t = c_long;
pub const __syscall_ulong_t = c_ulong;
pub const __loff_t = __off64_t;
pub const __caddr_t = [*c]u8;
pub const __intptr_t = c_long;
pub const __socklen_t = c_uint;
pub const __sig_atomic_t = c_int;
pub const int_least8_t = __int_least8_t;
pub const int_least16_t = __int_least16_t;
pub const int_least32_t = __int_least32_t;
pub const int_least64_t = __int_least64_t;
pub const uint_least8_t = __uint_least8_t;
pub const uint_least16_t = __uint_least16_t;
pub const uint_least32_t = __uint_least32_t;
pub const uint_least64_t = __uint_least64_t;
pub const int_fast8_t = i8;
pub const int_fast16_t = c_long;
pub const int_fast32_t = c_long;
pub const int_fast64_t = c_long;
pub const uint_fast8_t = u8;
pub const uint_fast16_t = c_ulong;
pub const uint_fast32_t = c_ulong;
pub const uint_fast64_t = c_ulong;
pub const intmax_t = __intmax_t;
pub const uintmax_t = __uintmax_t;
pub const ptrdiff_t = c_long;
pub const wchar_t = c_int;
pub const max_align_t = extern struct {
    __clang_max_align_nonce1: c_longlong align(8),
    __clang_max_align_nonce2: c_longdouble align(16),
};
pub const time_t = __time_t;
pub const struct_timeval = extern struct {
    tv_sec: __time_t,
    tv_usec: __suseconds_t,
};
pub const suseconds_t = __suseconds_t;
pub const __sigset_t = extern struct {
    __val: [16]c_ulong,
};
pub const sigset_t = __sigset_t;
pub const struct_timespec = extern struct {
    tv_sec: __time_t,
    tv_nsec: __syscall_slong_t,
};
pub const __fd_mask = c_long;
pub const fd_set = extern struct {
    __fds_bits: [16]__fd_mask,
};
pub const fd_mask = __fd_mask;
pub extern fn select(__nfds: c_int, noalias __readfds: [*c]fd_set, noalias __writefds: [*c]fd_set, noalias __exceptfds: [*c]fd_set, noalias __timeout: [*c]struct_timeval) c_int;
pub extern fn pselect(__nfds: c_int, noalias __readfds: [*c]fd_set, noalias __writefds: [*c]fd_set, noalias __exceptfds: [*c]fd_set, noalias __timeout: [*c]const struct_timespec, noalias __sigmask: [*c]const __sigset_t) c_int;
pub const struct_timezone = extern struct {
    tz_minuteswest: c_int,
    tz_dsttime: c_int,
};
pub extern fn gettimeofday(noalias __tv: [*c]struct_timeval, noalias __tz: ?*anyopaque) c_int;
pub extern fn settimeofday(__tv: [*c]const struct_timeval, __tz: [*c]const struct_timezone) c_int;
pub extern fn adjtime(__delta: [*c]const struct_timeval, __olddelta: [*c]struct_timeval) c_int;
pub const ITIMER_REAL: c_int = 0;
pub const ITIMER_VIRTUAL: c_int = 1;
pub const ITIMER_PROF: c_int = 2;
pub const enum___itimer_which = c_uint;
pub const struct_itimerval = extern struct {
    it_interval: struct_timeval,
    it_value: struct_timeval,
};
pub const __itimer_which_t = c_int;
pub extern fn getitimer(__which: __itimer_which_t, __value: [*c]struct_itimerval) c_int;
pub extern fn setitimer(__which: __itimer_which_t, noalias __new: [*c]const struct_itimerval, noalias __old: [*c]struct_itimerval) c_int;
pub extern fn utimes(__file: [*c]const u8, __tvp: [*c]const struct_timeval) c_int;
pub extern fn lutimes(__file: [*c]const u8, __tvp: [*c]const struct_timeval) c_int;
pub extern fn futimes(__fd: c_int, __tvp: [*c]const struct_timeval) c_int;
pub const clock_t = __clock_t;
pub const struct_tm = extern struct {
    tm_sec: c_int,
    tm_min: c_int,
    tm_hour: c_int,
    tm_mday: c_int,
    tm_mon: c_int,
    tm_year: c_int,
    tm_wday: c_int,
    tm_yday: c_int,
    tm_isdst: c_int,
    tm_gmtoff: c_long,
    tm_zone: [*c]const u8,
};
pub const clockid_t = __clockid_t;
pub const timer_t = __timer_t;
pub const struct_itimerspec = extern struct {
    it_interval: struct_timespec,
    it_value: struct_timespec,
};
pub const union_sigval = extern union {
    sival_int: c_int,
    sival_ptr: ?*anyopaque,
};
pub const __sigval_t = union_sigval;
pub const union_pthread_attr_t = extern union {
    __size: [56]u8,
    __align: c_long,
};
pub const pthread_attr_t = union_pthread_attr_t;
const struct_unnamed_2 = extern struct {
    _function: ?fn (__sigval_t) callconv(.C) void,
    _attribute: [*c]pthread_attr_t,
};
const union_unnamed_1 = extern union {
    _pad: [12]c_int,
    _tid: __pid_t,
    _sigev_thread: struct_unnamed_2,
};
pub const struct_sigevent = extern struct {
    sigev_value: __sigval_t,
    sigev_signo: c_int,
    sigev_notify: c_int,
    _sigev_un: union_unnamed_1,
};
pub const pid_t = __pid_t;
pub const struct___locale_data = opaque {};
pub const struct___locale_struct = extern struct {
    __locales: [13]?*struct___locale_data,
    __ctype_b: [*c]const c_ushort,
    __ctype_tolower: [*c]const c_int,
    __ctype_toupper: [*c]const c_int,
    __names: [13][*c]const u8,
};
pub const __locale_t = [*c]struct___locale_struct;
pub const locale_t = __locale_t;
pub extern fn clock() clock_t;
pub extern fn time(__timer: [*c]time_t) time_t;
pub extern fn difftime(__time1: time_t, __time0: time_t) f64;
pub extern fn mktime(__tp: [*c]struct_tm) time_t;
pub extern fn strftime(noalias __s: [*c]u8, __maxsize: usize, noalias __format: [*c]const u8, noalias __tp: [*c]const struct_tm) usize;
pub extern fn strftime_l(noalias __s: [*c]u8, __maxsize: usize, noalias __format: [*c]const u8, noalias __tp: [*c]const struct_tm, __loc: locale_t) usize;
pub extern fn gmtime(__timer: [*c]const time_t) [*c]struct_tm;
pub extern fn localtime(__timer: [*c]const time_t) [*c]struct_tm;
pub extern fn gmtime_r(noalias __timer: [*c]const time_t, noalias __tp: [*c]struct_tm) [*c]struct_tm;
pub extern fn localtime_r(noalias __timer: [*c]const time_t, noalias __tp: [*c]struct_tm) [*c]struct_tm;
pub extern fn asctime(__tp: [*c]const struct_tm) [*c]u8;
pub extern fn ctime(__timer: [*c]const time_t) [*c]u8;
pub extern fn asctime_r(noalias __tp: [*c]const struct_tm, noalias __buf: [*c]u8) [*c]u8;
pub extern fn ctime_r(noalias __timer: [*c]const time_t, noalias __buf: [*c]u8) [*c]u8;
pub extern var __tzname: [2][*c]u8;
pub extern var __daylight: c_int;
pub extern var __timezone: c_long;
pub extern var tzname: [2][*c]u8;
pub extern fn tzset() void;
pub extern var daylight: c_int;
pub extern var timezone: c_long;
pub extern fn timegm(__tp: [*c]struct_tm) time_t;
pub extern fn timelocal(__tp: [*c]struct_tm) time_t;
pub extern fn dysize(__year: c_int) c_int;
pub extern fn nanosleep(__requested_time: [*c]const struct_timespec, __remaining: [*c]struct_timespec) c_int;
pub extern fn clock_getres(__clock_id: clockid_t, __res: [*c]struct_timespec) c_int;
pub extern fn clock_gettime(__clock_id: clockid_t, __tp: [*c]struct_timespec) c_int;
pub extern fn clock_settime(__clock_id: clockid_t, __tp: [*c]const struct_timespec) c_int;
pub extern fn clock_nanosleep(__clock_id: clockid_t, __flags: c_int, __req: [*c]const struct_timespec, __rem: [*c]struct_timespec) c_int;
pub extern fn clock_getcpuclockid(__pid: pid_t, __clock_id: [*c]clockid_t) c_int;
pub extern fn timer_create(__clock_id: clockid_t, noalias __evp: [*c]struct_sigevent, noalias __timerid: [*c]timer_t) c_int;
pub extern fn timer_delete(__timerid: timer_t) c_int;
pub extern fn timer_settime(__timerid: timer_t, __flags: c_int, noalias __value: [*c]const struct_itimerspec, noalias __ovalue: [*c]struct_itimerspec) c_int;
pub extern fn timer_gettime(__timerid: timer_t, __value: [*c]struct_itimerspec) c_int;
pub extern fn timer_getoverrun(__timerid: timer_t) c_int;
pub extern fn timespec_get(__ts: [*c]struct_timespec, __base: c_int) c_int;
pub const _Float32 = f32;
pub const _Float64 = f64;
pub const _Float32x = f64;
pub const _Float64x = c_longdouble;
pub const div_t = extern struct {
    quot: c_int,
    rem: c_int,
};
pub const ldiv_t = extern struct {
    quot: c_long,
    rem: c_long,
};
pub const lldiv_t = extern struct {
    quot: c_longlong,
    rem: c_longlong,
};
pub extern fn __ctype_get_mb_cur_max() usize;
pub fn atof(arg___nptr: [*c]const u8) callconv(.C) f64 {
    var __nptr = arg___nptr;
    return strtod(__nptr, @ptrCast([*c][*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), @intToPtr(?*anyopaque, @as(c_int, 0)))));
}
pub fn atoi(arg___nptr: [*c]const u8) callconv(.C) c_int {
    var __nptr = arg___nptr;
    return @bitCast(c_int, @truncate(c_int, strtol(__nptr, @ptrCast([*c][*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), @intToPtr(?*anyopaque, @as(c_int, 0)))), @as(c_int, 10))));
}
pub fn atol(arg___nptr: [*c]const u8) callconv(.C) c_long {
    var __nptr = arg___nptr;
    return strtol(__nptr, @ptrCast([*c][*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), @intToPtr(?*anyopaque, @as(c_int, 0)))), @as(c_int, 10));
}
pub fn atoll(arg___nptr: [*c]const u8) callconv(.C) c_longlong {
    var __nptr = arg___nptr;
    return strtoll(__nptr, @ptrCast([*c][*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), @intToPtr(?*anyopaque, @as(c_int, 0)))), @as(c_int, 10));
}
pub extern fn strtod(__nptr: [*c]const u8, __endptr: [*c][*c]u8) f64;
pub extern fn strtof(__nptr: [*c]const u8, __endptr: [*c][*c]u8) f32;
pub extern fn strtold(__nptr: [*c]const u8, __endptr: [*c][*c]u8) c_longdouble;
pub extern fn strtol(__nptr: [*c]const u8, __endptr: [*c][*c]u8, __base: c_int) c_long;
pub extern fn strtoul(__nptr: [*c]const u8, __endptr: [*c][*c]u8, __base: c_int) c_ulong;
pub extern fn strtoq(noalias __nptr: [*c]const u8, noalias __endptr: [*c][*c]u8, __base: c_int) c_longlong;
pub extern fn strtouq(noalias __nptr: [*c]const u8, noalias __endptr: [*c][*c]u8, __base: c_int) c_ulonglong;
pub extern fn strtoll(__nptr: [*c]const u8, __endptr: [*c][*c]u8, __base: c_int) c_longlong;
pub extern fn strtoull(__nptr: [*c]const u8, __endptr: [*c][*c]u8, __base: c_int) c_ulonglong;
pub extern fn l64a(__n: c_long) [*c]u8;
pub extern fn a64l(__s: [*c]const u8) c_long;
pub const u_char = __u_char;
pub const u_short = __u_short;
pub const u_int = __u_int;
pub const u_long = __u_long;
pub const quad_t = __quad_t;
pub const u_quad_t = __u_quad_t;
pub const fsid_t = __fsid_t;
pub const loff_t = __loff_t;
pub const ino_t = __ino_t;
pub const dev_t = __dev_t;
pub const gid_t = __gid_t;
pub const mode_t = __mode_t;
pub const nlink_t = __nlink_t;
pub const uid_t = __uid_t;
pub const off_t = __off_t;
pub const id_t = __id_t;
pub const daddr_t = __daddr_t;
pub const caddr_t = __caddr_t;
pub const key_t = __key_t;
pub const ulong = c_ulong;
pub const ushort = c_ushort;
pub const uint = c_uint;
pub const u_int8_t = __uint8_t;
pub const u_int16_t = __uint16_t;
pub const u_int32_t = __uint32_t;
pub const u_int64_t = __uint64_t;
pub const register_t = c_long;
pub fn __bswap_16(arg___bsx: __uint16_t) callconv(.C) __uint16_t {
    var __bsx = arg___bsx;
    return @bitCast(__uint16_t, @truncate(c_short, ((@bitCast(c_int, @as(c_uint, __bsx)) >> @intCast(@import("std").math.Log2Int(c_int), 8)) & @as(c_int, 255)) | ((@bitCast(c_int, @as(c_uint, __bsx)) & @as(c_int, 255)) << @intCast(@import("std").math.Log2Int(c_int), 8))));
}
pub fn __bswap_32(arg___bsx: __uint32_t) callconv(.C) __uint32_t {
    var __bsx = arg___bsx;
    return ((((__bsx & @as(c_uint, 4278190080)) >> @intCast(@import("std").math.Log2Int(c_uint), 24)) | ((__bsx & @as(c_uint, 16711680)) >> @intCast(@import("std").math.Log2Int(c_uint), 8))) | ((__bsx & @as(c_uint, 65280)) << @intCast(@import("std").math.Log2Int(c_uint), 8))) | ((__bsx & @as(c_uint, 255)) << @intCast(@import("std").math.Log2Int(c_uint), 24));
}
pub fn __bswap_64(arg___bsx: __uint64_t) callconv(.C) __uint64_t {
    var __bsx = arg___bsx;
    return @bitCast(__uint64_t, @truncate(c_ulong, ((((((((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 18374686479671623680)) >> @intCast(@import("std").math.Log2Int(c_ulonglong), 56)) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 71776119061217280)) >> @intCast(@import("std").math.Log2Int(c_ulonglong), 40))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 280375465082880)) >> @intCast(@import("std").math.Log2Int(c_ulonglong), 24))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 1095216660480)) >> @intCast(@import("std").math.Log2Int(c_ulonglong), 8))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 4278190080)) << @intCast(@import("std").math.Log2Int(c_ulonglong), 8))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 16711680)) << @intCast(@import("std").math.Log2Int(c_ulonglong), 24))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 65280)) << @intCast(@import("std").math.Log2Int(c_ulonglong), 40))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 255)) << @intCast(@import("std").math.Log2Int(c_ulonglong), 56))));
}
pub fn __uint16_identity(arg___x: __uint16_t) callconv(.C) __uint16_t {
    var __x = arg___x;
    return __x;
}
pub fn __uint32_identity(arg___x: __uint32_t) callconv(.C) __uint32_t {
    var __x = arg___x;
    return __x;
}
pub fn __uint64_identity(arg___x: __uint64_t) callconv(.C) __uint64_t {
    var __x = arg___x;
    return __x;
}
pub const blksize_t = __blksize_t;
pub const blkcnt_t = __blkcnt_t;
pub const fsblkcnt_t = __fsblkcnt_t;
pub const fsfilcnt_t = __fsfilcnt_t;
pub const struct___pthread_internal_list = extern struct {
    __prev: [*c]struct___pthread_internal_list,
    __next: [*c]struct___pthread_internal_list,
};
pub const __pthread_list_t = struct___pthread_internal_list;
pub const struct___pthread_internal_slist = extern struct {
    __next: [*c]struct___pthread_internal_slist,
};
pub const __pthread_slist_t = struct___pthread_internal_slist;
pub const struct___pthread_mutex_s = extern struct {
    __lock: c_int,
    __count: c_uint,
    __owner: c_int,
    __nusers: c_uint,
    __kind: c_int,
    __spins: c_short,
    __elision: c_short,
    __list: __pthread_list_t,
};
pub const struct___pthread_rwlock_arch_t = extern struct {
    __readers: c_uint,
    __writers: c_uint,
    __wrphase_futex: c_uint,
    __writers_futex: c_uint,
    __pad3: c_uint,
    __pad4: c_uint,
    __cur_writer: c_int,
    __shared: c_int,
    __rwelision: i8,
    __pad1: [7]u8,
    __pad2: c_ulong,
    __flags: c_uint,
};
const struct_unnamed_4 = extern struct {
    __low: c_uint,
    __high: c_uint,
};
const union_unnamed_3 = extern union {
    __wseq: c_ulonglong,
    __wseq32: struct_unnamed_4,
};
const struct_unnamed_6 = extern struct {
    __low: c_uint,
    __high: c_uint,
};
const union_unnamed_5 = extern union {
    __g1_start: c_ulonglong,
    __g1_start32: struct_unnamed_6,
};
pub const struct___pthread_cond_s = extern struct {
    unnamed_0: union_unnamed_3,
    unnamed_1: union_unnamed_5,
    __g_refs: [2]c_uint,
    __g_size: [2]c_uint,
    __g1_orig_size: c_uint,
    __wrefs: c_uint,
    __g_signals: [2]c_uint,
};
pub const __tss_t = c_uint;
pub const __thrd_t = c_ulong;
pub const __once_flag = extern struct {
    __data: c_int,
};
pub const pthread_t = c_ulong;
pub const pthread_mutexattr_t = extern union {
    __size: [4]u8,
    __align: c_int,
};
pub const pthread_condattr_t = extern union {
    __size: [4]u8,
    __align: c_int,
};
pub const pthread_key_t = c_uint;
pub const pthread_once_t = c_int;
pub const pthread_mutex_t = extern union {
    __data: struct___pthread_mutex_s,
    __size: [40]u8,
    __align: c_long,
};
pub const pthread_cond_t = extern union {
    __data: struct___pthread_cond_s,
    __size: [48]u8,
    __align: c_longlong,
};
pub const pthread_rwlock_t = extern union {
    __data: struct___pthread_rwlock_arch_t,
    __size: [56]u8,
    __align: c_long,
};
pub const pthread_rwlockattr_t = extern union {
    __size: [8]u8,
    __align: c_long,
};
pub const pthread_spinlock_t = c_int;
pub const pthread_barrier_t = extern union {
    __size: [32]u8,
    __align: c_long,
};
pub const pthread_barrierattr_t = extern union {
    __size: [4]u8,
    __align: c_int,
};
pub extern fn random() c_long;
pub extern fn srandom(__seed: c_uint) void;
pub extern fn initstate(__seed: c_uint, __statebuf: [*c]u8, __statelen: usize) [*c]u8;
pub extern fn setstate(__statebuf: [*c]u8) [*c]u8;
pub const struct_random_data = extern struct {
    fptr: [*c]i32,
    rptr: [*c]i32,
    state: [*c]i32,
    rand_type: c_int,
    rand_deg: c_int,
    rand_sep: c_int,
    end_ptr: [*c]i32,
};
pub extern fn random_r(noalias __buf: [*c]struct_random_data, noalias __result: [*c]i32) c_int;
pub extern fn srandom_r(__seed: c_uint, __buf: [*c]struct_random_data) c_int;
pub extern fn initstate_r(__seed: c_uint, noalias __statebuf: [*c]u8, __statelen: usize, noalias __buf: [*c]struct_random_data) c_int;
pub extern fn setstate_r(noalias __statebuf: [*c]u8, noalias __buf: [*c]struct_random_data) c_int;
pub extern fn rand() c_int;
pub extern fn srand(__seed: c_uint) void;
pub extern fn rand_r(__seed: [*c]c_uint) c_int;
pub extern fn drand48() f64;
pub extern fn erand48(__xsubi: [*c]c_ushort) f64;
pub extern fn lrand48() c_long;
pub extern fn nrand48(__xsubi: [*c]c_ushort) c_long;
pub extern fn mrand48() c_long;
pub extern fn jrand48(__xsubi: [*c]c_ushort) c_long;
pub extern fn srand48(__seedval: c_long) void;
pub extern fn seed48(__seed16v: [*c]c_ushort) [*c]c_ushort;
pub extern fn lcong48(__param: [*c]c_ushort) void;
pub const struct_drand48_data = extern struct {
    __x: [3]c_ushort,
    __old_x: [3]c_ushort,
    __c: c_ushort,
    __init: c_ushort,
    __a: c_ulonglong,
};
pub extern fn drand48_r(noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]f64) c_int;
pub extern fn erand48_r(__xsubi: [*c]c_ushort, noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]f64) c_int;
pub extern fn lrand48_r(noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]c_long) c_int;
pub extern fn nrand48_r(__xsubi: [*c]c_ushort, noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]c_long) c_int;
pub extern fn mrand48_r(noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]c_long) c_int;
pub extern fn jrand48_r(__xsubi: [*c]c_ushort, noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]c_long) c_int;
pub extern fn srand48_r(__seedval: c_long, __buffer: [*c]struct_drand48_data) c_int;
pub extern fn seed48_r(__seed16v: [*c]c_ushort, __buffer: [*c]struct_drand48_data) c_int;
pub extern fn lcong48_r(__param: [*c]c_ushort, __buffer: [*c]struct_drand48_data) c_int;
pub extern fn malloc(__size: c_ulong) ?*anyopaque;
pub extern fn calloc(__nmemb: c_ulong, __size: c_ulong) ?*anyopaque;
pub extern fn realloc(__ptr: ?*anyopaque, __size: c_ulong) ?*anyopaque;
pub extern fn reallocarray(__ptr: ?*anyopaque, __nmemb: usize, __size: usize) ?*anyopaque;
pub extern fn free(__ptr: ?*anyopaque) void;
pub extern fn alloca(__size: c_ulong) ?*anyopaque;
pub extern fn valloc(__size: usize) ?*anyopaque;
pub extern fn posix_memalign(__memptr: [*c]?*anyopaque, __alignment: usize, __size: usize) c_int;
pub extern fn aligned_alloc(__alignment: c_ulong, __size: c_ulong) ?*anyopaque;
pub extern fn abort() noreturn;
pub extern fn atexit(__func: ?fn () callconv(.C) void) c_int;
pub extern fn at_quick_exit(__func: ?fn () callconv(.C) void) c_int;
pub extern fn on_exit(__func: ?fn (c_int, ?*anyopaque) callconv(.C) void, __arg: ?*anyopaque) c_int;
pub extern fn exit(__status: c_int) noreturn;
pub extern fn quick_exit(__status: c_int) noreturn;
pub extern fn _Exit(__status: c_int) noreturn;
pub extern fn getenv(__name: [*c]const u8) [*c]u8;
pub extern fn putenv(__string: [*c]u8) c_int;
pub extern fn setenv(__name: [*c]const u8, __value: [*c]const u8, __replace: c_int) c_int;
pub extern fn unsetenv(__name: [*c]const u8) c_int;
pub extern fn clearenv() c_int;
pub extern fn mktemp(__template: [*c]u8) [*c]u8;
pub extern fn mkstemp(__template: [*c]u8) c_int;
pub extern fn mkstemps(__template: [*c]u8, __suffixlen: c_int) c_int;
pub extern fn mkdtemp(__template: [*c]u8) [*c]u8;
pub extern fn system(__command: [*c]const u8) c_int;
pub extern fn realpath(noalias __name: [*c]const u8, noalias __resolved: [*c]u8) [*c]u8;
pub const __compar_fn_t = ?fn (?*const anyopaque, ?*const anyopaque) callconv(.C) c_int;
pub fn bsearch(arg___key: ?*const anyopaque, arg___base: ?*const anyopaque, arg___nmemb: usize, arg___size: usize, arg___compar: __compar_fn_t) callconv(.C) ?*anyopaque {
    var __key = arg___key;
    var __base = arg___base;
    var __nmemb = arg___nmemb;
    var __size = arg___size;
    var __compar = arg___compar;
    var __l: usize = undefined;
    var __u: usize = undefined;
    var __idx: usize = undefined;
    var __p: ?*const anyopaque = undefined;
    var __comparison: c_int = undefined;
    __l = 0;
    __u = __nmemb;
    while (__l < __u) {
        __idx = (__l +% __u) / @bitCast(c_ulong, @as(c_long, @as(c_int, 2)));
        __p = @intToPtr(?*anyopaque, @ptrToInt(@ptrCast([*c]const u8, @alignCast(@import("std").meta.alignment(u8), __base)) + (__idx *% __size)));
        __comparison = __compar.?(__key, __p);
        if (__comparison < @as(c_int, 0)) {
            __u = __idx;
        } else if (__comparison > @as(c_int, 0)) {
            __l = __idx +% @bitCast(c_ulong, @as(c_long, @as(c_int, 1)));
        } else return @intToPtr(?*anyopaque, @ptrToInt(__p));
    }
    return @intToPtr(?*anyopaque, @as(c_int, 0));
}
pub extern fn qsort(__base: ?*anyopaque, __nmemb: usize, __size: usize, __compar: __compar_fn_t) void;
pub extern fn abs(__x: c_int) c_int;
pub extern fn labs(__x: c_long) c_long;
pub extern fn llabs(__x: c_longlong) c_longlong;
pub extern fn div(__numer: c_int, __denom: c_int) div_t;
pub extern fn ldiv(__numer: c_long, __denom: c_long) ldiv_t;
pub extern fn lldiv(__numer: c_longlong, __denom: c_longlong) lldiv_t;
pub extern fn ecvt(__value: f64, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int) [*c]u8;
pub extern fn fcvt(__value: f64, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int) [*c]u8;
pub extern fn gcvt(__value: f64, __ndigit: c_int, __buf: [*c]u8) [*c]u8;
pub extern fn qecvt(__value: c_longdouble, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int) [*c]u8;
pub extern fn qfcvt(__value: c_longdouble, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int) [*c]u8;
pub extern fn qgcvt(__value: c_longdouble, __ndigit: c_int, __buf: [*c]u8) [*c]u8;
pub extern fn ecvt_r(__value: f64, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int, noalias __buf: [*c]u8, __len: usize) c_int;
pub extern fn fcvt_r(__value: f64, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int, noalias __buf: [*c]u8, __len: usize) c_int;
pub extern fn qecvt_r(__value: c_longdouble, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int, noalias __buf: [*c]u8, __len: usize) c_int;
pub extern fn qfcvt_r(__value: c_longdouble, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int, noalias __buf: [*c]u8, __len: usize) c_int;
pub extern fn mblen(__s: [*c]const u8, __n: usize) c_int;
pub extern fn mbtowc(noalias __pwc: [*c]wchar_t, noalias __s: [*c]const u8, __n: usize) c_int;
pub extern fn wctomb(__s: [*c]u8, __wchar: wchar_t) c_int;
pub extern fn mbstowcs(noalias __pwcs: [*c]wchar_t, noalias __s: [*c]const u8, __n: usize) usize;
pub extern fn wcstombs(noalias __s: [*c]u8, noalias __pwcs: [*c]const wchar_t, __n: usize) usize;
pub extern fn rpmatch(__response: [*c]const u8) c_int;
pub extern fn getsubopt(noalias __optionp: [*c][*c]u8, noalias __tokens: [*c]const [*c]u8, noalias __valuep: [*c][*c]u8) c_int;
pub extern fn getloadavg(__loadavg: [*c]f64, __nelem: c_int) c_int;
pub const scm_t_timespec = struct_timespec;
pub const scm_t_off = i64;
pub const scm_t_signed_bits = isize;
pub const scm_t_bits = usize;
pub const struct_scm_unused_struct = extern struct {
    scm_unused_field: u8,
};
pub const SCM = [*c]struct_scm_unused_struct;
pub const scm_tc8_flag: c_int = 4;
pub const scm_tc8_char: c_int = 12;
pub const scm_tc8_unused_0: c_int = 20;
pub const scm_tc8_unused_1: c_int = 28;
pub const enum_scm_tc8_tags = c_uint;
pub const scm_t_subr = ?*anyopaque;
pub const struct_scm_dynamic_state = opaque {};
pub const scm_t_dynamic_state = struct_scm_dynamic_state;
pub const struct_scm_print_state = extern struct {
    handle: SCM,
    revealed: c_int,
    writingp: c_ulong,
    fancyp: c_ulong,
    level: c_ulong,
    length: c_ulong,
    hot_ref: SCM,
    list_offset: c_ulong,
    top: c_ulong,
    ceiling: c_ulong,
    ref_vect: SCM,
    highlight_objects: SCM,
};
pub const scm_print_state = struct_scm_print_state;
pub const struct_scm_dynstack = extern struct {
    base: [*c]scm_t_bits,
    top: [*c]scm_t_bits,
    limit: [*c]scm_t_bits,
};
pub const scm_t_dynstack = struct_scm_dynstack;
pub const scm_t_wchar = i32;
pub const struct_scm_frame = opaque {};
pub const union_scm_vm_stack_element = extern union {
    as_uint: usize,
    as_vcode: [*c]u32,
    as_mcode: [*c]u8,
    as_scm: SCM,
    as_f64: f64,
    as_u64: u64,
    as_s64: i64,
    as_ptr: ?*anyopaque,
    as_bits: scm_t_bits,
};
pub const __jmp_buf = [8]c_long;
pub const struct___jmp_buf_tag = extern struct {
    __jmpbuf: __jmp_buf,
    __mask_was_saved: c_int,
    __saved_mask: __sigset_t,
};
pub const jmp_buf = [1]struct___jmp_buf_tag;
pub const struct_scm_vm = extern struct {
    ip: [*c]u32,
    sp: [*c]union_scm_vm_stack_element,
    fp: [*c]union_scm_vm_stack_element,
    stack_limit: [*c]union_scm_vm_stack_element,
    compare_result: u8,
    apply_hook_enabled: u8,
    return_hook_enabled: u8,
    next_hook_enabled: u8,
    abort_hook_enabled: u8,
    disable_mcode: u8,
    engine: u8,
    unused: u8,
    stack_size: usize,
    stack_bottom: [*c]union_scm_vm_stack_element,
    apply_hook: SCM,
    return_hook: SCM,
    next_hook: SCM,
    abort_hook: SCM,
    stack_top: [*c]union_scm_vm_stack_element,
    overflow_handler_stack: SCM,
    registers: [*c]jmp_buf,
    mra_after_abort: [*c]u8,
    trace_level: c_int,
};
pub const struct_scm_thread_wake_data = opaque {};
pub const SCM_STACKITEM = c_long;
pub const struct_scm_jit_state = opaque {};
pub const struct_scm_thread = extern struct {
    next_thread: [*c]struct_scm_thread,
    vm: struct_scm_vm,
    pending_asyncs: SCM,
    block_asyncs: c_uint,
    freelists: [16]?*anyopaque,
    pointerless_freelists: [16]?*anyopaque,
    handle: SCM,
    pthread: pthread_t,
    result: SCM,
    exited: c_int,
    guile_mode: c_int,
    needs_unregister: c_int,
    wake: ?*struct_scm_thread_wake_data,
    sleep_cond: pthread_cond_t,
    sleep_pipe: [2]c_int,
    dynamic_state: ?*scm_t_dynamic_state,
    dynstack: scm_t_dynstack,
    continuation_root: SCM,
    continuation_base: [*c]SCM_STACKITEM,
    base: [*c]SCM_STACKITEM,
    jit_state: ?*struct_scm_jit_state,
};
pub const scm_thread = struct_scm_thread;
pub extern var scm_system_error_key: SCM;
pub extern var scm_num_overflow_key: SCM;
pub extern var scm_out_of_range_key: SCM;
pub extern var scm_args_number_key: SCM;
pub extern var scm_arg_type_key: SCM;
pub extern var scm_misc_error_key: SCM;
pub extern fn scm_error(key: SCM, subr: [*c]const u8, message: [*c]const u8, args: SCM, rest: SCM) noreturn;
pub extern fn scm_error_scm(key: SCM, subr: SCM, message: SCM, args: SCM, rest: SCM) noreturn;
pub extern fn scm_strerror(err: SCM) SCM;
pub extern fn scm_syserror(subr: [*c]const u8) noreturn;
pub extern fn scm_syserror_msg(subr: [*c]const u8, message: [*c]const u8, args: SCM, eno: c_int) noreturn;
pub extern fn scm_num_overflow(subr: [*c]const u8) noreturn;
pub extern fn scm_out_of_range(subr: [*c]const u8, bad_value: SCM) noreturn;
pub extern fn scm_out_of_range_pos(subr: [*c]const u8, bad_value: SCM, pos: SCM) noreturn;
pub extern fn scm_wrong_num_args(proc: SCM) noreturn;
pub extern fn scm_error_num_args_subr(subr: [*c]const u8) noreturn;
pub extern fn scm_wrong_type_arg(subr: [*c]const u8, pos: c_int, bad_value: SCM) noreturn;
pub extern fn scm_i_wrong_type_arg_symbol(symbol: SCM, pos: c_int, bad_value: SCM) noreturn;
pub extern fn scm_wrong_type_arg_msg(subr: [*c]const u8, pos: c_int, bad_value: SCM, sz: [*c]const u8) noreturn;
pub extern fn scm_misc_error(subr: [*c]const u8, message: [*c]const u8, args: SCM) noreturn;
pub extern fn scm_init_error() void;
pub const SCM_C_HOOK_NORMAL: c_int = 0;
pub const SCM_C_HOOK_OR: c_int = 1;
pub const SCM_C_HOOK_AND: c_int = 2;
pub const enum_scm_t_c_hook_type = c_uint;
pub const scm_t_c_hook_type = enum_scm_t_c_hook_type;
pub const scm_t_c_hook_function = ?fn (?*anyopaque, ?*anyopaque, ?*anyopaque) callconv(.C) ?*anyopaque;
pub const struct_scm_t_c_hook_entry = extern struct {
    next: [*c]struct_scm_t_c_hook_entry,
    func: scm_t_c_hook_function,
    data: ?*anyopaque,
};
pub const scm_t_c_hook_entry = struct_scm_t_c_hook_entry;
pub const struct_scm_t_c_hook = extern struct {
    first: [*c]scm_t_c_hook_entry,
    type: scm_t_c_hook_type,
    data: ?*anyopaque,
};
pub const scm_t_c_hook = struct_scm_t_c_hook;
pub extern fn scm_c_hook_init(hook: [*c]scm_t_c_hook, hook_data: ?*anyopaque, @"type": scm_t_c_hook_type) void;
pub extern fn scm_c_hook_add(hook: [*c]scm_t_c_hook, func: scm_t_c_hook_function, fn_data: ?*anyopaque, appendp: c_int) void;
pub extern fn scm_c_hook_remove(hook: [*c]scm_t_c_hook, func: scm_t_c_hook_function, fn_data: ?*anyopaque) void;
pub extern fn scm_c_hook_run(hook: [*c]scm_t_c_hook, data: ?*anyopaque) ?*anyopaque;
pub const struct_scm_t_cell = extern struct {
    word_0: SCM,
    word_1: SCM,
};
pub const scm_t_cell = struct_scm_t_cell;
pub extern var scm_gc_ports_collected: c_ulong;
pub extern var scm_after_gc_hook: SCM;
pub extern var scm_before_gc_c_hook: scm_t_c_hook;
pub extern var scm_before_mark_c_hook: scm_t_c_hook;
pub extern var scm_before_sweep_c_hook: scm_t_c_hook;
pub extern var scm_after_sweep_c_hook: scm_t_c_hook;
pub extern var scm_after_gc_c_hook: scm_t_c_hook;
pub extern fn scm_set_debug_cell_accesses_x(flag: SCM) SCM;
pub extern fn scm_object_address(obj: SCM) SCM;
pub extern fn scm_gc_enable() SCM;
pub extern fn scm_gc_disable() SCM;
pub extern fn scm_gc_dump() SCM;
pub extern fn scm_gc_stats() SCM;
pub extern fn scm_gc() SCM;
pub extern fn scm_i_gc(what: [*c]const u8) void;
pub extern fn scm_gc_mark(p: SCM) void;
pub extern fn scm_gc_sweep() void;
pub extern fn scm_gc_register_allocation(size: usize) void;
pub extern fn scm_malloc(size: usize) ?*anyopaque;
pub extern fn scm_calloc(size: usize) ?*anyopaque;
pub extern fn scm_realloc(mem: ?*anyopaque, size: usize) ?*anyopaque;
pub extern fn scm_strdup(str: [*c]const u8) [*c]u8;
pub extern fn scm_strndup(str: [*c]const u8, n: usize) [*c]u8;
pub extern fn scm_gc_register_collectable_memory(mem: ?*anyopaque, size: usize, what: [*c]const u8) void;
pub extern fn scm_gc_unregister_collectable_memory(mem: ?*anyopaque, size: usize, what: [*c]const u8) void;
pub extern fn scm_gc_malloc_pointerless(size: usize, what: [*c]const u8) ?*anyopaque;
pub extern fn scm_gc_calloc(size: usize, what: [*c]const u8) ?*anyopaque;
pub extern fn scm_gc_malloc(size: usize, what: [*c]const u8) ?*anyopaque;
pub extern fn scm_gc_realloc(mem: ?*anyopaque, old_size: usize, new_size: usize, what: [*c]const u8) ?*anyopaque;
pub extern fn scm_gc_free(mem: ?*anyopaque, size: usize, what: [*c]const u8) void;
pub extern fn scm_gc_strdup(str: [*c]const u8, what: [*c]const u8) [*c]u8;
pub extern fn scm_gc_strndup(str: [*c]const u8, n: usize, what: [*c]const u8) [*c]u8;
pub fn scm_cell(arg_car: scm_t_bits, arg_cdr: scm_t_bits) callconv(.C) SCM {
    var car = arg_car;
    var cdr = arg_cdr;
    var cell: SCM = @intToPtr(SCM, @intCast(scm_t_bits, @ptrToInt(scm_gc_malloc(@sizeOf(scm_t_cell), null))));
    _ = blk: {
        const tmp = @intToPtr(SCM, cdr);
        (blk_1: {
            const tmp_2 = @as(c_int, 1);
            if (tmp_2 >= 0) break :blk_1 @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_2: {
                const tmp_3 = cell;
                @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_3;
                break :blk_2 tmp_3;
            } else cell))))))) + @intCast(usize, tmp_2) else break :blk_1 @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_2: {
                const tmp_3 = cell;
                @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_3;
                break :blk_2 tmp_3;
            } else cell))))))) - ~@bitCast(usize, @intCast(isize, tmp_2) +% -1);
        }).* = tmp;
        break :blk tmp;
    };
    _ = blk: {
        const tmp = @intToPtr(SCM, car);
        (blk_1: {
            const tmp_2 = @as(c_int, 0);
            if (tmp_2 >= 0) break :blk_1 @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_2: {
                const tmp_3 = cell;
                @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_3;
                break :blk_2 tmp_3;
            } else cell))))))) + @intCast(usize, tmp_2) else break :blk_1 @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_2: {
                const tmp_3 = cell;
                @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_3;
                break :blk_2 tmp_3;
            } else cell))))))) - ~@bitCast(usize, @intCast(isize, tmp_2) +% -1);
        }).* = tmp;
        break :blk tmp;
    };
    return cell;
} // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gc.h:203:3: warning: TODO implement translation of stmt class GCCAsmStmtClass
// /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gc.h:174:1: warning: unable to translate function, demoted to extern
pub extern fn scm_double_cell(arg_car: scm_t_bits, arg_cbr: scm_t_bits, arg_ccr: scm_t_bits, arg_cdr: scm_t_bits) callconv(.C) SCM; // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gc.h:236:3: warning: TODO implement translation of stmt class GCCAsmStmtClass
// /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gc.h:214:1: warning: unable to translate function, demoted to extern
pub extern fn scm_words(arg_car: scm_t_bits, arg_n_words: u32) callconv(.C) SCM;
pub extern fn scm_remember_upto_here_1(obj: SCM) void;
pub extern fn scm_remember_upto_here_2(obj1: SCM, obj2: SCM) void;
pub extern fn scm_remember_upto_here(obj1: SCM, ...) void;
pub extern fn scm_return_first(elt: SCM, ...) SCM;
pub extern fn scm_return_first_int(x: c_int, ...) c_int;
pub extern fn scm_permanent_object(obj: SCM) SCM;
pub extern fn scm_gc_protect_object(obj: SCM) SCM;
pub extern fn scm_gc_unprotect_object(obj: SCM) SCM;
pub extern fn scm_gc_register_root(p: [*c]SCM) void;
pub extern fn scm_gc_unregister_root(p: [*c]SCM) void;
pub extern fn scm_gc_register_roots(b: [*c]SCM, n: c_ulong) void;
pub extern fn scm_gc_unregister_roots(b: [*c]SCM, n: c_ulong) void;
pub extern fn scm_gc_after_nonlocal_exit() void;
pub extern fn scm_storage_prehistory() void;
pub extern fn scm_init_gc_protect_object() void;
pub extern fn scm_init_gc() void;
pub fn scm_is_pair(arg_x: SCM) callconv(.C) c_int {
    var x = arg_x;
    return @boolToInt(!((@bitCast(c_ulong, @as(c_long, @as(c_int, 6))) & @intCast(scm_t_bits, @ptrToInt(if (false) blk: {
        const tmp = x;
        @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp;
        break :blk tmp;
    } else x))) != 0) and ((@bitCast(c_ulong, @as(c_long, @as(c_int, 1))) & @intCast(scm_t_bits, @ptrToInt(if (false) blk: {
        const tmp = (blk_1: {
            const tmp_2 = @as(c_int, 0);
            if (tmp_2 >= 0) break :blk_1 @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_2: {
                const tmp_3 = x;
                @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_3;
                break :blk_2 tmp_3;
            } else x))))))) + @intCast(usize, tmp_2) else break :blk_1 @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_2: {
                const tmp_3 = x;
                @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_3;
                break :blk_2 tmp_3;
            } else x))))))) - ~@bitCast(usize, @intCast(isize, tmp_2) +% -1);
        }).*;
        @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp;
        break :blk tmp;
    } else (blk: {
        const tmp = @as(c_int, 0);
        if (tmp >= 0) break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = x;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else x))))))) + @intCast(usize, tmp) else break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = x;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else x))))))) - ~@bitCast(usize, @intCast(isize, tmp) +% -1);
    }).*))) == @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))));
}
pub fn scm_cons(arg_x: SCM, arg_y: SCM) callconv(.C) SCM {
    var x = arg_x;
    var y = arg_y;
    return scm_cell(@intCast(scm_t_bits, @ptrToInt(if (false) blk: {
        const tmp = x;
        @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp;
        break :blk tmp;
    } else x)), @intCast(scm_t_bits, @ptrToInt(if (false) blk: {
        const tmp = y;
        @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp;
        break :blk tmp;
    } else y)));
}
pub fn scm_car(arg_x: SCM) callconv(.C) SCM {
    var x = arg_x;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(scm_is_pair(x) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
        scm_wrong_type_arg_msg("car", @as(c_int, 0), x, "pair");
    }
    return (blk: {
        const tmp = @as(c_int, 0);
        if (tmp >= 0) break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = x;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else x))))))) + @intCast(usize, tmp) else break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = x;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else x))))))) - ~@bitCast(usize, @intCast(isize, tmp) +% -1);
    }).*;
}
pub fn scm_cdr(arg_x: SCM) callconv(.C) SCM {
    var x = arg_x;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(scm_is_pair(x) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
        scm_wrong_type_arg_msg("cdr", @as(c_int, 0), x, "pair");
    }
    return (blk: {
        const tmp = @as(c_int, 1);
        if (tmp >= 0) break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = x;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else x))))))) + @intCast(usize, tmp) else break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = x;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else x))))))) - ~@bitCast(usize, @intCast(isize, tmp) +% -1);
    }).*;
}
pub extern fn scm_cons2(w: SCM, x: SCM, y: SCM) SCM;
pub extern fn scm_pair_p(x: SCM) SCM;
pub extern fn scm_set_car_x(pair: SCM, value: SCM) SCM;
pub extern fn scm_set_cdr_x(pair: SCM, value: SCM) SCM;
pub extern fn scm_cddr(x: SCM) SCM;
pub extern fn scm_cdar(x: SCM) SCM;
pub extern fn scm_cadr(x: SCM) SCM;
pub extern fn scm_caar(x: SCM) SCM;
pub extern fn scm_cdddr(x: SCM) SCM;
pub extern fn scm_cddar(x: SCM) SCM;
pub extern fn scm_cdadr(x: SCM) SCM;
pub extern fn scm_cdaar(x: SCM) SCM;
pub extern fn scm_caddr(x: SCM) SCM;
pub extern fn scm_cadar(x: SCM) SCM;
pub extern fn scm_caadr(x: SCM) SCM;
pub extern fn scm_caaar(x: SCM) SCM;
pub extern fn scm_cddddr(x: SCM) SCM;
pub extern fn scm_cdddar(x: SCM) SCM;
pub extern fn scm_cddadr(x: SCM) SCM;
pub extern fn scm_cddaar(x: SCM) SCM;
pub extern fn scm_cdaddr(x: SCM) SCM;
pub extern fn scm_cdadar(x: SCM) SCM;
pub extern fn scm_cdaadr(x: SCM) SCM;
pub extern fn scm_cdaaar(x: SCM) SCM;
pub extern fn scm_cadddr(x: SCM) SCM;
pub extern fn scm_caddar(x: SCM) SCM;
pub extern fn scm_cadadr(x: SCM) SCM;
pub extern fn scm_cadaar(x: SCM) SCM;
pub extern fn scm_caaddr(x: SCM) SCM;
pub extern fn scm_caadar(x: SCM) SCM;
pub extern fn scm_caaadr(x: SCM) SCM;
pub extern fn scm_caaaar(x: SCM) SCM;
pub extern fn scm_init_pairs() void;
pub extern fn scm_acons(w: SCM, x: SCM, y: SCM) SCM;
pub extern fn scm_sloppy_assq(x: SCM, alist: SCM) SCM;
pub extern fn scm_sloppy_assv(x: SCM, alist: SCM) SCM;
pub extern fn scm_sloppy_assoc(x: SCM, alist: SCM) SCM;
pub extern fn scm_assq(x: SCM, alist: SCM) SCM;
pub extern fn scm_assv(x: SCM, alist: SCM) SCM;
pub extern fn scm_assoc(x: SCM, alist: SCM) SCM;
pub extern fn scm_assq_ref(alist: SCM, key: SCM) SCM;
pub extern fn scm_assv_ref(alist: SCM, key: SCM) SCM;
pub extern fn scm_assoc_ref(alist: SCM, key: SCM) SCM;
pub extern fn scm_assq_set_x(alist: SCM, key: SCM, val: SCM) SCM;
pub extern fn scm_assv_set_x(alist: SCM, key: SCM, val: SCM) SCM;
pub extern fn scm_assoc_set_x(alist: SCM, key: SCM, val: SCM) SCM;
pub extern fn scm_assq_remove_x(alist: SCM, key: SCM) SCM;
pub extern fn scm_assv_remove_x(alist: SCM, key: SCM) SCM;
pub extern fn scm_assoc_remove_x(alist: SCM, key: SCM) SCM;
pub extern fn scm_init_alist() void;
pub extern fn scm_char_p(x: SCM) SCM;
pub extern fn scm_char_eq_p(x: SCM, y: SCM) SCM;
pub extern fn scm_char_less_p(x: SCM, y: SCM) SCM;
pub extern fn scm_char_leq_p(x: SCM, y: SCM) SCM;
pub extern fn scm_char_gr_p(x: SCM, y: SCM) SCM;
pub extern fn scm_char_geq_p(x: SCM, y: SCM) SCM;
pub extern fn scm_char_ci_eq_p(x: SCM, y: SCM) SCM;
pub extern fn scm_char_ci_less_p(x: SCM, y: SCM) SCM;
pub extern fn scm_char_ci_leq_p(x: SCM, y: SCM) SCM;
pub extern fn scm_char_ci_gr_p(x: SCM, y: SCM) SCM;
pub extern fn scm_char_ci_geq_p(x: SCM, y: SCM) SCM;
pub extern fn scm_char_alphabetic_p(chr: SCM) SCM;
pub extern fn scm_char_numeric_p(chr: SCM) SCM;
pub extern fn scm_char_whitespace_p(chr: SCM) SCM;
pub extern fn scm_char_upper_case_p(chr: SCM) SCM;
pub extern fn scm_char_lower_case_p(chr: SCM) SCM;
pub extern fn scm_char_is_both_p(chr: SCM) SCM;
pub extern fn scm_char_to_integer(chr: SCM) SCM;
pub extern fn scm_integer_to_char(n: SCM) SCM;
pub extern fn scm_char_upcase(chr: SCM) SCM;
pub extern fn scm_char_downcase(chr: SCM) SCM;
pub extern fn scm_char_titlecase(chr: SCM) SCM;
pub extern fn scm_char_general_category(chr: SCM) SCM;
pub fn scm_c_make_char(arg_c: scm_t_wchar) callconv(.C) SCM {
    var c = arg_c;
    return if (c <= @as(c_int, 1)) @intToPtr(SCM, (@bitCast(scm_t_bits, @as(c_ulong, @bitCast(u8, @truncate(i8, c)))) << @intCast(@import("std").math.Log2Int(scm_t_bits), 8)) +% @bitCast(c_ulong, @as(c_long, scm_tc8_char))) else @intToPtr(SCM, (@bitCast(scm_t_bits, @as(c_long, c)) << @intCast(@import("std").math.Log2Int(scm_t_bits), 8)) +% @bitCast(c_ulong, @as(c_long, scm_tc8_char)));
}
pub extern fn scm_c_upcase(c: scm_t_wchar) scm_t_wchar;
pub extern fn scm_c_downcase(c: scm_t_wchar) scm_t_wchar;
pub extern fn scm_c_titlecase(c: scm_t_wchar) scm_t_wchar;
pub extern fn scm_i_charname(chr: SCM) [*c]const u8;
pub extern fn scm_i_charname_to_char(charname: [*c]const u8, charname_len: usize) SCM;
pub extern fn scm_init_chars() void;
pub const struct_scm_t_option = extern struct {
    type: c_uint,
    name: [*c]const u8,
    val: scm_t_bits,
    doc: [*c]u8,
};
pub const scm_t_option = struct_scm_t_option;
pub extern fn scm_options_try(args: SCM, options: [*c]scm_t_option, s: [*c]const u8, dry_run: c_int) SCM;
pub extern fn scm_options(SCM, [*c]scm_t_option, [*c]const u8) SCM;
pub extern fn scm_init_opts(?fn (SCM) callconv(.C) SCM, [*c]scm_t_option) void;
pub extern fn scm_init_options() void;
pub extern var scm_print_state_vtable: SCM;
pub extern var scm_tc16_port_with_ps: scm_t_bits;
pub extern fn scm_print_options(setting: SCM) SCM;
pub extern fn scm_make_print_state() SCM;
pub extern fn scm_free_print_state(print_state: SCM) void;
pub extern fn scm_i_port_with_print_state(port: SCM, print_state: SCM) SCM;
pub extern fn scm_intprint(n: intmax_t, radix: c_int, port: SCM) void;
pub extern fn scm_uintprint(n: uintmax_t, radix: c_int, port: SCM) void;
pub extern fn scm_ipruk(hdr: [*c]u8, ptr: SCM, port: SCM) void;
pub extern fn scm_iprlist(hdr: [*c]u8, exp: SCM, tlr: c_int, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_print_symbol_name(str: [*c]const u8, len: usize, port: SCM) void;
pub extern fn scm_prin1(exp: SCM, port: SCM, writingp: c_int) void;
pub extern fn scm_iprin1(exp: SCM, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_write(obj: SCM, port: SCM) SCM;
pub extern fn scm_display(obj: SCM, port: SCM) SCM;
pub extern fn scm_simple_format(port: SCM, message: SCM, args: SCM) SCM;
pub extern fn scm_newline(port: SCM) SCM;
pub extern fn scm_write_char(chr: SCM, port: SCM) SCM;
pub extern fn scm_printer_apply(proc: SCM, exp: SCM, port: SCM, [*c]scm_print_state) SCM;
pub extern fn scm_port_with_print_state(port: SCM, pstate: SCM) SCM;
pub extern fn scm_get_print_state(port: SCM) SCM;
pub extern fn scm_valid_oport_value_p(val: SCM) c_int;
pub extern fn scm_init_print() void;
pub const scm_t_inum = c_long;
pub const struct_scm_t_double = extern struct {
    type: SCM,
    real: f64,
};
pub const scm_t_double = struct_scm_t_double;
pub const struct_scm_t_complex = extern struct {
    type: SCM,
    real: f64,
    imag: f64,
};
pub const scm_t_complex = struct_scm_t_complex;
pub extern fn scm_exact_p(x: SCM) SCM;
pub extern fn scm_is_exact(x: SCM) c_int;
pub extern fn scm_odd_p(n: SCM) SCM;
pub extern fn scm_even_p(n: SCM) SCM;
pub extern fn scm_finite_p(x: SCM) SCM;
pub extern fn scm_inf_p(x: SCM) SCM;
pub extern fn scm_nan_p(x: SCM) SCM;
pub extern fn scm_inf() SCM;
pub extern fn scm_nan() SCM;
pub extern fn scm_abs(x: SCM) SCM;
pub extern fn scm_quotient(x: SCM, y: SCM) SCM;
pub extern fn scm_remainder(x: SCM, y: SCM) SCM;
pub extern fn scm_modulo(x: SCM, y: SCM) SCM;
pub extern fn scm_euclidean_divide(x: SCM, y: SCM, q: [*c]SCM, r: [*c]SCM) void;
pub extern fn scm_euclidean_quotient(x: SCM, y: SCM) SCM;
pub extern fn scm_euclidean_remainder(x: SCM, y: SCM) SCM;
pub extern fn scm_floor_divide(x: SCM, y: SCM, q: [*c]SCM, r: [*c]SCM) void;
pub extern fn scm_floor_quotient(x: SCM, y: SCM) SCM;
pub extern fn scm_floor_remainder(x: SCM, y: SCM) SCM;
pub extern fn scm_ceiling_divide(x: SCM, y: SCM, q: [*c]SCM, r: [*c]SCM) void;
pub extern fn scm_ceiling_quotient(x: SCM, y: SCM) SCM;
pub extern fn scm_ceiling_remainder(x: SCM, y: SCM) SCM;
pub extern fn scm_truncate_divide(x: SCM, y: SCM, q: [*c]SCM, r: [*c]SCM) void;
pub extern fn scm_truncate_quotient(x: SCM, y: SCM) SCM;
pub extern fn scm_truncate_remainder(x: SCM, y: SCM) SCM;
pub extern fn scm_centered_divide(x: SCM, y: SCM, q: [*c]SCM, r: [*c]SCM) void;
pub extern fn scm_centered_quotient(x: SCM, y: SCM) SCM;
pub extern fn scm_centered_remainder(x: SCM, y: SCM) SCM;
pub extern fn scm_round_divide(x: SCM, y: SCM, q: [*c]SCM, r: [*c]SCM) void;
pub extern fn scm_round_quotient(x: SCM, y: SCM) SCM;
pub extern fn scm_round_remainder(x: SCM, y: SCM) SCM;
pub extern fn scm_gcd(x: SCM, y: SCM) SCM;
pub extern fn scm_lcm(n1: SCM, n2: SCM) SCM;
pub extern fn scm_logand(n1: SCM, n2: SCM) SCM;
pub extern fn scm_logior(n1: SCM, n2: SCM) SCM;
pub extern fn scm_logxor(n1: SCM, n2: SCM) SCM;
pub extern fn scm_logtest(n1: SCM, n2: SCM) SCM;
pub extern fn scm_logbit_p(n1: SCM, n2: SCM) SCM;
pub extern fn scm_lognot(n: SCM) SCM;
pub extern fn scm_modulo_expt(n: SCM, k: SCM, m: SCM) SCM;
pub extern fn scm_integer_expt(z1: SCM, z2: SCM) SCM;
pub extern fn scm_ash(n: SCM, count: SCM) SCM;
pub extern fn scm_round_ash(n: SCM, count: SCM) SCM;
pub extern fn scm_bit_extract(n: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_logcount(n: SCM) SCM;
pub extern fn scm_integer_length(n: SCM) SCM;
pub extern fn scm_i_euclidean_divide(x: SCM, y: SCM) SCM;
pub extern fn scm_i_floor_divide(x: SCM, y: SCM) SCM;
pub extern fn scm_i_ceiling_divide(x: SCM, y: SCM) SCM;
pub extern fn scm_i_truncate_divide(x: SCM, y: SCM) SCM;
pub extern fn scm_i_centered_divide(x: SCM, y: SCM) SCM;
pub extern fn scm_i_round_divide(x: SCM, y: SCM) SCM;
pub extern fn scm_i_gcd(x: SCM, y: SCM, rest: SCM) SCM;
pub extern fn scm_i_lcm(x: SCM, y: SCM, rest: SCM) SCM;
pub extern fn scm_i_logand(x: SCM, y: SCM, rest: SCM) SCM;
pub extern fn scm_i_logior(x: SCM, y: SCM, rest: SCM) SCM;
pub extern fn scm_i_logxor(x: SCM, y: SCM, rest: SCM) SCM;
pub extern fn scm_iint2str(num: intmax_t, rad: c_int, p: [*c]u8) usize;
pub extern fn scm_iuint2str(num: uintmax_t, rad: c_int, p: [*c]u8) usize;
pub extern fn scm_number_to_string(x: SCM, radix: SCM) SCM;
pub extern fn scm_print_real(sexp: SCM, port: SCM, pstate: [*c]scm_print_state) c_int;
pub extern fn scm_print_complex(sexp: SCM, port: SCM, pstate: [*c]scm_print_state) c_int;
pub extern fn scm_bigprint(exp: SCM, port: SCM, pstate: [*c]scm_print_state) c_int;
pub extern fn scm_c_locale_stringn_to_number(mem: [*c]const u8, len: usize, radix: c_uint) SCM;
pub extern fn scm_i_string_to_number(str: SCM, radix: c_uint) SCM;
pub extern fn scm_string_to_number(str: SCM, radix: SCM) SCM;
pub extern fn scm_bigequal(x: SCM, y: SCM) SCM;
pub extern fn scm_real_equalp(x: SCM, y: SCM) SCM;
pub extern fn scm_complex_equalp(x: SCM, y: SCM) SCM;
pub extern fn scm_i_heap_numbers_equal_p(x: SCM, y: SCM) c_int;
pub extern fn scm_number_p(x: SCM) SCM;
pub extern fn scm_complex_p(x: SCM) SCM;
pub extern fn scm_real_p(x: SCM) SCM;
pub extern fn scm_rational_p(z: SCM) SCM;
pub extern fn scm_integer_p(x: SCM) SCM;
pub extern fn scm_exact_integer_p(x: SCM) SCM;
pub extern fn scm_inexact_p(x: SCM) SCM;
pub extern fn scm_is_inexact(x: SCM) c_int;
pub extern fn scm_num_eq_p(x: SCM, y: SCM) SCM;
pub extern fn scm_less_p(x: SCM, y: SCM) SCM;
pub extern fn scm_gr_p(x: SCM, y: SCM) SCM;
pub extern fn scm_leq_p(x: SCM, y: SCM) SCM;
pub extern fn scm_geq_p(x: SCM, y: SCM) SCM;
pub extern fn scm_zero_p(z: SCM) SCM;
pub extern fn scm_positive_p(x: SCM) SCM;
pub extern fn scm_negative_p(x: SCM) SCM;
pub extern fn scm_max(x: SCM, y: SCM) SCM;
pub extern fn scm_min(x: SCM, y: SCM) SCM;
pub extern fn scm_sum(x: SCM, y: SCM) SCM;
pub extern fn scm_oneplus(x: SCM) SCM;
pub extern fn scm_difference(x: SCM, y: SCM) SCM;
pub extern fn scm_oneminus(x: SCM) SCM;
pub extern fn scm_product(x: SCM, y: SCM) SCM;
pub extern fn scm_divide(x: SCM, y: SCM) SCM;
pub extern fn scm_floor(x: SCM) SCM;
pub extern fn scm_ceiling(x: SCM) SCM;
pub extern fn scm_c_truncate(x: f64) f64;
pub extern fn scm_c_round(x: f64) f64;
pub extern fn scm_truncate_number(x: SCM) SCM;
pub extern fn scm_round_number(x: SCM) SCM;
pub extern fn scm_expt(z1: SCM, z2: SCM) SCM;
pub extern fn scm_sin(z: SCM) SCM;
pub extern fn scm_cos(z: SCM) SCM;
pub extern fn scm_tan(z: SCM) SCM;
pub extern fn scm_sinh(z: SCM) SCM;
pub extern fn scm_cosh(z: SCM) SCM;
pub extern fn scm_tanh(z: SCM) SCM;
pub extern fn scm_asin(z: SCM) SCM;
pub extern fn scm_acos(z: SCM) SCM;
pub extern fn scm_atan(x: SCM, y: SCM) SCM;
pub extern fn scm_sys_asinh(z: SCM) SCM;
pub extern fn scm_sys_acosh(z: SCM) SCM;
pub extern fn scm_sys_atanh(z: SCM) SCM;
pub extern fn scm_make_rectangular(z1: SCM, z2: SCM) SCM;
pub extern fn scm_make_polar(z1: SCM, z2: SCM) SCM;
pub extern fn scm_real_part(z: SCM) SCM;
pub extern fn scm_imag_part(z: SCM) SCM;
pub extern fn scm_magnitude(z: SCM) SCM;
pub extern fn scm_angle(z: SCM) SCM;
pub extern fn scm_exact_to_inexact(z: SCM) SCM;
pub extern fn scm_inexact_to_exact(z: SCM) SCM;
pub extern fn scm_trunc(x: SCM) SCM;
pub extern fn scm_log(z: SCM) SCM;
pub extern fn scm_log10(z: SCM) SCM;
pub extern fn scm_exp(z: SCM) SCM;
pub extern fn scm_sqrt(z: SCM) SCM;
pub extern fn scm_exact_integer_sqrt(k: SCM, s: [*c]SCM, r: [*c]SCM) void;
pub extern fn scm_i_min(x: SCM, y: SCM, rest: SCM) SCM;
pub extern fn scm_i_max(x: SCM, y: SCM, rest: SCM) SCM;
pub extern fn scm_i_sum(x: SCM, y: SCM, rest: SCM) SCM;
pub extern fn scm_i_difference(x: SCM, y: SCM, rest: SCM) SCM;
pub extern fn scm_i_product(x: SCM, y: SCM, rest: SCM) SCM;
pub extern fn scm_i_divide(x: SCM, y: SCM, rest: SCM) SCM;
pub extern fn scm_i_exact_integer_sqrt(k: SCM) SCM;
pub extern fn scm_rationalize(x: SCM, err: SCM) SCM;
pub extern fn scm_numerator(z: SCM) SCM;
pub extern fn scm_denominator(z: SCM) SCM;
pub extern fn scm_i_fraction2double(z: SCM) f64;
pub extern fn scm_i_fraction_equalp(x: SCM, y: SCM) SCM;
pub extern fn scm_i_print_fraction(sexp: SCM, port: SCM, pstate: [*c]scm_print_state) c_int;
pub extern fn scm_i_print_double(val: f64, port: SCM) void;
pub extern fn scm_i_print_complex(real: f64, imag: f64, port: SCM) void;
pub extern fn scm_is_integer(val: SCM) c_int;
pub extern fn scm_is_exact_integer(val: SCM) c_int;
pub extern fn scm_is_signed_integer(val: SCM, min: intmax_t, max: intmax_t) c_int;
pub extern fn scm_is_unsigned_integer(val: SCM, min: uintmax_t, max: uintmax_t) c_int;
pub extern fn scm_from_signed_integer(val: intmax_t) SCM;
pub extern fn scm_from_unsigned_integer(val: uintmax_t) SCM;
pub extern fn scm_to_signed_integer(val: SCM, min: intmax_t, max: intmax_t) intmax_t;
pub extern fn scm_to_unsigned_integer(val: SCM, min: uintmax_t, max: uintmax_t) uintmax_t;
pub extern fn scm_to_int8(x: SCM) i8;
pub extern fn scm_from_int8(x: i8) SCM;
pub extern fn scm_to_uint8(x: SCM) u8;
pub extern fn scm_from_uint8(x: u8) SCM;
pub extern fn scm_to_int16(x: SCM) i16;
pub extern fn scm_from_int16(x: i16) SCM;
pub extern fn scm_to_uint16(x: SCM) u16;
pub extern fn scm_from_uint16(x: u16) SCM;
pub extern fn scm_to_int32(x: SCM) i32;
pub extern fn scm_from_int32(x: i32) SCM;
pub extern fn scm_to_uint32(x: SCM) u32;
pub extern fn scm_from_uint32(x: u32) SCM;
pub extern fn scm_to_wchar(x: SCM) scm_t_wchar;
pub extern fn scm_from_wchar(x: scm_t_wchar) SCM;
pub extern fn scm_to_int64(x: SCM) i64;
pub extern fn scm_from_int64(x: i64) SCM;
pub extern fn scm_to_uint64(x: SCM) u64;
pub extern fn scm_from_uint64(x: u64) SCM;
pub extern fn scm_is_real(val: SCM) c_int;
pub extern fn scm_is_rational(val: SCM) c_int;
pub extern fn scm_to_double(val: SCM) f64;
pub extern fn scm_from_double(val: f64) SCM;
pub extern fn scm_is_complex(val: SCM) c_int;
pub extern fn scm_c_make_rectangular(re: f64, im: f64) SCM;
pub extern fn scm_c_make_polar(mag: f64, ang: f64) SCM;
pub extern fn scm_c_real_part(z: SCM) f64;
pub extern fn scm_c_imag_part(z: SCM) f64;
pub extern fn scm_c_magnitude(z: SCM) f64;
pub extern fn scm_c_angle(z: SCM) f64;
pub extern fn scm_is_number(val: SCM) c_int;
pub extern fn scm_init_numbers() void;
pub extern fn scm_make_array(fill: SCM, bounds: SCM) SCM;
pub extern fn scm_make_typed_array(@"type": SCM, fill: SCM, bounds: SCM) SCM;
pub extern fn scm_shared_array_root(ra: SCM) SCM;
pub extern fn scm_shared_array_offset(ra: SCM) SCM;
pub extern fn scm_shared_array_increments(ra: SCM) SCM;
pub extern fn scm_make_shared_array(oldra: SCM, mapfunc: SCM, dims: SCM) SCM;
pub extern fn scm_transpose_array(ra: SCM, args: SCM) SCM;
pub extern fn scm_array_contents(ra: SCM, strict: SCM) SCM;
pub extern fn scm_array_slice(ra: SCM, indices: SCM) SCM;
pub extern fn scm_array_cell_ref(ra: SCM, indices: SCM) SCM;
pub extern fn scm_array_cell_set_x(ra: SCM, b: SCM, indices: SCM) SCM;
pub extern fn scm_list_to_array(ndim: SCM, lst: SCM) SCM;
pub extern fn scm_list_to_typed_array(@"type": SCM, ndim: SCM, lst: SCM) SCM;
pub extern fn scm_c_array_rank(ra: SCM) usize;
pub extern fn scm_array_rank(ra: SCM) SCM;
pub extern fn scm_is_array(obj: SCM) c_int;
pub extern fn scm_array_p(v: SCM, unused: SCM) SCM;
pub extern fn scm_array_p_2(SCM) SCM;
pub extern fn scm_is_typed_array(obj: SCM, @"type": SCM) c_int;
pub extern fn scm_typed_array_p(v: SCM, @"type": SCM) SCM;
pub extern fn scm_c_array_length(ra: SCM) usize;
pub extern fn scm_array_length(ra: SCM) SCM;
pub extern fn scm_array_dimensions(ra: SCM) SCM;
pub extern fn scm_array_type(ra: SCM) SCM;
pub extern fn scm_array_type_code(ra: SCM) SCM;
pub extern fn scm_array_in_bounds_p(v: SCM, args: SCM) SCM;
pub extern fn scm_c_array_ref_1(v: SCM, idx0: isize) SCM;
pub extern fn scm_c_array_ref_2(v: SCM, idx0: isize, idx1: isize) SCM;
pub extern fn scm_c_array_set_1_x(v: SCM, obj: SCM, idx0: isize) void;
pub extern fn scm_c_array_set_2_x(v: SCM, obj: SCM, idx0: isize, idx1: isize) void;
pub extern fn scm_array_ref(v: SCM, args: SCM) SCM;
pub extern fn scm_array_set_x(v: SCM, obj: SCM, args: SCM) SCM;
pub extern fn scm_array_to_list(v: SCM) SCM;
pub const struct_scm_t_array_dim = extern struct {
    lbnd: isize,
    ubnd: isize,
    inc: isize,
};
pub const scm_t_array_dim = struct_scm_t_array_dim;
pub fn scm_i_raw_array(arg_ndim: c_int) callconv(.C) SCM {
    var ndim = arg_ndim;
    return scm_words((@bitCast(scm_t_bits, @as(c_long, ndim)) << @intCast(@import("std").math.Log2Int(scm_t_bits), 17)) +% @bitCast(c_ulong, @as(c_long, @as(c_int, 93))), @bitCast(u32, @as(c_int, 3) + (ndim * @as(c_int, 3))));
}
pub extern fn scm_i_make_array(ndim: c_int) SCM;
pub extern fn scm_i_print_array(array: SCM, port: SCM, pstate: [*c]scm_print_state) c_int;
pub extern fn scm_i_shap2ra(args: SCM) SCM;
pub extern fn scm_init_arrays() void;
pub const scm_t_vector_ref = ?fn (SCM, usize) callconv(.C) SCM;
pub const scm_t_vector_set = ?fn (SCM, usize, SCM) callconv(.C) void;
pub const SCM_ARRAY_ELEMENT_TYPE_SCM: c_int = 0;
pub const SCM_ARRAY_ELEMENT_TYPE_CHAR: c_int = 1;
pub const SCM_ARRAY_ELEMENT_TYPE_BIT: c_int = 2;
pub const SCM_ARRAY_ELEMENT_TYPE_VU8: c_int = 3;
pub const SCM_ARRAY_ELEMENT_TYPE_U8: c_int = 4;
pub const SCM_ARRAY_ELEMENT_TYPE_S8: c_int = 5;
pub const SCM_ARRAY_ELEMENT_TYPE_U16: c_int = 6;
pub const SCM_ARRAY_ELEMENT_TYPE_S16: c_int = 7;
pub const SCM_ARRAY_ELEMENT_TYPE_U32: c_int = 8;
pub const SCM_ARRAY_ELEMENT_TYPE_S32: c_int = 9;
pub const SCM_ARRAY_ELEMENT_TYPE_U64: c_int = 10;
pub const SCM_ARRAY_ELEMENT_TYPE_S64: c_int = 11;
pub const SCM_ARRAY_ELEMENT_TYPE_F32: c_int = 12;
pub const SCM_ARRAY_ELEMENT_TYPE_F64: c_int = 13;
pub const SCM_ARRAY_ELEMENT_TYPE_C32: c_int = 14;
pub const SCM_ARRAY_ELEMENT_TYPE_C64: c_int = 15;
pub const SCM_ARRAY_ELEMENT_TYPE_LAST: c_int = 15;
pub const scm_t_array_element_type = c_uint;
pub extern var scm_i_array_element_types: [*c]SCM;
pub const struct_scm_t_array_handle = extern struct {
    array: SCM,
    base: usize,
    ndims: usize,
    dims: [*c]scm_t_array_dim,
    dim0: scm_t_array_dim,
    element_type: scm_t_array_element_type,
    elements: ?*const anyopaque,
    writable_elements: ?*anyopaque,
    vector: SCM,
    // vref: scm_t_vector_ref,
    // vset: scm_t_vector_set,
};

pub const scm_t_array_handle = struct_scm_t_array_handle;
pub extern fn scm_array_get_handle(array: SCM, h: [*c]scm_t_array_handle) void;
pub extern fn scm_array_handle_pos(h: [*c]scm_t_array_handle, indices: SCM) isize;
pub extern fn scm_array_handle_pos_1(h: [*c]scm_t_array_handle, idx0: isize) isize;
pub extern fn scm_array_handle_pos_2(h: [*c]scm_t_array_handle, idx0: isize, idx1: isize) isize;
pub extern fn scm_array_handle_element_type(h: [*c]scm_t_array_handle) SCM;
pub extern fn scm_array_handle_release(h: [*c]scm_t_array_handle) void;
pub extern fn scm_array_handle_elements(h: [*c]scm_t_array_handle) [*c]const SCM;
pub extern fn scm_array_handle_writable_elements(h: [*c]scm_t_array_handle) [*c]SCM;
pub fn scm_array_handle_ref(arg_h: [*c]scm_t_array_handle, arg_p: isize) callconv(.C) SCM {
    var h = arg_h;
    var p = arg_p;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((p < @bitCast(c_long, @as(c_long, @as(c_int, 0)))) and (@bitCast(usize, -p) > h.*.base)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
        scm_out_of_range(null, scm_from_int64(p));
    }
    return h.*.vref.?(h.*.vector, h.*.base +% @bitCast(c_ulong, p));
}
pub fn scm_array_handle_set(arg_h: [*c]scm_t_array_handle, arg_p: isize, arg_v: SCM) callconv(.C) void {
    var h = arg_h;
    var p = arg_p;
    var v = arg_v;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((p < @bitCast(c_long, @as(c_long, @as(c_int, 0)))) and (@bitCast(usize, -p) > h.*.base)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
        scm_out_of_range(null, scm_from_int64(p));
    }
    h.*.vset.?(h.*.vector, h.*.base +% @bitCast(c_ulong, p), v);
}
pub extern fn scm_init_array_handle() void;
pub extern fn scm_ra_matchp(ra0: SCM, ras: SCM) c_int;
pub extern fn scm_ramapc(cproc: ?*anyopaque, data: SCM, ra0: SCM, lra: SCM, what: [*c]const u8) c_int;
pub extern fn scm_array_fill_x(ra: SCM, fill: SCM) SCM;
pub extern fn scm_array_copy_x(src: SCM, dst: SCM) SCM;
pub extern fn scm_array_map_x(ra0: SCM, proc: SCM, lra: SCM) SCM;
pub extern fn scm_array_for_each(proc: SCM, ra0: SCM, lra: SCM) SCM;
pub extern fn scm_array_index_map_x(ra: SCM, proc: SCM) SCM;
pub extern fn scm_array_equal_p(ra0: SCM, ra1: SCM) SCM;
pub extern fn scm_array_slice_for_each(frank: SCM, op: SCM, args: SCM) SCM;
pub extern fn scm_array_slice_for_each_in_order(frank: SCM, op: SCM, args: SCM) SCM;
pub extern fn scm_i_array_rebase(a: SCM, base: usize) SCM;
pub extern fn scm_init_array_map() void;
pub extern fn scm_is_bool(SCM) c_int;
pub extern fn scm_to_bool(x: SCM) c_int;
pub extern fn scm_not(x: SCM) SCM;
pub extern fn scm_boolean_p(obj: SCM) SCM;
pub extern fn scm_nil_p(obj: SCM) SCM;
pub extern fn scm_init_boolean() void;
pub extern fn scm_procedure_p(obj: SCM) SCM;
pub extern fn scm_thunk_p(obj: SCM) SCM;
pub extern fn scm_procedure_with_setter_p(obj: SCM) SCM;
pub extern fn scm_make_procedure_with_setter(procedure: SCM, setter: SCM) SCM;
pub extern fn scm_procedure(proc: SCM) SCM;
pub extern fn scm_setter(proc: SCM) SCM;
pub extern fn scm_init_procs() void;
pub const scm_t_thunk = ?fn (?*anyopaque) callconv(.C) SCM;
pub const scm_t_exception_handler = ?fn (?*anyopaque, SCM) callconv(.C) SCM;
pub extern fn scm_c_make_thunk(body: scm_t_thunk, body_data: ?*anyopaque) SCM;
pub extern fn scm_c_make_exception_handler(h: scm_t_exception_handler, handler_data: ?*anyopaque) SCM;
pub extern fn scm_c_with_exception_handler(@"type": SCM, handler: scm_t_exception_handler, handler_data: ?*anyopaque, thunk: scm_t_thunk, thunk_data: ?*anyopaque) SCM;
pub extern fn scm_c_with_default_exception_handler(thunk: scm_t_thunk, data: ?*anyopaque) SCM;
pub extern fn scm_with_exception_handler(@"type": SCM, handler: SCM, thunk: SCM) SCM;
pub extern fn scm_with_pre_unwind_exception_handler(handler: SCM, thunk: SCM) SCM;
pub extern fn scm_raise_exception(exn: SCM) noreturn;
pub extern fn scm_exception_kind(exn: SCM) SCM;
pub extern fn scm_exception_args(exn: SCM) SCM;
pub extern fn scm_dynwind_throw_handler() void;
pub extern fn scm_report_stack_overflow() void;
pub extern fn scm_report_out_of_memory() void;
pub extern fn scm_init_exceptions() void;
pub const scm_t_catch_body = scm_t_thunk;
pub const scm_t_catch_handler = ?fn (?*anyopaque, SCM, SCM) callconv(.C) SCM;
pub extern fn scm_i_make_catch_handler(h: scm_t_catch_handler, data: ?*anyopaque) SCM;
pub extern fn scm_c_catch(tag: SCM, body: scm_t_catch_body, body_data: ?*anyopaque, handler: scm_t_catch_handler, handler_data: ?*anyopaque, pre_unwind_handler: scm_t_catch_handler, pre_unwind_handler_data: ?*anyopaque) SCM;
pub extern fn scm_c_with_throw_handler(tag: SCM, body: scm_t_catch_body, body_data: ?*anyopaque, handler: scm_t_catch_handler, handler_data: ?*anyopaque, lazy_catch_p: c_int) SCM;
pub extern fn scm_internal_catch(tag: SCM, body: scm_t_catch_body, body_data: ?*anyopaque, handler: scm_t_catch_handler, handler_data: ?*anyopaque) SCM;
pub const struct_scm_body_thunk_data = extern struct {
    tag: SCM,
    body_proc: SCM,
};
pub extern fn scm_body_thunk(?*anyopaque) SCM;
pub extern fn scm_handle_by_proc(?*anyopaque, SCM, SCM) SCM;
pub extern fn scm_handle_by_proc_catching_all(?*anyopaque, SCM, SCM) SCM;
pub extern fn scm_handle_by_message(?*anyopaque, SCM, SCM) SCM;
pub extern fn scm_handle_by_message_noexit(?*anyopaque, SCM, SCM) SCM;
pub extern fn scm_handle_by_throw(?*anyopaque, SCM, SCM) SCM;
pub extern fn scm_exit_status(args: SCM) c_int;
pub extern fn scm_catch_with_pre_unwind_handler(tag: SCM, thunk: SCM, handler: SCM, lazy_handler: SCM) SCM;
pub extern fn scm_catch(tag: SCM, thunk: SCM, handler: SCM) SCM;
pub extern fn scm_with_throw_handler(tag: SCM, thunk: SCM, handler: SCM) SCM;
pub extern fn scm_ithrow(key: SCM, args: SCM, no_return: c_int) noreturn;
pub extern fn scm_throw(key: SCM, args: SCM) noreturn;
pub extern fn scm_init_throw() void;
pub extern fn setjmp(__env: [*c]struct___jmp_buf_tag) c_int;
pub extern fn __sigsetjmp(__env: [*c]struct___jmp_buf_tag, __savemask: c_int) c_int;
pub extern fn _setjmp(__env: [*c]struct___jmp_buf_tag) c_int;
pub extern fn longjmp(__env: [*c]struct___jmp_buf_tag, __val: c_int) noreturn;
pub extern fn _longjmp(__env: [*c]struct___jmp_buf_tag, __val: c_int) noreturn;
pub const sigjmp_buf = [1]struct___jmp_buf_tag;
pub extern fn siglongjmp(__env: [*c]struct___jmp_buf_tag, __val: c_int) noreturn;
pub const sig_atomic_t = __sig_atomic_t;
const struct_unnamed_8 = extern struct {
    si_pid: __pid_t,
    si_uid: __uid_t,
};
const struct_unnamed_9 = extern struct {
    si_tid: c_int,
    si_overrun: c_int,
    si_sigval: __sigval_t,
};
const struct_unnamed_10 = extern struct {
    si_pid: __pid_t,
    si_uid: __uid_t,
    si_sigval: __sigval_t,
};
const struct_unnamed_11 = extern struct {
    si_pid: __pid_t,
    si_uid: __uid_t,
    si_status: c_int,
    si_utime: __clock_t,
    si_stime: __clock_t,
};
const struct_unnamed_14 = extern struct {
    _lower: ?*anyopaque,
    _upper: ?*anyopaque,
};
const union_unnamed_13 = extern union {
    _addr_bnd: struct_unnamed_14,
    _pkey: __uint32_t,
};
const struct_unnamed_12 = extern struct {
    si_addr: ?*anyopaque,
    si_addr_lsb: c_short,
    _bounds: union_unnamed_13,
};
const struct_unnamed_15 = extern struct {
    si_band: c_long,
    si_fd: c_int,
};
const struct_unnamed_16 = extern struct {
    _call_addr: ?*anyopaque,
    _syscall: c_int,
    _arch: c_uint,
};
const union_unnamed_7 = extern union {
    _pad: [28]c_int,
    _kill: struct_unnamed_8,
    _timer: struct_unnamed_9,
    _rt: struct_unnamed_10,
    _sigchld: struct_unnamed_11,
    _sigfault: struct_unnamed_12,
    _sigpoll: struct_unnamed_15,
    _sigsys: struct_unnamed_16,
};
pub const siginfo_t = extern struct {
    si_signo: c_int,
    si_errno: c_int,
    si_code: c_int,
    __pad0: c_int,
    _sifields: union_unnamed_7,
};
pub const SI_ASYNCNL: c_int = -60;
pub const SI_DETHREAD: c_int = -7;
pub const SI_TKILL: c_int = -6;
pub const SI_SIGIO: c_int = -5;
pub const SI_ASYNCIO: c_int = -4;
pub const SI_MESGQ: c_int = -3;
pub const SI_TIMER: c_int = -2;
pub const SI_QUEUE: c_int = -1;
pub const SI_USER: c_int = 0;
pub const SI_KERNEL: c_int = 128;
const enum_unnamed_17 = c_int;
pub const ILL_ILLOPC: c_int = 1;
pub const ILL_ILLOPN: c_int = 2;
pub const ILL_ILLADR: c_int = 3;
pub const ILL_ILLTRP: c_int = 4;
pub const ILL_PRVOPC: c_int = 5;
pub const ILL_PRVREG: c_int = 6;
pub const ILL_COPROC: c_int = 7;
pub const ILL_BADSTK: c_int = 8;
pub const ILL_BADIADDR: c_int = 9;
const enum_unnamed_18 = c_uint;
pub const FPE_INTDIV: c_int = 1;
pub const FPE_INTOVF: c_int = 2;
pub const FPE_FLTDIV: c_int = 3;
pub const FPE_FLTOVF: c_int = 4;
pub const FPE_FLTUND: c_int = 5;
pub const FPE_FLTRES: c_int = 6;
pub const FPE_FLTINV: c_int = 7;
pub const FPE_FLTSUB: c_int = 8;
pub const FPE_FLTUNK: c_int = 14;
pub const FPE_CONDTRAP: c_int = 15;
const enum_unnamed_19 = c_uint;
pub const SEGV_MAPERR: c_int = 1;
pub const SEGV_ACCERR: c_int = 2;
pub const SEGV_BNDERR: c_int = 3;
pub const SEGV_PKUERR: c_int = 4;
pub const SEGV_ACCADI: c_int = 5;
pub const SEGV_ADIDERR: c_int = 6;
pub const SEGV_ADIPERR: c_int = 7;
pub const SEGV_MTEAERR: c_int = 8;
pub const SEGV_MTESERR: c_int = 9;
const enum_unnamed_20 = c_uint;
pub const BUS_ADRALN: c_int = 1;
pub const BUS_ADRERR: c_int = 2;
pub const BUS_OBJERR: c_int = 3;
pub const BUS_MCEERR_AR: c_int = 4;
pub const BUS_MCEERR_AO: c_int = 5;
const enum_unnamed_21 = c_uint;
pub const CLD_EXITED: c_int = 1;
pub const CLD_KILLED: c_int = 2;
pub const CLD_DUMPED: c_int = 3;
pub const CLD_TRAPPED: c_int = 4;
pub const CLD_STOPPED: c_int = 5;
pub const CLD_CONTINUED: c_int = 6;
const enum_unnamed_22 = c_uint;
pub const POLL_IN: c_int = 1;
pub const POLL_OUT: c_int = 2;
pub const POLL_MSG: c_int = 3;
pub const POLL_ERR: c_int = 4;
pub const POLL_PRI: c_int = 5;
pub const POLL_HUP: c_int = 6;
const enum_unnamed_23 = c_uint;
pub const sigval_t = __sigval_t;
pub const sigevent_t = struct_sigevent;
pub const SIGEV_SIGNAL: c_int = 0;
pub const SIGEV_NONE: c_int = 1;
pub const SIGEV_THREAD: c_int = 2;
pub const SIGEV_THREAD_ID: c_int = 4;
const enum_unnamed_24 = c_uint;
pub const __sighandler_t = ?fn (c_int) callconv(.C) void;
pub extern fn __sysv_signal(__sig: c_int, __handler: __sighandler_t) __sighandler_t;
pub extern fn signal(__sig: c_int, __handler: __sighandler_t) __sighandler_t;
pub extern fn kill(__pid: __pid_t, __sig: c_int) c_int;
pub extern fn killpg(__pgrp: __pid_t, __sig: c_int) c_int;
pub extern fn raise(__sig: c_int) c_int;
pub extern fn ssignal(__sig: c_int, __handler: __sighandler_t) __sighandler_t;
pub extern fn gsignal(__sig: c_int) c_int;
pub extern fn psignal(__sig: c_int, __s: [*c]const u8) void;
pub extern fn psiginfo(__pinfo: [*c]const siginfo_t, __s: [*c]const u8) void;
pub extern fn sigblock(__mask: c_int) c_int;
pub extern fn sigsetmask(__mask: c_int) c_int;
pub extern fn siggetmask() c_int;
pub const sig_t = __sighandler_t;
pub extern fn sigemptyset(__set: [*c]sigset_t) c_int;
pub extern fn sigfillset(__set: [*c]sigset_t) c_int;
pub extern fn sigaddset(__set: [*c]sigset_t, __signo: c_int) c_int;
pub extern fn sigdelset(__set: [*c]sigset_t, __signo: c_int) c_int;
pub extern fn sigismember(__set: [*c]const sigset_t, __signo: c_int) c_int;
const union_unnamed_25 = extern union {
    sa_handler: __sighandler_t,
    sa_sigaction: ?fn (c_int, [*c]siginfo_t, ?*anyopaque) callconv(.C) void,
};
pub const struct_sigaction = extern struct {
    __sigaction_handler: union_unnamed_25,
    sa_mask: __sigset_t,
    sa_flags: c_int,
    sa_restorer: ?fn () callconv(.C) void,
};
pub extern fn sigprocmask(__how: c_int, noalias __set: [*c]const sigset_t, noalias __oset: [*c]sigset_t) c_int;
pub extern fn sigsuspend(__set: [*c]const sigset_t) c_int;
pub extern fn sigaction(__sig: c_int, noalias __act: [*c]const struct_sigaction, noalias __oact: [*c]struct_sigaction) c_int;
pub extern fn sigpending(__set: [*c]sigset_t) c_int;
pub extern fn sigwait(noalias __set: [*c]const sigset_t, noalias __sig: [*c]c_int) c_int;
pub extern fn sigwaitinfo(noalias __set: [*c]const sigset_t, noalias __info: [*c]siginfo_t) c_int;
pub extern fn sigtimedwait(noalias __set: [*c]const sigset_t, noalias __info: [*c]siginfo_t, noalias __timeout: [*c]const struct_timespec) c_int;
pub extern fn sigqueue(__pid: __pid_t, __sig: c_int, __val: union_sigval) c_int;
pub const struct__fpx_sw_bytes = extern struct {
    magic1: __uint32_t,
    extended_size: __uint32_t,
    xstate_bv: __uint64_t,
    xstate_size: __uint32_t,
    __glibc_reserved1: [7]__uint32_t,
};
pub const struct__fpreg = extern struct {
    significand: [4]c_ushort,
    exponent: c_ushort,
};
pub const struct__fpxreg = extern struct {
    significand: [4]c_ushort,
    exponent: c_ushort,
    __glibc_reserved1: [3]c_ushort,
};
pub const struct__xmmreg = extern struct {
    element: [4]__uint32_t,
};
pub const struct__fpstate = extern struct {
    cwd: __uint16_t,
    swd: __uint16_t,
    ftw: __uint16_t,
    fop: __uint16_t,
    rip: __uint64_t,
    rdp: __uint64_t,
    mxcsr: __uint32_t,
    mxcr_mask: __uint32_t,
    _st: [8]struct__fpxreg,
    _xmm: [16]struct__xmmreg,
    __glibc_reserved1: [24]__uint32_t,
};
const union_unnamed_26 = extern union {
    fpstate: [*c]struct__fpstate,
    __fpstate_word: __uint64_t,
};
pub const struct_sigcontext = extern struct {
    r8: __uint64_t,
    r9: __uint64_t,
    r10: __uint64_t,
    r11: __uint64_t,
    r12: __uint64_t,
    r13: __uint64_t,
    r14: __uint64_t,
    r15: __uint64_t,
    rdi: __uint64_t,
    rsi: __uint64_t,
    rbp: __uint64_t,
    rbx: __uint64_t,
    rdx: __uint64_t,
    rax: __uint64_t,
    rcx: __uint64_t,
    rsp: __uint64_t,
    rip: __uint64_t,
    eflags: __uint64_t,
    cs: c_ushort,
    gs: c_ushort,
    fs: c_ushort,
    __pad0: c_ushort,
    err: __uint64_t,
    trapno: __uint64_t,
    oldmask: __uint64_t,
    cr2: __uint64_t,
    unnamed_0: union_unnamed_26,
    __reserved1: [8]__uint64_t,
};
pub const struct__xsave_hdr = extern struct {
    xstate_bv: __uint64_t,
    __glibc_reserved1: [2]__uint64_t,
    __glibc_reserved2: [5]__uint64_t,
};
pub const struct__ymmh_state = extern struct {
    ymmh_space: [64]__uint32_t,
};
pub const struct__xstate = extern struct {
    fpstate: struct__fpstate,
    xstate_hdr: struct__xsave_hdr,
    ymmh: struct__ymmh_state,
};
pub extern fn sigreturn(__scp: [*c]struct_sigcontext) c_int;
pub const stack_t = extern struct {
    ss_sp: ?*anyopaque,
    ss_flags: c_int,
    ss_size: usize,
};
pub const greg_t = c_longlong;
pub const gregset_t = [23]greg_t;
pub const struct__libc_fpxreg = extern struct {
    significand: [4]c_ushort,
    exponent: c_ushort,
    __glibc_reserved1: [3]c_ushort,
};
pub const struct__libc_xmmreg = extern struct {
    element: [4]__uint32_t,
};
pub const struct__libc_fpstate = extern struct {
    cwd: __uint16_t,
    swd: __uint16_t,
    ftw: __uint16_t,
    fop: __uint16_t,
    rip: __uint64_t,
    rdp: __uint64_t,
    mxcsr: __uint32_t,
    mxcr_mask: __uint32_t,
    _st: [8]struct__libc_fpxreg,
    _xmm: [16]struct__libc_xmmreg,
    __glibc_reserved1: [24]__uint32_t,
};
pub const fpregset_t = [*c]struct__libc_fpstate;
pub const mcontext_t = extern struct {
    gregs: gregset_t,
    fpregs: fpregset_t,
    __reserved1: [8]c_ulonglong,
};
pub const struct_ucontext_t = extern struct {
    uc_flags: c_ulong,
    uc_link: [*c]struct_ucontext_t,
    uc_stack: stack_t,
    uc_mcontext: mcontext_t,
    uc_sigmask: sigset_t,
    __fpregs_mem: struct__libc_fpstate,
    __ssp: [4]c_ulonglong,
};
pub const ucontext_t = struct_ucontext_t;
pub extern fn siginterrupt(__sig: c_int, __interrupt: c_int) c_int;
pub const SS_ONSTACK: c_int = 1;
pub const SS_DISABLE: c_int = 2;
const enum_unnamed_27 = c_uint;
pub extern fn sigaltstack(noalias __ss: [*c]const stack_t, noalias __oss: [*c]stack_t) c_int;
pub const struct_sigstack = extern struct {
    ss_sp: ?*anyopaque,
    ss_onstack: c_int,
};
pub extern fn sigstack(__ss: [*c]struct_sigstack, __oss: [*c]struct_sigstack) c_int;
pub extern fn pthread_sigmask(__how: c_int, noalias __newmask: [*c]const __sigset_t, noalias __oldmask: [*c]__sigset_t) c_int;
pub extern fn pthread_kill(__threadid: pthread_t, __signo: c_int) c_int;
pub extern fn __libc_current_sigrtmin() c_int;
pub extern fn __libc_current_sigrtmax() c_int;
pub const SCM_DYNSTACK_TYPE_NONE: c_int = 0;
pub const SCM_DYNSTACK_TYPE_FRAME: c_int = 1;
pub const SCM_DYNSTACK_TYPE_UNWINDER: c_int = 2;
pub const SCM_DYNSTACK_TYPE_REWINDER: c_int = 3;
pub const SCM_DYNSTACK_TYPE_WITH_FLUID: c_int = 4;
pub const SCM_DYNSTACK_TYPE_PROMPT: c_int = 5;
pub const SCM_DYNSTACK_TYPE_DYNWIND: c_int = 6;
pub const SCM_DYNSTACK_TYPE_DYNAMIC_STATE: c_int = 7;
pub const scm_t_dynstack_item_type = c_uint;
pub const SCM_F_DYNSTACK_FRAME_REWINDABLE: c_int = 16;
pub const scm_t_dynstack_frame_flags = c_uint;
pub const SCM_F_DYNSTACK_WINDER_EXPLICIT: c_int = 16;
pub const scm_t_dynstack_winder_flags = c_uint;
pub const SCM_F_DYNSTACK_PROMPT_ESCAPE_ONLY: c_int = 16;
pub const scm_t_dynstack_prompt_flags = c_uint;
pub const scm_t_guard = ?fn (?*anyopaque) callconv(.C) void;
pub extern fn scm_dynstack_push_frame([*c]scm_t_dynstack, scm_t_dynstack_frame_flags) void;
pub extern fn scm_dynstack_push_rewinder([*c]scm_t_dynstack, scm_t_dynstack_winder_flags, scm_t_guard, ?*anyopaque) void;
pub extern fn scm_dynstack_push_unwinder([*c]scm_t_dynstack, scm_t_dynstack_winder_flags, scm_t_guard, ?*anyopaque) void;
pub extern fn scm_dynstack_push_fluid([*c]scm_t_dynstack, fluid: SCM, value: SCM, dynamic_state: ?*scm_t_dynamic_state) void;
pub extern fn scm_dynstack_push_dynamic_state([*c]scm_t_dynstack, SCM, ?*scm_t_dynamic_state) void;
pub extern fn scm_dynstack_push_prompt([*c]scm_t_dynstack, scm_t_dynstack_prompt_flags, key: SCM, fp_offset: ptrdiff_t, sp_offset: ptrdiff_t, vra: [*c]u32, mra: [*c]u8, registers: [*c]jmp_buf) void;
pub extern fn scm_dynstack_push_dynwind([*c]scm_t_dynstack, enter: SCM, leave: SCM) void;
pub extern fn scm_dynstack_pop([*c]scm_t_dynstack) void;
pub extern fn scm_dynstack_capture_all(dynstack: [*c]scm_t_dynstack) [*c]scm_t_dynstack;
pub extern fn scm_dynstack_capture(dynstack: [*c]scm_t_dynstack, item: [*c]scm_t_bits) [*c]scm_t_dynstack;
pub extern fn scm_dynstack_wind_1([*c]scm_t_dynstack, [*c]scm_t_bits) void;
pub extern fn scm_dynstack_unwind_1([*c]scm_t_dynstack) scm_t_bits;
pub extern fn scm_dynstack_wind([*c]scm_t_dynstack, [*c]scm_t_bits) void;
pub extern fn scm_dynstack_unwind([*c]scm_t_dynstack, [*c]scm_t_bits) void;
pub extern fn scm_dynstack_unwind_fork([*c]scm_t_dynstack, [*c]scm_t_dynstack) [*c]scm_t_bits;
pub extern fn scm_dynstack_unwind_frame([*c]scm_t_dynstack) void;
pub extern fn scm_dynstack_unwind_fluid(dynstack: [*c]scm_t_dynstack, dynamic_state: ?*scm_t_dynamic_state) void;
pub extern fn scm_dynstack_unwind_dynamic_state(dynstack: [*c]scm_t_dynstack, dynamic_state: ?*scm_t_dynamic_state) void;
pub extern fn scm_dynstack_find_prompt([*c]scm_t_dynstack, SCM, [*c]scm_t_dynstack_prompt_flags, [*c]ptrdiff_t, [*c]ptrdiff_t, [*c][*c]u32, [*c][*c]u8, [*c][*c]jmp_buf) [*c]scm_t_bits;
pub extern fn scm_dynstack_find_old_fluid_value([*c]scm_t_dynstack, SCM, usize, SCM) SCM;
pub extern fn scm_dynstack_relocate_prompts([*c]scm_t_dynstack, ptrdiff_t) void;
pub extern fn scm_dynstack_wind_prompt([*c]scm_t_dynstack, [*c]scm_t_bits, ptrdiff_t, [*c]jmp_buf) void;
pub extern fn scm_std_select(fds: c_int, rfds: [*c]fd_set, wfds: [*c]fd_set, efds: [*c]fd_set, timeout: [*c]struct_timeval) c_int;
pub const struct_scm_smob_descriptor = extern struct {
    name: [*c]const u8,
    size: usize,
    mark: ?fn (SCM) callconv(.C) SCM,
    free: ?fn (SCM) callconv(.C) usize,
    print: ?fn (SCM, SCM, [*c]scm_print_state) callconv(.C) c_int,
    equalp: ?fn (SCM, SCM) callconv(.C) SCM,
    apply: scm_t_subr,
    apply_trampoline: SCM,
};
pub const scm_smob_descriptor = struct_scm_smob_descriptor;
pub extern var scm_numsmob: c_long;
pub extern var scm_smobs: [*c]scm_smob_descriptor;
pub extern fn scm_i_new_smob(tc: scm_t_bits, scm_t_bits) SCM;
pub extern fn scm_i_new_double_smob(tc: scm_t_bits, scm_t_bits, scm_t_bits, scm_t_bits) SCM;
pub fn scm_new_smob(arg_tc: scm_t_bits, arg_data: scm_t_bits) callconv(.C) SCM {
    var tc = arg_tc;
    var data = arg_data;
    var smobnum: scm_t_bits = @bitCast(c_ulong, @as(c_long, @as(c_int, 255))) & (tc >> @intCast(@import("std").math.Log2Int(scm_t_bits), 8));
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((scm_smobs[smobnum].mark != null) or (scm_smobs[smobnum].free != null)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) return scm_i_new_smob(tc, data) else return scm_cell(tc, data);
    return null;
}
pub fn scm_new_double_smob(arg_tc: scm_t_bits, arg_data1: scm_t_bits, arg_data2: scm_t_bits, arg_data3: scm_t_bits) callconv(.C) SCM {
    var tc = arg_tc;
    var data1 = arg_data1;
    var data2 = arg_data2;
    var data3 = arg_data3;
    var smobnum: scm_t_bits = @bitCast(c_ulong, @as(c_long, @as(c_int, 255))) & (tc >> @intCast(@import("std").math.Log2Int(scm_t_bits), 8));
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((scm_smobs[smobnum].mark != null) or (scm_smobs[smobnum].free != null)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) return scm_i_new_double_smob(tc, data1, data2, data3) else return scm_double_cell(tc, data1, data2, data3);
    return null;
}
pub extern fn scm_mark0(ptr: SCM) SCM;
pub extern fn scm_markcdr(ptr: SCM) SCM;
pub extern fn scm_free0(ptr: SCM) usize;
pub extern fn scm_smob_print(exp: SCM, port: SCM, pstate: [*c]scm_print_state) c_int;
pub extern fn scm_make_smob_type(name: [*c]const u8, size: usize) scm_t_bits;
pub extern fn scm_set_smob_mark(tc: scm_t_bits, mark: ?fn (SCM) callconv(.C) SCM) void;
pub extern fn scm_set_smob_free(tc: scm_t_bits, free: ?fn (SCM) callconv(.C) usize) void;
pub extern fn scm_set_smob_print(tc: scm_t_bits, print: ?fn (SCM, SCM, [*c]scm_print_state) callconv(.C) c_int) void;
pub extern fn scm_set_smob_equalp(tc: scm_t_bits, equalp: ?fn (SCM, SCM) callconv(.C) SCM) void;
pub extern fn scm_set_smob_apply(tc: scm_t_bits, apply: scm_t_subr, req: c_uint, opt: c_uint, rst: c_uint) void;
pub extern fn scm_smob_type_class(tc: scm_t_bits) SCM;
pub extern fn scm_assert_smob_type(tag: scm_t_bits, val: SCM) void;
pub extern fn scm_make_smob(tc: scm_t_bits) SCM;
pub extern fn scm_smob_prehistory() void;
pub extern fn scm_program_p(obj: SCM) SCM;
pub extern fn scm_program_code(program: SCM) SCM;
pub extern fn scm_primitive_code_p(code: SCM) SCM;
pub extern fn scm_primitive_code_name(code: SCM) SCM;
pub extern fn scm_primitive_call_ip(prim: SCM) SCM;
pub extern fn scm_i_program_name(program: SCM) SCM;
pub extern fn scm_i_program_documentation(program: SCM) SCM;
pub extern fn scm_i_program_properties(program: SCM) SCM;
pub extern fn scm_find_source_for_addr(ip: SCM) SCM;
pub extern fn scm_program_address_range(program: SCM) SCM;
pub extern fn scm_program_num_free_variables(program: SCM) SCM;
pub extern fn scm_program_free_variable_ref(program: SCM, i: SCM) SCM;
pub extern fn scm_program_free_variable_set_x(program: SCM, i: SCM, x: SCM) SCM;
pub extern fn scm_i_program_arity(program: SCM, req: [*c]c_int, opt: [*c]c_int, rest: [*c]c_int) c_int;
pub extern fn scm_i_program_print(program: SCM, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_bootstrap_programs() void;
pub extern fn scm_init_programs() void;
pub const SCM_F_COMPARE_NONE: c_int = 0;
pub const SCM_F_COMPARE_EQUAL: c_int = 1;
pub const SCM_F_COMPARE_LESS_THAN: c_int = 2;
pub const SCM_F_COMPARE_INVALID: c_int = 3;
pub const enum_scm_compare = c_uint;
pub extern fn scm_call_with_vm(proc: SCM, args: SCM) SCM;
pub extern fn scm_call_with_stack_overflow_handler(limit: SCM, thunk: SCM, handler: SCM) SCM;
pub extern fn scm_vm_add_apply_hook_x(SCM) SCM;
pub extern fn scm_vm_add_return_hook_x(SCM) SCM;
pub extern fn scm_vm_add_abort_hook_x(SCM) SCM;
pub extern fn scm_vm_add_next_hook_x(SCM) SCM;
pub extern fn scm_vm_remove_apply_hook_x(SCM) SCM;
pub extern fn scm_vm_remove_return_hook_x(SCM) SCM;
pub extern fn scm_vm_remove_abort_hook_x(SCM) SCM;
pub extern fn scm_vm_remove_next_hook_x(SCM) SCM;
pub extern fn scm_vm_trace_level() SCM;
pub extern fn scm_set_vm_trace_level_x(level: SCM) SCM;
pub extern fn scm_vm_engine() SCM;
pub extern fn scm_set_vm_engine_x(engine: SCM) SCM;
pub extern fn scm_set_default_vm_engine_x(engine: SCM) SCM;
pub extern fn scm_c_set_vm_engine_x(engine: c_int) void;
pub extern fn scm_c_set_default_vm_engine_x(engine: c_int) void;
pub extern fn scm_i_vm_prepare_stack(vp: [*c]struct_scm_vm) void;
pub const struct_GC_ms_entry = opaque {};
pub extern fn scm_i_vm_mark_stack([*c]struct_scm_vm, ?*struct_GC_ms_entry, ?*struct_GC_ms_entry) ?*struct_GC_ms_entry;
pub extern fn scm_i_vm_free_stack(vp: [*c]struct_scm_vm) void;
pub const struct_scm_vm_cont = extern struct {
    vra: [*c]u32,
    mra: [*c]u8,
    fp_offset: ptrdiff_t,
    stack_size: ptrdiff_t,
    stack_bottom: [*c]union_scm_vm_stack_element,
    dynstack: [*c]scm_t_dynstack,
    flags: u32,
};
pub extern fn scm_load_compiled_with_vm(file: SCM) SCM;
pub extern fn scm_i_call_with_current_continuation(proc: SCM) SCM;
pub extern fn scm_i_capture_current_stack() SCM;
pub extern fn scm_i_vm_abort(tag_and_argv: [*c]SCM, n: usize) noreturn;
pub extern fn scm_i_vm_emergency_abort(tag_and_argv: [*c]SCM, n: usize) noreturn;
pub extern fn scm_i_vm_cont_to_frame(cont: SCM, frame: ?*struct_scm_frame) c_int;
pub extern fn scm_i_vm_cont_print(x: SCM, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_i_vm_is_boot_continuation_code(ip: [*c]u32) c_int;
pub extern fn scm_bootstrap_vm() void;
pub extern fn scm_init_vm() void;
pub const struct_sched_param = extern struct {
    sched_priority: c_int,
};
pub const __cpu_mask = c_ulong;
pub const cpu_set_t = extern struct {
    __bits: [16]__cpu_mask,
};
pub extern fn __sched_cpucount(__setsize: usize, __setp: [*c]const cpu_set_t) c_int;
pub extern fn __sched_cpualloc(__count: usize) [*c]cpu_set_t;
pub extern fn __sched_cpufree(__set: [*c]cpu_set_t) void;
pub extern fn sched_setparam(__pid: __pid_t, __param: [*c]const struct_sched_param) c_int;
pub extern fn sched_getparam(__pid: __pid_t, __param: [*c]struct_sched_param) c_int;
pub extern fn sched_setscheduler(__pid: __pid_t, __policy: c_int, __param: [*c]const struct_sched_param) c_int;
pub extern fn sched_getscheduler(__pid: __pid_t) c_int;
pub extern fn sched_yield() c_int;
pub extern fn sched_get_priority_max(__algorithm: c_int) c_int;
pub extern fn sched_get_priority_min(__algorithm: c_int) c_int;
pub extern fn sched_rr_get_interval(__pid: __pid_t, __t: [*c]struct_timespec) c_int;
pub const PTHREAD_CREATE_JOINABLE: c_int = 0;
pub const PTHREAD_CREATE_DETACHED: c_int = 1;
const enum_unnamed_28 = c_uint;
pub const PTHREAD_MUTEX_TIMED_NP: c_int = 0;
pub const PTHREAD_MUTEX_RECURSIVE_NP: c_int = 1;
pub const PTHREAD_MUTEX_ERRORCHECK_NP: c_int = 2;
pub const PTHREAD_MUTEX_ADAPTIVE_NP: c_int = 3;
pub const PTHREAD_MUTEX_NORMAL: c_int = 0;
pub const PTHREAD_MUTEX_RECURSIVE: c_int = 1;
pub const PTHREAD_MUTEX_ERRORCHECK: c_int = 2;
pub const PTHREAD_MUTEX_DEFAULT: c_int = 0;
const enum_unnamed_29 = c_uint;
pub const PTHREAD_MUTEX_STALLED: c_int = 0;
pub const PTHREAD_MUTEX_STALLED_NP: c_int = 0;
pub const PTHREAD_MUTEX_ROBUST: c_int = 1;
pub const PTHREAD_MUTEX_ROBUST_NP: c_int = 1;
const enum_unnamed_30 = c_uint;
pub const PTHREAD_PRIO_NONE: c_int = 0;
pub const PTHREAD_PRIO_INHERIT: c_int = 1;
pub const PTHREAD_PRIO_PROTECT: c_int = 2;
const enum_unnamed_31 = c_uint;
pub const PTHREAD_RWLOCK_PREFER_READER_NP: c_int = 0;
pub const PTHREAD_RWLOCK_PREFER_WRITER_NP: c_int = 1;
pub const PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP: c_int = 2;
pub const PTHREAD_RWLOCK_DEFAULT_NP: c_int = 0;
const enum_unnamed_32 = c_uint;
pub const PTHREAD_INHERIT_SCHED: c_int = 0;
pub const PTHREAD_EXPLICIT_SCHED: c_int = 1;
const enum_unnamed_33 = c_uint;
pub const PTHREAD_SCOPE_SYSTEM: c_int = 0;
pub const PTHREAD_SCOPE_PROCESS: c_int = 1;
const enum_unnamed_34 = c_uint;
pub const PTHREAD_PROCESS_PRIVATE: c_int = 0;
pub const PTHREAD_PROCESS_SHARED: c_int = 1;
const enum_unnamed_35 = c_uint;
pub const struct__pthread_cleanup_buffer = extern struct {
    __routine: ?fn (?*anyopaque) callconv(.C) void,
    __arg: ?*anyopaque,
    __canceltype: c_int,
    __prev: [*c]struct__pthread_cleanup_buffer,
};
pub const PTHREAD_CANCEL_ENABLE: c_int = 0;
pub const PTHREAD_CANCEL_DISABLE: c_int = 1;
const enum_unnamed_36 = c_uint;
pub const PTHREAD_CANCEL_DEFERRED: c_int = 0;
pub const PTHREAD_CANCEL_ASYNCHRONOUS: c_int = 1;
const enum_unnamed_37 = c_uint;
pub extern fn pthread_create(noalias __newthread: [*c]pthread_t, noalias __attr: [*c]const pthread_attr_t, __start_routine: ?fn (?*anyopaque) callconv(.C) ?*anyopaque, noalias __arg: ?*anyopaque) c_int;
pub extern fn pthread_exit(__retval: ?*anyopaque) noreturn;
pub extern fn pthread_join(__th: pthread_t, __thread_return: [*c]?*anyopaque) c_int;
pub extern fn pthread_detach(__th: pthread_t) c_int;
pub extern fn pthread_self() pthread_t;
pub fn pthread_equal(arg___thread1: pthread_t, arg___thread2: pthread_t) callconv(.C) c_int {
    var __thread1 = arg___thread1;
    var __thread2 = arg___thread2;
    return @boolToInt(__thread1 == __thread2);
}
pub extern fn pthread_attr_init(__attr: [*c]pthread_attr_t) c_int;
pub extern fn pthread_attr_destroy(__attr: [*c]pthread_attr_t) c_int;
pub extern fn pthread_attr_getdetachstate(__attr: [*c]const pthread_attr_t, __detachstate: [*c]c_int) c_int;
pub extern fn pthread_attr_setdetachstate(__attr: [*c]pthread_attr_t, __detachstate: c_int) c_int;
pub extern fn pthread_attr_getguardsize(__attr: [*c]const pthread_attr_t, __guardsize: [*c]usize) c_int;
pub extern fn pthread_attr_setguardsize(__attr: [*c]pthread_attr_t, __guardsize: usize) c_int;
pub extern fn pthread_attr_getschedparam(noalias __attr: [*c]const pthread_attr_t, noalias __param: [*c]struct_sched_param) c_int;
pub extern fn pthread_attr_setschedparam(noalias __attr: [*c]pthread_attr_t, noalias __param: [*c]const struct_sched_param) c_int;
pub extern fn pthread_attr_getschedpolicy(noalias __attr: [*c]const pthread_attr_t, noalias __policy: [*c]c_int) c_int;
pub extern fn pthread_attr_setschedpolicy(__attr: [*c]pthread_attr_t, __policy: c_int) c_int;
pub extern fn pthread_attr_getinheritsched(noalias __attr: [*c]const pthread_attr_t, noalias __inherit: [*c]c_int) c_int;
pub extern fn pthread_attr_setinheritsched(__attr: [*c]pthread_attr_t, __inherit: c_int) c_int;
pub extern fn pthread_attr_getscope(noalias __attr: [*c]const pthread_attr_t, noalias __scope: [*c]c_int) c_int;
pub extern fn pthread_attr_setscope(__attr: [*c]pthread_attr_t, __scope: c_int) c_int;
pub extern fn pthread_attr_getstackaddr(noalias __attr: [*c]const pthread_attr_t, noalias __stackaddr: [*c]?*anyopaque) c_int;
pub extern fn pthread_attr_setstackaddr(__attr: [*c]pthread_attr_t, __stackaddr: ?*anyopaque) c_int;
pub extern fn pthread_attr_getstacksize(noalias __attr: [*c]const pthread_attr_t, noalias __stacksize: [*c]usize) c_int;
pub extern fn pthread_attr_setstacksize(__attr: [*c]pthread_attr_t, __stacksize: usize) c_int;
pub extern fn pthread_attr_getstack(noalias __attr: [*c]const pthread_attr_t, noalias __stackaddr: [*c]?*anyopaque, noalias __stacksize: [*c]usize) c_int;
pub extern fn pthread_attr_setstack(__attr: [*c]pthread_attr_t, __stackaddr: ?*anyopaque, __stacksize: usize) c_int;
pub extern fn pthread_setschedparam(__target_thread: pthread_t, __policy: c_int, __param: [*c]const struct_sched_param) c_int;
pub extern fn pthread_getschedparam(__target_thread: pthread_t, noalias __policy: [*c]c_int, noalias __param: [*c]struct_sched_param) c_int;
pub extern fn pthread_setschedprio(__target_thread: pthread_t, __prio: c_int) c_int;
pub extern fn pthread_once(__once_control: [*c]pthread_once_t, __init_routine: ?fn () callconv(.C) void) c_int;
pub extern fn pthread_setcancelstate(__state: c_int, __oldstate: [*c]c_int) c_int;
pub extern fn pthread_setcanceltype(__type: c_int, __oldtype: [*c]c_int) c_int;
pub extern fn pthread_cancel(__th: pthread_t) c_int;
pub extern fn pthread_testcancel() void;
pub const struct___cancel_jmp_buf_tag = extern struct {
    __cancel_jmp_buf: __jmp_buf,
    __mask_was_saved: c_int,
};
pub const __pthread_unwind_buf_t = extern struct {
    __cancel_jmp_buf: [1]struct___cancel_jmp_buf_tag,
    __pad: [4]?*anyopaque,
};
pub const struct___pthread_cleanup_frame = extern struct {
    __cancel_routine: ?fn (?*anyopaque) callconv(.C) void,
    __cancel_arg: ?*anyopaque,
    __do_it: c_int,
    __cancel_type: c_int,
};
pub extern fn __pthread_register_cancel(__buf: [*c]__pthread_unwind_buf_t) void;
pub extern fn __pthread_unregister_cancel(__buf: [*c]__pthread_unwind_buf_t) void;
pub extern fn __pthread_unwind_next(__buf: [*c]__pthread_unwind_buf_t) noreturn;
pub extern fn pthread_mutex_init(__mutex: [*c]pthread_mutex_t, __mutexattr: [*c]const pthread_mutexattr_t) c_int;
pub extern fn pthread_mutex_destroy(__mutex: [*c]pthread_mutex_t) c_int;
pub extern fn pthread_mutex_trylock(__mutex: [*c]pthread_mutex_t) c_int;
pub extern fn pthread_mutex_lock(__mutex: [*c]pthread_mutex_t) c_int;
pub extern fn pthread_mutex_timedlock(noalias __mutex: [*c]pthread_mutex_t, noalias __abstime: [*c]const struct_timespec) c_int;
pub extern fn pthread_mutex_unlock(__mutex: [*c]pthread_mutex_t) c_int;
pub extern fn pthread_mutex_getprioceiling(noalias __mutex: [*c]const pthread_mutex_t, noalias __prioceiling: [*c]c_int) c_int;
pub extern fn pthread_mutex_setprioceiling(noalias __mutex: [*c]pthread_mutex_t, __prioceiling: c_int, noalias __old_ceiling: [*c]c_int) c_int;
pub extern fn pthread_mutex_consistent(__mutex: [*c]pthread_mutex_t) c_int;
pub extern fn pthread_mutexattr_init(__attr: [*c]pthread_mutexattr_t) c_int;
pub extern fn pthread_mutexattr_destroy(__attr: [*c]pthread_mutexattr_t) c_int;
pub extern fn pthread_mutexattr_getpshared(noalias __attr: [*c]const pthread_mutexattr_t, noalias __pshared: [*c]c_int) c_int;
pub extern fn pthread_mutexattr_setpshared(__attr: [*c]pthread_mutexattr_t, __pshared: c_int) c_int;
pub extern fn pthread_mutexattr_gettype(noalias __attr: [*c]const pthread_mutexattr_t, noalias __kind: [*c]c_int) c_int;
pub extern fn pthread_mutexattr_settype(__attr: [*c]pthread_mutexattr_t, __kind: c_int) c_int;
pub extern fn pthread_mutexattr_getprotocol(noalias __attr: [*c]const pthread_mutexattr_t, noalias __protocol: [*c]c_int) c_int;
pub extern fn pthread_mutexattr_setprotocol(__attr: [*c]pthread_mutexattr_t, __protocol: c_int) c_int;
pub extern fn pthread_mutexattr_getprioceiling(noalias __attr: [*c]const pthread_mutexattr_t, noalias __prioceiling: [*c]c_int) c_int;
pub extern fn pthread_mutexattr_setprioceiling(__attr: [*c]pthread_mutexattr_t, __prioceiling: c_int) c_int;
pub extern fn pthread_mutexattr_getrobust(__attr: [*c]const pthread_mutexattr_t, __robustness: [*c]c_int) c_int;
pub extern fn pthread_mutexattr_setrobust(__attr: [*c]pthread_mutexattr_t, __robustness: c_int) c_int;
pub extern fn pthread_rwlock_init(noalias __rwlock: [*c]pthread_rwlock_t, noalias __attr: [*c]const pthread_rwlockattr_t) c_int;
pub extern fn pthread_rwlock_destroy(__rwlock: [*c]pthread_rwlock_t) c_int;
pub extern fn pthread_rwlock_rdlock(__rwlock: [*c]pthread_rwlock_t) c_int;
pub extern fn pthread_rwlock_tryrdlock(__rwlock: [*c]pthread_rwlock_t) c_int;
pub extern fn pthread_rwlock_timedrdlock(noalias __rwlock: [*c]pthread_rwlock_t, noalias __abstime: [*c]const struct_timespec) c_int;
pub extern fn pthread_rwlock_wrlock(__rwlock: [*c]pthread_rwlock_t) c_int;
pub extern fn pthread_rwlock_trywrlock(__rwlock: [*c]pthread_rwlock_t) c_int;
pub extern fn pthread_rwlock_timedwrlock(noalias __rwlock: [*c]pthread_rwlock_t, noalias __abstime: [*c]const struct_timespec) c_int;
pub extern fn pthread_rwlock_unlock(__rwlock: [*c]pthread_rwlock_t) c_int;
pub extern fn pthread_rwlockattr_init(__attr: [*c]pthread_rwlockattr_t) c_int;
pub extern fn pthread_rwlockattr_destroy(__attr: [*c]pthread_rwlockattr_t) c_int;
pub extern fn pthread_rwlockattr_getpshared(noalias __attr: [*c]const pthread_rwlockattr_t, noalias __pshared: [*c]c_int) c_int;
pub extern fn pthread_rwlockattr_setpshared(__attr: [*c]pthread_rwlockattr_t, __pshared: c_int) c_int;
pub extern fn pthread_rwlockattr_getkind_np(noalias __attr: [*c]const pthread_rwlockattr_t, noalias __pref: [*c]c_int) c_int;
pub extern fn pthread_rwlockattr_setkind_np(__attr: [*c]pthread_rwlockattr_t, __pref: c_int) c_int;
pub extern fn pthread_cond_init(noalias __cond: [*c]pthread_cond_t, noalias __cond_attr: [*c]const pthread_condattr_t) c_int;
pub extern fn pthread_cond_destroy(__cond: [*c]pthread_cond_t) c_int;
pub extern fn pthread_cond_signal(__cond: [*c]pthread_cond_t) c_int;
pub extern fn pthread_cond_broadcast(__cond: [*c]pthread_cond_t) c_int;
pub extern fn pthread_cond_wait(noalias __cond: [*c]pthread_cond_t, noalias __mutex: [*c]pthread_mutex_t) c_int;
pub extern fn pthread_cond_timedwait(noalias __cond: [*c]pthread_cond_t, noalias __mutex: [*c]pthread_mutex_t, noalias __abstime: [*c]const struct_timespec) c_int;
pub extern fn pthread_condattr_init(__attr: [*c]pthread_condattr_t) c_int;
pub extern fn pthread_condattr_destroy(__attr: [*c]pthread_condattr_t) c_int;
pub extern fn pthread_condattr_getpshared(noalias __attr: [*c]const pthread_condattr_t, noalias __pshared: [*c]c_int) c_int;
pub extern fn pthread_condattr_setpshared(__attr: [*c]pthread_condattr_t, __pshared: c_int) c_int;
pub extern fn pthread_condattr_getclock(noalias __attr: [*c]const pthread_condattr_t, noalias __clock_id: [*c]__clockid_t) c_int;
pub extern fn pthread_condattr_setclock(__attr: [*c]pthread_condattr_t, __clock_id: __clockid_t) c_int;
pub extern fn pthread_spin_init(__lock: [*c]volatile pthread_spinlock_t, __pshared: c_int) c_int;
pub extern fn pthread_spin_destroy(__lock: [*c]volatile pthread_spinlock_t) c_int;
pub extern fn pthread_spin_lock(__lock: [*c]volatile pthread_spinlock_t) c_int;
pub extern fn pthread_spin_trylock(__lock: [*c]volatile pthread_spinlock_t) c_int;
pub extern fn pthread_spin_unlock(__lock: [*c]volatile pthread_spinlock_t) c_int;
pub extern fn pthread_barrier_init(noalias __barrier: [*c]pthread_barrier_t, noalias __attr: [*c]const pthread_barrierattr_t, __count: c_uint) c_int;
pub extern fn pthread_barrier_destroy(__barrier: [*c]pthread_barrier_t) c_int;
pub extern fn pthread_barrier_wait(__barrier: [*c]pthread_barrier_t) c_int;
pub extern fn pthread_barrierattr_init(__attr: [*c]pthread_barrierattr_t) c_int;
pub extern fn pthread_barrierattr_destroy(__attr: [*c]pthread_barrierattr_t) c_int;
pub extern fn pthread_barrierattr_getpshared(noalias __attr: [*c]const pthread_barrierattr_t, noalias __pshared: [*c]c_int) c_int;
pub extern fn pthread_barrierattr_setpshared(__attr: [*c]pthread_barrierattr_t, __pshared: c_int) c_int;
pub extern fn pthread_key_create(__key: [*c]pthread_key_t, __destr_function: ?fn (?*anyopaque) callconv(.C) void) c_int;
pub extern fn pthread_key_delete(__key: pthread_key_t) c_int;
pub extern fn pthread_getspecific(__key: pthread_key_t) ?*anyopaque;
pub extern fn pthread_setspecific(__key: pthread_key_t, __pointer: ?*const anyopaque) c_int;
pub extern fn pthread_getcpuclockid(__thread_id: pthread_t, __clock_id: [*c]__clockid_t) c_int;
pub extern fn pthread_atfork(__prepare: ?fn () callconv(.C) void, __parent: ?fn () callconv(.C) void, __child: ?fn () callconv(.C) void) c_int;
pub extern var scm_i_pthread_mutexattr_recursive: [1]pthread_mutexattr_t;
pub extern var scm_tc16_thread: scm_t_bits;
pub extern var scm_tc16_mutex: scm_t_bits;
pub extern var scm_tc16_condvar: scm_t_bits;
pub extern fn scm_spawn_thread(body: scm_t_catch_body, body_data: ?*anyopaque, handler: scm_t_catch_handler, handler_data: ?*anyopaque) SCM;
pub extern fn scm_without_guile(func: ?fn (?*anyopaque) callconv(.C) ?*anyopaque, data: ?*anyopaque) ?*anyopaque;
pub extern fn scm_with_guile(func: ?fn (?*anyopaque) callconv(.C) ?*anyopaque, data: ?*anyopaque) ?*anyopaque;
pub extern fn scm_threads_prehistory(?*anyopaque) void;
pub extern fn scm_init_threads() void;
pub extern fn scm_init_threads_default_dynamic_state() void;
pub extern fn scm_i_dynwind_pthread_mutex_lock_block_asyncs(mutex: [*c]pthread_mutex_t) void;
pub extern fn scm_call_with_new_thread(thunk: SCM, handler: SCM) SCM;
pub extern fn scm_yield() SCM;
pub extern fn scm_cancel_thread(t: SCM) SCM;
pub extern fn scm_join_thread(t: SCM) SCM;
pub extern fn scm_join_thread_timed(t: SCM, timeout: SCM, timeoutval: SCM) SCM;
pub extern fn scm_thread_p(t: SCM) SCM;
pub extern fn scm_make_mutex() SCM;
pub extern fn scm_make_recursive_mutex() SCM;
pub extern fn scm_make_mutex_with_kind(kind: SCM) SCM;
pub extern fn scm_lock_mutex(m: SCM) SCM;
pub extern fn scm_timed_lock_mutex(m: SCM, timeout: SCM) SCM;
pub extern fn scm_dynwind_lock_mutex(mutex: SCM) void;
pub extern fn scm_try_mutex(m: SCM) SCM;
pub extern fn scm_unlock_mutex(m: SCM) SCM;
pub extern fn scm_mutex_p(o: SCM) SCM;
pub extern fn scm_mutex_locked_p(m: SCM) SCM;
pub extern fn scm_mutex_owner(m: SCM) SCM;
pub extern fn scm_mutex_level(m: SCM) SCM;
pub extern fn scm_make_condition_variable() SCM;
pub extern fn scm_wait_condition_variable(cond: SCM, mutex: SCM) SCM;
pub extern fn scm_timed_wait_condition_variable(cond: SCM, mutex: SCM, abstime: SCM) SCM;
pub extern fn scm_signal_condition_variable(cond: SCM) SCM;
pub extern fn scm_broadcast_condition_variable(cond: SCM) SCM;
pub extern fn scm_condition_variable_p(o: SCM) SCM;
pub extern fn scm_current_thread() SCM;
pub extern fn scm_all_threads() SCM;
pub extern fn scm_c_thread_exited_p(thread: SCM) c_int;
pub extern fn scm_thread_exited_p(thread: SCM) SCM;
pub extern var scm_i_misc_mutex: pthread_mutex_t;
pub extern fn scm_pthread_mutex_lock(mutex: [*c]pthread_mutex_t) c_int;
pub extern fn scm_dynwind_pthread_mutex_lock(mutex: [*c]pthread_mutex_t) void;
pub extern fn scm_pthread_cond_wait(cond: [*c]pthread_cond_t, mutex: [*c]pthread_mutex_t) c_int;
pub extern fn scm_pthread_cond_timedwait(cond: [*c]pthread_cond_t, mutex: [*c]pthread_mutex_t, abstime: [*c]const scm_t_timespec) c_int;
pub extern fn scm_std_sleep(c_uint) c_uint;
pub extern fn scm_std_usleep(c_ulong) c_ulong;
pub extern fn scm_total_processor_count() SCM;
pub extern fn scm_current_processor_count() SCM;
pub extern fn scm_async_tick() void;
pub extern fn scm_switch() void;
pub extern fn scm_system_async_mark(a: SCM) SCM;
pub extern fn scm_system_async_mark_for_thread(a: SCM, thread: SCM) SCM;
pub extern fn scm_c_prepare_to_wait_on_fd(fd: c_int) c_int;
pub extern fn scm_c_prepare_to_wait_on_cond(m: [*c]pthread_mutex_t, c: [*c]pthread_cond_t) c_int;
pub extern fn scm_c_wait_finished() void;
pub extern fn scm_noop(args: SCM) SCM;
pub extern fn scm_call_with_blocked_asyncs(proc: SCM) SCM;
pub extern fn scm_call_with_unblocked_asyncs(proc: SCM) SCM;
pub extern fn scm_c_call_with_blocked_asyncs(p: ?fn (?*anyopaque) callconv(.C) ?*anyopaque, d: ?*anyopaque) ?*anyopaque;
pub extern fn scm_c_call_with_unblocked_asyncs(p: ?fn (?*anyopaque) callconv(.C) ?*anyopaque, d: ?*anyopaque) ?*anyopaque;
pub extern fn scm_dynwind_block_asyncs() void;
pub extern fn scm_dynwind_unblock_asyncs() void;
pub extern fn scm_i_prepare_to_wait([*c]scm_thread, ?*struct_scm_thread_wake_data) c_int;
pub extern fn scm_i_wait_finished([*c]scm_thread) void;
pub extern fn scm_i_prepare_to_wait_on_fd([*c]scm_thread, c_int) c_int;
pub extern fn scm_i_prepare_to_wait_on_cond([*c]scm_thread, [*c]pthread_mutex_t, [*c]pthread_cond_t) c_int;
pub extern fn scm_i_async_push(t: [*c]scm_thread, proc: SCM) void;
pub extern fn scm_i_async_pop(t: [*c]scm_thread) SCM;
pub extern fn scm_init_async() void;
pub fn scm_is_atomic_box(arg_obj: SCM) callconv(.C) c_int {
    var obj = arg_obj;
    return @boolToInt(!((@bitCast(c_ulong, @as(c_long, @as(c_int, 6))) & @intCast(scm_t_bits, @ptrToInt(if (false) blk: {
        const tmp = obj;
        @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp;
        break :blk tmp;
    } else obj))) != 0) and ((@bitCast(c_ulong, @as(c_long, @as(c_int, 127))) & @intCast(scm_t_bits, @ptrToInt(if (false) blk: {
        const tmp = (blk_1: {
            const tmp_2 = @as(c_int, 0);
            if (tmp_2 >= 0) break :blk_1 @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_2: {
                const tmp_3 = obj;
                @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_3;
                break :blk_2 tmp_3;
            } else obj))))))) + @intCast(usize, tmp_2) else break :blk_1 @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_2: {
                const tmp_3 = obj;
                @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_3;
                break :blk_2 tmp_3;
            } else obj))))))) - ~@bitCast(usize, @intCast(isize, tmp_2) +% -1);
        }).*;
        @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp;
        break :blk tmp;
    } else (blk: {
        const tmp = @as(c_int, 0);
        if (tmp >= 0) break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = obj;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else obj))))))) + @intCast(usize, tmp) else break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = obj;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else obj))))))) - ~@bitCast(usize, @intCast(isize, tmp) +% -1);
    }).*))) == @bitCast(c_ulong, @as(c_long, @as(c_int, 55)))));
}
pub fn scm_atomic_box_loc(arg_obj: SCM) callconv(.C) [*c]SCM {
    var obj = arg_obj;
    return &(blk: {
        const tmp = @as(c_int, 1);
        if (tmp >= 0) break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = obj;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else obj))))))) + @intCast(usize, tmp) else break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = obj;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else obj))))))) - ~@bitCast(usize, @intCast(isize, tmp) +% -1);
    }).*;
}
pub extern fn scm_list_to_bitvector(list: SCM) SCM;
pub extern fn scm_bitvector_to_list(vec: SCM) SCM;
pub extern fn scm_bitvector_copy(vec: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_bitvector_position(v: SCM, item: SCM, start: SCM) SCM;
pub extern fn scm_is_bitvector(obj: SCM) c_int;
pub extern fn scm_c_make_bitvector(len: usize, fill: SCM) SCM;
pub extern fn scm_c_bitvector_length(vec: SCM) usize;
pub extern fn scm_c_bitvector_count(v: SCM) usize;
pub extern fn scm_c_bitvector_bit_is_set(vec: SCM, idx: usize) c_int;
pub extern fn scm_c_bitvector_bit_is_clear(vec: SCM, idx: usize) c_int;
pub extern fn scm_c_bitvector_set_bit_x(vec: SCM, idx: usize) void;
pub extern fn scm_c_bitvector_clear_bit_x(vec: SCM, idx: usize) void;
pub extern fn scm_c_bitvector_set_bits_x(v: SCM, bits: SCM) void;
pub extern fn scm_c_bitvector_clear_bits_x(v: SCM, bits: SCM) void;
pub extern fn scm_c_bitvector_set_all_bits_x(vec: SCM) void;
pub extern fn scm_c_bitvector_clear_all_bits_x(vec: SCM) void;
pub extern fn scm_c_bitvector_flip_all_bits_x(vec: SCM) void;
pub extern fn scm_c_bitvector_count_bits(v: SCM, bits: SCM) usize;
pub extern fn scm_array_handle_bit_elements(h: [*c]scm_t_array_handle) [*c]const u32;
pub extern fn scm_array_handle_bit_writable_elements(h: [*c]scm_t_array_handle) [*c]u32;
pub extern fn scm_array_handle_bit_elements_offset(h: [*c]scm_t_array_handle) usize;
pub extern fn scm_bitvector_elements(vec: SCM, h: [*c]scm_t_array_handle, offp: [*c]usize, lenp: [*c]usize, incp: [*c]isize) [*c]const u32;
pub extern fn scm_bitvector_writable_elements(vec: SCM, h: [*c]scm_t_array_handle, offp: [*c]usize, lenp: [*c]usize, incp: [*c]isize) [*c]u32;
pub extern fn scm_i_bitvector_bits(vec: SCM) [*c]u32;
pub extern fn scm_i_is_mutable_bitvector(vec: SCM) c_int;
pub extern fn scm_i_print_bitvector(vec: SCM, port: SCM, pstate: [*c]scm_print_state) c_int;
pub extern fn scm_i_bitvector_equal_p(vec1: SCM, vec2: SCM) SCM;
pub extern fn scm_init_bitvectors() void;
pub extern const scm_i_array_element_type_sizes: [*c]const usize;
pub extern fn scm_array_handle_uniform_element_size(h: [*c]scm_t_array_handle) usize;
pub extern fn scm_array_handle_uniform_element_bit_size(h: [*c]scm_t_array_handle) usize;
pub extern fn scm_array_handle_uniform_elements(h: [*c]scm_t_array_handle) ?*const anyopaque;
pub extern fn scm_array_handle_uniform_writable_elements(h: [*c]scm_t_array_handle) ?*anyopaque;
pub extern fn scm_init_uniform() void;
pub extern var scm_endianness_big: SCM;
pub extern var scm_endianness_little: SCM;
pub extern fn scm_c_make_bytevector(usize) SCM;
pub extern fn scm_is_bytevector(SCM) c_int;
pub extern fn scm_c_bytevector_length(SCM) usize;
pub extern fn scm_c_bytevector_ref(SCM, usize) u8;
pub extern fn scm_c_bytevector_set_x(SCM, usize, u8) void;
pub extern fn scm_make_bytevector(SCM, SCM) SCM;
pub extern fn scm_native_endianness() SCM;
pub extern fn scm_bytevector_p(SCM) SCM;
pub extern fn scm_bytevector_length(SCM) SCM;
pub extern fn scm_bytevector_eq_p(SCM, SCM) SCM;
pub extern fn scm_bytevector_fill_x(SCM, SCM) SCM;
pub extern fn scm_bytevector_copy_x(SCM, SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_copy(SCM) SCM;
pub extern fn scm_uniform_array_to_bytevector(SCM) SCM;
pub extern fn scm_bytevector_to_u8_list(SCM) SCM;
pub extern fn scm_u8_list_to_bytevector(SCM) SCM;
pub extern fn scm_uint_list_to_bytevector(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_to_uint_list(SCM, SCM, SCM) SCM;
pub extern fn scm_sint_list_to_bytevector(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_to_sint_list(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_u16_native_ref(SCM, SCM) SCM;
pub extern fn scm_bytevector_s16_native_ref(SCM, SCM) SCM;
pub extern fn scm_bytevector_u32_native_ref(SCM, SCM) SCM;
pub extern fn scm_bytevector_s32_native_ref(SCM, SCM) SCM;
pub extern fn scm_bytevector_u64_native_ref(SCM, SCM) SCM;
pub extern fn scm_bytevector_s64_native_ref(SCM, SCM) SCM;
pub extern fn scm_bytevector_u8_ref(SCM, SCM) SCM;
pub extern fn scm_bytevector_s8_ref(SCM, SCM) SCM;
pub extern fn scm_bytevector_uint_ref(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_sint_ref(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_u16_ref(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_s16_ref(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_u32_ref(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_s32_ref(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_u64_ref(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_s64_ref(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_u16_native_set_x(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_s16_native_set_x(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_u32_native_set_x(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_s32_native_set_x(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_u64_native_set_x(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_s64_native_set_x(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_u8_set_x(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_s8_set_x(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_uint_set_x(SCM, SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_sint_set_x(SCM, SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_u16_set_x(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_s16_set_x(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_u32_set_x(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_s32_set_x(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_u64_set_x(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_s64_set_x(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_ieee_single_ref(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_ieee_single_native_ref(SCM, SCM) SCM;
pub extern fn scm_bytevector_ieee_single_set_x(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_ieee_single_native_set_x(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_ieee_double_ref(SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_ieee_double_native_ref(SCM, SCM) SCM;
pub extern fn scm_bytevector_ieee_double_set_x(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_bytevector_ieee_double_native_set_x(SCM, SCM, SCM) SCM;
pub extern fn scm_string_to_utf8(SCM) SCM;
pub extern fn scm_string_to_utf16(SCM, SCM) SCM;
pub extern fn scm_string_to_utf32(SCM, SCM) SCM;
pub extern fn scm_utf8_to_string(SCM) SCM;
pub extern fn scm_utf16_to_string(SCM, SCM) SCM;
pub extern fn scm_utf32_to_string(SCM, SCM) SCM;
pub extern fn scm_i_make_typed_bytevector(usize, scm_t_array_element_type) SCM;
pub extern fn scm_c_take_typed_bytevector([*c]i8, usize, scm_t_array_element_type, SCM) SCM;
pub extern fn scm_bootstrap_bytevectors() void;
pub extern fn scm_init_bytevectors() void;
pub extern var scm_i_native_endianness: SCM;
pub extern fn scm_c_take_gc_bytevector([*c]i8, usize, SCM) SCM;
pub extern fn scm_i_print_bytevector(SCM, SCM, [*c]scm_print_state) c_int;
pub extern fn scm_c_shrink_bytevector(SCM, usize) SCM;
pub extern fn scm_i_bytevector_generalized_set_x(SCM, usize, SCM) void;
pub extern var scm_null_bytevector: SCM;
pub const scm_t_contregs = extern struct {
    jmpbuf: jmp_buf,
    num_stack_items: usize,
    root: SCM,
    vm_cont: SCM,
    offset: ptrdiff_t,
    stack: [1]SCM_STACKITEM,
};
pub extern fn scm_i_make_continuation(thread: [*c]scm_thread, vm_cont: SCM) SCM;
pub extern fn scm_i_reinstate_continuation(cont: SCM, mra: [*c]u8) noreturn;
pub extern fn scm_i_continuation_to_frame(cont: SCM, frame: ?*struct_scm_frame) c_int;
pub extern fn scm_i_contregs(contregs: SCM) [*c]scm_t_contregs;
pub extern fn scm_c_with_continuation_barrier(func: ?fn (?*anyopaque) callconv(.C) ?*anyopaque, ?*anyopaque) ?*anyopaque;
pub extern fn scm_with_continuation_barrier(proc: SCM) SCM;
pub extern fn scm_i_with_continuation_barrier(body: scm_t_catch_body, body_data: ?*anyopaque, handler: scm_t_catch_handler, handler_data: ?*anyopaque, pre_unwind_handler: scm_t_catch_handler, pre_unwind_handler_data: ?*anyopaque) SCM;
pub extern fn scm_init_continuations() void;
pub extern fn scm_dynamic_link(fname: SCM) SCM;
pub extern fn scm_dynamic_object_p(obj: SCM) SCM;
pub extern fn scm_dynamic_pointer(name: SCM, obj: SCM) SCM;
pub extern fn scm_dynamic_func(name: SCM, obj: SCM) SCM;
pub extern fn scm_dynamic_call(name: SCM, obj: SCM) SCM;
pub extern fn scm_init_dynamic_linking() void;
pub extern fn scm_dynamic_wind(thunk1: SCM, thunk2: SCM, thunk3: SCM) SCM;
pub extern fn scm_init_dynwind() void;
pub extern fn scm_swap_bindings(vars: SCM, vals: SCM) void;
pub const SCM_F_DYNWIND_REWINDABLE: c_int = 16;
pub const scm_t_dynwind_flags = c_uint;
pub const SCM_F_WIND_EXPLICITLY: c_int = 16;
pub const scm_t_wind_flags = c_uint;
pub extern fn scm_dynwind_begin(scm_t_dynwind_flags) void;
pub extern fn scm_dynwind_end() void;
pub extern fn scm_dynwind_unwind_handler(func: ?fn (?*anyopaque) callconv(.C) void, data: ?*anyopaque, scm_t_wind_flags) void;
pub extern fn scm_dynwind_rewind_handler(func: ?fn (?*anyopaque) callconv(.C) void, data: ?*anyopaque, scm_t_wind_flags) void;
pub extern fn scm_dynwind_unwind_handler_with_scm(func: ?fn (SCM) callconv(.C) void, data: SCM, scm_t_wind_flags) void;
pub extern fn scm_dynwind_rewind_handler_with_scm(func: ?fn (SCM) callconv(.C) void, data: SCM, scm_t_wind_flags) void;
pub extern fn scm_dynwind_free(mem: ?*anyopaque) void;
pub extern fn scm_eq_p(x: SCM, y: SCM) SCM;
pub extern fn scm_eqv_p(x: SCM, y: SCM) SCM;
pub extern fn scm_equal_p(x: SCM, y: SCM) SCM;
pub extern fn scm_init_eq() void;
pub const scm_t_struct_finalize = ?fn (SCM) callconv(.C) void;
pub extern var scm_standard_vtable_vtable: SCM;
pub extern var scm_applicable_struct_vtable_vtable: SCM;
pub extern var scm_applicable_struct_with_setter_vtable_vtable: SCM;
pub extern fn scm_make_struct_layout(fields: SCM) SCM;
pub extern fn scm_struct_p(x: SCM) SCM;
pub extern fn scm_struct_vtable_p(x: SCM) SCM;
pub extern fn scm_allocate_struct(vtable: SCM, n_words: SCM) SCM;
pub extern fn scm_make_struct_simple(vtable: SCM, init: SCM) SCM;
pub extern fn scm_make_struct_no_tail(vtable: SCM, init: SCM) SCM;
pub extern fn scm_c_make_struct(vtable: SCM, n_tail: usize, n_inits: usize, init: scm_t_bits, ...) SCM;
pub extern fn scm_c_make_structv(vtable: SCM, n_tail: usize, n_inits: usize, init: [*c]scm_t_bits) SCM;
pub extern fn scm_make_vtable(fields: SCM, printer: SCM) SCM;
pub extern fn scm_i_make_vtable_vtable(fields: SCM) SCM;
pub extern fn scm_struct_ref(handle: SCM, pos: SCM) SCM;
pub extern fn scm_struct_set_x(handle: SCM, pos: SCM, val: SCM) SCM;
pub extern fn scm_struct_ref_unboxed(handle: SCM, pos: SCM) SCM;
pub extern fn scm_struct_set_x_unboxed(handle: SCM, pos: SCM, val: SCM) SCM;
pub extern fn scm_struct_vtable(handle: SCM) SCM;
pub extern fn scm_struct_vtable_name(vtable: SCM) SCM;
pub extern fn scm_set_struct_vtable_name_x(vtable: SCM, name: SCM) SCM;
pub extern fn scm_print_struct(exp: SCM, port: SCM, [*c]scm_print_state) void;
pub extern fn scm_i_struct_equalp(s1: SCM, s2: SCM) SCM;
pub extern fn scm_struct_ihashq(SCM, c_ulong, ?*anyopaque) c_ulong;
pub extern fn scm_i_struct_inherit_vtable_magic(vtable: SCM, obj: SCM) void;
pub extern fn scm_init_struct() void;
pub extern var scm_sym_and: SCM;
pub extern var scm_sym_begin: SCM;
pub extern var scm_sym_case: SCM;
pub extern var scm_sym_cond: SCM;
pub extern var scm_sym_define: SCM;
pub extern var scm_sym_do: SCM;
pub extern var scm_sym_if: SCM;
pub extern var scm_sym_lambda: SCM;
pub extern var scm_sym_let: SCM;
pub extern var scm_sym_letstar: SCM;
pub extern var scm_sym_letrec: SCM;
pub extern var scm_sym_quote: SCM;
pub extern var scm_sym_quasiquote: SCM;
pub extern var scm_sym_unquote: SCM;
pub extern var scm_sym_uq_splicing: SCM;
pub extern var scm_sym_at: SCM;
pub extern var scm_sym_atat: SCM;
pub extern var scm_sym_delay: SCM;
pub extern var scm_sym_eval_when: SCM;
pub extern var scm_sym_arrow: SCM;
pub extern var scm_sym_else: SCM;
pub extern var scm_sym_apply: SCM;
pub extern var scm_sym_set_x: SCM;
pub extern var scm_sym_args: SCM;
pub const SCM_M_SEQ: c_int = 0;
pub const SCM_M_IF: c_int = 1;
pub const SCM_M_LAMBDA: c_int = 2;
pub const SCM_M_CAPTURE_ENV: c_int = 3;
pub const SCM_M_LET: c_int = 4;
pub const SCM_M_QUOTE: c_int = 5;
pub const SCM_M_CAPTURE_MODULE: c_int = 6;
pub const SCM_M_APPLY: c_int = 7;
pub const SCM_M_CONT: c_int = 8;
pub const SCM_M_CALL_WITH_VALUES: c_int = 9;
pub const SCM_M_CALL: c_int = 10;
pub const SCM_M_LEXICAL_REF: c_int = 11;
pub const SCM_M_LEXICAL_SET: c_int = 12;
pub const SCM_M_BOX_REF: c_int = 13;
pub const SCM_M_BOX_SET: c_int = 14;
pub const SCM_M_RESOLVE: c_int = 15;
pub const SCM_M_CALL_WITH_PROMPT: c_int = 16;
const enum_unnamed_38 = c_uint;
pub extern fn scm_memoize_expression(exp: SCM) SCM;
pub extern fn scm_unmemoize_expression(memoized: SCM) SCM;
pub extern fn scm_memoized_typecode(sym: SCM) SCM;
pub extern fn scm_sys_resolve_variable(loc: SCM, module: SCM) SCM;
pub extern fn scm_init_memoize() void;
pub const scm_t_trampoline_0 = ?fn (SCM) callconv(.C) SCM;
pub const scm_t_trampoline_1 = ?fn (SCM, SCM) callconv(.C) SCM;
pub const scm_t_trampoline_2 = ?fn (SCM, SCM, SCM) callconv(.C) SCM;
pub extern fn scm_call_0(proc: SCM) SCM;
pub extern fn scm_call_1(proc: SCM, arg1: SCM) SCM;
pub extern fn scm_call_2(proc: SCM, arg1: SCM, arg2: SCM) SCM;
pub extern fn scm_call_3(proc: SCM, arg1: SCM, arg2: SCM, arg3: SCM) SCM;
pub extern fn scm_call_4(proc: SCM, arg1: SCM, arg2: SCM, arg3: SCM, arg4: SCM) SCM;
pub extern fn scm_call_5(proc: SCM, arg1: SCM, arg2: SCM, arg3: SCM, arg4: SCM, arg5: SCM) SCM;
pub extern fn scm_call_6(proc: SCM, arg1: SCM, arg2: SCM, arg3: SCM, arg4: SCM, arg5: SCM, arg6: SCM) SCM;
pub extern fn scm_call_7(proc: SCM, arg1: SCM, arg2: SCM, arg3: SCM, arg4: SCM, arg5: SCM, arg6: SCM, arg7: SCM) SCM;
pub extern fn scm_call_8(proc: SCM, arg1: SCM, arg2: SCM, arg3: SCM, arg4: SCM, arg5: SCM, arg6: SCM, arg7: SCM, arg8: SCM) SCM;
pub extern fn scm_call_9(proc: SCM, arg1: SCM, arg2: SCM, arg3: SCM, arg4: SCM, arg5: SCM, arg6: SCM, arg7: SCM, arg8: SCM, arg9: SCM) SCM;
pub extern fn scm_call_n(proc: SCM, argv: [*c]SCM, nargs: usize) SCM;
pub extern fn scm_call(proc: SCM, ...) SCM;
pub extern fn scm_apply_0(proc: SCM, args: SCM) SCM;
pub extern fn scm_apply_1(proc: SCM, arg1: SCM, args: SCM) SCM;
pub extern fn scm_apply_2(proc: SCM, arg1: SCM, arg2: SCM, args: SCM) SCM;
pub extern fn scm_apply_3(proc: SCM, arg1: SCM, arg2: SCM, arg3: SCM, args: SCM) SCM;
pub extern fn scm_nconc2last(lst: SCM) SCM;
pub extern fn scm_apply(proc: SCM, arg1: SCM, args: SCM) SCM;
pub extern fn scm_map(proc: SCM, arg1: SCM, args: SCM) SCM;
pub extern fn scm_for_each(proc: SCM, arg1: SCM, args: SCM) SCM;
pub extern fn scm_primitive_eval(exp: SCM) SCM;
pub extern fn scm_eval(exp: SCM, module: SCM) SCM;
pub extern fn scm_init_eval() void;
pub extern fn scm_defined_p(sym: SCM, env: SCM) SCM;
pub extern fn scm_self_evaluating_p(obj: SCM) SCM;
pub extern fn scm_init_evalext() void;
pub const scm_t_extension_init_func = ?fn (?*anyopaque) callconv(.C) void;
pub extern fn scm_c_register_extension(lib: [*c]const u8, init: [*c]const u8, func: ?fn (?*anyopaque) callconv(.C) void, data: ?*anyopaque) void;
pub extern fn scm_c_load_extension(lib: [*c]const u8, init: [*c]const u8) void;
pub extern fn scm_load_extension(lib: SCM, init: SCM) SCM;
pub extern fn scm_init_extensions() void;
pub extern fn scm_add_fdes_finalizer_x(fd: SCM, finalizer: SCM) SCM;
pub extern fn scm_remove_fdes_finalizer_x(fd: SCM, finalizer: SCM) SCM;
pub extern fn scm_run_fdes_finalizers(fd: c_int) void;
pub extern fn scm_register_fdes_finalizers() void;
pub extern fn scm_add_feature(str: [*c]const u8) void;
pub extern fn scm_program_arguments() SCM;
pub extern fn scm_set_program_arguments(argc: c_int, argv: [*c][*c]u8, first: [*c]u8) void;
pub extern fn scm_set_program_arguments_scm(lst: SCM) SCM;
pub extern var scm_program_arguments_fluid: SCM;
pub extern fn scm_init_feature() void;
pub extern var scm_tc16_dir: scm_t_bits;
pub extern fn scm_chown(object: SCM, owner: SCM, group: SCM) SCM;
pub extern fn scm_chmod(object: SCM, mode: SCM) SCM;
pub extern fn scm_umask(mode: SCM) SCM;
pub extern fn scm_open_fdes(path: SCM, flags: SCM, mode: SCM) SCM;
pub extern fn scm_open(path: SCM, flags: SCM, mode: SCM) SCM;
pub extern fn scm_close(fd_or_port: SCM) SCM;
pub extern fn scm_close_fdes(fd: SCM) SCM;
pub extern fn scm_stat(object: SCM, exception_on_error: SCM) SCM;
pub extern fn scm_link(oldpath: SCM, newpath: SCM) SCM;
pub extern fn scm_rename(oldname: SCM, newname: SCM) SCM;
pub extern fn scm_delete_file(str: SCM) SCM;
pub extern fn scm_mkdir(path: SCM, mode: SCM) SCM;
pub extern fn scm_rmdir(path: SCM) SCM;
pub extern fn scm_directory_stream_p(obj: SCM) SCM;
pub extern fn scm_opendir(dirname: SCM) SCM;
pub extern fn scm_readdir(port: SCM) SCM;
pub extern fn scm_rewinddir(port: SCM) SCM;
pub extern fn scm_closedir(port: SCM) SCM;
pub extern fn scm_chdir(str: SCM) SCM;
pub extern fn scm_getcwd() SCM;
pub extern fn scm_select(reads: SCM, writes: SCM, excepts: SCM, secs: SCM, msecs: SCM) SCM;
pub extern fn scm_fcntl(object: SCM, cmd: SCM, value: SCM) SCM;
pub extern fn scm_fsync(object: SCM) SCM;
pub extern fn scm_symlink(oldpath: SCM, newpath: SCM) SCM;
pub extern fn scm_readlink(path: SCM) SCM;
pub extern fn scm_lstat(str: SCM) SCM;
pub extern fn scm_copy_file(oldfile: SCM, newfile: SCM) SCM;
pub extern fn scm_mkstemp(tmpl: SCM) SCM;
pub extern fn scm_mkdtemp(tmpl: SCM) SCM;
pub extern fn scm_dirname(filename: SCM) SCM;
pub extern fn scm_basename(filename: SCM, suffix: SCM) SCM;
pub extern fn scm_canonicalize_path(path: SCM) SCM;
pub extern fn scm_sendfile(out: SCM, in: SCM, count: SCM, offset: SCM) SCM;
pub extern fn scm_i_relativize_path(path: SCM, in_path: SCM) SCM;
pub extern fn scm_init_filesys() void;
pub const scm_t_finalizer_proc = ?fn (?*anyopaque, ?*anyopaque) callconv(.C) void;
pub extern fn scm_i_set_finalizer(obj: ?*anyopaque, scm_t_finalizer_proc, data: ?*anyopaque) void;
pub extern fn scm_i_add_finalizer(obj: ?*anyopaque, scm_t_finalizer_proc, data: ?*anyopaque) void;
pub extern fn scm_i_add_resuscitator(obj: ?*anyopaque, scm_t_finalizer_proc, data: ?*anyopaque) void;
pub extern fn scm_i_finalizer_pre_fork() void;
pub extern fn scm_i_register_async_gc_callback(callback: ?fn () callconv(.C) void) void;
pub extern fn scm_set_automatic_finalization_enabled(enabled_p: c_int) c_int;
pub extern fn scm_run_finalizers() c_int;
pub extern fn scm_init_finalizers() void;
pub extern fn scm_init_finalizer_thread() void;
pub extern fn scm_vector_p(x: SCM) SCM;
pub extern fn scm_vector_length(v: SCM) SCM;
pub extern fn scm_vector(l: SCM) SCM;
pub extern fn scm_vector_ref(v: SCM, k: SCM) SCM;
pub extern fn scm_vector_set_x(v: SCM, k: SCM, obj: SCM) SCM;
pub extern fn scm_make_vector(k: SCM, fill: SCM) SCM;
pub extern fn scm_vector_to_list(v: SCM) SCM;
pub extern fn scm_vector_fill_x(v: SCM, fill_x: SCM) SCM;
pub extern fn scm_vector_move_left_x(vec1: SCM, start1: SCM, end1: SCM, vec2: SCM, start2: SCM) SCM;
pub extern fn scm_vector_move_right_x(vec1: SCM, start1: SCM, end1: SCM, vec2: SCM, start2: SCM) SCM;
pub extern fn scm_vector_copy(vec: SCM) SCM;
pub extern fn scm_vector_copy_partial(vec: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_vector_copy_x(dst: SCM, at: SCM, src: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_is_vector(obj: SCM) c_int;
pub extern fn scm_c_make_vector(len: usize, fill: SCM) SCM;
pub extern fn scm_c_vector_length(vec: SCM) usize;
pub extern fn scm_c_vector_ref(vec: SCM, k: usize) SCM;
pub extern fn scm_c_vector_set_x(vec: SCM, k: usize, obj: SCM) void;
pub extern fn scm_vector_elements(array: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const SCM;
pub extern fn scm_vector_writable_elements(array: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]SCM;
pub extern fn scm_i_vector_equal_p(x: SCM, y: SCM) SCM;
pub extern fn scm_init_vectors() void;
pub extern fn scm_make_fluid() SCM;
pub extern fn scm_make_fluid_with_default(dflt: SCM) SCM;
pub extern fn scm_make_unbound_fluid() SCM;
pub extern fn scm_make_thread_local_fluid(dflt: SCM) SCM;
pub extern fn scm_is_fluid(obj: SCM) c_int;
pub extern fn scm_fluid_p(fl: SCM) SCM;
pub extern fn scm_fluid_thread_local_p(fluid: SCM) SCM;
pub extern fn scm_fluid_ref(fluid: SCM) SCM;
pub extern fn scm_fluid_ref_star(fluid: SCM, depth: SCM) SCM;
pub extern fn scm_fluid_set_x(fluid: SCM, value: SCM) SCM;
pub extern fn scm_fluid_unset_x(fluid: SCM) SCM;
pub extern fn scm_fluid_bound_p(fluid: SCM) SCM;
pub extern fn scm_i_fluid_ref(thread: [*c]scm_thread, fluid: SCM) SCM;
pub extern fn scm_swap_fluid(fluid: SCM, value_box: SCM, dynamic_state: ?*scm_t_dynamic_state) void;
pub extern fn scm_c_with_fluids(fluids: SCM, vals: SCM, cproc: ?fn (?*anyopaque) callconv(.C) SCM, cdata: ?*anyopaque) SCM;
pub extern fn scm_c_with_fluid(fluid: SCM, val: SCM, cproc: ?fn (?*anyopaque) callconv(.C) SCM, cdata: ?*anyopaque) SCM;
pub extern fn scm_with_fluids(fluids: SCM, vals: SCM, thunk: SCM) SCM;
pub extern fn scm_with_fluid(fluid: SCM, val: SCM, thunk: SCM) SCM;
pub extern fn scm_dynwind_fluid(fluid: SCM, value: SCM) void;
pub extern fn scm_dynamic_state_p(obj: SCM) SCM;
pub extern fn scm_is_dynamic_state(obj: SCM) c_int;
pub extern fn scm_current_dynamic_state() SCM;
pub extern fn scm_set_current_dynamic_state(state: SCM) SCM;
pub extern fn scm_dynwind_current_dynamic_state(state: SCM) void;
pub extern fn scm_c_with_dynamic_state(state: SCM, func: ?fn (?*anyopaque) callconv(.C) ?*anyopaque, data: ?*anyopaque) ?*anyopaque;
pub extern fn scm_with_dynamic_state(state: SCM, proc: SCM) SCM;
pub extern fn scm_dynamic_state_ref(state: SCM, fluid: SCM, dflt: SCM) SCM;
pub extern fn scm_i_make_initial_dynamic_state() SCM;
pub extern fn scm_i_fluid_print(exp: SCM, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_i_dynamic_state_print(exp: SCM, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_init_fluids() void;
pub const SCM_FOREIGN_TYPE_VOID: c_int = 0;
pub const SCM_FOREIGN_TYPE_FLOAT: c_int = 1;
pub const SCM_FOREIGN_TYPE_DOUBLE: c_int = 2;
pub const SCM_FOREIGN_TYPE_UINT8: c_int = 3;
pub const SCM_FOREIGN_TYPE_INT8: c_int = 4;
pub const SCM_FOREIGN_TYPE_UINT16: c_int = 5;
pub const SCM_FOREIGN_TYPE_INT16: c_int = 6;
pub const SCM_FOREIGN_TYPE_UINT32: c_int = 7;
pub const SCM_FOREIGN_TYPE_INT32: c_int = 8;
pub const SCM_FOREIGN_TYPE_UINT64: c_int = 9;
pub const SCM_FOREIGN_TYPE_INT64: c_int = 10;
pub const SCM_FOREIGN_TYPE_LAST: c_int = 10;
pub const enum_scm_t_foreign_type = c_uint;
pub const scm_t_foreign_type = enum_scm_t_foreign_type;
pub const scm_t_pointer_finalizer = ?fn (?*anyopaque) callconv(.C) void;
pub extern fn scm_to_pointer(pointer: SCM) ?*anyopaque;
pub extern fn scm_from_pointer(?*anyopaque, scm_t_pointer_finalizer) SCM;
pub extern fn scm_alignof(@"type": SCM) SCM;
pub extern fn scm_sizeof(@"type": SCM) SCM;
pub extern fn scm_pointer_address(pointer: SCM) SCM;
pub extern fn scm_pointer_to_scm(pointer: SCM) SCM;
pub extern fn scm_scm_to_pointer(scm: SCM) SCM;
pub extern fn scm_pointer_to_bytevector(pointer: SCM, @"type": SCM, offset: SCM, len: SCM) SCM;
pub extern fn scm_set_pointer_finalizer_x(pointer: SCM, finalizer: SCM) SCM;
pub extern fn scm_bytevector_to_pointer(bv: SCM, offset: SCM) SCM;
pub extern fn scm_pointer_p(obj: SCM) SCM;
pub extern fn scm_make_pointer(address: SCM, finalizer: SCM) SCM;
pub extern fn scm_i_pointer_print(pointer: SCM, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_dereference_pointer(pointer: SCM) SCM;
pub extern fn scm_string_to_pointer(string: SCM, encoding: SCM) SCM;
pub extern fn scm_pointer_to_string(pointer: SCM, length: SCM, encoding: SCM) SCM;
pub extern fn scm_pointer_to_procedure(return_type: SCM, func_ptr: SCM, arg_types: SCM) SCM;
pub extern fn scm_pointer_to_procedure_with_errno(return_type: SCM, func_ptr: SCM, arg_types: SCM) SCM;
pub extern fn scm_procedure_to_pointer(return_type: SCM, func_ptr: SCM, arg_types: SCM) SCM;
pub extern fn scm_i_foreign_call(cif_scm: SCM, pointer_scm: SCM, errno_ret: [*c]c_int, argv: [*c]const union_scm_vm_stack_element) SCM;
pub extern fn scm_register_foreign() void;
pub extern fn scm_make_foreign_object_type(name: SCM, slot_names: SCM, finalizer: scm_t_struct_finalize) SCM;
pub extern fn scm_assert_foreign_object_type(@"type": SCM, val: SCM) void;
pub extern fn scm_make_foreign_object_0(@"type": SCM) SCM;
pub extern fn scm_make_foreign_object_1(@"type": SCM, val0: ?*anyopaque) SCM;
pub extern fn scm_make_foreign_object_2(@"type": SCM, val0: ?*anyopaque, val1: ?*anyopaque) SCM;
pub extern fn scm_make_foreign_object_3(@"type": SCM, val0: ?*anyopaque, val1: ?*anyopaque, val2: ?*anyopaque) SCM;
pub extern fn scm_make_foreign_object_n(@"type": SCM, n: usize, vals: [*c]?*anyopaque) SCM;
pub extern fn scm_foreign_object_ref(obj: SCM, n: usize) ?*anyopaque;
pub extern fn scm_foreign_object_set_x(obj: SCM, n: usize, val: ?*anyopaque) void;
pub extern fn scm_foreign_object_unsigned_ref(obj: SCM, n: usize) scm_t_bits;
pub extern fn scm_foreign_object_unsigned_set_x(obj: SCM, n: usize, val: scm_t_bits) void;
pub extern fn scm_foreign_object_signed_ref(obj: SCM, n: usize) scm_t_signed_bits;
pub extern fn scm_foreign_object_signed_set_x(obj: SCM, n: usize, val: scm_t_signed_bits) void;
pub extern fn scm_register_foreign_object() void;
pub const SCM_FAILED_CONVERSION_ERROR: c_int = 0;
pub const SCM_FAILED_CONVERSION_QUESTION_MARK: c_int = 1;
pub const SCM_FAILED_CONVERSION_ESCAPE_SEQUENCE: c_int = 2;
pub const scm_t_string_failed_conversion_handler = c_uint;
pub extern var scm_nullstr: SCM;
pub extern fn scm_i_default_string_failed_conversion_handler() scm_t_string_failed_conversion_handler;
pub fn scm_is_string(arg_x: SCM) callconv(.C) c_int {
    var x = arg_x;
    return @boolToInt(!((@bitCast(c_ulong, @as(c_long, @as(c_int, 6))) & @intCast(scm_t_bits, @ptrToInt(if (false) blk: {
        const tmp = x;
        @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp;
        break :blk tmp;
    } else x))) != 0) and ((@bitCast(c_ulong, @as(c_long, @as(c_int, 127))) & @intCast(scm_t_bits, @ptrToInt(if (false) blk: {
        const tmp = (blk_1: {
            const tmp_2 = @as(c_int, 0);
            if (tmp_2 >= 0) break :blk_1 @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_2: {
                const tmp_3 = x;
                @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_3;
                break :blk_2 tmp_3;
            } else x))))))) + @intCast(usize, tmp_2) else break :blk_1 @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_2: {
                const tmp_3 = x;
                @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_3;
                break :blk_2 tmp_3;
            } else x))))))) - ~@bitCast(usize, @intCast(isize, tmp_2) +% -1);
        }).*;
        @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp;
        break :blk tmp;
    } else (blk: {
        const tmp = @as(c_int, 0);
        if (tmp >= 0) break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = x;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else x))))))) + @intCast(usize, tmp) else break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = x;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else x))))))) - ~@bitCast(usize, @intCast(isize, tmp) +% -1);
    }).*))) == @bitCast(c_ulong, @as(c_long, @as(c_int, 21)))));
}
pub extern fn scm_string_p(x: SCM) SCM;
pub extern fn scm_string(chrs: SCM) SCM;
pub extern fn scm_make_string(k: SCM, chr: SCM) SCM;
pub extern fn scm_string_length(str: SCM) SCM;
pub extern fn scm_string_utf8_length(str: SCM) SCM;
pub extern fn scm_string_bytes_per_char(str: SCM) SCM;
pub extern fn scm_string_ref(str: SCM, k: SCM) SCM;
pub extern fn scm_string_set_x(str: SCM, k: SCM, chr: SCM) SCM;
pub extern fn scm_substring(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_substring_read_only(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_substring_shared(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_substring_copy(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_append(args: SCM) SCM;
pub extern fn scm_from_stringn(str: [*c]const u8, len: usize, encoding: [*c]const u8, handler: scm_t_string_failed_conversion_handler) SCM;
pub extern fn scm_c_make_string(len: usize, chr: SCM) SCM;
pub extern fn scm_c_string_length(str: SCM) usize;
pub extern fn scm_c_string_utf8_length(str: SCM) usize;
pub extern fn scm_c_symbol_length(sym: SCM) usize;
pub extern fn scm_c_string_ref(str: SCM, pos: usize) SCM;
pub extern fn scm_c_string_set_x(str: SCM, pos: usize, chr: SCM) void;
pub extern fn scm_c_substring(str: SCM, start: usize, end: usize) SCM;
pub extern fn scm_c_substring_read_only(str: SCM, start: usize, end: usize) SCM;
pub extern fn scm_c_substring_shared(str: SCM, start: usize, end: usize) SCM;
pub extern fn scm_c_substring_copy(str: SCM, start: usize, end: usize) SCM;
pub extern fn scm_from_locale_string(str: [*c]const u8) SCM;
pub extern fn scm_from_locale_stringn(str: [*c]const u8, len: usize) SCM;
pub extern fn scm_take_locale_string(str: [*c]u8) SCM;
pub extern fn scm_take_locale_stringn(str: [*c]u8, len: usize) SCM;
pub extern fn scm_to_locale_string(str: SCM) [*c]u8;
pub extern fn scm_to_locale_stringn(str: SCM, lenp: [*c]usize) [*c]u8;
pub extern fn scm_from_latin1_string(str: [*c]const u8) SCM;
pub extern fn scm_from_latin1_stringn(str: [*c]const u8, len: usize) SCM;
pub extern fn scm_to_latin1_string(str: SCM) [*c]u8;
pub extern fn scm_to_latin1_stringn(str: SCM, lenp: [*c]usize) [*c]u8;
pub extern fn scm_to_utf8_string(str: SCM) [*c]u8;
pub extern fn scm_to_utf8_stringn(str: SCM, lenp: [*c]usize) [*c]u8;
pub extern fn scm_from_utf8_string(str: [*c]const u8) SCM;
pub extern fn scm_from_utf8_stringn(str: [*c]const u8, len: usize) SCM;
pub extern fn scm_to_utf32_string(str: SCM) [*c]scm_t_wchar;
pub extern fn scm_to_utf32_stringn(str: SCM, lenp: [*c]usize) [*c]scm_t_wchar;
pub extern fn scm_from_utf32_string(str: [*c]const scm_t_wchar) SCM;
pub extern fn scm_from_utf32_stringn(str: [*c]const scm_t_wchar, len: usize) SCM;
pub extern fn scm_to_port_string(str: SCM, port: SCM) [*c]u8;
pub extern fn scm_to_port_stringn(str: SCM, lenp: [*c]usize, port: SCM) [*c]u8;
pub extern fn scm_from_port_string(str: [*c]const u8, port: SCM) SCM;
pub extern fn scm_from_port_stringn(str: [*c]const u8, len: usize, port: SCM) SCM;
pub extern fn scm_to_stringn(str: SCM, lenp: [*c]usize, encoding: [*c]const u8, handler: scm_t_string_failed_conversion_handler) [*c]u8;
pub extern fn scm_to_locale_stringbuf(str: SCM, buf: [*c]u8, max_len: usize) usize;
pub extern fn scm_string_normalize_nfd(str: SCM) SCM;
pub extern fn scm_string_normalize_nfkd(str: SCM) SCM;
pub extern fn scm_string_normalize_nfc(str: SCM) SCM;
pub extern fn scm_string_normalize_nfkc(str: SCM) SCM;
pub extern fn scm_makfromstrs(argc: c_int, argv: [*c][*c]u8) SCM;
pub extern fn scm_i_print_stringbuf(exp: SCM, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_i_make_string(len: usize, datap: [*c][*c]u8, read_only_p: c_int) SCM;
pub extern fn scm_i_make_wide_string(len: usize, datap: [*c][*c]scm_t_wchar, read_only_p: c_int) SCM;
pub extern fn scm_i_substring(str: SCM, start: usize, end: usize) SCM;
pub extern fn scm_i_substring_read_only(str: SCM, start: usize, end: usize) SCM;
pub extern fn scm_i_substring_shared(str: SCM, start: usize, end: usize) SCM;
pub extern fn scm_i_substring_copy(str: SCM, start: usize, end: usize) SCM;
pub extern fn scm_i_string_length(str: SCM) usize;
pub extern fn scm_i_string_is_mutable(str: SCM) c_int;
pub extern fn scm_i_string_chars(str: SCM) [*c]const u8;
pub extern fn scm_i_string_writable_chars(str: SCM) [*c]u8;
pub extern fn scm_i_string_wide_chars(str: SCM) [*c]const scm_t_wchar;
pub extern fn scm_i_string_data(str: SCM) ?*const anyopaque;
pub extern fn scm_i_string_start_writing(str: SCM) SCM;
pub extern fn scm_i_string_stop_writing() void;
pub extern fn scm_i_is_narrow_string(str: SCM) c_int;
pub extern fn scm_i_string_ref(str: SCM, x: usize) scm_t_wchar;
pub extern fn scm_i_string_contains_char(str: SCM, c: u8) c_int;
pub extern fn scm_i_string_strcmp(sstr: SCM, start_x: usize, cstr: [*c]const u8) c_int;
pub extern fn scm_i_string_set_x(str: SCM, p: usize, chr: scm_t_wchar) void;
pub extern fn scm_i_make_symbol(name: SCM, flags: scm_t_bits, hash: c_ulong) SCM;
pub extern fn scm_i_symbol_chars(sym: SCM) [*c]const u8;
pub extern fn scm_i_symbol_wide_chars(sym: SCM) [*c]const scm_t_wchar;
pub extern fn scm_i_symbol_length(sym: SCM) usize;
pub extern fn scm_i_is_narrow_symbol(str: SCM) c_int;
pub extern fn scm_i_try_narrow_string(str: SCM) c_int;
pub extern fn scm_i_symbol_substring(sym: SCM, start: usize, end: usize) SCM;
pub extern fn scm_i_symbol_ref(sym: SCM, x: usize) scm_t_wchar;
pub extern fn scm_encoding_error(subr: [*c]const u8, err: c_int, message: [*c]const u8, port: SCM, chr: SCM) void;
pub extern fn scm_decoding_error(subr: [*c]const u8, err: c_int, message: [*c]const u8, port: SCM) void;
pub extern fn scm_i_allocate_string_pointers(list: SCM) [*c][*c]u8;
pub extern fn scm_i_get_substring_spec(len: usize, start: SCM, cstart: [*c]usize, end: SCM, cend: [*c]usize) void;
pub extern fn scm_sys_string_dump(SCM) SCM;
pub extern fn scm_sys_symbol_dump(SCM) SCM;
pub extern fn scm_init_strings() void;
pub extern var scm_i_port_weak_set: SCM;
pub const struct_scm_t_port_type = opaque {};
pub const scm_t_port_type = struct_scm_t_port_type;
pub const struct_scm_t_port = opaque {};
pub const scm_t_port = struct_scm_t_port;
pub extern fn scm_make_port_type(name: [*c]u8, read: ?fn (SCM, SCM, usize, usize) callconv(.C) usize, write: ?fn (SCM, SCM, usize, usize) callconv(.C) usize) ?*scm_t_port_type;
pub extern fn scm_set_port_scm_read(ptob: ?*scm_t_port_type, read: SCM) void;
pub extern fn scm_set_port_scm_write(ptob: ?*scm_t_port_type, write: SCM) void;
pub extern fn scm_set_port_read_wait_fd(ptob: ?*scm_t_port_type, wait_fd: ?fn (SCM) callconv(.C) c_int) void;
pub extern fn scm_set_port_write_wait_fd(ptob: ?*scm_t_port_type, wait_fd: ?fn (SCM) callconv(.C) c_int) void;
pub extern fn scm_set_port_print(ptob: ?*scm_t_port_type, print: ?fn (SCM, SCM, [*c]scm_print_state) callconv(.C) c_int) void;
pub extern fn scm_set_port_close(ptob: ?*scm_t_port_type, close: ?fn (SCM) callconv(.C) void) void;
pub extern fn scm_set_port_needs_close_on_gc(ptob: ?*scm_t_port_type, needs_close_p: c_int) void;
pub extern fn scm_set_port_seek(ptob: ?*scm_t_port_type, seek: ?fn (SCM, scm_t_off, c_int) callconv(.C) scm_t_off) void;
pub extern fn scm_set_port_truncate(ptob: ?*scm_t_port_type, truncate: ?fn (SCM, scm_t_off) callconv(.C) void) void;
pub extern fn scm_set_port_input_waiting(ptob: ?*scm_t_port_type, input_waiting: ?fn (SCM) callconv(.C) c_int) void;
pub extern fn scm_set_port_get_natural_buffer_sizes(ptob: ?*scm_t_port_type, get_natural_buffer_sizes: ?fn (SCM, [*c]usize, [*c]usize) callconv(.C) void) void;
pub extern fn scm_set_port_random_access_p(ptob: ?*scm_t_port_type, random_access_p: ?fn (SCM) callconv(.C) c_int) void;
pub extern fn scm_current_input_port() SCM;
pub extern fn scm_current_output_port() SCM;
pub extern fn scm_current_error_port() SCM;
pub extern fn scm_current_warning_port() SCM;
pub extern fn scm_current_load_port() SCM;
pub extern fn scm_set_current_input_port(port: SCM) SCM;
pub extern fn scm_set_current_output_port(port: SCM) SCM;
pub extern fn scm_set_current_error_port(port: SCM) SCM;
pub extern fn scm_set_current_warning_port(port: SCM) SCM;
pub extern fn scm_dynwind_current_input_port(port: SCM) void;
pub extern fn scm_dynwind_current_output_port(port: SCM) void;
pub extern fn scm_dynwind_current_error_port(port: SCM) void;
pub extern fn scm_i_dynwind_current_load_port(port: SCM) void;
pub extern fn scm_i_mode_bits(modes: SCM) c_long;
pub extern fn scm_mode_bits(modes: [*c]u8) c_long;
pub extern fn scm_port_mode(port: SCM) SCM;
pub extern fn scm_c_make_port_with_encoding(ptob: ?*scm_t_port_type, mode_bits: c_ulong, encoding: SCM, conversion_strategy: SCM, stream: scm_t_bits) SCM;
pub extern fn scm_c_make_port(ptob: ?*scm_t_port_type, mode_bits: c_ulong, stream: scm_t_bits) SCM;
pub extern fn scm_port_p(x: SCM) SCM;
pub extern fn scm_input_port_p(x: SCM) SCM;
pub extern fn scm_output_port_p(x: SCM) SCM;
pub extern fn scm_port_closed_p(port: SCM) SCM;
pub extern fn scm_eof_object_p(x: SCM) SCM;
pub extern fn scm_close_port(port: SCM) SCM;
pub extern fn scm_close_input_port(port: SCM) SCM;
pub extern fn scm_close_output_port(port: SCM) SCM;
pub extern fn scm_i_string_failed_conversion_handler(conversion_strategy: SCM) scm_t_string_failed_conversion_handler;
pub extern fn scm_i_default_port_encoding() SCM;
pub extern fn scm_i_set_default_port_encoding(encoding: [*c]const u8) void;
pub extern fn scm_i_default_port_conversion_strategy() SCM;
pub extern fn scm_i_set_default_port_conversion_strategy(strategy: SCM) void;
pub extern fn scm_i_set_port_encoding_x(port: SCM, str: [*c]const u8) void;
pub extern fn scm_sys_port_encoding(port: SCM) SCM;
pub extern fn scm_sys_set_port_encoding_x(port: SCM, encoding: SCM) SCM;
pub extern fn scm_port_encoding(port: SCM) SCM;
pub extern fn scm_set_port_encoding_x(port: SCM, encoding: SCM) SCM;
pub extern fn scm_port_conversion_strategy(port: SCM) SCM;
pub extern fn scm_set_port_conversion_strategy_x(port: SCM, behavior: SCM) SCM;
pub extern fn scm_port_maybe_consume_initial_byte_order_mark(SCM, SCM, SCM) SCM;
pub extern fn scm_get_byte_or_eof(port: SCM) c_int;
pub extern fn scm_peek_byte_or_eof(port: SCM) c_int;
pub extern fn scm_c_read(port: SCM, buffer: ?*anyopaque, size: usize) usize;
pub extern fn scm_c_read_bytes(port: SCM, dst: SCM, start: usize, count: usize) usize;
pub extern fn scm_getc(port: SCM) scm_t_wchar;
pub extern fn scm_read_char(port: SCM) SCM;
pub extern fn scm_unget_bytes(buf: [*c]const u8, len: usize, port: SCM) void;
pub extern fn scm_unget_byte(c: c_int, port: SCM) void;
pub extern fn scm_ungetc(c: scm_t_wchar, port: SCM) void;
pub extern fn scm_ungets(s: [*c]const u8, n: c_int, port: SCM) void;
pub extern fn scm_peek_char(port: SCM) SCM;
pub extern fn scm_unread_char(cobj: SCM, port: SCM) SCM;
pub extern fn scm_unread_string(str: SCM, port: SCM) SCM;
pub extern fn scm_setvbuf(port: SCM, mode: SCM, size: SCM) SCM;
pub extern fn scm_fill_input(port: SCM, minimum_size: usize, cur_out: [*c]usize, avail_out: [*c]usize) SCM;
pub extern fn scm_take_from_input_buffers(port: SCM, dest: [*c]u8, read_len: usize) usize;
pub extern fn scm_drain_input(port: SCM) SCM;
pub extern fn scm_end_input(port: SCM) void;
pub extern fn scm_force_output(port: SCM) SCM;
pub extern fn scm_flush(port: SCM) void;
pub extern fn scm_port_random_access_p(port: SCM) SCM;
pub extern fn scm_port_read_buffering(port: SCM) SCM;
pub extern fn scm_expand_port_read_buffer_x(port: SCM, size: SCM, putback_p: SCM) SCM;
pub extern fn scm_port_read(port: SCM) SCM;
pub extern fn scm_port_write(port: SCM) SCM;
pub extern fn scm_port_read_buffer(port: SCM) SCM;
pub extern fn scm_port_write_buffer(port: SCM) SCM;
pub extern fn scm_port_auxiliary_write_buffer(port: SCM) SCM;
pub extern fn scm_c_write(port: SCM, buffer: ?*const anyopaque, size: usize) void;
pub extern fn scm_c_write_bytes(port: SCM, src: SCM, start: usize, count: usize) void;
pub extern fn scm_c_put_latin1_chars(port: SCM, buf: [*c]const u8, len: usize) void;
pub extern fn scm_c_put_utf32_chars(port: SCM, buf: [*c]const u32, len: usize) void;
pub extern fn scm_c_put_string(port: SCM, str: SCM, start: usize, count: usize) void;
pub extern fn scm_put_string(port: SCM, str: SCM, start: SCM, count: SCM) SCM;
pub extern fn scm_c_put_char(port: SCM, ch: scm_t_wchar) void;
pub extern fn scm_put_char(port: SCM, ch: SCM) SCM;
pub extern fn scm_c_put_escaped_char(port: SCM, ch: scm_t_wchar) void;
pub extern fn scm_c_can_put_char(port: SCM, ch: scm_t_wchar) c_int;
pub extern fn scm_putc(c: u8, port: SCM) void;
pub extern fn scm_puts(str_data: [*c]const u8, port: SCM) void;
pub extern fn scm_lfwrite(ptr: [*c]const u8, size: usize, port: SCM) void;
pub extern fn scm_lfwrite_substr(str: SCM, start: usize, end: usize, port: SCM) void;
pub extern fn scm_char_ready_p(port: SCM) SCM;
pub extern fn scm_seek(object: SCM, offset: SCM, whence: SCM) SCM;
pub extern fn scm_truncate_file(object: SCM, length: SCM) SCM;
pub extern fn scm_port_line(port: SCM) SCM;
pub extern fn scm_set_port_line_x(port: SCM, line: SCM) SCM;
pub extern fn scm_port_column(port: SCM) SCM;
pub extern fn scm_set_port_column_x(port: SCM, line: SCM) SCM;
pub extern fn scm_port_filename(port: SCM) SCM;
pub extern fn scm_set_port_filename_x(port: SCM, filename: SCM) SCM;
pub extern fn scm_i_port_property(port: SCM, key: SCM) SCM;
pub extern fn scm_i_set_port_property_x(port: SCM, key: SCM, value: SCM) SCM;
pub extern fn scm_port_print(exp: SCM, port: SCM, [*c]scm_print_state) c_int;
pub extern fn scm_print_port_mode(exp: SCM, port: SCM) void;
pub extern fn scm_port_for_each(proc: SCM) SCM;
pub extern fn scm_c_port_for_each(proc: ?fn (?*anyopaque, SCM) callconv(.C) void, data: ?*anyopaque) void;
pub extern fn scm_flush_all_ports() SCM;
pub extern fn scm_void_port(mode_str: [*c]u8) SCM;
pub extern fn scm_sys_make_void_port(mode: SCM) SCM;
pub extern fn scm_init_ports() void;
pub const struct_scm_t_fport = extern struct {
    fdes: c_int,
    revealed: c_uint,
    options: c_uint,
};
pub const scm_t_fport = struct_scm_t_fport;
pub extern var scm_file_port_type: ?*scm_t_port_type;
pub extern fn scm_evict_ports(fd: c_int) void;
pub extern fn scm_i_mode_to_open_flags(mode: SCM, is_binary: [*c]c_int, FUNC_NAME: [*c]const u8) c_int;
pub extern fn scm_open_file_with_encoding(filename: SCM, modes: SCM, guess_encoding: SCM, encoding: SCM) SCM;
pub extern fn scm_open_file(filename: SCM, modes: SCM) SCM;
pub extern fn scm_fdes_to_port(fdes: c_int, mode: [*c]u8, name: SCM) SCM;
pub extern fn scm_file_port_p(obj: SCM) SCM;
pub extern fn scm_revealed_count(port: SCM) c_int;
pub extern fn scm_port_revealed(port: SCM) SCM;
pub extern fn scm_set_port_revealed_x(port: SCM, rcount: SCM) SCM;
pub extern fn scm_adjust_port_revealed_x(port: SCM, addend: SCM) SCM;
pub extern fn scm_init_fports_keywords() void;
pub extern fn scm_init_fports() void;
pub extern fn scm_frame_p(obj: SCM) SCM;
pub extern fn scm_frame_procedure_name(frame: SCM) SCM;
pub extern fn scm_frame_call_representation(frame: SCM) SCM;
pub extern fn scm_frame_arguments(frame: SCM) SCM;
pub extern fn scm_frame_source(frame: SCM) SCM;
pub extern fn scm_frame_address(frame: SCM) SCM;
pub extern fn scm_frame_stack_pointer(frame: SCM) SCM;
pub extern fn scm_frame_instruction_pointer(frame: SCM) SCM;
pub extern fn scm_frame_return_address(frame: SCM) SCM;
pub extern fn scm_frame_dynamic_link(frame: SCM) SCM;
pub extern fn scm_frame_previous(frame: SCM) SCM;
pub extern fn scm_i_frame_print(frame: SCM, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_init_frames() void;
pub extern fn scm_make_generalized_vector(@"type": SCM, len: SCM, fill: SCM) SCM;
pub extern fn scm_i_register_vector_constructor(@"type": SCM, ctor: ?fn (SCM, SCM) callconv(.C) SCM) void;
pub extern fn scm_init_generalized_vectors() void;
pub extern fn scm_list_1(e1: SCM) SCM;
pub extern fn scm_list_2(e1: SCM, e2: SCM) SCM;
pub extern fn scm_list_3(e1: SCM, e2: SCM, e3: SCM) SCM;
pub extern fn scm_list_4(e1: SCM, e2: SCM, e3: SCM, e4: SCM) SCM;
pub extern fn scm_list_5(e1: SCM, e2: SCM, e3: SCM, e4: SCM, e5: SCM) SCM;
pub extern fn scm_list_n(elt: SCM, ...) SCM;
pub extern fn scm_list(objs: SCM) SCM;
pub extern fn scm_list_head(lst: SCM, k: SCM) SCM;
pub extern fn scm_make_list(n: SCM, init: SCM) SCM;
pub extern fn scm_cons_star(arg: SCM, objs: SCM) SCM;
pub extern fn scm_null_p(x: SCM) SCM;
pub extern fn scm_list_p(x: SCM) SCM;
pub extern fn scm_ilength(sx: SCM) c_long;
pub extern fn scm_length(x: SCM) SCM;
pub extern fn scm_append(args: SCM) SCM;
pub extern fn scm_append_x(args: SCM) SCM;
pub extern fn scm_reverse(lst: SCM) SCM;
pub extern fn scm_reverse_x(lst: SCM, newtail: SCM) SCM;
pub extern fn scm_list_ref(lst: SCM, k: SCM) SCM;
pub extern fn scm_list_set_x(lst: SCM, k: SCM, val: SCM) SCM;
pub extern fn scm_list_cdr_set_x(lst: SCM, k: SCM, val: SCM) SCM;
pub extern fn scm_last_pair(sx: SCM) SCM;
pub extern fn scm_list_tail(lst: SCM, k: SCM) SCM;
pub extern fn scm_c_memq(x: SCM, lst: SCM) SCM;
pub extern fn scm_memq(x: SCM, lst: SCM) SCM;
pub extern fn scm_memv(x: SCM, lst: SCM) SCM;
pub extern fn scm_member(x: SCM, lst: SCM) SCM;
pub extern fn scm_delq_x(item: SCM, lst: SCM) SCM;
pub extern fn scm_delv_x(item: SCM, lst: SCM) SCM;
pub extern fn scm_delete_x(item: SCM, lst: SCM) SCM;
pub extern fn scm_list_copy(lst: SCM) SCM;
pub extern fn scm_delq(item: SCM, lst: SCM) SCM;
pub extern fn scm_delv(item: SCM, lst: SCM) SCM;
pub extern fn scm_delete(item: SCM, lst: SCM) SCM;
pub extern fn scm_delq1_x(item: SCM, lst: SCM) SCM;
pub extern fn scm_delv1_x(item: SCM, lst: SCM) SCM;
pub extern fn scm_delete1_x(item: SCM, lst: SCM) SCM;
pub extern fn scm_filter(pred: SCM, list: SCM) SCM;
pub extern fn scm_filter_x(pred: SCM, list: SCM) SCM;
pub extern fn scm_copy_tree(obj: SCM) SCM;
pub extern fn scm_i_finite_list_copy(SCM) SCM;
pub extern fn scm_init_list() void;
pub extern var scm_i_smob_class: [*c]SCM;
pub extern var scm_module_goops: SCM;
pub extern fn scm_goops_version() SCM;
pub extern fn scm_load_goops() void;
pub extern fn scm_make_extended_class(type_name: [*c]const u8, applicablep: c_int) SCM;
pub extern fn scm_make_port_classes(ptob: ?*scm_t_port_type) void;
pub extern fn scm_ensure_accessor(name: SCM) SCM;
pub extern fn scm_class_of(obj: SCM) SCM;
pub extern fn scm_make_standard_class(meta: SCM, name: SCM, dsupers: SCM, dslots: SCM) SCM;
pub extern fn scm_slot_ref(obj: SCM, slot_name: SCM) SCM;
pub extern fn scm_slot_set_x(obj: SCM, slot_name: SCM, value: SCM) SCM;
pub extern fn scm_i_inherit_applicable(c: SCM) void;
pub extern fn scm_instance_p(obj: SCM) SCM;
pub extern fn scm_is_generic(x: SCM) c_int;
pub extern fn scm_is_method(x: SCM) c_int;
pub extern fn scm_class_name(obj: SCM) SCM;
pub extern fn scm_class_direct_supers(obj: SCM) SCM;
pub extern fn scm_class_direct_slots(obj: SCM) SCM;
pub extern fn scm_class_direct_subclasses(obj: SCM) SCM;
pub extern fn scm_class_direct_methods(obj: SCM) SCM;
pub extern fn scm_class_precedence_list(obj: SCM) SCM;
pub extern fn scm_class_slots(obj: SCM) SCM;
pub extern fn scm_generic_function_name(obj: SCM) SCM;
pub extern fn scm_generic_function_methods(obj: SCM) SCM;
pub extern fn scm_method_generic_function(obj: SCM) SCM;
pub extern fn scm_method_specializers(obj: SCM) SCM;
pub extern fn scm_method_procedure(obj: SCM) SCM;
pub extern fn scm_slot_bound_p(obj: SCM, slot_name: SCM) SCM;
pub extern fn scm_slot_exists_p(obj: SCM, slot_name: SCM) SCM;
pub extern fn scm_sys_modify_instance(old: SCM, newinst: SCM) SCM;
pub extern fn scm_generic_capability_p(proc: SCM) SCM;
pub extern fn scm_enable_primitive_generic_x(subrs: SCM) SCM;
pub extern fn scm_set_primitive_generic_x(subr: SCM, generic: SCM) SCM;
pub extern fn scm_primitive_generic_generic(subr: SCM) SCM;
pub extern fn scm_make(args: SCM) SCM;
pub extern fn scm_wta_dispatch_0(gf: SCM, subr: [*c]const u8) SCM;
pub extern fn scm_wta_dispatch_1(gf: SCM, a1: SCM, pos: c_int, subr: [*c]const u8) SCM;
pub extern fn scm_wta_dispatch_2(gf: SCM, a1: SCM, a2: SCM, pos: c_int, subr: [*c]const u8) SCM;
pub extern fn scm_wta_dispatch_n(gf: SCM, args: SCM, pos: c_int, subr: [*c]const u8) SCM;
pub extern fn scm_i_define_class_for_vtable(vtable: SCM) SCM;
pub extern fn scm_init_goops() void;
pub extern fn scm_i_alloc_primitive_code_with_instrumentation(uint32_count: usize, write_ptr: [*c][*c]u32) [*c]u32;
pub extern fn scm_i_primitive_code_p(code: [*c]const u32) c_int;
pub extern fn scm_i_primitive_call_ip(subr: SCM) usize;
pub extern fn scm_i_primitive_name(code: [*c]const u32) SCM;
pub extern fn scm_subr_function(subr: SCM) scm_t_subr;
pub extern fn scm_subr_function_by_index(subr_idx: u32) scm_t_subr;
pub extern fn scm_subr_name(subr: SCM) SCM;
pub extern fn scm_apply_subr(sp: [*c]union_scm_vm_stack_element, subr_idx: u32, nargs: ptrdiff_t) SCM;
pub extern fn scm_c_make_gsubr(name: [*c]const u8, req: c_int, opt: c_int, rst: c_int, fcn: scm_t_subr) SCM;
pub extern fn scm_c_make_gsubr_with_generic(name: [*c]const u8, req: c_int, opt: c_int, rst: c_int, fcn: scm_t_subr, gf: [*c]SCM) SCM;
pub extern fn scm_c_define_gsubr(name: [*c]const u8, req: c_int, opt: c_int, rst: c_int, fcn: scm_t_subr) SCM;
pub extern fn scm_c_define_gsubr_with_generic(name: [*c]const u8, req: c_int, opt: c_int, rst: c_int, fcn: scm_t_subr, gf: [*c]SCM) SCM;
pub extern fn scm_init_gsubr() void;
pub extern fn scm_make_guardian() SCM;
pub extern fn scm_i_init_guardians_for_gc() void;
pub extern fn scm_i_identify_inaccessible_guardeds() void;
pub extern fn scm_i_mark_inaccessible_guardeds() c_int;
pub extern fn scm_init_guardians() void;
pub extern fn scm_i_locale_string_hash(str: [*c]const u8, len: usize) c_ulong;
pub extern fn scm_i_latin1_string_hash(str: [*c]const u8, len: usize) c_ulong;
pub extern fn scm_i_utf8_string_hash(str: [*c]const u8, len: usize) c_ulong;
pub extern fn scm_i_string_hash(str: SCM) c_ulong;
pub extern fn scm_ihashq(obj: SCM, n: c_ulong) c_ulong;
pub extern fn scm_hashq(obj: SCM, n: SCM) SCM;
pub extern fn scm_ihashv(obj: SCM, n: c_ulong) c_ulong;
pub extern fn scm_hashv(obj: SCM, n: SCM) SCM;
pub extern fn scm_ihash(obj: SCM, n: c_ulong) c_ulong;
pub extern fn scm_hash(obj: SCM, n: SCM) SCM;
pub extern fn scm_init_hash() void;
pub const scm_t_hash_fn = ?fn (SCM, c_ulong, ?*anyopaque) callconv(.C) c_ulong;
pub const scm_t_assoc_fn = ?fn (SCM, SCM, ?*anyopaque) callconv(.C) SCM;
pub const scm_t_hash_fold_fn = ?fn (?*anyopaque, SCM, SCM, SCM) callconv(.C) SCM;
pub const scm_t_hash_handle_fn = ?fn (?*anyopaque, SCM) callconv(.C) SCM;
pub const struct_scm_t_hashtable = extern struct {
    n_items: c_ulong,
    lower: c_ulong,
    upper: c_ulong,
    size_index: c_int,
    min_size_index: c_int,
    hash_fn: scm_t_hash_fn,
};
pub const scm_t_hashtable = struct_scm_t_hashtable;
pub extern fn scm_vector_to_hash_table(vector: SCM) SCM;
pub extern fn scm_c_make_hash_table(k: c_ulong) SCM;
pub extern fn scm_make_hash_table(n: SCM) SCM;
pub extern fn scm_hash_table_p(h: SCM) SCM;
pub extern fn scm_i_rehash(table: SCM, hash_fn: scm_t_hash_fn, closure: ?*anyopaque, func_name: [*c]const u8) void;
pub extern fn scm_hash_fn_get_handle(table: SCM, obj: SCM, hash_fn: scm_t_hash_fn, assoc_fn: scm_t_assoc_fn, closure: ?*anyopaque) SCM;
pub extern fn scm_hash_fn_create_handle_x(table: SCM, obj: SCM, init: SCM, hash_fn: scm_t_hash_fn, assoc_fn: scm_t_assoc_fn, closure: ?*anyopaque) SCM;
pub extern fn scm_hash_fn_ref(table: SCM, obj: SCM, dflt: SCM, hash_fn: scm_t_hash_fn, assoc_fn: scm_t_assoc_fn, closure: ?*anyopaque) SCM;
pub extern fn scm_hash_fn_set_x(table: SCM, obj: SCM, val: SCM, hash_fn: scm_t_hash_fn, assoc_fn: scm_t_assoc_fn, closure: ?*anyopaque) SCM;
pub extern fn scm_hash_fn_remove_x(table: SCM, obj: SCM, hash_fn: scm_t_hash_fn, assoc_fn: scm_t_assoc_fn, closure: ?*anyopaque) SCM;
pub extern fn scm_internal_hash_fold(@"fn": scm_t_hash_fold_fn, closure: ?*anyopaque, init: SCM, table: SCM) SCM;
pub extern fn scm_internal_hash_for_each_handle(@"fn": scm_t_hash_handle_fn, closure: ?*anyopaque, table: SCM) void;
pub extern fn scm_hash_clear_x(table: SCM) SCM;
pub extern fn scm_hashq_get_handle(table: SCM, obj: SCM) SCM;
pub extern fn scm_hashq_create_handle_x(table: SCM, obj: SCM, init: SCM) SCM;
pub extern fn scm_hashq_ref(table: SCM, obj: SCM, dflt: SCM) SCM;
pub extern fn scm_hashq_set_x(table: SCM, obj: SCM, val: SCM) SCM;
pub extern fn scm_hashq_remove_x(table: SCM, obj: SCM) SCM;
pub extern fn scm_hashv_get_handle(table: SCM, obj: SCM) SCM;
pub extern fn scm_hashv_create_handle_x(table: SCM, obj: SCM, init: SCM) SCM;
pub extern fn scm_hashv_ref(table: SCM, obj: SCM, dflt: SCM) SCM;
pub extern fn scm_hashv_set_x(table: SCM, obj: SCM, val: SCM) SCM;
pub extern fn scm_hashv_remove_x(table: SCM, obj: SCM) SCM;
pub extern fn scm_hash_get_handle(table: SCM, obj: SCM) SCM;
pub extern fn scm_hash_create_handle_x(table: SCM, obj: SCM, init: SCM) SCM;
pub extern fn scm_hash_ref(table: SCM, obj: SCM, dflt: SCM) SCM;
pub extern fn scm_hash_set_x(table: SCM, obj: SCM, val: SCM) SCM;
pub extern fn scm_hash_remove_x(table: SCM, obj: SCM) SCM;
pub extern fn scm_hashx_get_handle(hash: SCM, assoc: SCM, table: SCM, obj: SCM) SCM;
pub extern fn scm_hashx_create_handle_x(hash: SCM, assoc: SCM, table: SCM, obj: SCM, init: SCM) SCM;
pub extern fn scm_hashx_ref(hash: SCM, assoc: SCM, table: SCM, obj: SCM, dflt: SCM) SCM;
pub extern fn scm_hashx_set_x(hash: SCM, assoc: SCM, table: SCM, obj: SCM, val: SCM) SCM;
pub extern fn scm_hashx_remove_x(hash: SCM, assoc: SCM, table: SCM, obj: SCM) SCM;
pub extern fn scm_hash_fold(proc: SCM, init: SCM, hash: SCM) SCM;
pub extern fn scm_hash_for_each(proc: SCM, hash: SCM) SCM;
pub extern fn scm_hash_for_each_handle(proc: SCM, hash: SCM) SCM;
pub extern fn scm_hash_map_to_list(proc: SCM, hash: SCM) SCM;
pub extern fn scm_hash_count(hash: SCM, pred: SCM) SCM;
pub extern fn scm_i_hashtable_print(exp: SCM, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_init_hashtab() void;
pub extern var scm_tc16_hook: scm_t_bits;
pub extern fn scm_make_hook(n_args: SCM) SCM;
pub extern fn scm_hook_p(x: SCM) SCM;
pub extern fn scm_hook_empty_p(hook: SCM) SCM;
pub extern fn scm_add_hook_x(hook: SCM, thunk: SCM, appendp: SCM) SCM;
pub extern fn scm_remove_hook_x(hook: SCM, thunk: SCM) SCM;
pub extern fn scm_reset_hook_x(hook: SCM) SCM;
pub extern fn scm_run_hook(hook: SCM, args: SCM) SCM;
pub extern fn scm_c_run_hook(hook: SCM, args: SCM) void;
pub extern fn scm_c_run_hookn(hook: SCM, argv: [*c]SCM, nargs: usize) void;
pub extern fn scm_hook_to_list(hook: SCM) SCM;
pub extern fn scm_init_hooks() void;
pub extern var scm_global_locale: SCM;
pub extern fn scm_make_locale(category_mask: SCM, locale_name: SCM, base_locale: SCM) SCM;
pub extern fn scm_locale_p(obj: SCM) SCM;
pub extern fn scm_string_locale_lt(s1: SCM, s2: SCM, locale: SCM) SCM;
pub extern fn scm_string_locale_gt(s1: SCM, s2: SCM, locale: SCM) SCM;
pub extern fn scm_string_locale_ci_lt(s1: SCM, s2: SCM, locale: SCM) SCM;
pub extern fn scm_string_locale_ci_gt(s1: SCM, s2: SCM, locale: SCM) SCM;
pub extern fn scm_string_locale_ci_eq(s1: SCM, s2: SCM, locale: SCM) SCM;
pub extern fn scm_char_locale_lt(c1: SCM, c2: SCM, locale: SCM) SCM;
pub extern fn scm_char_locale_gt(c1: SCM, c2: SCM, locale: SCM) SCM;
pub extern fn scm_char_locale_ci_lt(c1: SCM, c2: SCM, locale: SCM) SCM;
pub extern fn scm_char_locale_ci_gt(c1: SCM, c2: SCM, locale: SCM) SCM;
pub extern fn scm_char_locale_ci_eq(c1: SCM, c2: SCM, locale: SCM) SCM;
pub extern fn scm_char_locale_upcase(chr: SCM, locale: SCM) SCM;
pub extern fn scm_char_locale_downcase(chr: SCM, locale: SCM) SCM;
pub extern fn scm_char_locale_titlecase(chr: SCM, locale: SCM) SCM;
pub extern fn scm_string_locale_upcase(chr: SCM, locale: SCM) SCM;
pub extern fn scm_string_locale_downcase(chr: SCM, locale: SCM) SCM;
pub extern fn scm_string_locale_titlecase(chr: SCM, locale: SCM) SCM;
pub extern fn scm_locale_string_to_integer(str: SCM, base: SCM, locale: SCM) SCM;
pub extern fn scm_locale_string_to_inexact(str: SCM, locale: SCM) SCM;
pub extern fn scm_nl_langinfo(item: SCM, locale: SCM) SCM;
pub extern fn scm_init_i18n() void;
pub extern fn scm_bootstrap_i18n() void;
pub extern var scm_i_init_mutex: pthread_mutex_t;
pub extern var scm_initialized_p: c_int;
pub extern fn scm_init_guile() void;
pub extern fn scm_boot_guile(argc: c_int, argv: [*c][*c]u8, main_func: ?fn (?*anyopaque, c_int, [*c][*c]u8) callconv(.C) void, closure: ?*anyopaque) void;
pub extern fn scm_i_init_guile(base: ?*anyopaque) void;
pub extern fn scm_load_startup_files() void;
pub extern fn scm_ftell(object: SCM) SCM;
pub extern fn scm_redirect_port(into_pt: SCM, from_pt: SCM) SCM;
pub extern fn scm_dup_to_fdes(fd_or_port: SCM, newfd: SCM) SCM;
pub extern fn scm_dup2(oldfd: SCM, newfd: SCM) SCM;
pub extern fn scm_fileno(port: SCM) SCM;
pub extern fn scm_isatty_p(port: SCM) SCM;
pub extern fn scm_fdopen(fdes: SCM, modes: SCM) SCM;
pub extern fn scm_primitive_move_to_fdes(port: SCM, fd: SCM) SCM;
pub extern fn scm_fdes_to_ports(fd: SCM) SCM;
pub extern fn scm_init_ioext() void;
pub extern fn scm_read_delimited_x(delims: SCM, buf: SCM, gobble: SCM, port: SCM, offset: SCM, length: SCM) SCM;
pub extern fn scm_read_line(port: SCM) SCM;
pub extern fn scm_write_line(obj: SCM, port: SCM) SCM;
pub extern fn scm_init_rdelim_builtins() SCM;
pub extern fn scm_init_rdelim() void;
pub extern fn scm_read_string_x_partial(str: SCM, port_or_fdes: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_write_string_partial(str: SCM, port_or_fdes: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_init_rw_builtins() SCM;
pub extern fn scm_init_rw() void;
pub extern fn scm_keyword_p(obj: SCM) SCM;
pub extern fn scm_symbol_to_keyword(symbol: SCM) SCM;
pub extern fn scm_keyword_to_symbol(keyword: SCM) SCM;
pub extern fn scm_is_keyword(val: SCM) c_int;
pub extern fn scm_from_locale_keyword(name: [*c]const u8) SCM;
pub extern fn scm_from_locale_keywordn(name: [*c]const u8, len: usize) SCM;
pub extern fn scm_from_latin1_keyword(name: [*c]const u8) SCM;
pub extern fn scm_from_utf8_keyword(name: [*c]const u8) SCM;
pub const SCM_ALLOW_OTHER_KEYS: c_int = 1;
pub const SCM_ALLOW_NON_KEYWORD_ARGUMENTS: c_int = 2;
pub const enum_scm_keyword_arguments_flags = c_uint;
pub const scm_t_keyword_arguments_flags = enum_scm_keyword_arguments_flags;
pub extern fn scm_c_bind_keyword_arguments(subr: [*c]const u8, rest: SCM, flags: scm_t_keyword_arguments_flags, ...) void;
pub extern fn scm_init_keywords() void;
pub extern fn scm_parse_path(path: SCM, tail: SCM) SCM;
pub extern fn scm_parse_path_with_ellipsis(path: SCM, base: SCM) SCM;
pub extern fn scm_primitive_load(filename: SCM) SCM;
pub extern fn scm_c_primitive_load(filename: [*c]const u8) SCM;
pub extern fn scm_sys_package_data_dir() SCM;
pub extern fn scm_sys_library_dir() SCM;
pub extern fn scm_sys_site_dir() SCM;
pub extern fn scm_sys_global_site_dir() SCM;
pub extern fn scm_sys_site_ccache_dir() SCM;
pub extern fn scm_search_path(path: SCM, filename: SCM, rest: SCM) SCM;
pub extern fn scm_sys_search_load_path(filename: SCM) SCM;
pub extern fn scm_primitive_load_path(filename_and_exception_on_not_found: SCM) SCM;
pub extern fn scm_c_primitive_load_path(filename: [*c]const u8) SCM;
pub extern fn scm_sys_warn_auto_compilation_enabled() SCM;
pub extern fn scm_init_load_path() void;
pub extern fn scm_init_load() void;
pub extern fn scm_init_load_should_auto_compile() void;
pub extern fn scm_init_eval_in_scheme() void;
pub extern fn scm_i_mirror_backslashes(path: [*c]u8) [*c]u8;
pub const scm_t_macro_primitive = ?fn (SCM, SCM) callconv(.C) SCM;
pub extern fn scm_make_syntax_transformer(name_or_existing_definition: SCM, @"type": SCM, binding: SCM) SCM;
pub extern fn scm_macro_p(obj: SCM) SCM;
pub extern fn scm_macro_type(m: SCM) SCM;
pub extern fn scm_macro_name(m: SCM) SCM;
pub extern fn scm_macro_binding(m: SCM) SCM;
pub extern fn scm_macro_transformer(m: SCM) SCM;
pub extern fn scm_i_make_primitive_macro(name: [*c]const u8, @"fn": scm_t_macro_primitive) SCM;
pub extern fn scm_i_macro_primitive(m: SCM) scm_t_macro_primitive;
pub extern fn scm_init_macros() void;
pub extern var scm_tc16_malloc: scm_t_bits;
pub extern fn scm_malloc_obj(n: usize) SCM;
pub extern fn scm_init_mallocs() void;
pub extern var scm_module_system_booted_p: c_int;
pub extern var scm_module_tag: scm_t_bits;
pub extern fn scm_current_module() SCM;
pub extern fn scm_i_current_module(thread: [*c]scm_thread) SCM;
pub extern fn scm_the_root_module() SCM;
pub extern fn scm_interaction_environment() SCM;
pub extern fn scm_set_current_module(module: SCM) SCM;
pub extern fn scm_c_call_with_current_module(module: SCM, func: ?fn (?*anyopaque) callconv(.C) SCM, data: ?*anyopaque) SCM;
pub extern fn scm_dynwind_current_module(module: SCM) void;
pub extern fn scm_module_variable(module: SCM, sym: SCM) SCM;
pub extern fn scm_module_local_variable(module: SCM, sym: SCM) SCM;
pub extern fn scm_module_ensure_local_variable(module: SCM, sym: SCM) SCM;
pub extern fn scm_c_lookup(name: [*c]const u8) SCM;
pub extern fn scm_c_define(name: [*c]const u8, val: SCM) SCM;
pub extern fn scm_lookup(symbol: SCM) SCM;
pub extern fn scm_define(symbol: SCM, val: SCM) SCM;
pub extern fn scm_c_module_lookup(module: SCM, name: [*c]const u8) SCM;
pub extern fn scm_c_module_define(module: SCM, name: [*c]const u8, val: SCM) SCM;
pub extern fn scm_module_lookup(module: SCM, symbol: SCM) SCM;
pub extern fn scm_module_define(module: SCM, symbol: SCM, val: SCM) SCM;
pub extern fn scm_module_export(module: SCM, symbol_list: SCM) SCM;
pub extern fn scm_module_reverse_lookup(module: SCM, variable: SCM) SCM;
pub extern fn scm_public_variable(module_name: SCM, name: SCM) SCM;
pub extern fn scm_private_variable(module_name: SCM, name: SCM) SCM;
pub extern fn scm_c_public_variable(module_name: [*c]const u8, name: [*c]const u8) SCM;
pub extern fn scm_c_private_variable(module_name: [*c]const u8, name: [*c]const u8) SCM;
pub extern fn scm_public_lookup(module_name: SCM, name: SCM) SCM;
pub extern fn scm_private_lookup(module_name: SCM, name: SCM) SCM;
pub extern fn scm_c_public_lookup(module_name: [*c]const u8, name: [*c]const u8) SCM;
pub extern fn scm_c_private_lookup(module_name: [*c]const u8, name: [*c]const u8) SCM;
pub extern fn scm_public_ref(module_name: SCM, name: SCM) SCM;
pub extern fn scm_private_ref(module_name: SCM, name: SCM) SCM;
pub extern fn scm_c_public_ref(module_name: [*c]const u8, name: [*c]const u8) SCM;
pub extern fn scm_c_private_ref(module_name: [*c]const u8, name: [*c]const u8) SCM;
pub extern fn scm_c_resolve_module(name: [*c]const u8) SCM;
pub extern fn scm_resolve_module(name: SCM) SCM;
pub extern fn scm_maybe_resolve_module(name: SCM) SCM;
pub extern fn scm_c_define_module(name: [*c]const u8, init: ?fn (?*anyopaque) callconv(.C) void, data: ?*anyopaque) SCM;
pub extern fn scm_c_use_module(name: [*c]const u8) void;
pub extern fn scm_c_export(name: [*c]const u8, ...) void;
pub extern fn scm_module_public_interface(module: SCM) SCM;
pub extern fn scm_module_import_interface(module: SCM, sym: SCM) SCM;
pub extern fn scm_module_transformer(module: SCM) SCM;
pub extern fn scm_current_module_transformer() SCM;
pub extern fn scm_get_pre_modules_obarray() SCM;
pub extern fn scm_modules_prehistory() void;
pub extern fn scm_init_modules() void;
pub extern fn scm_gethost(host: SCM) SCM;
pub extern fn scm_getnet(name: SCM) SCM;
pub extern fn scm_getproto(name: SCM) SCM;
pub extern fn scm_getserv(name: SCM, proto: SCM) SCM;
pub extern fn scm_sethost(arg: SCM) SCM;
pub extern fn scm_setnet(arg: SCM) SCM;
pub extern fn scm_setproto(arg: SCM) SCM;
pub extern fn scm_setserv(arg: SCM) SCM;
pub extern fn scm_getaddrinfo(SCM, SCM, SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_gai_strerror(SCM) SCM;
pub extern fn scm_init_net_db() void;
pub extern fn scm_object_properties(obj: SCM) SCM;
pub extern fn scm_set_object_properties_x(obj: SCM, plist: SCM) SCM;
pub extern fn scm_object_property(obj: SCM, key: SCM) SCM;
pub extern fn scm_set_object_property_x(obj: SCM, key: SCM, val: SCM) SCM;
pub extern fn scm_init_objprop() void;
pub extern fn scm_tcsetpgrp(port: SCM, pgid: SCM) SCM;
pub extern fn scm_tcgetpgrp(port: SCM) SCM;
pub extern fn scm_ctermid() SCM;
pub extern fn scm_setsid() SCM;
pub extern fn scm_getsid(pid: SCM) SCM;
pub extern fn scm_setpgid(pid: SCM, pgid: SCM) SCM;
pub extern fn scm_pipe() SCM;
pub extern fn scm_getgroups() SCM;
pub extern fn scm_setgroups(groups: SCM) SCM;
pub extern fn scm_getpgrp() SCM;
pub extern fn scm_getpwuid(user: SCM) SCM;
pub extern fn scm_setpwent(arg: SCM) SCM;
pub extern fn scm_getgrgid(name: SCM) SCM;
pub extern fn scm_setgrent(arg: SCM) SCM;
pub extern fn scm_getrlimit(resource: SCM) SCM;
pub extern fn scm_setrlimit(resource: SCM, soft: SCM, hard: SCM) SCM;
pub extern fn scm_kill(pid: SCM, sig: SCM) SCM;
pub extern fn scm_waitpid(pid: SCM, options: SCM) SCM;
pub extern fn scm_status_exit_val(status: SCM) SCM;
pub extern fn scm_status_term_sig(status: SCM) SCM;
pub extern fn scm_status_stop_sig(status: SCM) SCM;
pub extern fn scm_getppid() SCM;
pub extern fn scm_getuid() SCM;
pub extern fn scm_getgid() SCM;
pub extern fn scm_geteuid() SCM;
pub extern fn scm_getegid() SCM;
pub extern fn scm_setuid(uid: SCM) SCM;
pub extern fn scm_setgid(gid: SCM) SCM;
pub extern fn scm_seteuid(euid: SCM) SCM;
pub extern fn scm_setegid(egid: SCM) SCM;
pub extern fn scm_ttyname(port: SCM) SCM;
pub extern fn scm_execl(filename: SCM, args: SCM) SCM;
pub extern fn scm_execlp(filename: SCM, args: SCM) SCM;
pub extern fn scm_execle(filename: SCM, env: SCM, args: SCM) SCM;
pub extern fn scm_fork() SCM;
pub extern fn scm_uname() SCM;
pub extern fn scm_environ(env: SCM) SCM;
pub extern fn scm_tmpnam() SCM;
pub extern fn scm_tmpfile() SCM;
pub extern fn scm_open_pipe(pipestr: SCM, modes: SCM) SCM;
pub extern fn scm_close_pipe(port: SCM) SCM;
pub extern fn scm_system_star(cmds: SCM) SCM;
pub extern fn scm_utime(pathname: SCM, actime: SCM, modtime: SCM, actimens: SCM, modtimens: SCM, flags: SCM) SCM;
pub extern fn scm_access(path: SCM, how: SCM) SCM;
pub extern fn scm_getpid() SCM;
pub extern fn scm_putenv(str: SCM) SCM;
pub extern fn scm_setlocale(category: SCM, locale: SCM) SCM;
pub extern fn scm_mknod(path: SCM, @"type": SCM, perms: SCM, dev: SCM) SCM;
pub extern fn scm_nice(incr: SCM) SCM;
pub extern fn scm_sync() SCM;
pub extern fn scm_crypt(key: SCM, salt: SCM) SCM;
pub extern fn scm_chroot(path: SCM) SCM;
pub extern fn scm_getlogin() SCM;
pub extern fn scm_getpriority(which: SCM, who: SCM) SCM;
pub extern fn scm_setpriority(which: SCM, who: SCM, prio: SCM) SCM;
pub extern fn scm_getpass(prompt: SCM) SCM;
pub extern fn scm_flock(file: SCM, operation: SCM) SCM;
pub extern fn scm_sethostname(name: SCM) SCM;
pub extern fn scm_gethostname() SCM;
pub extern fn scm_getaffinity(pid: SCM) SCM;
pub extern fn scm_setaffinity(pid: SCM, cpu_set: SCM) SCM;
pub extern fn scm_init_posix() void;
pub extern var scm_i_locale_mutex: pthread_mutex_t;
pub extern var scm_sym_name: SCM;
pub extern var scm_sym_system_procedure: SCM;
pub extern var scm_sym_documentation: SCM;
pub extern fn scm_i_procedure_arity(proc: SCM, req: [*c]c_int, opt: [*c]c_int, rest: [*c]c_int) c_int;
pub extern fn scm_set_procedure_minimum_arity_x(proc: SCM, req: SCM, opt: SCM, rest: SCM) SCM;
pub extern fn scm_procedure_minimum_arity(proc: SCM) SCM;
pub extern fn scm_procedure_properties(proc: SCM) SCM;
pub extern fn scm_set_procedure_properties_x(proc: SCM, alist: SCM) SCM;
pub extern fn scm_procedure_property(proc: SCM, key: SCM) SCM;
pub extern fn scm_set_procedure_property_x(proc: SCM, key: SCM, val: SCM) SCM;
pub extern fn scm_procedure_source(proc: SCM) SCM;
pub extern fn scm_procedure_name(proc: SCM) SCM;
pub extern fn scm_procedure_documentation(proc: SCM) SCM;
pub extern fn scm_init_procprop() void;
pub extern var scm_tc16_promise: scm_t_bits;
pub extern fn scm_make_promise(thunk: SCM) SCM;
pub extern fn scm_force(x: SCM) SCM;
pub extern fn scm_promise_p(x: SCM) SCM;
pub extern fn scm_init_promises() void;
pub extern fn scm_eof_object() SCM;
pub extern fn scm_open_bytevector_input_port(SCM, SCM) SCM;
pub extern fn scm_make_custom_binary_input_port(SCM, SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_get_u8(SCM) SCM;
pub extern fn scm_lookahead_u8(SCM) SCM;
pub extern fn scm_get_bytevector_n(SCM, SCM) SCM;
pub extern fn scm_get_bytevector_n_x(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_get_bytevector_some(SCM) SCM;
pub extern fn scm_get_bytevector_all(SCM) SCM;
pub extern fn scm_put_u8(SCM, SCM) SCM;
pub extern fn scm_put_bytevector(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_open_bytevector_output_port(SCM) SCM;
pub extern fn scm_make_custom_binary_output_port(SCM, SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_make_custom_binary_input_output_port(SCM, SCM, SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_get_string_n_x(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_init_r6rs_ports() void;
pub extern fn scm_register_r6rs_ports() void;
pub extern fn scm_unget_bytevector(SCM, SCM, SCM, SCM) SCM;
pub extern fn scm_get_bytevector_some_x(SCM, SCM, SCM, SCM) SCM;
pub const scm_t_rstate = struct_scm_t_rstate;
pub const struct_scm_t_rng = extern struct {
    rstate_size: usize,
    random_bits: ?fn ([*c]scm_t_rstate) callconv(.C) u32,
    init_rstate: ?fn ([*c]scm_t_rstate, [*c]const u8, c_int) callconv(.C) void,
    copy_rstate: ?fn ([*c]scm_t_rstate) callconv(.C) [*c]scm_t_rstate,
    from_datum: ?fn ([*c]scm_t_rstate, SCM) callconv(.C) void,
    to_datum: ?fn ([*c]scm_t_rstate) callconv(.C) SCM,
};
pub const struct_scm_t_rstate = extern struct {
    rng: [*c]struct_scm_t_rng,
    normal_next: f64,
};
pub const scm_t_rng = struct_scm_t_rng;
pub extern var scm_the_rng: scm_t_rng;
pub extern fn scm_c_make_rstate([*c]const u8, c_int) [*c]scm_t_rstate;
pub extern fn scm_c_rstate_from_datum(datum: SCM) [*c]scm_t_rstate;
pub extern fn scm_c_default_rstate() [*c]scm_t_rstate;
pub extern fn scm_c_uniform01([*c]scm_t_rstate) f64;
pub extern fn scm_c_normal01([*c]scm_t_rstate) f64;
pub extern fn scm_c_exp1([*c]scm_t_rstate) f64;
pub extern fn scm_c_random([*c]scm_t_rstate, m: u32) u32;
pub extern fn scm_c_random64(state: [*c]scm_t_rstate, m: u64) u64;
pub extern fn scm_c_random_bignum([*c]scm_t_rstate, m: SCM) SCM;
pub extern var scm_tc16_rstate: scm_t_bits;
pub extern var scm_masktab: [256]u8;
pub extern var scm_var_random_state: SCM;
pub extern fn scm_random(n: SCM, state: SCM) SCM;
pub extern fn scm_copy_random_state(state: SCM) SCM;
pub extern fn scm_seed_to_random_state(seed: SCM) SCM;
pub extern fn scm_datum_to_random_state(datum: SCM) SCM;
pub extern fn scm_random_state_to_datum(state: SCM) SCM;
pub extern fn scm_random_state_from_platform() SCM;
pub extern fn scm_random_uniform(state: SCM) SCM;
pub extern fn scm_random_solid_sphere_x(v: SCM, state: SCM) SCM;
pub extern fn scm_random_hollow_sphere_x(v: SCM, state: SCM) SCM;
pub extern fn scm_random_normal(state: SCM) SCM;
pub extern fn scm_random_normal_vector_x(v: SCM, state: SCM) SCM;
pub extern fn scm_random_exp(state: SCM) SCM;
pub extern fn scm_init_random() void;
pub extern fn scm_i_random_bytes_from_platform(buf: [*c]u8, len: usize) void;
pub extern var scm_sym_dot: SCM;
pub extern fn scm_primitive_read(port: SCM) SCM;
pub extern fn scm_read_options(setting: SCM) SCM;
pub extern fn scm_read(port: SCM) SCM;
pub extern fn scm_read_hash_extend(chr: SCM, proc: SCM) SCM;
pub extern fn scm_i_scan_for_encoding(port: SCM) [*c]u8;
pub extern fn scm_file_encoding(port: SCM) SCM;
pub extern fn scm_i_input_error(func: [*c]const u8, port: SCM, message: [*c]const u8, arg: SCM) noreturn;
pub extern fn scm_init_read() void;
pub extern fn scm_sigaction(signum: SCM, handler: SCM, flags: SCM) SCM;
pub extern fn scm_sigaction_for_thread(signum: SCM, handler: SCM, flags: SCM, thread: SCM) SCM;
pub extern fn scm_restore_signals() SCM;
pub extern fn scm_alarm(i: SCM) SCM;
pub extern fn scm_setitimer(which_timer: SCM, interval_seconds: SCM, interval_microseconds: SCM, value_seconds: SCM, value_microseconds: SCM) SCM;
pub extern fn scm_getitimer(which_timer: SCM) SCM;
pub extern fn scm_pause() SCM;
pub extern fn scm_sleep(i: SCM) SCM;
pub extern fn scm_usleep(i: SCM) SCM;
pub extern fn scm_raise(sig: SCM) SCM;
pub extern fn scm_init_scmsigs() void;
pub extern fn scm_i_close_signal_pipe() void;
pub extern fn scm_i_ensure_signal_delivery_thread() void;
pub extern var scm_i_signal_delivery_thread: [*c]scm_thread;
pub extern fn scm_get_meta_args(argc: c_int, argv: [*c][*c]u8) [*c][*c]u8;
pub extern fn scm_count_argv(argv: [*c][*c]u8) c_int;
pub extern fn scm_shell_usage(fatal: c_int, message: [*c]u8) void;
pub extern fn scm_compile_shell_switches(argc: c_int, argv: [*c][*c]u8) SCM;
pub extern fn scm_shell(argc: c_int, argv: [*c][*c]u8) void;
pub extern var scm_usage_name: [*c]u8;
pub extern fn scm_i_set_boot_program_arguments(argc: c_int, argv: [*c][*c]u8) void;
pub extern fn scm_init_script() void;
pub extern fn scm_system(cmd: SCM) SCM;
pub extern fn scm_getenv(nam: SCM) SCM;
pub extern fn scm_primitive_exit(status: SCM) SCM;
pub extern fn scm_primitive__exit(status: SCM) SCM;
pub extern fn scm_getenv_int(@"var": [*c]const u8, def: c_int) c_int;
pub extern fn scm_init_simpos() void;
pub extern fn scm_inet_netof(address: SCM) SCM;
pub extern fn scm_lnaof(address: SCM) SCM;
pub extern fn scm_inet_makeaddr(net: SCM, lna: SCM) SCM;
pub extern fn scm_inet_pton(family: SCM, address: SCM) SCM;
pub extern fn scm_inet_ntop(family: SCM, address: SCM) SCM;
pub extern fn scm_socket(family: SCM, style: SCM, proto: SCM) SCM;
pub extern fn scm_socketpair(family: SCM, style: SCM, proto: SCM) SCM;
pub extern fn scm_getsockopt(sfd: SCM, level: SCM, optname: SCM) SCM;
pub extern fn scm_setsockopt(sfd: SCM, level: SCM, optname: SCM, value: SCM) SCM;
pub extern fn scm_shutdown(sfd: SCM, how: SCM) SCM;
pub extern fn scm_connect(sockfd: SCM, fam: SCM, address: SCM, args: SCM) SCM;
pub extern fn scm_bind(sockfd: SCM, fam: SCM, address: SCM, args: SCM) SCM;
pub extern fn scm_listen(sfd: SCM, backlog: SCM) SCM;
pub extern fn scm_accept4(sockfd: SCM, flags: SCM) SCM;
pub extern fn scm_accept(sockfd: SCM) SCM;
pub extern fn scm_getsockname(sockfd: SCM) SCM;
pub extern fn scm_getpeername(sockfd: SCM) SCM;
pub extern fn scm_recv(sockfd: SCM, buff_or_size: SCM, flags: SCM) SCM;
pub extern fn scm_send(sockfd: SCM, message: SCM, flags: SCM) SCM;
pub extern fn scm_recvfrom(sockfd: SCM, buff_or_size: SCM, flags: SCM, offset: SCM, length: SCM) SCM;
pub extern fn scm_sendto(sockfd: SCM, message: SCM, fam: SCM, address: SCM, args_and_flags: SCM) SCM;
pub extern fn scm_init_socket() void;
pub const struct_sockaddr = opaque {};
pub extern fn scm_from_sockaddr(address: ?*const struct_sockaddr, addr_size: c_uint) SCM;
pub extern fn scm_to_sockaddr(address: SCM, adress_size: [*c]usize) ?*struct_sockaddr;
pub extern fn scm_c_make_socket_address(family: SCM, address: SCM, args: SCM, address_size: [*c]usize) ?*struct_sockaddr;
pub extern fn scm_make_socket_address(family: SCM, address: SCM, args: SCM) SCM;
pub extern fn scm_restricted_vector_sort_x(vec: SCM, less: SCM, startpos: SCM, endpos: SCM) SCM;
pub extern fn scm_sorted_p(ls: SCM, less: SCM) SCM;
pub extern fn scm_merge(ls1: SCM, ls2: SCM, less: SCM) SCM;
pub extern fn scm_merge_x(ls1: SCM, ls2: SCM, less: SCM) SCM;
pub extern fn scm_sort(ls: SCM, less: SCM) SCM;
pub extern fn scm_sort_x(ls: SCM, less: SCM) SCM;
pub extern fn scm_stable_sort(ls: SCM, less: SCM) SCM;
pub extern fn scm_stable_sort_x(ls: SCM, less: SCM) SCM;
pub extern fn scm_sort_list(ls: SCM, less: SCM) SCM;
pub extern fn scm_sort_list_x(ls: SCM, less: SCM) SCM;
pub extern fn scm_init_sort() void;
pub extern var scm_sym_filename: SCM;
pub extern var scm_sym_line: SCM;
pub extern var scm_sym_column: SCM;
pub extern fn scm_supports_source_properties_p(obj: SCM) SCM;
pub extern fn scm_source_property(obj: SCM, key: SCM) SCM;
pub extern fn scm_set_source_property_x(obj: SCM, key: SCM, datum: SCM) SCM;
pub extern fn scm_source_properties(obj: SCM) SCM;
pub extern fn scm_set_source_properties_x(obj: SCM, props: SCM) SCM;
pub extern fn scm_i_make_srcprops(line: SCM, col: SCM, fname: SCM, alist: SCM) SCM;
pub extern fn scm_i_has_source_properties(obj: SCM) c_int;
pub extern fn scm_i_set_source_properties_x(obj: SCM, line: SCM, col: SCM, fname: SCM) void;
pub extern fn scm_cons_source(xorig: SCM, x: SCM, y: SCM) SCM;
pub extern fn scm_init_srcprop() void;
pub extern fn scm_local_eval(exp: SCM, env: SCM) SCM;
pub extern fn scm_reverse_lookup(env: SCM, data: SCM) SCM;
pub extern fn scm_debug_options(setting: SCM) SCM;
pub extern fn scm_init_debug() void;
pub extern var scm_stack_checking_enabled_p: c_int;
pub extern fn scm_stack_size(start: [*c]SCM_STACKITEM) c_long;
pub extern fn scm_stack_report() void;
pub extern fn scm_sys_get_stack_size() SCM;
pub extern fn scm_init_stackchk() void;
pub extern var scm_c_time_units_per_second: c_long;
pub extern fn scm_c_get_internal_run_time() c_long;
pub extern fn scm_get_internal_real_time() SCM;
pub extern fn scm_get_internal_run_time() SCM;
pub extern fn scm_current_time() SCM;
pub extern fn scm_gettimeofday() SCM;
pub extern fn scm_localtime(time: SCM, zone: SCM) SCM;
pub extern fn scm_gmtime(time: SCM) SCM;
pub extern fn scm_mktime(sbd_time: SCM, zone: SCM) SCM;
pub extern fn scm_tzset() SCM;
pub extern fn scm_times() SCM;
pub extern fn scm_strftime(format: SCM, stime: SCM) SCM;
pub extern fn scm_strptime(format: SCM, string: SCM) SCM;
pub extern fn scm_init_stime() void;
pub extern fn scm_string_null_p(s: SCM) SCM;
pub extern fn scm_string_any(pred: SCM, s: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_every(pred: SCM, s: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_tabulate(proc: SCM, len: SCM) SCM;
pub extern fn scm_string_to_list(str: SCM) SCM;
pub extern fn scm_substring_to_list(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_reverse_list_to_string(chrs: SCM) SCM;
pub extern fn scm_string_join(ls: SCM, delimiter: SCM, grammar: SCM) SCM;
pub extern fn scm_string_copy(str: SCM) SCM;
pub extern fn scm_string_copy_x(target: SCM, tstart: SCM, s: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_substring_move_x(str1: SCM, start1: SCM, end1: SCM, str2: SCM, start2: SCM) SCM;
pub extern fn scm_string_take(s: SCM, n: SCM) SCM;
pub extern fn scm_string_drop(s: SCM, n: SCM) SCM;
pub extern fn scm_string_take_right(s: SCM, n: SCM) SCM;
pub extern fn scm_string_drop_right(s: SCM, n: SCM) SCM;
pub extern fn scm_string_pad(s: SCM, len: SCM, chr: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_pad_right(s: SCM, len: SCM, chr: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_trim(s: SCM, char_pred: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_trim_right(s: SCM, char_pred: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_trim_both(s: SCM, char_pred: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_substring_fill_x(str: SCM, chr: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_fill_x(str: SCM, chr: SCM) SCM;
pub extern fn scm_string_compare(s1: SCM, s2: SCM, proc_lt: SCM, proc_eq: SCM, proc_gt: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_compare_ci(s1: SCM, s2: SCM, proc_lt: SCM, proc_eq: SCM, proc_gt: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_eq(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_neq(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_lt(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_gt(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_le(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_ge(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_ci_eq(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_ci_neq(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_ci_lt(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_ci_gt(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_ci_le(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_ci_ge(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_substring_hash(s: SCM, bound: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_substring_hash_ci(s: SCM, bound: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_prefix_length(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_prefix_length_ci(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_suffix_length(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_suffix_length_ci(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_prefix_p(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_prefix_ci_p(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_suffix_p(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_suffix_ci_p(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_index(s: SCM, char_pred: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_index_right(s: SCM, char_pred: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_rindex(s: SCM, char_pred: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_skip(s: SCM, char_pred: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_skip_right(s: SCM, char_pred: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_count(s: SCM, char_pred: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_contains(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_contains_ci(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_substring_upcase_x(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_substring_upcase(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_upcase_x(str: SCM) SCM;
pub extern fn scm_string_upcase(str: SCM) SCM;
pub extern fn scm_substring_downcase_x(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_substring_downcase(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_downcase_x(str: SCM) SCM;
pub extern fn scm_string_downcase(str: SCM) SCM;
pub extern fn scm_string_titlecase_x(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_titlecase(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_capitalize_x(str: SCM) SCM;
pub extern fn scm_string_capitalize(str: SCM) SCM;
pub extern fn scm_string_reverse(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_reverse_x(str: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_append_shared(ls: SCM) SCM;
pub extern fn scm_string_concatenate(ls: SCM) SCM;
pub extern fn scm_string_concatenate_shared(ls: SCM) SCM;
pub extern fn scm_string_concatenate_reverse(ls: SCM, final_string: SCM, end: SCM) SCM;
pub extern fn scm_string_concatenate_reverse_shared(ls: SCM, final_string: SCM, end: SCM) SCM;
pub extern fn scm_string_map(proc: SCM, s: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_map_x(proc: SCM, s: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_fold(kons: SCM, knil: SCM, s: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_fold_right(kons: SCM, knil: SCM, s: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_unfold(p: SCM, f: SCM, g: SCM, seed: SCM, base: SCM, make_final: SCM) SCM;
pub extern fn scm_string_unfold_right(p: SCM, f: SCM, g: SCM, seed: SCM, base: SCM, make_final: SCM) SCM;
pub extern fn scm_string_for_each(proc: SCM, s: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_for_each_index(proc: SCM, s: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_xsubstring(s: SCM, from: SCM, to: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_xcopy_x(target: SCM, tstart: SCM, s: SCM, sfrom: SCM, sto: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_replace(s1: SCM, s2: SCM, start1: SCM, end1: SCM, start2: SCM, end2: SCM) SCM;
pub extern fn scm_string_tokenize(s: SCM, token_char: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_split(s: SCM, char_pred: SCM) SCM;
pub extern fn scm_string_filter(char_pred: SCM, s: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_string_delete(char_pred: SCM, s: SCM, start: SCM, end: SCM) SCM;
pub extern fn scm_init_srfi_13() void;
pub extern fn scm_init_srfi_13_14() void;
pub const scm_t_char_range = extern struct {
    lo: scm_t_wchar,
    hi: scm_t_wchar,
};
pub const scm_t_char_set = extern struct {
    len: usize,
    ranges: [*c]scm_t_char_range,
};
pub const scm_t_char_set_cursor = extern struct {
    range: usize,
    n: scm_t_wchar,
};
pub extern var scm_tc16_charset: c_int;
pub extern fn scm_i_charset_get(cs: [*c]scm_t_char_set, n: scm_t_wchar) c_int;
pub extern fn scm_i_charset_set(cs: [*c]scm_t_char_set, n: scm_t_wchar) void;
pub extern fn scm_i_charset_unset(cs: [*c]scm_t_char_set, n: scm_t_wchar) void;
pub extern fn scm_char_set_p(obj: SCM) SCM;
pub extern fn scm_char_set_eq(char_sets: SCM) SCM;
pub extern fn scm_char_set_leq(char_sets: SCM) SCM;
pub extern fn scm_char_set_hash(cs: SCM, bound: SCM) SCM;
pub extern fn scm_char_set_cursor(cs: SCM) SCM;
pub extern fn scm_char_set_ref(cs: SCM, cursor: SCM) SCM;
pub extern fn scm_char_set_cursor_next(cs: SCM, cursor: SCM) SCM;
pub extern fn scm_end_of_char_set_p(cursor: SCM) SCM;
pub extern fn scm_char_set_fold(kons: SCM, knil: SCM, cs: SCM) SCM;
pub extern fn scm_char_set_unfold(p: SCM, f: SCM, g: SCM, seed: SCM, base_cs: SCM) SCM;
pub extern fn scm_char_set_unfold_x(p: SCM, f: SCM, g: SCM, seed: SCM, base_cs: SCM) SCM;
pub extern fn scm_char_set_for_each(proc: SCM, cs: SCM) SCM;
pub extern fn scm_char_set_map(proc: SCM, cs: SCM) SCM;
pub extern fn scm_char_set_copy(cs: SCM) SCM;
pub extern fn scm_char_set(rest: SCM) SCM;
pub extern fn scm_list_to_char_set(list: SCM, base_cs: SCM) SCM;
pub extern fn scm_list_to_char_set_x(list: SCM, base_cs: SCM) SCM;
pub extern fn scm_string_to_char_set(str: SCM, base_cs: SCM) SCM;
pub extern fn scm_string_to_char_set_x(str: SCM, base_cs: SCM) SCM;
pub extern fn scm_char_set_filter(pred: SCM, cs: SCM, base_cs: SCM) SCM;
pub extern fn scm_char_set_filter_x(pred: SCM, cs: SCM, base_cs: SCM) SCM;
pub extern fn scm_ucs_range_to_char_set(lower: SCM, upper: SCM, @"error": SCM, base_cs: SCM) SCM;
pub extern fn scm_ucs_range_to_char_set_x(lower: SCM, upper: SCM, @"error": SCM, base_cs: SCM) SCM;
pub extern fn scm_to_char_set(x: SCM) SCM;
pub extern fn scm_char_set_size(cs: SCM) SCM;
pub extern fn scm_char_set_count(pred: SCM, cs: SCM) SCM;
pub extern fn scm_char_set_to_list(cs: SCM) SCM;
pub extern fn scm_char_set_to_string(cs: SCM) SCM;
pub extern fn scm_char_set_contains_p(cs: SCM, ch: SCM) SCM;
pub extern fn scm_char_set_every(pred: SCM, cs: SCM) SCM;
pub extern fn scm_char_set_any(pred: SCM, cs: SCM) SCM;
pub extern fn scm_char_set_adjoin(cs: SCM, rest: SCM) SCM;
pub extern fn scm_char_set_delete(cs: SCM, rest: SCM) SCM;
pub extern fn scm_char_set_adjoin_x(cs: SCM, rest: SCM) SCM;
pub extern fn scm_char_set_delete_x(cs: SCM, rest: SCM) SCM;
pub extern fn scm_char_set_complement(cs: SCM) SCM;
pub extern fn scm_char_set_union(rest: SCM) SCM;
pub extern fn scm_char_set_intersection(rest: SCM) SCM;
pub extern fn scm_char_set_difference(cs1: SCM, rest: SCM) SCM;
pub extern fn scm_char_set_xor(rest: SCM) SCM;
pub extern fn scm_char_set_diff_plus_intersection(cs1: SCM, rest: SCM) SCM;
pub extern fn scm_char_set_complement_x(cs: SCM) SCM;
pub extern fn scm_char_set_union_x(cs1: SCM, rest: SCM) SCM;
pub extern fn scm_char_set_intersection_x(cs1: SCM, rest: SCM) SCM;
pub extern fn scm_char_set_difference_x(cs1: SCM, rest: SCM) SCM;
pub extern fn scm_char_set_xor_x(cs1: SCM, rest: SCM) SCM;
pub extern fn scm_char_set_diff_plus_intersection_x(cs1: SCM, cs2: SCM, rest: SCM) SCM;
pub extern fn scm_sys_char_set_dump(charset: SCM) SCM;
pub extern var scm_char_set_lower_case: SCM;
pub extern var scm_char_set_upper_case: SCM;
pub extern var scm_char_set_title_case: SCM;
pub extern var scm_char_set_letter: SCM;
pub extern var scm_char_set_digit: SCM;
pub extern var scm_char_set_letter_and_digit: SCM;
pub extern var scm_char_set_graphic: SCM;
pub extern var scm_char_set_printing: SCM;
pub extern var scm_char_set_whitespace: SCM;
pub extern var scm_char_set_iso_control: SCM;
pub extern var scm_char_set_punctuation: SCM;
pub extern var scm_char_set_symbol: SCM;
pub extern var scm_char_set_hex_digit: SCM;
pub extern var scm_char_set_blank: SCM;
pub extern var scm_char_set_ascii: SCM;
pub extern var scm_char_set_empty: SCM;
pub extern var scm_char_set_full: SCM;
pub extern fn scm_init_srfi_14() void;
pub extern fn scm_string_equal_p(s1: SCM, s2: SCM) SCM;
pub extern fn scm_string_ci_equal_p(s1: SCM, s2: SCM) SCM;
pub extern fn scm_string_less_p(s1: SCM, s2: SCM) SCM;
pub extern fn scm_string_leq_p(s1: SCM, s2: SCM) SCM;
pub extern fn scm_string_gr_p(s1: SCM, s2: SCM) SCM;
pub extern fn scm_string_geq_p(s1: SCM, s2: SCM) SCM;
pub extern fn scm_string_ci_less_p(s1: SCM, s2: SCM) SCM;
pub extern fn scm_string_ci_leq_p(s1: SCM, s2: SCM) SCM;
pub extern fn scm_string_ci_gr_p(s1: SCM, s2: SCM) SCM;
pub extern fn scm_string_ci_geq_p(s1: SCM, s2: SCM) SCM;
pub extern fn scm_init_strorder() void;
pub extern var scm_string_port_type: ?*scm_t_port_type;
pub extern fn scm_mkstrport(pos: SCM, str: SCM, modes: c_long, caller: [*c]const u8) SCM;
pub extern fn scm_strport_to_string(port: SCM) SCM;
pub extern fn scm_object_to_string(obj: SCM, printer: SCM) SCM;
pub extern fn scm_call_with_output_string(proc: SCM) SCM;
pub extern fn scm_call_with_input_string(str: SCM, proc: SCM) SCM;
pub extern fn scm_open_input_string(str: SCM) SCM;
pub extern fn scm_open_output_string() SCM;
pub extern fn scm_get_output_string(port: SCM) SCM;
pub extern fn scm_c_read_string(expr: [*c]const u8) SCM;
pub extern fn scm_c_eval_string(expr: [*c]const u8) SCM;
pub extern fn scm_c_eval_string_in_module(expr: [*c]const u8, module: SCM) SCM;
pub extern fn scm_eval_string(string: SCM) SCM;
pub extern fn scm_eval_string_in_module(string: SCM, module: SCM) SCM;
pub extern fn scm_init_strports() void;
pub extern fn scm_symbol_p(x: SCM) SCM;
pub extern fn scm_symbol_interned_p(sym: SCM) SCM;
pub extern fn scm_make_symbol(name: SCM) SCM;
pub extern fn scm_symbol_to_string(s: SCM) SCM;
pub extern fn scm_string_to_symbol(s: SCM) SCM;
pub extern fn scm_string_ci_to_symbol(s: SCM) SCM;
pub extern fn scm_symbol_hash(s: SCM) SCM;
pub extern fn scm_gensym(prefix: SCM) SCM;
pub extern fn scm_from_locale_symbol(str: [*c]const u8) SCM;
pub extern fn scm_from_locale_symboln(str: [*c]const u8, len: usize) SCM;
pub extern fn scm_take_locale_symbol(sym: [*c]u8) SCM;
pub extern fn scm_take_locale_symboln(sym: [*c]u8, len: usize) SCM;
pub extern fn scm_from_latin1_symbol(str: [*c]const u8) SCM;
pub extern fn scm_from_latin1_symboln(str: [*c]const u8, len: usize) SCM;
pub extern fn scm_take_latin1_symbol(sym: [*c]u8) SCM;
pub extern fn scm_take_latin1_symboln(sym: [*c]u8, len: usize) SCM;
pub extern fn scm_from_utf8_symbol(str: [*c]const u8) SCM;
pub extern fn scm_from_utf8_symboln(str: [*c]const u8, len: usize) SCM;
pub extern fn scm_take_utf8_symbol(sym: [*c]u8) SCM;
pub extern fn scm_take_utf8_symboln(sym: [*c]u8, len: usize) SCM;
pub extern fn scm_i_hash_symbol(obj: SCM, n: c_ulong, closure: ?*anyopaque) c_ulong;
pub extern fn scm_symbols_prehistory() void;
pub extern fn scm_init_symbols() void;
pub fn scm_is_values(arg_x: SCM) callconv(.C) c_int {
    var x = arg_x;
    return @boolToInt(!((@bitCast(c_ulong, @as(c_long, @as(c_int, 6))) & @intCast(scm_t_bits, @ptrToInt(if (false) blk: {
        const tmp = x;
        @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp;
        break :blk tmp;
    } else x))) != 0) and ((@bitCast(c_ulong, @as(c_long, @as(c_int, 127))) & @intCast(scm_t_bits, @ptrToInt(if (false) blk: {
        const tmp = (blk_1: {
            const tmp_2 = @as(c_int, 0);
            if (tmp_2 >= 0) break :blk_1 @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_2: {
                const tmp_3 = x;
                @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_3;
                break :blk_2 tmp_3;
            } else x))))))) + @intCast(usize, tmp_2) else break :blk_1 @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_2: {
                const tmp_3 = x;
                @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_3;
                break :blk_2 tmp_3;
            } else x))))))) - ~@bitCast(usize, @intCast(isize, tmp_2) +% -1);
        }).*;
        @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp;
        break :blk tmp;
    } else (blk: {
        const tmp = @as(c_int, 0);
        if (tmp >= 0) break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = x;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else x))))))) + @intCast(usize, tmp) else break :blk @ptrCast([*c]SCM, @alignCast(@import("std").meta.alignment(SCM), @ptrCast([*c]scm_t_cell, @alignCast(@import("std").meta.alignment(scm_t_cell), @intToPtr([*c]scm_t_bits, @intCast(scm_t_bits, @ptrToInt(if (false) blk_1: {
            const tmp_2 = x;
            @intToPtr([*c]volatile SCM, @as(c_int, 0)).* = tmp_2;
            break :blk_1 tmp_2;
        } else x))))))) - ~@bitCast(usize, @intCast(isize, tmp) +% -1);
    }).*))) == @bitCast(c_ulong, @as(c_long, @as(c_int, 63)))));
}
pub extern fn scm_i_extract_values_2(obj: SCM, p1: [*c]SCM, p2: [*c]SCM) void;
pub extern fn scm_values(args: SCM) SCM;
pub extern fn scm_c_values(base: [*c]SCM, n: usize) SCM;
pub extern fn scm_values_2(a: SCM, b: SCM) SCM;
pub extern fn scm_values_3(a: SCM, b: SCM, c: SCM) SCM;
pub extern fn scm_c_nvalues(obj: SCM) usize;
pub extern fn scm_c_value_ref(obj: SCM, idx: usize) SCM;
pub extern fn scm_init_values() void;
pub extern fn scm_make_variable(init: SCM) SCM;
pub extern fn scm_make_undefined_variable() SCM;
pub extern fn scm_variable_p(obj: SCM) SCM;
pub extern fn scm_variable_ref(@"var": SCM) SCM;
pub extern fn scm_variable_set_x(@"var": SCM, val: SCM) SCM;
pub extern fn scm_variable_unset_x(@"var": SCM) SCM;
pub extern fn scm_variable_bound_p(@"var": SCM) SCM;
pub extern fn scm_i_variable_print(@"var": SCM, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_init_variable() void;
pub extern fn scm_make_srfi_4_vector(@"type": SCM, len: SCM, fill: SCM) SCM;
pub extern fn scm_srfi_4_vector_type_size(vec: SCM) SCM;
pub extern fn scm_u8vector_p(obj: SCM) SCM;
pub extern fn scm_make_u8vector(n: SCM, fill: SCM) SCM;
pub extern fn scm_take_u8vector(data: [*c]u8, n: usize) SCM;
pub extern fn scm_u8vector(l: SCM) SCM;
pub extern fn scm_u8vector_length(uvec: SCM) SCM;
pub extern fn scm_u8vector_ref(uvec: SCM, index: SCM) SCM;
pub extern fn scm_u8vector_set_x(uvec: SCM, index: SCM, value: SCM) SCM;
pub extern fn scm_u8vector_to_list(uvec: SCM) SCM;
pub extern fn scm_list_to_u8vector(l: SCM) SCM;
pub extern fn scm_any_to_u8vector(obj: SCM) SCM;
pub extern fn scm_array_handle_u8_elements(h: [*c]scm_t_array_handle) [*c]const u8;
pub extern fn scm_array_handle_u8_writable_elements(h: [*c]scm_t_array_handle) [*c]u8;
pub extern fn scm_u8vector_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const u8;
pub extern fn scm_u8vector_writable_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]u8;
pub extern fn scm_s8vector_p(obj: SCM) SCM;
pub extern fn scm_make_s8vector(n: SCM, fill: SCM) SCM;
pub extern fn scm_take_s8vector(data: [*c]i8, n: usize) SCM;
pub extern fn scm_s8vector(l: SCM) SCM;
pub extern fn scm_s8vector_length(uvec: SCM) SCM;
pub extern fn scm_s8vector_ref(uvec: SCM, index: SCM) SCM;
pub extern fn scm_s8vector_set_x(uvec: SCM, index: SCM, value: SCM) SCM;
pub extern fn scm_s8vector_to_list(uvec: SCM) SCM;
pub extern fn scm_list_to_s8vector(l: SCM) SCM;
pub extern fn scm_any_to_s8vector(obj: SCM) SCM;
pub extern fn scm_array_handle_s8_elements(h: [*c]scm_t_array_handle) [*c]const i8;
pub extern fn scm_array_handle_s8_writable_elements(h: [*c]scm_t_array_handle) [*c]i8;
pub extern fn scm_s8vector_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const i8;
pub extern fn scm_s8vector_writable_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]i8;
pub extern fn scm_u16vector_p(obj: SCM) SCM;
pub extern fn scm_make_u16vector(n: SCM, fill: SCM) SCM;
pub extern fn scm_take_u16vector(data: [*c]u16, n: usize) SCM;
pub extern fn scm_u16vector(l: SCM) SCM;
pub extern fn scm_u16vector_length(uvec: SCM) SCM;
pub extern fn scm_u16vector_ref(uvec: SCM, index: SCM) SCM;
pub extern fn scm_u16vector_set_x(uvec: SCM, index: SCM, value: SCM) SCM;
pub extern fn scm_u16vector_to_list(uvec: SCM) SCM;
pub extern fn scm_list_to_u16vector(l: SCM) SCM;
pub extern fn scm_any_to_u16vector(obj: SCM) SCM;
pub extern fn scm_array_handle_u16_elements(h: [*c]scm_t_array_handle) [*c]const u16;
pub extern fn scm_array_handle_u16_writable_elements(h: [*c]scm_t_array_handle) [*c]u16;
pub extern fn scm_u16vector_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const u16;
pub extern fn scm_u16vector_writable_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]u16;
pub extern fn scm_s16vector_p(obj: SCM) SCM;
pub extern fn scm_make_s16vector(n: SCM, fill: SCM) SCM;
pub extern fn scm_take_s16vector(data: [*c]i16, n: usize) SCM;
pub extern fn scm_s16vector(l: SCM) SCM;
pub extern fn scm_s16vector_length(uvec: SCM) SCM;
pub extern fn scm_s16vector_ref(uvec: SCM, index: SCM) SCM;
pub extern fn scm_s16vector_set_x(uvec: SCM, index: SCM, value: SCM) SCM;
pub extern fn scm_s16vector_to_list(uvec: SCM) SCM;
pub extern fn scm_list_to_s16vector(l: SCM) SCM;
pub extern fn scm_any_to_s16vector(obj: SCM) SCM;
pub extern fn scm_array_handle_s16_elements(h: [*c]scm_t_array_handle) [*c]const i16;
pub extern fn scm_array_handle_s16_writable_elements(h: [*c]scm_t_array_handle) [*c]i16;
pub extern fn scm_s16vector_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const i16;
pub extern fn scm_s16vector_writable_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]i16;
pub extern fn scm_u32vector_p(obj: SCM) SCM;
pub extern fn scm_make_u32vector(n: SCM, fill: SCM) SCM;
pub extern fn scm_take_u32vector(data: [*c]u32, n: usize) SCM;
pub extern fn scm_u32vector(l: SCM) SCM;
pub extern fn scm_u32vector_length(uvec: SCM) SCM;
pub extern fn scm_u32vector_ref(uvec: SCM, index: SCM) SCM;
pub extern fn scm_u32vector_set_x(uvec: SCM, index: SCM, value: SCM) SCM;
pub extern fn scm_u32vector_to_list(uvec: SCM) SCM;
pub extern fn scm_list_to_u32vector(l: SCM) SCM;
pub extern fn scm_any_to_u32vector(obj: SCM) SCM;
pub extern fn scm_array_handle_u32_elements(h: [*c]scm_t_array_handle) [*c]const u32;
pub extern fn scm_array_handle_u32_writable_elements(h: [*c]scm_t_array_handle) [*c]u32;
pub extern fn scm_u32vector_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const u32;
pub extern fn scm_u32vector_writable_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]u32;
pub extern fn scm_s32vector_p(obj: SCM) SCM;
pub extern fn scm_make_s32vector(n: SCM, fill: SCM) SCM;
pub extern fn scm_take_s32vector(data: [*c]i32, n: usize) SCM;
pub extern fn scm_s32vector(l: SCM) SCM;
pub extern fn scm_s32vector_length(uvec: SCM) SCM;
pub extern fn scm_s32vector_ref(uvec: SCM, index: SCM) SCM;
pub extern fn scm_s32vector_set_x(uvec: SCM, index: SCM, value: SCM) SCM;
pub extern fn scm_s32vector_to_list(uvec: SCM) SCM;
pub extern fn scm_list_to_s32vector(l: SCM) SCM;
pub extern fn scm_any_to_s32vector(obj: SCM) SCM;
pub extern fn scm_array_handle_s32_elements(h: [*c]scm_t_array_handle) [*c]const i32;
pub extern fn scm_array_handle_s32_writable_elements(h: [*c]scm_t_array_handle) [*c]i32;
pub extern fn scm_s32vector_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const i32;
pub extern fn scm_s32vector_writable_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]i32;
pub extern fn scm_u64vector_p(obj: SCM) SCM;
pub extern fn scm_make_u64vector(n: SCM, fill: SCM) SCM;
pub extern fn scm_take_u64vector(data: [*c]u64, n: usize) SCM;
pub extern fn scm_u64vector(l: SCM) SCM;
pub extern fn scm_u64vector_length(uvec: SCM) SCM;
pub extern fn scm_u64vector_ref(uvec: SCM, index: SCM) SCM;
pub extern fn scm_u64vector_set_x(uvec: SCM, index: SCM, value: SCM) SCM;
pub extern fn scm_u64vector_to_list(uvec: SCM) SCM;
pub extern fn scm_list_to_u64vector(l: SCM) SCM;
pub extern fn scm_any_to_u64vector(obj: SCM) SCM;
pub extern fn scm_array_handle_u64_elements(h: [*c]scm_t_array_handle) [*c]const u64;
pub extern fn scm_array_handle_u64_writable_elements(h: [*c]scm_t_array_handle) [*c]u64;
pub extern fn scm_u64vector_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const u64;
pub extern fn scm_u64vector_writable_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]u64;
pub extern fn scm_s64vector_p(obj: SCM) SCM;
pub extern fn scm_make_s64vector(n: SCM, fill: SCM) SCM;
pub extern fn scm_take_s64vector(data: [*c]i64, n: usize) SCM;
pub extern fn scm_s64vector(l: SCM) SCM;
pub extern fn scm_s64vector_length(uvec: SCM) SCM;
pub extern fn scm_s64vector_ref(uvec: SCM, index: SCM) SCM;
pub extern fn scm_s64vector_set_x(uvec: SCM, index: SCM, value: SCM) SCM;
pub extern fn scm_s64vector_to_list(uvec: SCM) SCM;
pub extern fn scm_list_to_s64vector(l: SCM) SCM;
pub extern fn scm_any_to_s64vector(obj: SCM) SCM;
pub extern fn scm_array_handle_s64_elements(h: [*c]scm_t_array_handle) [*c]const i64;
pub extern fn scm_array_handle_s64_writable_elements(h: [*c]scm_t_array_handle) [*c]i64;
pub extern fn scm_s64vector_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const i64;
pub extern fn scm_s64vector_writable_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]i64;
pub extern fn scm_f32vector_p(obj: SCM) SCM;
pub extern fn scm_make_f32vector(n: SCM, fill: SCM) SCM;
pub extern fn scm_take_f32vector(data: [*c]f32, n: usize) SCM;
pub extern fn scm_f32vector(l: SCM) SCM;
pub extern fn scm_f32vector_length(uvec: SCM) SCM;
pub extern fn scm_f32vector_ref(uvec: SCM, index: SCM) SCM;
pub extern fn scm_f32vector_set_x(uvec: SCM, index: SCM, value: SCM) SCM;
pub extern fn scm_f32vector_to_list(uvec: SCM) SCM;
pub extern fn scm_list_to_f32vector(l: SCM) SCM;
pub extern fn scm_any_to_f32vector(obj: SCM) SCM;
pub extern fn scm_array_handle_f32_elements(h: [*c]scm_t_array_handle) [*c]const f32;
pub extern fn scm_array_handle_f32_writable_elements(h: [*c]scm_t_array_handle) [*c]f32;
pub extern fn scm_f32vector_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const f32;
pub extern fn scm_f32vector_writable_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]f32;
pub extern fn scm_f64vector_p(obj: SCM) SCM;
pub extern fn scm_make_f64vector(n: SCM, fill: SCM) SCM;
pub extern fn scm_take_f64vector(data: [*c]f64, n: usize) SCM;
pub extern fn scm_f64vector(l: SCM) SCM;
pub extern fn scm_f64vector_length(uvec: SCM) SCM;
pub extern fn scm_f64vector_ref(uvec: SCM, index: SCM) SCM;
pub extern fn scm_f64vector_set_x(uvec: SCM, index: SCM, value: SCM) SCM;
pub extern fn scm_f64vector_to_list(uvec: SCM) SCM;
pub extern fn scm_list_to_f64vector(l: SCM) SCM;
pub extern fn scm_any_to_f64vector(obj: SCM) SCM;
pub extern fn scm_array_handle_f64_elements(h: [*c]scm_t_array_handle) [*c]const f64;
pub extern fn scm_array_handle_f64_writable_elements(h: [*c]scm_t_array_handle) [*c]f64;
pub extern fn scm_f64vector_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const f64;
pub extern fn scm_f64vector_writable_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]f64;
pub extern fn scm_c32vector_p(obj: SCM) SCM;
pub extern fn scm_make_c32vector(n: SCM, fill: SCM) SCM;
pub extern fn scm_take_c32vector(data: [*c]f32, n: usize) SCM;
pub extern fn scm_c32vector(l: SCM) SCM;
pub extern fn scm_c32vector_length(uvec: SCM) SCM;
pub extern fn scm_c32vector_ref(uvec: SCM, index: SCM) SCM;
pub extern fn scm_c32vector_set_x(uvec: SCM, index: SCM, value: SCM) SCM;
pub extern fn scm_c32vector_to_list(uvec: SCM) SCM;
pub extern fn scm_list_to_c32vector(l: SCM) SCM;
pub extern fn scm_any_to_c32vector(obj: SCM) SCM;
pub extern fn scm_array_handle_c32_elements(h: [*c]scm_t_array_handle) [*c]const f32;
pub extern fn scm_array_handle_c32_writable_elements(h: [*c]scm_t_array_handle) [*c]f32;
pub extern fn scm_c32vector_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const f32;
pub extern fn scm_c32vector_writable_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]f32;
pub extern fn scm_c64vector_p(obj: SCM) SCM;
pub extern fn scm_make_c64vector(n: SCM, fill: SCM) SCM;
pub extern fn scm_take_c64vector(data: [*c]f64, n: usize) SCM;
pub extern fn scm_c64vector(l: SCM) SCM;
pub extern fn scm_c64vector_length(uvec: SCM) SCM;
pub extern fn scm_c64vector_ref(uvec: SCM, index: SCM) SCM;
pub extern fn scm_c64vector_set_x(uvec: SCM, index: SCM, value: SCM) SCM;
pub extern fn scm_c64vector_to_list(uvec: SCM) SCM;
pub extern fn scm_list_to_c64vector(l: SCM) SCM;
pub extern fn scm_any_to_c64vector(obj: SCM) SCM;
pub extern fn scm_array_handle_c64_elements(h: [*c]scm_t_array_handle) [*c]const f64;
pub extern fn scm_array_handle_c64_writable_elements(h: [*c]scm_t_array_handle) [*c]f64;
pub extern fn scm_c64vector_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]const f64;
pub extern fn scm_c64vector_writable_elements(uvec: SCM, h: [*c]scm_t_array_handle, lenp: [*c]usize, incp: [*c]isize) [*c]f64;
pub extern fn scm_bootstrap_srfi_4() void;
pub extern fn scm_init_srfi_4() void;
pub extern fn scm_major_version() SCM;
pub extern fn scm_minor_version() SCM;
pub extern fn scm_micro_version() SCM;
pub extern fn scm_effective_version() SCM;
pub extern fn scm_version() SCM;
pub extern fn scm_init_version() void;
pub extern fn scm_make_soft_port(pv: SCM, modes: SCM) SCM;
pub extern fn scm_init_vports() void;
pub const scm_t_set_predicate_fn = ?fn (SCM, ?*anyopaque) callconv(.C) c_int;
pub const scm_t_set_fold_fn = ?fn (?*anyopaque, SCM, SCM) callconv(.C) SCM;
pub extern fn scm_c_make_weak_set(k: c_ulong) SCM;
pub extern fn scm_weak_set_p(h: SCM) SCM;
pub extern fn scm_c_weak_set_lookup(set: SCM, raw_hash: c_ulong, pred: scm_t_set_predicate_fn, closure: ?*anyopaque, dflt: SCM) SCM;
pub extern fn scm_c_weak_set_add_x(set: SCM, raw_hash: c_ulong, pred: scm_t_set_predicate_fn, closure: ?*anyopaque, obj: SCM) SCM;
pub extern fn scm_c_weak_set_remove_x(set: SCM, raw_hash: c_ulong, pred: scm_t_set_predicate_fn, closure: ?*anyopaque) void;
pub extern fn scm_weak_set_add_x(set: SCM, obj: SCM) SCM;
pub extern fn scm_weak_set_remove_x(set: SCM, obj: SCM) SCM;
pub extern fn scm_weak_set_clear_x(set: SCM) SCM;
pub extern fn scm_c_weak_set_fold(proc: scm_t_set_fold_fn, closure: ?*anyopaque, init: SCM, set: SCM) SCM;
pub extern fn scm_weak_set_fold(proc: SCM, init: SCM, set: SCM) SCM;
pub extern fn scm_weak_set_for_each(proc: SCM, set: SCM) SCM;
pub extern fn scm_weak_set_map_to_list(proc: SCM, set: SCM) SCM;
pub extern fn scm_i_weak_set_print(exp: SCM, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_init_weak_set() void;
pub const SCM_WEAK_TABLE_KIND_KEY: c_int = 0;
pub const SCM_WEAK_TABLE_KIND_VALUE: c_int = 1;
pub const SCM_WEAK_TABLE_KIND_BOTH: c_int = 2;
pub const scm_t_weak_table_kind = c_uint;
pub const scm_t_table_predicate_fn = ?fn (SCM, SCM, ?*anyopaque) callconv(.C) c_int;
pub const scm_t_table_fold_fn = ?fn (?*anyopaque, SCM, SCM, SCM) callconv(.C) SCM;
pub extern fn scm_c_make_weak_table(k: c_ulong, kind: scm_t_weak_table_kind) SCM;
pub extern fn scm_weak_table_p(h: SCM) SCM;
pub extern fn scm_c_weak_table_ref(table: SCM, raw_hash: c_ulong, pred: scm_t_table_predicate_fn, closure: ?*anyopaque, dflt: SCM) SCM;
pub extern fn scm_c_weak_table_put_x(table: SCM, raw_hash: c_ulong, pred: scm_t_table_predicate_fn, closure: ?*anyopaque, key: SCM, value: SCM) void;
pub extern fn scm_c_weak_table_remove_x(table: SCM, raw_hash: c_ulong, pred: scm_t_table_predicate_fn, closure: ?*anyopaque) void;
pub extern fn scm_weak_table_refq(table: SCM, key: SCM, dflt: SCM) SCM;
pub extern fn scm_weak_table_putq_x(table: SCM, key: SCM, value: SCM) void;
pub extern fn scm_weak_table_remq_x(table: SCM, key: SCM) void;
pub extern fn scm_weak_table_clear_x(table: SCM) void;
pub extern fn scm_c_weak_table_fold(proc: scm_t_table_fold_fn, closure: ?*anyopaque, init: SCM, table: SCM) SCM;
pub extern fn scm_weak_table_fold(proc: SCM, init: SCM, table: SCM) SCM;
pub extern fn scm_weak_table_for_each(proc: SCM, table: SCM) void;
pub extern fn scm_weak_table_map_to_list(proc: SCM, table: SCM) SCM;
pub extern fn scm_make_weak_key_hash_table(k: SCM) SCM;
pub extern fn scm_make_weak_value_hash_table(k: SCM) SCM;
pub extern fn scm_make_doubly_weak_hash_table(k: SCM) SCM;
pub extern fn scm_weak_key_hash_table_p(h: SCM) SCM;
pub extern fn scm_weak_value_hash_table_p(h: SCM) SCM;
pub extern fn scm_doubly_weak_hash_table_p(h: SCM) SCM;
pub extern fn scm_i_weak_table_print(exp: SCM, port: SCM, pstate: [*c]scm_print_state) void;
pub extern fn scm_weak_table_prehistory() void;
pub extern fn scm_init_weak_table() void;
pub extern fn scm_make_weak_vector(len: SCM, fill: SCM) SCM;
pub extern fn scm_weak_vector(l: SCM) SCM;
pub extern fn scm_weak_vector_p(x: SCM) SCM;
pub extern fn scm_weak_vector_length(v: SCM) SCM;
pub extern fn scm_weak_vector_ref(v: SCM, k: SCM) SCM;
pub extern fn scm_weak_vector_set_x(v: SCM, k: SCM, x: SCM) SCM;
pub extern fn scm_c_make_weak_vector(len: usize, fill: SCM) SCM;
pub extern fn scm_is_weak_vector(obj: SCM) c_int;
pub extern fn scm_c_weak_vector_length(vec: SCM) usize;
pub extern fn scm_c_weak_vector_ref(v: SCM, k: usize) SCM;
pub extern fn scm_c_weak_vector_set_x(v: SCM, k: usize, x: SCM) void;
pub extern fn scm_init_weak_vectors() void;
pub extern fn scm_print_exception(port: SCM, frame: SCM, key: SCM, args: SCM) SCM;
pub extern fn scm_display_error_message(message: SCM, args: SCM, port: SCM) void;
pub extern fn scm_i_display_error(frame: SCM, port: SCM, subr: SCM, message: SCM, args: SCM, rest: SCM) void;
pub extern fn scm_display_error(frame: SCM, port: SCM, subr: SCM, message: SCM, args: SCM, rest: SCM) SCM;
pub extern fn scm_display_application(frame: SCM, port: SCM, indent: SCM) SCM;
pub extern fn scm_display_backtrace(stack: SCM, port: SCM, first: SCM, depth: SCM) SCM;
pub extern fn scm_display_backtrace_with_highlights(stack: SCM, port: SCM, first: SCM, depth: SCM, highlights: SCM) SCM;
pub extern fn scm_backtrace() SCM;
pub extern fn scm_backtrace_with_highlights(highlights: SCM) SCM;
pub extern fn scm_init_backtrace() void;
pub extern var scm_stack_type: SCM;
pub extern fn scm_stack_p(obj: SCM) SCM;
pub extern fn scm_make_stack(obj: SCM, args: SCM) SCM;
pub extern fn scm_stack_id(stack: SCM) SCM;
pub extern fn scm_stack_ref(stack: SCM, i: SCM) SCM;
pub extern fn scm_stack_length(stack: SCM) SCM;
pub extern fn scm_init_stacks() void;
pub const scm_t_int8 = i8;
pub const scm_t_uint8 = u8;
pub const scm_t_int16 = i16;
pub const scm_t_uint16 = u16;
pub const scm_t_int32 = i32;
pub const scm_t_uint32 = u32;
pub const scm_t_intmax = intmax_t;
pub const scm_t_uintmax = uintmax_t;
pub const scm_t_intptr = isize;
pub const scm_t_uintptr = usize;
pub const scm_t_int64 = i64;
pub const scm_t_uint64 = u64;
pub const scm_t_ptrdiff = ptrdiff_t;
pub const scm_i_thread = struct_scm_thread;
pub extern fn scm_find_executable(name: [*c]const u8) [*c]u8;
pub extern fn scm_is_simple_vector(obj: SCM) c_int;
pub extern fn scm_bitvector_p(vec: SCM) SCM;
pub extern fn scm_bitvector(bits: SCM) SCM;
pub extern fn scm_make_bitvector(len: SCM, fill: SCM) SCM;
pub extern fn scm_bitvector_length(vec: SCM) SCM;
pub extern fn scm_c_bitvector_ref(vec: SCM, idx: usize) SCM;
pub extern fn scm_bitvector_ref(vec: SCM, idx: SCM) SCM;
pub extern fn scm_c_bitvector_set_x(vec: SCM, idx: usize, val: SCM) void;
pub extern fn scm_bitvector_set_x(vec: SCM, idx: SCM, val: SCM) SCM;
pub extern fn scm_bitvector_fill_x(vec: SCM, val: SCM) SCM;
pub extern fn scm_bit_invert_x(vec: SCM) SCM;
pub extern fn scm_bit_count(item: SCM, seq: SCM) SCM;
pub extern fn scm_bit_count_star(v: SCM, kv: SCM, obj: SCM) SCM;
pub extern fn scm_bit_position(item: SCM, v: SCM, k: SCM) SCM;
pub extern fn scm_bit_set_star_x(v: SCM, kv: SCM, obj: SCM) SCM;
pub extern fn scm_istr2bve(str: SCM) SCM;
pub extern fn scm_from_contiguous_typed_array(@"type": SCM, bounds: SCM, bytes: ?*const anyopaque, byte_len: usize) SCM;
pub extern var scm_tc16_srcprops: scm_t_bits;
pub extern var scm_sym_copy: SCM;
pub extern fn scm_make_srcprops(line: c_long, col: c_int, filename: SCM, copy: SCM, alist: SCM) SCM;
pub extern fn scm_symbol_fref(s: SCM) SCM;
pub extern fn scm_symbol_pref(s: SCM) SCM;
pub extern fn scm_symbol_fset_x(s: SCM, val: SCM) SCM;
pub extern fn scm_symbol_pset_x(s: SCM, val: SCM) SCM;
pub extern fn scm_dynamic_unlink(obj: SCM) SCM;
pub extern var scm_install_gmp_memory_functions: c_int;
pub extern fn scm_i_normbig(x: SCM) SCM;
pub extern fn scm_i_big2dbl(b: SCM) f64;
pub extern fn scm_i_long2big(n: c_long) SCM;
pub extern fn scm_i_ulong2big(n: c_ulong) SCM;
pub extern fn scm_i_clonebig(src_big: SCM, same_sign_p: c_int) SCM;
pub extern fn scm_i_init_deprecated() void;
pub const __INTMAX_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `L`"); // (no file):67:9
pub const __UINTMAX_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `UL`"); // (no file):73:9
pub const __INT64_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `L`"); // (no file):164:9
pub const __UINT32_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `U`"); // (no file):186:9
pub const __UINT64_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `UL`"); // (no file):194:9
pub const __seg_gs = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // (no file):312:9
pub const __seg_fs = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // (no file):313:9
pub const __GLIBC_USE = @compileError("unable to translate macro: undefined identifier `__GLIBC_USE_`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/features.h:179:9
pub const __THROW = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:55:11
pub const __THROWNL = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:56:11
pub const __NTH = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:57:11
pub const __NTHNL = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:58:11
pub const __glibc_clang_has_extension = @compileError("unable to translate macro: undefined identifier `__has_extension`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:96:10
pub const __CONCAT = @compileError("unable to translate C expr: unexpected token .HashHash"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:109:9
pub const __STRING = @compileError("unable to translate C expr: unexpected token .Hash"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:110:9
pub const __warnattr = @compileError("unable to translate C expr: unexpected token .Eof"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:144:10
pub const __errordecl = @compileError("unable to translate C expr: unexpected token .Keyword_extern"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:145:10
pub const __flexarr = @compileError("unable to translate C expr: unexpected token .LBracket"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:153:10
pub const __REDIRECT = @compileError("unable to translate macro: undefined identifier `__asm__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:184:10
pub const __REDIRECT_NTH = @compileError("unable to translate macro: undefined identifier `__asm__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:191:11
pub const __REDIRECT_NTHNL = @compileError("unable to translate macro: undefined identifier `__asm__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:193:11
pub const __ASMNAME2 = @compileError("unable to translate C expr: unexpected token .Identifier"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:197:10
pub const __attribute_malloc__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:218:10
pub const __attribute_alloc_size__ = @compileError("unable to translate C expr: unexpected token .Eof"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:229:10
pub const __attribute_pure__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:236:10
pub const __attribute_const__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:243:10
pub const __attribute_used__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:252:10
pub const __attribute_noinline__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:253:10
pub const __attribute_deprecated__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:261:10
pub const __attribute_deprecated_msg__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:271:10
pub const __attribute_format_arg__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:284:10
pub const __attribute_format_strfmon__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:294:10
pub const __nonnull = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:303:10
pub const __attribute_warn_unused_result__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:311:10
pub const __always_inline = @compileError("unable to translate macro: undefined identifier `__inline`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:329:10
pub const __extern_inline = @compileError("unable to translate macro: undefined identifier `__inline`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:356:11
pub const __extern_always_inline = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:357:11
pub const __restrict_arr = @compileError("unable to translate macro: undefined identifier `__restrict`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:397:10
pub const __glibc_has_attribute = @compileError("unable to translate macro: undefined identifier `__has_attribute`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:420:10
pub const __attribute_copy__ = @compileError("unable to translate C expr: unexpected token .Eof"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:451:10
pub const __LDBL_REDIR2_DECL = @compileError("unable to translate C expr: unexpected token .Eof"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:522:10
pub const __LDBL_REDIR_DECL = @compileError("unable to translate C expr: unexpected token .Eof"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:523:10
pub const __glibc_macro_warning1 = @compileError("unable to translate macro: undefined identifier `_Pragma`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:537:10
pub const __glibc_macro_warning = @compileError("unable to translate macro: undefined identifier `GCC`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:538:10
pub const __attr_access = @compileError("unable to translate C expr: unexpected token .Eof"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:569:11
pub const __attribute_returns_twice__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/cdefs.h:575:10
pub const __STD_TYPE = @compileError("unable to translate C expr: unexpected token .Keyword_typedef"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types.h:137:10
pub const __FSID_T_TYPE = @compileError("unable to translate macro: undefined identifier `__val`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/typesizes.h:73:9
pub const __INT64_C = @compileError("unable to translate macro: undefined identifier `L`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/stdint.h:106:11
pub const __UINT64_C = @compileError("unable to translate macro: undefined identifier `UL`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/stdint.h:107:11
pub const INT64_C = @compileError("unable to translate macro: undefined identifier `L`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/stdint.h:252:11
pub const UINT32_C = @compileError("unable to translate macro: undefined identifier `U`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/stdint.h:260:10
pub const UINT64_C = @compileError("unable to translate macro: undefined identifier `UL`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/stdint.h:262:11
pub const INTMAX_C = @compileError("unable to translate macro: undefined identifier `L`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/stdint.h:269:11
pub const UINTMAX_C = @compileError("unable to translate macro: undefined identifier `UL`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/stdint.h:270:11
pub const offsetof = @compileError("unable to translate macro: undefined identifier `__builtin_offsetof`"); // /gnu/store/r2bpgz9i5xansdxp4ix6q22fcd7r68y8-zig-0.9.1/lib/zig/include/stddef.h:104:9
pub const __FD_ZERO = @compileError("unable to translate macro: undefined identifier `__i`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/select.h:25:9
pub const __FD_SET = @compileError("unable to translate C expr: expected ')' instead got: PipeEqual"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/select.h:32:9
pub const __FD_CLR = @compileError("unable to translate C expr: expected ')' instead got: AmpersandEqual"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/select.h:34:9
pub const timerclear = @compileError("unable to translate C expr: expected ')' instead got: Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/time.h:161:10
pub const timercmp = @compileError("unable to translate C expr: expected ')' instead got: Identifier"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/time.h:162:10
pub const timeradd = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/time.h:166:10
pub const timersub = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/sys/time.h:176:10
pub const __f32 = @compileError("unable to translate macro: undefined identifier `f`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:91:12
pub const __f64x = @compileError("unable to translate macro: undefined identifier `l`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:120:13
pub const __CFLOAT32 = @compileError("unable to translate: TODO _Complex"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:149:12
pub const __CFLOAT64 = @compileError("unable to translate: TODO _Complex"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:160:13
pub const __CFLOAT32X = @compileError("unable to translate: TODO _Complex"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:169:12
pub const __CFLOAT64X = @compileError("unable to translate: TODO _Complex"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:178:13
pub const __builtin_nansf32 = @compileError("unable to translate macro: undefined identifier `__builtin_nansf`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:221:12
pub const __builtin_huge_valf64 = @compileError("unable to translate macro: undefined identifier `__builtin_huge_val`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:255:13
pub const __builtin_inff64 = @compileError("unable to translate macro: undefined identifier `__builtin_inf`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:256:13
pub const __builtin_nanf64 = @compileError("unable to translate macro: undefined identifier `__builtin_nan`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:257:13
pub const __builtin_nansf64 = @compileError("unable to translate macro: undefined identifier `__builtin_nans`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:258:13
pub const __builtin_huge_valf32x = @compileError("unable to translate macro: undefined identifier `__builtin_huge_val`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:272:12
pub const __builtin_inff32x = @compileError("unable to translate macro: undefined identifier `__builtin_inf`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:273:12
pub const __builtin_nanf32x = @compileError("unable to translate macro: undefined identifier `__builtin_nan`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:274:12
pub const __builtin_nansf32x = @compileError("unable to translate macro: undefined identifier `__builtin_nans`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:275:12
pub const __builtin_huge_valf64x = @compileError("unable to translate macro: undefined identifier `__builtin_huge_vall`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:289:13
pub const __builtin_inff64x = @compileError("unable to translate macro: undefined identifier `__builtin_infl`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:290:13
pub const __builtin_nanf64x = @compileError("unable to translate macro: undefined identifier `__builtin_nanl`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:291:13
pub const __builtin_nansf64x = @compileError("unable to translate macro: undefined identifier `__builtin_nansl`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/floatn-common.h:292:13
pub const __PTHREAD_MUTEX_INITIALIZER = @compileError("unable to translate C expr: unexpected token .LBrace"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/struct_mutex.h:56:10
pub const __PTHREAD_RWLOCK_ELISION_EXTRA = @compileError("unable to translate C expr: unexpected token .LBrace"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/struct_rwlock.h:40:11
pub const __ONCE_FLAG_INIT = @compileError("unable to translate C expr: unexpected token .LBrace"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/thread-shared-types.h:127:9
pub const SCM_C_INLINE = @compileError("unable to translate C expr: unexpected token .Keyword_inline"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scmconfig.h:51:9
pub const SCM_UNPACK = @compileError("unable to translate C expr: unexpected token .Keyword_volatile"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scm.h:171:11
pub const scm_tcs_cons_imcar = @compileError("unable to translate C expr: unexpected token .Colon"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scm.h:651:9
pub const scm_tcs_cons_nimcar = @compileError("unable to translate C expr: unexpected token .Colon"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scm.h:670:9
pub const scm_tcs_struct = @compileError("unable to translate C expr: unexpected token .Colon"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scm.h:689:9
pub const SCM_API = @compileError("unable to translate C expr: unexpected token .Keyword_extern"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scm.h:728:10
pub const SCM_INTERNAL = @compileError("unable to translate C expr: unexpected token .Keyword_extern"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scm.h:735:9
pub const SCM_DEPRECATED = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scm.h:743:10
pub const SCM_NORETURN = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scm.h:752:10
pub const SCM_UNUSED = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scm.h:764:10
pub const SCM_MALLOC = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scm.h:774:10
pub const SCM_ALIGNED = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scm.h:795:10
pub const SCM_THREAD_LOCAL = @compileError("unable to translate macro: undefined identifier `__thread`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/scm.h:804:10
pub const SCM_ASSERT = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/error.h:39:9
pub const SCM_ASSERT_TYPE = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/error.h:43:9
pub const SCM_MAKE_VALIDATE = @compileError("unable to translate macro: undefined identifier `SCM_`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/error.h:99:9
pub const SCM_I_MAKE_VALIDATE_MSG2 = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/error.h:104:9
pub const SCM_MAKE_VALIDATE_MSG = @compileError("unable to translate macro: undefined identifier `SCM_`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/error.h:109:9
pub const SCM_SYSERROR = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/error.h:112:9
pub const SCM_SYSERROR_MSG = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/error.h:114:9
pub const SCM_MISC_ERROR = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/error.h:117:9
pub const SCM_WRONG_NUM_ARGS = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/error.h:120:9
pub const SCM_WRONG_TYPE_ARG = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/error.h:123:9
pub const SCM_C_EXTERN_INLINE = @compileError("unable to translate macro: undefined identifier `__inline__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/inline.h:60:12
pub const SCM_GC_SET_CELL_OBJECT = @compileError("unable to translate C expr: expected ')' instead got: Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gc.h:49:9
pub const scm_gc_typed_calloc = @compileError("unable to translate C expr: unexpected token .RParen"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gc.h:139:9
pub const SCM_VALIDATE_NULL_OR_NIL = @compileError("unable to translate macro: undefined identifier `NULL_OR_NIL_P`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/pairs.h:113:9
pub const SCM_VALIDATE_NULLORCONS = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/pairs.h:131:9
pub const SCM_VALIDATE_ALISTCELL = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/alist.h:31:9
pub const SCM_VALIDATE_ALISTCELL_COPYSCM = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/alist.h:37:9
pub const SCM_VALIDATE_CHAR = @compileError("unable to translate macro: undefined identifier `CHARP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/chars.h:56:9
pub const SCM_VALIDATE_CHAR_COPY = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/chars.h:58:9
pub const RESET_PRINT_STATE = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/print.h:39:9
pub const SCM_SET_WRITINGP = @compileError("unable to translate C expr: unexpected token .LBrace"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/print.h:46:9
pub const SCM_VALIDATE_OPORT_VALUE = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/print.h:55:9
pub const SCM_VALIDATE_PRINTSTATE = @compileError("unable to translate macro: undefined identifier `PRINT_STATE_P`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/print.h:60:9
pub const FLT_ROUNDS = @compileError("unable to translate macro: undefined identifier `__builtin_flt_rounds`"); // /gnu/store/r2bpgz9i5xansdxp4ix6q22fcd7r68y8-zig-0.9.1/lib/zig/include/float.h:83:9
pub const SCM_OUT_OF_RANGE = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:657:9
pub const SCM_ASSERT_RANGE = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:660:9
pub const SCM_VALIDATE_REAL = @compileError("unable to translate macro: undefined identifier `REALP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:665:9
pub const SCM_VALIDATE_NUMBER = @compileError("unable to translate macro: undefined identifier `NUMBERP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:667:9
pub const SCM_VALIDATE_USHORT_COPY = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:669:9
pub const SCM_VALIDATE_SHORT_COPY = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:674:9
pub const SCM_VALIDATE_UINT_COPY = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:679:9
pub const SCM_VALIDATE_INT_COPY = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:684:9
pub const SCM_VALIDATE_ULONG_COPY = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:689:9
pub const SCM_VALIDATE_LONG_COPY = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:694:9
pub const SCM_VALIDATE_SIZE_COPY = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:699:9
pub const SCM_VALIDATE_FLOAT_COPY = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:704:9
pub const SCM_VALIDATE_DOUBLE_COPY = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:709:9
pub const SCM_VALIDATE_DOUBLE_DEF_COPY = @compileError("unable to translate C expr: expected ')'"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/numbers.h:714:9
pub const SCM_VALIDATE_ARRAY = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/arrays.h:33:9
pub const SCM_VALIDATE_BOOL = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/boolean.h:134:9
pub const SCM_VALIDATE_BOOL_COPY = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/boolean.h:139:9
pub const SCM_VALIDATE_THUNK = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/procs.h:30:9
pub const SCM_VALIDATE_PROC = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/procs.h:35:9
pub const si_pid = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:128:9
pub const si_uid = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:129:9
pub const si_timerid = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:130:9
pub const si_overrun = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:131:9
pub const si_status = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:132:9
pub const si_utime = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:133:9
pub const si_stime = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:134:9
pub const si_value = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:135:9
pub const si_int = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:136:9
pub const si_ptr = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:137:9
pub const si_addr = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:138:9
pub const si_addr_lsb = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:139:9
pub const si_lower = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:140:9
pub const si_upper = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:141:9
pub const si_pkey = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:142:9
pub const si_band = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:143:9
pub const si_fd = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:144:9
pub const si_call_addr = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:146:10
pub const si_syscall = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:147:10
pub const si_arch = @compileError("unable to translate macro: undefined identifier `_sifields`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/siginfo_t.h:148:10
pub const sigev_notify_function = @compileError("unable to translate macro: undefined identifier `_sigev_un`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/sigevent_t.h:45:9
pub const sigev_notify_attributes = @compileError("unable to translate macro: undefined identifier `_sigev_un`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/types/sigevent_t.h:46:9
pub const sa_handler = @compileError("unable to translate macro: undefined identifier `__sigaction_handler`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/sigaction.h:39:10
pub const sa_sigaction = @compileError("unable to translate macro: undefined identifier `__sigaction_handler`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/sigaction.h:40:10
pub const SCM_DYNSTACK_SET_PREV_OFFSET = @compileError("unable to translate C expr: unexpected token .Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/dynstack.h:73:9
pub const SCM_DYNSTACK_SET_TAG = @compileError("unable to translate C expr: unexpected token .Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/dynstack.h:76:9
pub const scm_i_paste = @compileError("unable to translate C expr: unexpected token .HashHash"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/snarf.h:37:9
pub const scm_i_paste3 = @compileError("unable to translate C expr: unexpected token .HashHash"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/snarf.h:38:9
pub const SCM_SNARF_INIT = @compileError("unable to translate C expr: unexpected token .Eof"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/snarf.h:81:11
pub const SCM_SNARF_DOCS = @compileError("unable to translate C expr: unexpected token .Eof"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/snarf.h:82:11
pub const SCM_IMMUTABLE_CELL = @compileError("unable to translate macro: undefined identifier `_raw`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/snarf.h:91:9
pub const SCM_IMMUTABLE_DOUBLE_CELL = @compileError("unable to translate macro: undefined identifier `_raw`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/snarf.h:96:9
pub const SCM_VALIDATE_SMOB = @compileError("unable to translate macro: undefined identifier `scm_tc16_`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:58:9
pub const SCM_SMOB = @compileError("unable to translate C expr: unexpected token .Keyword_static"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:73:9
pub const SCM_GLOBAL_SMOB = @compileError("unable to translate C expr: unexpected token .Identifier"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:77:9
pub const SCM_SMOB_MARK = @compileError("unable to translate C expr: unexpected token .Keyword_static"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:81:9
pub const SCM_GLOBAL_SMOB_MARK = @compileError("unable to translate C expr: unexpected token .Identifier"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:85:9
pub const SCM_SMOB_FREE = @compileError("unable to translate C expr: unexpected token .Keyword_static"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:89:9
pub const SCM_GLOBAL_SMOB_FREE = @compileError("unable to translate C expr: unexpected token .Identifier"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:93:9
pub const SCM_SMOB_PRINT = @compileError("unable to translate C expr: unexpected token .Keyword_static"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:97:9
pub const SCM_GLOBAL_SMOB_PRINT = @compileError("unable to translate C expr: unexpected token .Identifier"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:101:9
pub const SCM_SMOB_EQUALP = @compileError("unable to translate C expr: unexpected token .Keyword_static"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:105:9
pub const SCM_GLOBAL_SMOB_EQUALP = @compileError("unable to translate C expr: unexpected token .Identifier"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:109:9
pub const SCM_SMOB_APPLY = @compileError("unable to translate C expr: unexpected token .Keyword_static"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:113:9
pub const SCM_GLOBAL_SMOB_APPLY = @compileError("unable to translate C expr: unexpected token .Identifier"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:117:9
pub const SCM_NEWSMOB = @compileError("unable to translate C expr: unexpected token .Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:158:9
pub const SCM_RETURN_NEWSMOB = @compileError("unable to translate C expr: unexpected token .Keyword_return"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:160:9
pub const SCM_NEWSMOB2 = @compileError("unable to translate C expr: unexpected token .Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:163:9
pub const SCM_RETURN_NEWSMOB2 = @compileError("unable to translate C expr: unexpected token .Keyword_return"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:166:9
pub const SCM_NEWSMOB3 = @compileError("unable to translate C expr: unexpected token .Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:170:9
pub const SCM_RETURN_NEWSMOB3 = @compileError("unable to translate C expr: unexpected token .Keyword_return"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:173:9
pub const SCM_SMOB_APPLY_3 = @compileError("unable to translate macro: undefined identifier `a3`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/smob.h:221:9
pub const SCM_PROGRAM_FREE_VARIABLE_SET = @compileError("unable to translate C expr: expected ')' instead got: Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/programs.h:33:9
pub const SCM_VALIDATE_PROGRAM = @compileError("unable to translate macro: undefined identifier `PROGRAM_P`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/programs.h:35:9
pub const __CPU_ZERO_S = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/cpu-set.h:46:10
pub const __CPU_SET_S = @compileError("unable to translate macro: undefined identifier `__extension__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/cpu-set.h:58:9
pub const __CPU_CLR_S = @compileError("unable to translate macro: undefined identifier `__extension__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/cpu-set.h:65:9
pub const __CPU_ISSET_S = @compileError("unable to translate macro: undefined identifier `__extension__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/cpu-set.h:72:9
pub const __CPU_EQUAL_S = @compileError("unable to translate macro: undefined identifier `__builtin_memcmp`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/cpu-set.h:84:10
pub const __CPU_OP_S = @compileError("unable to translate macro: undefined identifier `__extension__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/bits/cpu-set.h:99:9
pub const PTHREAD_MUTEX_INITIALIZER = @compileError("unable to translate C expr: unexpected token .LBrace"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/pthread.h:88:9
pub const PTHREAD_RWLOCK_INITIALIZER = @compileError("unable to translate C expr: unexpected token .LBrace"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/pthread.h:112:10
pub const PTHREAD_COND_INITIALIZER = @compileError("unable to translate C expr: unexpected token .LBrace"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/pthread.h:153:9
pub const pthread_cleanup_push = @compileError("unable to translate macro: undefined identifier `__cancel_buf`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/pthread.h:658:10
pub const pthread_cleanup_pop = @compileError("unable to translate macro: undefined identifier `__cancel_buf`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/pthread.h:679:10
pub const SCM_VALIDATE_ATOMIC_BOX = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/atomic.h:41:9
pub const SCM_VALIDATE_BYTEVECTOR = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/bytevectors.h:152:9
pub const SCM_STRUCT_SLOT_SET = @compileError("unable to translate C expr: unexpected token .Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/struct.h:115:9
pub const SCM_STRUCT_DATA_SET = @compileError("unable to translate C expr: unexpected token .Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/struct.h:118:9
pub const SCM_VALIDATE_STRUCT = @compileError("unable to translate macro: undefined identifier `STRUCTP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/struct.h:120:9
pub const SCM_VALIDATE_VTABLE = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/struct.h:122:9
pub const SCM_SET_VTABLE_FLAGS = @compileError("unable to translate C expr: expected ')' instead got: PipeEqual"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/struct.h:132:9
pub const SCM_CLEAR_VTABLE_FLAGS = @compileError("unable to translate C expr: expected ')' instead got: AmpersandEqual"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/struct.h:133:9
pub const SCM_ILOCP = @compileError("unable to translate macro: undefined identifier `scm_tc8_iloc`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/eval.h:41:9
pub const SCM_VALIDATE_DIR = @compileError("unable to translate macro: undefined identifier `DIRP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/filesys.h:36:9
pub const SCM_VALIDATE_VECTOR = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/vectors.h:59:9
pub const SCM_VALIDATE_VECTOR_LEN = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/vectors.h:64:9
pub const SCM_SIMPLE_VECTOR_SET = @compileError("unable to translate C expr: expected ')' instead got: Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/vectors.h:73:9
pub const SCM_I_VECTOR_ELTS = @compileError("unable to translate C expr: unexpected token .Keyword_const"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/vectors.h:87:9
pub const SCM_VALIDATE_POINTER = @compileError("unable to translate macro: undefined identifier `POINTER_P`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/foreign.h:60:9
pub const SCM_IMMUTABLE_STRINGBUF = @compileError("unable to translate macro: undefined identifier `word_0`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/strings.h:186:9
pub const SCM_IMMUTABLE_STRING = @compileError("unable to translate macro: undefined identifier `_stringbuf`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/strings.h:201:9
pub const SCM_VALIDATE_STRING = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/strings.h:298:9
pub const SCM_VALIDATE_PORT = @compileError("unable to translate macro: undefined identifier `PORTP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/ports.h:78:9
pub const SCM_VALIDATE_INPUT_PORT = @compileError("unable to translate macro: undefined identifier `INPUT_PORT_P`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/ports.h:81:9
pub const SCM_VALIDATE_OUTPUT_PORT = @compileError("unable to translate macro: undefined identifier `OUTPUT_PORT_P`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/ports.h:84:9
pub const SCM_VALIDATE_OPINPORT = @compileError("unable to translate macro: undefined identifier `OPINPORTP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/ports.h:87:9
pub const SCM_VALIDATE_OPENPORT = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/ports.h:90:9
pub const SCM_VALIDATE_OPPORT = @compileError("unable to translate macro: undefined identifier `OPPORTP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/ports.h:96:9
pub const SCM_VALIDATE_OPOUTPORT = @compileError("unable to translate macro: undefined identifier `OPOUTPORTP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/ports.h:99:9
pub const SCM_VALIDATE_FPORT = @compileError("unable to translate macro: undefined identifier `FPORTP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/fports.h:51:9
pub const SCM_VALIDATE_OPFPORT = @compileError("unable to translate macro: undefined identifier `OPFPORTP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/fports.h:53:9
pub const SCM_FRAME_SET_MACHINE_RETURN_ADDRESS = @compileError("unable to translate C expr: expected ')' instead got: Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/frames.h:106:9
pub const SCM_FRAME_SET_VIRTUAL_RETURN_ADDRESS = @compileError("unable to translate C expr: expected ')' instead got: Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/frames.h:108:9
pub const SCM_FRAME_SET_DYNAMIC_LINK = @compileError("unable to translate C expr: expected ')' instead got: Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/frames.h:110:9
pub const SCM_VALIDATE_REST_ARGUMENT = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/list.h:72:9
pub const SCM_VALIDATE_LIST = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/list.h:81:9
pub const SCM_VALIDATE_NONEMPTYLIST = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/list.h:86:9
pub const SCM_VALIDATE_LIST_COPYLEN = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/list.h:93:9
pub const SCM_VALIDATE_NONEMPTYLIST_COPYLEN = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/list.h:101:9
pub const SCM_VALIDATE_CLASS = @compileError("unable to translate macro: undefined identifier `CLASSP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/goops.h:73:9
pub const SCM_VALIDATE_INSTANCE = @compileError("unable to translate macro: undefined identifier `INSTANCEP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/goops.h:77:9
pub const SCM_VALIDATE_GENERIC = @compileError("unable to translate macro: undefined identifier `GENERICP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/goops.h:88:9
pub const SCM_VALIDATE_METHOD = @compileError("unable to translate macro: undefined identifier `METHODP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/goops.h:91:9
pub const SCM_SET_CLASS_DESTRUCTOR = @compileError("unable to translate macro: undefined identifier `SCM_SET_VTABLE_DESTRUCTOR`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/goops.h:93:9
pub const SCM_SET_SUBR_GENERIC = @compileError("unable to translate C expr: expected ')' instead got: Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gsubr.h:47:9
pub const SCM_DEFINE_GSUBR = @compileError("unable to translate macro: undefined identifier `s_`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gsubr.h:82:9
pub const SCM_PRIMITIVE_GENERIC = @compileError("unable to translate macro: undefined identifier `s_`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gsubr.h:96:9
pub const SCM_DEFINE_PUBLIC = @compileError("unable to translate macro: undefined identifier `s_`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gsubr.h:110:9
pub const SCM_DEFINE_STATIC = @compileError("unable to translate macro: undefined identifier `s_`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gsubr.h:122:9
pub const SCM_PROC = @compileError("unable to translate C expr: expected ',' or ')'"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gsubr.h:133:9
pub const SCM_REGISTER_PROC = @compileError("unable to translate C expr: expected ',' or ')'"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gsubr.h:138:9
pub const SCM_GPROC = @compileError("unable to translate C expr: expected ',' or ')'"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/gsubr.h:145:9
pub const SCM_VALIDATE_HASHTABLE = @compileError("unable to translate macro: undefined identifier `HASHTABLE_P`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/hashtab.h:30:9
pub const SCM_SET_HASHTABLE_N_ITEMS = @compileError("unable to translate C expr: expected ')' instead got: Equal"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/hashtab.h:36:9
pub const SCM_HASHTABLE_INCREMENT = @compileError("TODO postfix inc/dec expr"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/hashtab.h:37:9
pub const SCM_HASHTABLE_DECREMENT = @compileError("TODO postfix inc/dec expr"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/hashtab.h:38:9
pub const SCM_VALIDATE_HOOK = @compileError("unable to translate macro: undefined identifier `HOOKP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/hooks.h:39:9
pub const SCM_VALIDATE_KEYWORD = @compileError("unable to translate macro: undefined identifier `KEYWORDP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/keywords.h:40:9
pub const SCM_KEYWORD = @compileError("unable to translate C expr: unexpected token .Keyword_static"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/keywords.h:43:9
pub const SCM_GLOBAL_KEYWORD = @compileError("unable to translate C expr: unexpected token .Identifier"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/keywords.h:47:9
pub const SCM_VALIDATE_MODULE = @compileError("unable to translate macro: undefined identifier `MODULEP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/modules.h:36:9
pub const SCM_VALIDATE_RSTATE = @compileError("unable to translate macro: undefined identifier `RSTATEP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/random.h:80:9
pub const SCM_SINGLE_SPACES = @compileError("unable to translate C expr: unexpected token .Colon"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/read.h:41:10
pub const SCM_WHITE_SPACES = @compileError("unable to translate C expr: unexpected token .Colon"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/read.h:44:9
pub const SCM_STACK_CHECKING_P = @compileError("unable to translate macro: undefined identifier `SCM_STACK_LIMIT`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/stackchk.h:32:9
pub const SCM_VALIDATE_OPOUTSTRPORT = @compileError("unable to translate macro: undefined identifier `OPOUTSTRPORTP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/strports.h:41:9
pub const SCM_VALIDATE_SYMBOL = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/symbols.h:40:9
pub const SCM_SYMBOL = @compileError("unable to translate macro: undefined identifier `_string`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/symbols.h:50:10
pub const SCM_GLOBAL_SYMBOL = @compileError("unable to translate macro: undefined identifier `_string`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/symbols.h:58:10
pub const SCM_VALIDATE_VARIABLE = @compileError("unable to translate macro: undefined identifier `VARIABLEP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/variable.h:38:9
pub const SCM_VARIABLE = @compileError("unable to translate C expr: unexpected token .Keyword_static"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/variable.h:44:9
pub const SCM_GLOBAL_VARIABLE = @compileError("unable to translate C expr: unexpected token .Identifier"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/variable.h:48:9
pub const SCM_VARIABLE_INIT = @compileError("unable to translate C expr: unexpected token .Keyword_static"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/variable.h:52:9
pub const SCM_GLOBAL_VARIABLE_INIT = @compileError("unable to translate C expr: unexpected token .Identifier"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/variable.h:56:9
pub const SCM_SRFI4_DECL = @compileError("unable to translate macro: undefined identifier `scm_`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/srfi-4.h:33:9
pub const SCM_FRAMEP = @compileError("unable to translate macro: undefined identifier `SCM_VM_FRAME_P`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/stacks.h:46:9
pub const SCM_VALIDATE_STACK = @compileError("unable to translate macro: undefined identifier `STACKP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/stacks.h:48:9
pub const SCM_VALIDATE_FRAME = @compileError("unable to translate macro: undefined identifier `FRAMEP`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/stacks.h:50:9
pub const scm_i_jmp_buf = @compileError("unable to translate macro: undefined identifier `scm_i_jmp_buf_GONE__USE_JMP_BUF_INSTEAD`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/deprecated.h:38:9
pub const SCM_VALIDATE_VECTOR_OR_DVECTOR = @compileError("unable to translate macro: undefined identifier `FUNC_NAME`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/deprecated.h:40:9
pub const SCM_STATIC_DOUBLE_CELL = @compileError("unable to translate macro: undefined identifier `_raw_cell`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/deprecated.h:47:9
pub const SCM_DEPRECATED_TYPE = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/deprecated.h:93:9
pub const SCM_MEMORY_ERROR = @compileError("unable to translate C expr: unexpected token .Keyword_do"); // /gnu/store/dbzy1ml4il9mfm7wyr24fjjrwck87a8m-profile/include/guile/3.0/libguile/deprecated.h:114:9
pub const __llvm__ = @as(c_int, 1);
pub const __clang__ = @as(c_int, 1);
pub const __clang_major__ = @as(c_int, 13);
pub const __clang_minor__ = @as(c_int, 0);
pub const __clang_patchlevel__ = @as(c_int, 1);
pub const __clang_version__ = "13.0.1 ";
pub const __GNUC__ = @as(c_int, 4);
pub const __GNUC_MINOR__ = @as(c_int, 2);
pub const __GNUC_PATCHLEVEL__ = @as(c_int, 1);
pub const __GXX_ABI_VERSION = @as(c_int, 1002);
pub const __ATOMIC_RELAXED = @as(c_int, 0);
pub const __ATOMIC_CONSUME = @as(c_int, 1);
pub const __ATOMIC_ACQUIRE = @as(c_int, 2);
pub const __ATOMIC_RELEASE = @as(c_int, 3);
pub const __ATOMIC_ACQ_REL = @as(c_int, 4);
pub const __ATOMIC_SEQ_CST = @as(c_int, 5);
pub const __OPENCL_MEMORY_SCOPE_WORK_ITEM = @as(c_int, 0);
pub const __OPENCL_MEMORY_SCOPE_WORK_GROUP = @as(c_int, 1);
pub const __OPENCL_MEMORY_SCOPE_DEVICE = @as(c_int, 2);
pub const __OPENCL_MEMORY_SCOPE_ALL_SVM_DEVICES = @as(c_int, 3);
pub const __OPENCL_MEMORY_SCOPE_SUB_GROUP = @as(c_int, 4);
pub const __PRAGMA_REDEFINE_EXTNAME = @as(c_int, 1);
pub const __VERSION__ = "Clang 13.0.1";
pub const __OBJC_BOOL_IS_BOOL = @as(c_int, 0);
pub const __CONSTANT_CFSTRINGS__ = @as(c_int, 1);
pub const __clang_literal_encoding__ = "UTF-8";
pub const __clang_wide_literal_encoding__ = "UTF-32";
pub const __OPTIMIZE__ = @as(c_int, 1);
pub const __ORDER_LITTLE_ENDIAN__ = @as(c_int, 1234);
pub const __ORDER_BIG_ENDIAN__ = @as(c_int, 4321);
pub const __ORDER_PDP_ENDIAN__ = @as(c_int, 3412);
pub const __BYTE_ORDER__ = __ORDER_LITTLE_ENDIAN__;
pub const __LITTLE_ENDIAN__ = @as(c_int, 1);
pub const _LP64 = @as(c_int, 1);
pub const __LP64__ = @as(c_int, 1);
pub const __CHAR_BIT__ = @as(c_int, 8);
pub const __SCHAR_MAX__ = @as(c_int, 127);
pub const __SHRT_MAX__ = @as(c_int, 32767);
pub const __INT_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __LONG_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __LONG_LONG_MAX__ = @as(c_longlong, 9223372036854775807);
pub const __WCHAR_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __WINT_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __INTMAX_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __SIZE_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINTMAX_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __PTRDIFF_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INTPTR_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __UINTPTR_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __SIZEOF_DOUBLE__ = @as(c_int, 8);
pub const __SIZEOF_FLOAT__ = @as(c_int, 4);
pub const __SIZEOF_INT__ = @as(c_int, 4);
pub const __SIZEOF_LONG__ = @as(c_int, 8);
pub const __SIZEOF_LONG_DOUBLE__ = @as(c_int, 16);
pub const __SIZEOF_LONG_LONG__ = @as(c_int, 8);
pub const __SIZEOF_POINTER__ = @as(c_int, 8);
pub const __SIZEOF_SHORT__ = @as(c_int, 2);
pub const __SIZEOF_PTRDIFF_T__ = @as(c_int, 8);
pub const __SIZEOF_SIZE_T__ = @as(c_int, 8);
pub const __SIZEOF_WCHAR_T__ = @as(c_int, 4);
pub const __SIZEOF_WINT_T__ = @as(c_int, 4);
pub const __SIZEOF_INT128__ = @as(c_int, 16);
pub const __INTMAX_TYPE__ = c_long;
pub const __INTMAX_FMTd__ = "ld";
pub const __INTMAX_FMTi__ = "li";
pub const __UINTMAX_TYPE__ = c_ulong;
pub const __UINTMAX_FMTo__ = "lo";
pub const __UINTMAX_FMTu__ = "lu";
pub const __UINTMAX_FMTx__ = "lx";
pub const __UINTMAX_FMTX__ = "lX";
pub const __INTMAX_WIDTH__ = @as(c_int, 64);
pub const __PTRDIFF_TYPE__ = c_long;
pub const __PTRDIFF_FMTd__ = "ld";
pub const __PTRDIFF_FMTi__ = "li";
pub const __PTRDIFF_WIDTH__ = @as(c_int, 64);
pub const __INTPTR_TYPE__ = c_long;
pub const __INTPTR_FMTd__ = "ld";
pub const __INTPTR_FMTi__ = "li";
pub const __INTPTR_WIDTH__ = @as(c_int, 64);
pub const __SIZE_TYPE__ = c_ulong;
pub const __SIZE_FMTo__ = "lo";
pub const __SIZE_FMTu__ = "lu";
pub const __SIZE_FMTx__ = "lx";
pub const __SIZE_FMTX__ = "lX";
pub const __SIZE_WIDTH__ = @as(c_int, 64);
pub const __WCHAR_TYPE__ = c_int;
pub const __WCHAR_WIDTH__ = @as(c_int, 32);
pub const __WINT_TYPE__ = c_uint;
pub const __WINT_WIDTH__ = @as(c_int, 32);
pub const __SIG_ATOMIC_WIDTH__ = @as(c_int, 32);
pub const __SIG_ATOMIC_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __CHAR16_TYPE__ = c_ushort;
pub const __CHAR32_TYPE__ = c_uint;
pub const __UINTMAX_WIDTH__ = @as(c_int, 64);
pub const __UINTPTR_TYPE__ = c_ulong;
pub const __UINTPTR_FMTo__ = "lo";
pub const __UINTPTR_FMTu__ = "lu";
pub const __UINTPTR_FMTx__ = "lx";
pub const __UINTPTR_FMTX__ = "lX";
pub const __UINTPTR_WIDTH__ = @as(c_int, 64);
pub const __FLT_DENORM_MIN__ = @as(f32, 1.40129846e-45);
pub const __FLT_HAS_DENORM__ = @as(c_int, 1);
pub const __FLT_DIG__ = @as(c_int, 6);
pub const __FLT_DECIMAL_DIG__ = @as(c_int, 9);
pub const __FLT_EPSILON__ = @as(f32, 1.19209290e-7);
pub const __FLT_HAS_INFINITY__ = @as(c_int, 1);
pub const __FLT_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __FLT_MANT_DIG__ = @as(c_int, 24);
pub const __FLT_MAX_10_EXP__ = @as(c_int, 38);
pub const __FLT_MAX_EXP__ = @as(c_int, 128);
pub const __FLT_MAX__ = @as(f32, 3.40282347e+38);
pub const __FLT_MIN_10_EXP__ = -@as(c_int, 37);
pub const __FLT_MIN_EXP__ = -@as(c_int, 125);
pub const __FLT_MIN__ = @as(f32, 1.17549435e-38);
pub const __DBL_DENORM_MIN__ = 4.9406564584124654e-324;
pub const __DBL_HAS_DENORM__ = @as(c_int, 1);
pub const __DBL_DIG__ = @as(c_int, 15);
pub const __DBL_DECIMAL_DIG__ = @as(c_int, 17);
pub const __DBL_EPSILON__ = 2.2204460492503131e-16;
pub const __DBL_HAS_INFINITY__ = @as(c_int, 1);
pub const __DBL_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __DBL_MANT_DIG__ = @as(c_int, 53);
pub const __DBL_MAX_10_EXP__ = @as(c_int, 308);
pub const __DBL_MAX_EXP__ = @as(c_int, 1024);
pub const __DBL_MAX__ = 1.7976931348623157e+308;
pub const __DBL_MIN_10_EXP__ = -@as(c_int, 307);
pub const __DBL_MIN_EXP__ = -@as(c_int, 1021);
pub const __DBL_MIN__ = 2.2250738585072014e-308;
pub const __LDBL_DENORM_MIN__ = @as(c_longdouble, 3.64519953188247460253e-4951);
pub const __LDBL_HAS_DENORM__ = @as(c_int, 1);
pub const __LDBL_DIG__ = @as(c_int, 18);
pub const __LDBL_DECIMAL_DIG__ = @as(c_int, 21);
pub const __LDBL_EPSILON__ = @as(c_longdouble, 1.08420217248550443401e-19);
pub const __LDBL_HAS_INFINITY__ = @as(c_int, 1);
pub const __LDBL_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __LDBL_MANT_DIG__ = @as(c_int, 64);
pub const __LDBL_MAX_10_EXP__ = @as(c_int, 4932);
pub const __LDBL_MAX_EXP__ = @as(c_int, 16384);
pub const __LDBL_MAX__ = @as(c_longdouble, 1.18973149535723176502e+4932);
pub const __LDBL_MIN_10_EXP__ = -@as(c_int, 4931);
pub const __LDBL_MIN_EXP__ = -@as(c_int, 16381);
pub const __LDBL_MIN__ = @as(c_longdouble, 3.36210314311209350626e-4932);
pub const __POINTER_WIDTH__ = @as(c_int, 64);
pub const __BIGGEST_ALIGNMENT__ = @as(c_int, 16);
pub const __WINT_UNSIGNED__ = @as(c_int, 1);
pub const __INT8_TYPE__ = i8;
pub const __INT8_FMTd__ = "hhd";
pub const __INT8_FMTi__ = "hhi";
pub const __INT8_C_SUFFIX__ = "";
pub const __INT16_TYPE__ = c_short;
pub const __INT16_FMTd__ = "hd";
pub const __INT16_FMTi__ = "hi";
pub const __INT16_C_SUFFIX__ = "";
pub const __INT32_TYPE__ = c_int;
pub const __INT32_FMTd__ = "d";
pub const __INT32_FMTi__ = "i";
pub const __INT32_C_SUFFIX__ = "";
pub const __INT64_TYPE__ = c_long;
pub const __INT64_FMTd__ = "ld";
pub const __INT64_FMTi__ = "li";
pub const __UINT8_TYPE__ = u8;
pub const __UINT8_FMTo__ = "hho";
pub const __UINT8_FMTu__ = "hhu";
pub const __UINT8_FMTx__ = "hhx";
pub const __UINT8_FMTX__ = "hhX";
pub const __UINT8_C_SUFFIX__ = "";
pub const __UINT8_MAX__ = @as(c_int, 255);
pub const __INT8_MAX__ = @as(c_int, 127);
pub const __UINT16_TYPE__ = c_ushort;
pub const __UINT16_FMTo__ = "ho";
pub const __UINT16_FMTu__ = "hu";
pub const __UINT16_FMTx__ = "hx";
pub const __UINT16_FMTX__ = "hX";
pub const __UINT16_C_SUFFIX__ = "";
pub const __UINT16_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const __INT16_MAX__ = @as(c_int, 32767);
pub const __UINT32_TYPE__ = c_uint;
pub const __UINT32_FMTo__ = "o";
pub const __UINT32_FMTu__ = "u";
pub const __UINT32_FMTx__ = "x";
pub const __UINT32_FMTX__ = "X";
pub const __UINT32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __INT32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __UINT64_TYPE__ = c_ulong;
pub const __UINT64_FMTo__ = "lo";
pub const __UINT64_FMTu__ = "lu";
pub const __UINT64_FMTx__ = "lx";
pub const __UINT64_FMTX__ = "lX";
pub const __UINT64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __INT64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INT_LEAST8_TYPE__ = i8;
pub const __INT_LEAST8_MAX__ = @as(c_int, 127);
pub const __INT_LEAST8_FMTd__ = "hhd";
pub const __INT_LEAST8_FMTi__ = "hhi";
pub const __UINT_LEAST8_TYPE__ = u8;
pub const __UINT_LEAST8_MAX__ = @as(c_int, 255);
pub const __UINT_LEAST8_FMTo__ = "hho";
pub const __UINT_LEAST8_FMTu__ = "hhu";
pub const __UINT_LEAST8_FMTx__ = "hhx";
pub const __UINT_LEAST8_FMTX__ = "hhX";
pub const __INT_LEAST16_TYPE__ = c_short;
pub const __INT_LEAST16_MAX__ = @as(c_int, 32767);
pub const __INT_LEAST16_FMTd__ = "hd";
pub const __INT_LEAST16_FMTi__ = "hi";
pub const __UINT_LEAST16_TYPE__ = c_ushort;
pub const __UINT_LEAST16_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const __UINT_LEAST16_FMTo__ = "ho";
pub const __UINT_LEAST16_FMTu__ = "hu";
pub const __UINT_LEAST16_FMTx__ = "hx";
pub const __UINT_LEAST16_FMTX__ = "hX";
pub const __INT_LEAST32_TYPE__ = c_int;
pub const __INT_LEAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __INT_LEAST32_FMTd__ = "d";
pub const __INT_LEAST32_FMTi__ = "i";
pub const __UINT_LEAST32_TYPE__ = c_uint;
pub const __UINT_LEAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __UINT_LEAST32_FMTo__ = "o";
pub const __UINT_LEAST32_FMTu__ = "u";
pub const __UINT_LEAST32_FMTx__ = "x";
pub const __UINT_LEAST32_FMTX__ = "X";
pub const __INT_LEAST64_TYPE__ = c_long;
pub const __INT_LEAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INT_LEAST64_FMTd__ = "ld";
pub const __INT_LEAST64_FMTi__ = "li";
pub const __UINT_LEAST64_TYPE__ = c_ulong;
pub const __UINT_LEAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINT_LEAST64_FMTo__ = "lo";
pub const __UINT_LEAST64_FMTu__ = "lu";
pub const __UINT_LEAST64_FMTx__ = "lx";
pub const __UINT_LEAST64_FMTX__ = "lX";
pub const __INT_FAST8_TYPE__ = i8;
pub const __INT_FAST8_MAX__ = @as(c_int, 127);
pub const __INT_FAST8_FMTd__ = "hhd";
pub const __INT_FAST8_FMTi__ = "hhi";
pub const __UINT_FAST8_TYPE__ = u8;
pub const __UINT_FAST8_MAX__ = @as(c_int, 255);
pub const __UINT_FAST8_FMTo__ = "hho";
pub const __UINT_FAST8_FMTu__ = "hhu";
pub const __UINT_FAST8_FMTx__ = "hhx";
pub const __UINT_FAST8_FMTX__ = "hhX";
pub const __INT_FAST16_TYPE__ = c_short;
pub const __INT_FAST16_MAX__ = @as(c_int, 32767);
pub const __INT_FAST16_FMTd__ = "hd";
pub const __INT_FAST16_FMTi__ = "hi";
pub const __UINT_FAST16_TYPE__ = c_ushort;
pub const __UINT_FAST16_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const __UINT_FAST16_FMTo__ = "ho";
pub const __UINT_FAST16_FMTu__ = "hu";
pub const __UINT_FAST16_FMTx__ = "hx";
pub const __UINT_FAST16_FMTX__ = "hX";
pub const __INT_FAST32_TYPE__ = c_int;
pub const __INT_FAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __INT_FAST32_FMTd__ = "d";
pub const __INT_FAST32_FMTi__ = "i";
pub const __UINT_FAST32_TYPE__ = c_uint;
pub const __UINT_FAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __UINT_FAST32_FMTo__ = "o";
pub const __UINT_FAST32_FMTu__ = "u";
pub const __UINT_FAST32_FMTx__ = "x";
pub const __UINT_FAST32_FMTX__ = "X";
pub const __INT_FAST64_TYPE__ = c_long;
pub const __INT_FAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INT_FAST64_FMTd__ = "ld";
pub const __INT_FAST64_FMTi__ = "li";
pub const __UINT_FAST64_TYPE__ = c_ulong;
pub const __UINT_FAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINT_FAST64_FMTo__ = "lo";
pub const __UINT_FAST64_FMTu__ = "lu";
pub const __UINT_FAST64_FMTx__ = "lx";
pub const __UINT_FAST64_FMTX__ = "lX";
pub const __USER_LABEL_PREFIX__ = "";
pub const __FINITE_MATH_ONLY__ = @as(c_int, 0);
pub const __GNUC_STDC_INLINE__ = @as(c_int, 1);
pub const __GCC_ATOMIC_TEST_AND_SET_TRUEVAL = @as(c_int, 1);
pub const __CLANG_ATOMIC_BOOL_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_CHAR_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_CHAR16_T_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_CHAR32_T_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_WCHAR_T_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_SHORT_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_INT_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_LONG_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_LLONG_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_POINTER_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_BOOL_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_CHAR_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_CHAR16_T_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_CHAR32_T_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_WCHAR_T_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_SHORT_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_INT_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_LONG_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_LLONG_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_POINTER_LOCK_FREE = @as(c_int, 2);
pub const __FLT_EVAL_METHOD__ = @as(c_int, 0);
pub const __FLT_RADIX__ = @as(c_int, 2);
pub const __DECIMAL_DIG__ = __LDBL_DECIMAL_DIG__;
pub const __GCC_ASM_FLAG_OUTPUTS__ = @as(c_int, 1);
pub const __code_model_small__ = @as(c_int, 1);
pub const __amd64__ = @as(c_int, 1);
pub const __amd64 = @as(c_int, 1);
pub const __x86_64 = @as(c_int, 1);
pub const __x86_64__ = @as(c_int, 1);
pub const __SEG_GS = @as(c_int, 1);
pub const __SEG_FS = @as(c_int, 1);
pub const __corei7 = @as(c_int, 1);
pub const __corei7__ = @as(c_int, 1);
pub const __tune_corei7__ = @as(c_int, 1);
pub const __REGISTER_PREFIX__ = "";
pub const __NO_MATH_INLINES = @as(c_int, 1);
pub const __AES__ = @as(c_int, 1);
pub const __PCLMUL__ = @as(c_int, 1);
pub const __LAHF_SAHF__ = @as(c_int, 1);
pub const __LZCNT__ = @as(c_int, 1);
pub const __RDRND__ = @as(c_int, 1);
pub const __FSGSBASE__ = @as(c_int, 1);
pub const __BMI__ = @as(c_int, 1);
pub const __BMI2__ = @as(c_int, 1);
pub const __POPCNT__ = @as(c_int, 1);
pub const __RTM__ = @as(c_int, 1);
pub const __PRFCHW__ = @as(c_int, 1);
pub const __RDSEED__ = @as(c_int, 1);
pub const __ADX__ = @as(c_int, 1);
pub const __MOVBE__ = @as(c_int, 1);
pub const __FMA__ = @as(c_int, 1);
pub const __F16C__ = @as(c_int, 1);
pub const __FXSR__ = @as(c_int, 1);
pub const __XSAVE__ = @as(c_int, 1);
pub const __XSAVEOPT__ = @as(c_int, 1);
pub const __XSAVEC__ = @as(c_int, 1);
pub const __XSAVES__ = @as(c_int, 1);
pub const __CLFLUSHOPT__ = @as(c_int, 1);
pub const __SGX__ = @as(c_int, 1);
pub const __INVPCID__ = @as(c_int, 1);
pub const __AVX2__ = @as(c_int, 1);
pub const __AVX__ = @as(c_int, 1);
pub const __SSE4_2__ = @as(c_int, 1);
pub const __SSE4_1__ = @as(c_int, 1);
pub const __SSSE3__ = @as(c_int, 1);
pub const __SSE3__ = @as(c_int, 1);
pub const __SSE2__ = @as(c_int, 1);
pub const __SSE2_MATH__ = @as(c_int, 1);
pub const __SSE__ = @as(c_int, 1);
pub const __SSE_MATH__ = @as(c_int, 1);
pub const __MMX__ = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = @as(c_int, 1);
pub const __SIZEOF_FLOAT128__ = @as(c_int, 16);
pub const unix = @as(c_int, 1);
pub const __unix = @as(c_int, 1);
pub const __unix__ = @as(c_int, 1);
pub const linux = @as(c_int, 1);
pub const __linux = @as(c_int, 1);
pub const __linux__ = @as(c_int, 1);
pub const __ELF__ = @as(c_int, 1);
pub const __gnu_linux__ = @as(c_int, 1);
pub const __FLOAT128__ = @as(c_int, 1);
pub const __STDC__ = @as(c_int, 1);
pub const __STDC_HOSTED__ = @as(c_int, 1);
pub const __STDC_VERSION__ = @as(c_long, 201710);
pub const __STDC_UTF_16__ = @as(c_int, 1);
pub const __STDC_UTF_32__ = @as(c_int, 1);
pub const _DEBUG = @as(c_int, 1);
pub const __GCC_HAVE_DWARF2_CFI_ASM = @as(c_int, 1);
pub const SCM_LIBGUILE_H = "";
pub const SCM_SCM_H = "";
pub const __CLANG_STDINT_H = "";
pub const _STDINT_H = @as(c_int, 1);
pub const __GLIBC_INTERNAL_STARTING_HEADER_IMPLEMENTATION = "";
pub const _FEATURES_H = @as(c_int, 1);
pub const __KERNEL_STRICT_NAMES = "";
pub inline fn __GNUC_PREREQ(maj: anytype, min: anytype) @TypeOf(((__GNUC__ << @as(c_int, 16)) + __GNUC_MINOR__) >= ((maj << @as(c_int, 16)) + min)) {
    return ((__GNUC__ << @as(c_int, 16)) + __GNUC_MINOR__) >= ((maj << @as(c_int, 16)) + min);
}
pub inline fn __glibc_clang_prereq(maj: anytype, min: anytype) @TypeOf(((__clang_major__ << @as(c_int, 16)) + __clang_minor__) >= ((maj << @as(c_int, 16)) + min)) {
    return ((__clang_major__ << @as(c_int, 16)) + __clang_minor__) >= ((maj << @as(c_int, 16)) + min);
}
pub const _DEFAULT_SOURCE = @as(c_int, 1);
pub const __GLIBC_USE_ISOC2X = @as(c_int, 0);
pub const __USE_ISOC11 = @as(c_int, 1);
pub const __USE_ISOC99 = @as(c_int, 1);
pub const __USE_ISOC95 = @as(c_int, 1);
pub const __USE_POSIX_IMPLICITLY = @as(c_int, 1);
pub const _POSIX_SOURCE = @as(c_int, 1);
pub const _POSIX_C_SOURCE = @as(c_long, 200809);
pub const __USE_POSIX = @as(c_int, 1);
pub const __USE_POSIX2 = @as(c_int, 1);
pub const __USE_POSIX199309 = @as(c_int, 1);
pub const __USE_POSIX199506 = @as(c_int, 1);
pub const __USE_XOPEN2K = @as(c_int, 1);
pub const __USE_XOPEN2K8 = @as(c_int, 1);
pub const _ATFILE_SOURCE = @as(c_int, 1);
pub const __USE_MISC = @as(c_int, 1);
pub const __USE_ATFILE = @as(c_int, 1);
pub const __USE_FORTIFY_LEVEL = @as(c_int, 0);
pub const __GLIBC_USE_DEPRECATED_GETS = @as(c_int, 0);
pub const __GLIBC_USE_DEPRECATED_SCANF = @as(c_int, 0);
pub const _STDC_PREDEF_H = @as(c_int, 1);
pub const __STDC_IEC_559__ = @as(c_int, 1);
pub const __STDC_IEC_559_COMPLEX__ = @as(c_int, 1);
pub const __STDC_ISO_10646__ = @as(c_long, 201706);
pub const __GNU_LIBRARY__ = @as(c_int, 6);
pub const __GLIBC__ = @as(c_int, 2);
pub const __GLIBC_MINOR__ = @as(c_int, 33);
pub inline fn __GLIBC_PREREQ(maj: anytype, min: anytype) @TypeOf(((__GLIBC__ << @as(c_int, 16)) + __GLIBC_MINOR__) >= ((maj << @as(c_int, 16)) + min)) {
    return ((__GLIBC__ << @as(c_int, 16)) + __GLIBC_MINOR__) >= ((maj << @as(c_int, 16)) + min);
}
pub const _SYS_CDEFS_H = @as(c_int, 1);
pub const __LEAF = "";
pub const __LEAF_ATTR = "";
pub inline fn __P(args: anytype) @TypeOf(args) {
    return args;
}
pub inline fn __PMT(args: anytype) @TypeOf(args) {
    return args;
}
pub const __ptr_t = ?*anyopaque;
pub const __BEGIN_DECLS = "";
pub const __END_DECLS = "";
pub inline fn __bos(ptr: anytype) @TypeOf(__builtin_object_size(ptr, __USE_FORTIFY_LEVEL > @as(c_int, 1))) {
    return __builtin_object_size(ptr, __USE_FORTIFY_LEVEL > @as(c_int, 1));
}
pub inline fn __bos0(ptr: anytype) @TypeOf(__builtin_object_size(ptr, @as(c_int, 0))) {
    return __builtin_object_size(ptr, @as(c_int, 0));
}
pub inline fn __glibc_objsize0(__o: anytype) @TypeOf(__bos0(__o)) {
    return __bos0(__o);
}
pub inline fn __glibc_objsize(__o: anytype) @TypeOf(__bos(__o)) {
    return __bos(__o);
}
pub const __glibc_c99_flexarr_available = @as(c_int, 1);
pub inline fn __ASMNAME(cname: anytype) @TypeOf(__ASMNAME2(__USER_LABEL_PREFIX__, cname)) {
    return __ASMNAME2(__USER_LABEL_PREFIX__, cname);
}
pub const __wur = "";
pub const __attribute_artificial__ = "";
pub const __fortify_function = __extern_always_inline ++ __attribute_artificial__;
pub inline fn __glibc_unlikely(cond: anytype) @TypeOf(__builtin_expect(cond, @as(c_int, 0))) {
    return __builtin_expect(cond, @as(c_int, 0));
}
pub inline fn __glibc_likely(cond: anytype) @TypeOf(__builtin_expect(cond, @as(c_int, 1))) {
    return __builtin_expect(cond, @as(c_int, 1));
}
pub const __attribute_nonstring__ = "";
pub const __WORDSIZE = @as(c_int, 64);
pub const __WORDSIZE_TIME64_COMPAT32 = @as(c_int, 1);
pub const __SYSCALL_WORDSIZE = @as(c_int, 64);
pub const __LDOUBLE_REDIRECTS_TO_FLOAT128_ABI = @as(c_int, 0);
pub inline fn __LDBL_REDIR1(name: anytype, proto: anytype, alias: anytype) @TypeOf(name ++ proto) {
    _ = alias;
    return name ++ proto;
}
pub inline fn __LDBL_REDIR(name: anytype, proto: anytype) @TypeOf(name ++ proto) {
    return name ++ proto;
}
pub inline fn __LDBL_REDIR1_NTH(name: anytype, proto: anytype, alias: anytype) @TypeOf(name ++ proto ++ __THROW) {
    _ = alias;
    return name ++ proto ++ __THROW;
}
pub inline fn __LDBL_REDIR_NTH(name: anytype, proto: anytype) @TypeOf(name ++ proto ++ __THROW) {
    return name ++ proto ++ __THROW;
}
pub inline fn __REDIRECT_LDBL(name: anytype, proto: anytype, alias: anytype) @TypeOf(__REDIRECT(name, proto, alias)) {
    return __REDIRECT(name, proto, alias);
}
pub inline fn __REDIRECT_NTH_LDBL(name: anytype, proto: anytype, alias: anytype) @TypeOf(__REDIRECT_NTH(name, proto, alias)) {
    return __REDIRECT_NTH(name, proto, alias);
}
pub const __HAVE_GENERIC_SELECTION = @as(c_int, 1);
pub const __USE_EXTERN_INLINES = @as(c_int, 1);
pub const __stub___compat_bdflush = "";
pub const __stub_chflags = "";
pub const __stub_fchflags = "";
pub const __stub_gtty = "";
pub const __stub_revoke = "";
pub const __stub_setlogin = "";
pub const __stub_sigreturn = "";
pub const __stub_stty = "";
pub const __GLIBC_USE_LIB_EXT2 = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_BFP_EXT = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_BFP_EXT_C2X = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_FUNCS_EXT = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_FUNCS_EXT_C2X = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_TYPES_EXT = @as(c_int, 0);
pub const _BITS_TYPES_H = @as(c_int, 1);
pub const __TIMESIZE = __WORDSIZE;
pub const __S16_TYPE = c_short;
pub const __U16_TYPE = c_ushort;
pub const __S32_TYPE = c_int;
pub const __U32_TYPE = c_uint;
pub const __SLONGWORD_TYPE = c_long;
pub const __ULONGWORD_TYPE = c_ulong;
pub const __SQUAD_TYPE = c_long;
pub const __UQUAD_TYPE = c_ulong;
pub const __SWORD_TYPE = c_long;
pub const __UWORD_TYPE = c_ulong;
pub const __SLONG32_TYPE = c_int;
pub const __ULONG32_TYPE = c_uint;
pub const __S64_TYPE = c_long;
pub const __U64_TYPE = c_ulong;
pub const _BITS_TYPESIZES_H = @as(c_int, 1);
pub const __SYSCALL_SLONG_TYPE = __SLONGWORD_TYPE;
pub const __SYSCALL_ULONG_TYPE = __ULONGWORD_TYPE;
pub const __DEV_T_TYPE = __UQUAD_TYPE;
pub const __UID_T_TYPE = __U32_TYPE;
pub const __GID_T_TYPE = __U32_TYPE;
pub const __INO_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __INO64_T_TYPE = __UQUAD_TYPE;
pub const __MODE_T_TYPE = __U32_TYPE;
pub const __NLINK_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __FSWORD_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __OFF_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __OFF64_T_TYPE = __SQUAD_TYPE;
pub const __PID_T_TYPE = __S32_TYPE;
pub const __RLIM_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __RLIM64_T_TYPE = __UQUAD_TYPE;
pub const __BLKCNT_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __BLKCNT64_T_TYPE = __SQUAD_TYPE;
pub const __FSBLKCNT_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __FSBLKCNT64_T_TYPE = __UQUAD_TYPE;
pub const __FSFILCNT_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __FSFILCNT64_T_TYPE = __UQUAD_TYPE;
pub const __ID_T_TYPE = __U32_TYPE;
pub const __CLOCK_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __TIME_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __USECONDS_T_TYPE = __U32_TYPE;
pub const __SUSECONDS_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __SUSECONDS64_T_TYPE = __SQUAD_TYPE;
pub const __DADDR_T_TYPE = __S32_TYPE;
pub const __KEY_T_TYPE = __S32_TYPE;
pub const __CLOCKID_T_TYPE = __S32_TYPE;
pub const __TIMER_T_TYPE = ?*anyopaque;
pub const __BLKSIZE_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __SSIZE_T_TYPE = __SWORD_TYPE;
pub const __CPU_MASK_TYPE = __SYSCALL_ULONG_TYPE;
pub const __OFF_T_MATCHES_OFF64_T = @as(c_int, 1);
pub const __INO_T_MATCHES_INO64_T = @as(c_int, 1);
pub const __RLIM_T_MATCHES_RLIM64_T = @as(c_int, 1);
pub const __STATFS_MATCHES_STATFS64 = @as(c_int, 1);
pub const __KERNEL_OLD_TIMEVAL_MATCHES_TIMEVAL64 = @as(c_int, 1);
pub const __FD_SETSIZE = @as(c_int, 1024);
pub const _BITS_TIME64_H = @as(c_int, 1);
pub const __TIME64_T_TYPE = __TIME_T_TYPE;
pub const _BITS_WCHAR_H = @as(c_int, 1);
pub const __WCHAR_MAX = __WCHAR_MAX__;
pub const __WCHAR_MIN = -__WCHAR_MAX - @as(c_int, 1);
pub const _BITS_STDINT_INTN_H = @as(c_int, 1);
pub const _BITS_STDINT_UINTN_H = @as(c_int, 1);
pub const __intptr_t_defined = "";
pub const INT8_MIN = -@as(c_int, 128);
pub const INT16_MIN = -@as(c_int, 32767) - @as(c_int, 1);
pub const INT32_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal) - @as(c_int, 1);
pub const INT64_MIN = -__INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const INT8_MAX = @as(c_int, 127);
pub const INT16_MAX = @as(c_int, 32767);
pub const INT32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const INT64_MAX = __INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const UINT8_MAX = @as(c_int, 255);
pub const UINT16_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const UINT32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const UINT64_MAX = __UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const INT_LEAST8_MIN = -@as(c_int, 128);
pub const INT_LEAST16_MIN = -@as(c_int, 32767) - @as(c_int, 1);
pub const INT_LEAST32_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal) - @as(c_int, 1);
pub const INT_LEAST64_MIN = -__INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const INT_LEAST8_MAX = @as(c_int, 127);
pub const INT_LEAST16_MAX = @as(c_int, 32767);
pub const INT_LEAST32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const INT_LEAST64_MAX = __INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const UINT_LEAST8_MAX = @as(c_int, 255);
pub const UINT_LEAST16_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const UINT_LEAST32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const UINT_LEAST64_MAX = __UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const INT_FAST8_MIN = -@as(c_int, 128);
pub const INT_FAST16_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal) - @as(c_int, 1);
pub const INT_FAST32_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal) - @as(c_int, 1);
pub const INT_FAST64_MIN = -__INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const INT_FAST8_MAX = @as(c_int, 127);
pub const INT_FAST16_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const INT_FAST32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const INT_FAST64_MAX = __INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const UINT_FAST8_MAX = @as(c_int, 255);
pub const UINT_FAST16_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const UINT_FAST32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const UINT_FAST64_MAX = __UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const INTPTR_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal) - @as(c_int, 1);
pub const INTPTR_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const UINTPTR_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const INTMAX_MIN = -__INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const INTMAX_MAX = __INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const UINTMAX_MAX = __UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const PTRDIFF_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal) - @as(c_int, 1);
pub const PTRDIFF_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const SIG_ATOMIC_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal) - @as(c_int, 1);
pub const SIG_ATOMIC_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const SIZE_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const WCHAR_MIN = __WCHAR_MIN;
pub const WCHAR_MAX = __WCHAR_MAX;
pub const WINT_MIN = @as(c_uint, 0);
pub const WINT_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub inline fn INT8_C(c: anytype) @TypeOf(c) {
    return c;
}
pub inline fn INT16_C(c: anytype) @TypeOf(c) {
    return c;
}
pub inline fn INT32_C(c: anytype) @TypeOf(c) {
    return c;
}
pub inline fn UINT8_C(c: anytype) @TypeOf(c) {
    return c;
}
pub inline fn UINT16_C(c: anytype) @TypeOf(c) {
    return c;
}
pub const SCM_SCMCONFIG_H = "";
pub const __STDDEF_H = "";
pub const __need_ptrdiff_t = "";
pub const __need_size_t = "";
pub const __need_wchar_t = "";
pub const __need_NULL = "";
pub const __need_STDDEF_H_misc = "";
pub const _PTRDIFF_T = "";
pub const _SIZE_T = "";
pub const _WCHAR_T = "";
pub const NULL = @import("std").zig.c_translation.cast(?*anyopaque, @as(c_int, 0));
pub const __CLANG_MAX_ALIGN_T_DEFINED = "";
pub const __CLANG_LIMITS_H = "";
pub const _GCC_LIMITS_H_ = "";
pub const _LIBC_LIMITS_H_ = @as(c_int, 1);
pub const MB_LEN_MAX = @as(c_int, 16);
pub const LLONG_MIN = -LLONG_MAX - @as(c_int, 1);
pub const LLONG_MAX = __LONG_LONG_MAX__;
pub const ULLONG_MAX = (LLONG_MAX * @as(c_ulonglong, 2)) + @as(c_int, 1);
pub const _BITS_POSIX1_LIM_H = @as(c_int, 1);
pub const _POSIX_AIO_LISTIO_MAX = @as(c_int, 2);
pub const _POSIX_AIO_MAX = @as(c_int, 1);
pub const _POSIX_ARG_MAX = @as(c_int, 4096);
pub const _POSIX_CHILD_MAX = @as(c_int, 25);
pub const _POSIX_DELAYTIMER_MAX = @as(c_int, 32);
pub const _POSIX_HOST_NAME_MAX = @as(c_int, 255);
pub const _POSIX_LINK_MAX = @as(c_int, 8);
pub const _POSIX_LOGIN_NAME_MAX = @as(c_int, 9);
pub const _POSIX_MAX_CANON = @as(c_int, 255);
pub const _POSIX_MAX_INPUT = @as(c_int, 255);
pub const _POSIX_MQ_OPEN_MAX = @as(c_int, 8);
pub const _POSIX_MQ_PRIO_MAX = @as(c_int, 32);
pub const _POSIX_NAME_MAX = @as(c_int, 14);
pub const _POSIX_NGROUPS_MAX = @as(c_int, 8);
pub const _POSIX_OPEN_MAX = @as(c_int, 20);
pub const _POSIX_PATH_MAX = @as(c_int, 256);
pub const _POSIX_PIPE_BUF = @as(c_int, 512);
pub const _POSIX_RE_DUP_MAX = @as(c_int, 255);
pub const _POSIX_RTSIG_MAX = @as(c_int, 8);
pub const _POSIX_SEM_NSEMS_MAX = @as(c_int, 256);
pub const _POSIX_SEM_VALUE_MAX = @as(c_int, 32767);
pub const _POSIX_SIGQUEUE_MAX = @as(c_int, 32);
pub const _POSIX_SSIZE_MAX = @as(c_int, 32767);
pub const _POSIX_STREAM_MAX = @as(c_int, 8);
pub const _POSIX_SYMLINK_MAX = @as(c_int, 255);
pub const _POSIX_SYMLOOP_MAX = @as(c_int, 8);
pub const _POSIX_TIMER_MAX = @as(c_int, 32);
pub const _POSIX_TTY_NAME_MAX = @as(c_int, 9);
pub const _POSIX_TZNAME_MAX = @as(c_int, 6);
pub const _POSIX_CLOCKRES_MIN = @import("std").zig.c_translation.promoteIntLiteral(c_int, 20000000, .decimal);
pub const __undef_NR_OPEN = "";
pub const __undef_LINK_MAX = "";
pub const __undef_OPEN_MAX = "";
pub const __undef_ARG_MAX = "";
pub const _LINUX_LIMITS_H = "";
pub const NR_OPEN = @as(c_int, 1024);
pub const NGROUPS_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65536, .decimal);
pub const ARG_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 131072, .decimal);
pub const LINK_MAX = @as(c_int, 127);
pub const MAX_CANON = @as(c_int, 255);
pub const MAX_INPUT = @as(c_int, 255);
pub const NAME_MAX = @as(c_int, 255);
pub const PATH_MAX = @as(c_int, 4096);
pub const PIPE_BUF = @as(c_int, 4096);
pub const XATTR_NAME_MAX = @as(c_int, 255);
pub const XATTR_SIZE_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65536, .decimal);
pub const XATTR_LIST_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65536, .decimal);
pub const RTSIG_MAX = @as(c_int, 32);
pub const _POSIX_THREAD_KEYS_MAX = @as(c_int, 128);
pub const PTHREAD_KEYS_MAX = @as(c_int, 1024);
pub const _POSIX_THREAD_DESTRUCTOR_ITERATIONS = @as(c_int, 4);
pub const PTHREAD_DESTRUCTOR_ITERATIONS = _POSIX_THREAD_DESTRUCTOR_ITERATIONS;
pub const _POSIX_THREAD_THREADS_MAX = @as(c_int, 64);
pub const AIO_PRIO_DELTA_MAX = @as(c_int, 20);
pub const PTHREAD_STACK_MIN = @as(c_int, 16384);
pub const DELAYTIMER_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const TTY_NAME_MAX = @as(c_int, 32);
pub const LOGIN_NAME_MAX = @as(c_int, 256);
pub const HOST_NAME_MAX = @as(c_int, 64);
pub const MQ_PRIO_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 32768, .decimal);
pub const SEM_VALUE_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const SSIZE_MAX = LONG_MAX;
pub const _BITS_POSIX2_LIM_H = @as(c_int, 1);
pub const _POSIX2_BC_BASE_MAX = @as(c_int, 99);
pub const _POSIX2_BC_DIM_MAX = @as(c_int, 2048);
pub const _POSIX2_BC_SCALE_MAX = @as(c_int, 99);
pub const _POSIX2_BC_STRING_MAX = @as(c_int, 1000);
pub const _POSIX2_COLL_WEIGHTS_MAX = @as(c_int, 2);
pub const _POSIX2_EXPR_NEST_MAX = @as(c_int, 32);
pub const _POSIX2_LINE_MAX = @as(c_int, 2048);
pub const _POSIX2_RE_DUP_MAX = @as(c_int, 255);
pub const _POSIX2_CHARCLASS_NAME_MAX = @as(c_int, 14);
pub const BC_BASE_MAX = _POSIX2_BC_BASE_MAX;
pub const BC_DIM_MAX = _POSIX2_BC_DIM_MAX;
pub const BC_SCALE_MAX = _POSIX2_BC_SCALE_MAX;
pub const BC_STRING_MAX = _POSIX2_BC_STRING_MAX;
pub const COLL_WEIGHTS_MAX = @as(c_int, 255);
pub const EXPR_NEST_MAX = _POSIX2_EXPR_NEST_MAX;
pub const LINE_MAX = _POSIX2_LINE_MAX;
pub const CHARCLASS_NAME_MAX = @as(c_int, 2048);
pub const RE_DUP_MAX = @as(c_int, 0x7fff);
pub const SCHAR_MAX = __SCHAR_MAX__;
pub const SHRT_MAX = __SHRT_MAX__;
pub const INT_MAX = __INT_MAX__;
pub const LONG_MAX = __LONG_MAX__;
pub const SCHAR_MIN = -__SCHAR_MAX__ - @as(c_int, 1);
pub const SHRT_MIN = -__SHRT_MAX__ - @as(c_int, 1);
pub const INT_MIN = -__INT_MAX__ - @as(c_int, 1);
pub const LONG_MIN = -__LONG_MAX__ - @as(c_long, 1);
pub const UCHAR_MAX = (__SCHAR_MAX__ * @as(c_int, 2)) + @as(c_int, 1);
pub const USHRT_MAX = (__SHRT_MAX__ * @as(c_int, 2)) + @as(c_int, 1);
pub const UINT_MAX = (__INT_MAX__ * @as(c_uint, 2)) + @as(c_uint, 1);
pub const ULONG_MAX = (__LONG_MAX__ * @as(c_ulong, 2)) + @as(c_ulong, 1);
pub const CHAR_BIT = __CHAR_BIT__;
pub const CHAR_MIN = SCHAR_MIN;
pub const CHAR_MAX = __SCHAR_MAX__;
pub const _SYS_TIME_H = @as(c_int, 1);
pub const __time_t_defined = @as(c_int, 1);
pub const __timeval_defined = @as(c_int, 1);
pub const __suseconds_t_defined = "";
pub const _SYS_SELECT_H = @as(c_int, 1);
pub inline fn __FD_ISSET(d: anytype, s: anytype) @TypeOf((__FDS_BITS(s)[__FD_ELT(d)] & __FD_MASK(d)) != @as(c_int, 0)) {
    return (__FDS_BITS(s)[__FD_ELT(d)] & __FD_MASK(d)) != @as(c_int, 0);
}
pub const __sigset_t_defined = @as(c_int, 1);
pub const ____sigset_t_defined = "";
pub const _SIGSET_NWORDS = @as(c_int, 1024) / (@as(c_int, 8) * @import("std").zig.c_translation.sizeof(c_ulong));
pub const _STRUCT_TIMESPEC = @as(c_int, 1);
pub const _BITS_ENDIAN_H = @as(c_int, 1);
pub const __LITTLE_ENDIAN = @as(c_int, 1234);
pub const __BIG_ENDIAN = @as(c_int, 4321);
pub const __PDP_ENDIAN = @as(c_int, 3412);
pub const _BITS_ENDIANNESS_H = @as(c_int, 1);
pub const __BYTE_ORDER = __LITTLE_ENDIAN;
pub const __FLOAT_WORD_ORDER = __BYTE_ORDER;
pub inline fn __LONG_LONG_PAIR(HI: anytype, LO: anytype) @TypeOf(HI) {
    return blk: {
        _ = LO;
        break :blk HI;
    };
}
pub const __NFDBITS = @as(c_int, 8) * @import("std").zig.c_translation.cast(c_int, @import("std").zig.c_translation.sizeof(__fd_mask));
pub inline fn __FD_ELT(d: anytype) @TypeOf(d / __NFDBITS) {
    return d / __NFDBITS;
}
pub inline fn __FD_MASK(d: anytype) __fd_mask {
    return @import("std").zig.c_translation.cast(__fd_mask, @as(c_ulong, 1) << (d % __NFDBITS));
}
pub inline fn __FDS_BITS(set: anytype) @TypeOf(set.*.__fds_bits) {
    return set.*.__fds_bits;
}
pub const FD_SETSIZE = __FD_SETSIZE;
pub const NFDBITS = __NFDBITS;
pub inline fn FD_SET(fd: anytype, fdsetp: anytype) @TypeOf(__FD_SET(fd, fdsetp)) {
    return __FD_SET(fd, fdsetp);
}
pub inline fn FD_CLR(fd: anytype, fdsetp: anytype) @TypeOf(__FD_CLR(fd, fdsetp)) {
    return __FD_CLR(fd, fdsetp);
}
pub inline fn FD_ISSET(fd: anytype, fdsetp: anytype) @TypeOf(__FD_ISSET(fd, fdsetp)) {
    return __FD_ISSET(fd, fdsetp);
}
pub inline fn FD_ZERO(fdsetp: anytype) @TypeOf(__FD_ZERO(fdsetp)) {
    return __FD_ZERO(fdsetp);
}
pub inline fn timerisset(tvp: anytype) @TypeOf((tvp.*.tv_sec != 0) or (tvp.*.tv_usec != 0)) {
    return (tvp.*.tv_sec != 0) or (tvp.*.tv_usec != 0);
}
pub const _TIME_H = @as(c_int, 1);
pub const _BITS_TIME_H = @as(c_int, 1);
pub const CLOCKS_PER_SEC = @import("std").zig.c_translation.cast(__clock_t, @import("std").zig.c_translation.promoteIntLiteral(c_int, 1000000, .decimal));
pub const CLOCK_REALTIME = @as(c_int, 0);
pub const CLOCK_MONOTONIC = @as(c_int, 1);
pub const CLOCK_PROCESS_CPUTIME_ID = @as(c_int, 2);
pub const CLOCK_THREAD_CPUTIME_ID = @as(c_int, 3);
pub const CLOCK_MONOTONIC_RAW = @as(c_int, 4);
pub const CLOCK_REALTIME_COARSE = @as(c_int, 5);
pub const CLOCK_MONOTONIC_COARSE = @as(c_int, 6);
pub const CLOCK_BOOTTIME = @as(c_int, 7);
pub const CLOCK_REALTIME_ALARM = @as(c_int, 8);
pub const CLOCK_BOOTTIME_ALARM = @as(c_int, 9);
pub const CLOCK_TAI = @as(c_int, 11);
pub const TIMER_ABSTIME = @as(c_int, 1);
pub const __clock_t_defined = @as(c_int, 1);
pub const __struct_tm_defined = @as(c_int, 1);
pub const __clockid_t_defined = @as(c_int, 1);
pub const __timer_t_defined = @as(c_int, 1);
pub const __itimerspec_defined = @as(c_int, 1);
pub const __pid_t_defined = "";
pub const _BITS_TYPES_LOCALE_T_H = @as(c_int, 1);
pub const _BITS_TYPES___LOCALE_T_H = @as(c_int, 1);
pub const TIME_UTC = @as(c_int, 1);
pub inline fn __isleap(year: anytype) @TypeOf(((year % @as(c_int, 4)) == @as(c_int, 0)) and (((year % @as(c_int, 100)) != @as(c_int, 0)) or ((year % @as(c_int, 400)) == @as(c_int, 0)))) {
    return ((year % @as(c_int, 4)) == @as(c_int, 0)) and (((year % @as(c_int, 100)) != @as(c_int, 0)) or ((year % @as(c_int, 400)) == @as(c_int, 0)));
}
pub const SCM_HAVE_STDC_HEADERS = @as(c_int, 1);
pub const _STDLIB_H = @as(c_int, 1);
pub const WNOHANG = @as(c_int, 1);
pub const WUNTRACED = @as(c_int, 2);
pub const WSTOPPED = @as(c_int, 2);
pub const WEXITED = @as(c_int, 4);
pub const WCONTINUED = @as(c_int, 8);
pub const WNOWAIT = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x01000000, .hexadecimal);
pub const __WNOTHREAD = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x20000000, .hexadecimal);
pub const __WALL = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x40000000, .hexadecimal);
pub const __WCLONE = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x80000000, .hexadecimal);
pub inline fn __WEXITSTATUS(status: anytype) @TypeOf((status & @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xff00, .hexadecimal)) >> @as(c_int, 8)) {
    return (status & @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xff00, .hexadecimal)) >> @as(c_int, 8);
}
pub inline fn __WTERMSIG(status: anytype) @TypeOf(status & @as(c_int, 0x7f)) {
    return status & @as(c_int, 0x7f);
}
pub inline fn __WSTOPSIG(status: anytype) @TypeOf(__WEXITSTATUS(status)) {
    return __WEXITSTATUS(status);
}
pub inline fn __WIFEXITED(status: anytype) @TypeOf(__WTERMSIG(status) == @as(c_int, 0)) {
    return __WTERMSIG(status) == @as(c_int, 0);
}
pub inline fn __WIFSIGNALED(status: anytype) @TypeOf((@import("std").zig.c_translation.cast(i8, (status & @as(c_int, 0x7f)) + @as(c_int, 1)) >> @as(c_int, 1)) > @as(c_int, 0)) {
    return (@import("std").zig.c_translation.cast(i8, (status & @as(c_int, 0x7f)) + @as(c_int, 1)) >> @as(c_int, 1)) > @as(c_int, 0);
}
pub inline fn __WIFSTOPPED(status: anytype) @TypeOf((status & @as(c_int, 0xff)) == @as(c_int, 0x7f)) {
    return (status & @as(c_int, 0xff)) == @as(c_int, 0x7f);
}
pub inline fn __WIFCONTINUED(status: anytype) @TypeOf(status == __W_CONTINUED) {
    return status == __W_CONTINUED;
}
pub inline fn __WCOREDUMP(status: anytype) @TypeOf(status & __WCOREFLAG) {
    return status & __WCOREFLAG;
}
pub inline fn __W_EXITCODE(ret: anytype, sig: anytype) @TypeOf((ret << @as(c_int, 8)) | sig) {
    return (ret << @as(c_int, 8)) | sig;
}
pub inline fn __W_STOPCODE(sig: anytype) @TypeOf((sig << @as(c_int, 8)) | @as(c_int, 0x7f)) {
    return (sig << @as(c_int, 8)) | @as(c_int, 0x7f);
}
pub const __W_CONTINUED = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xffff, .hexadecimal);
pub const __WCOREFLAG = @as(c_int, 0x80);
pub inline fn WEXITSTATUS(status: anytype) @TypeOf(__WEXITSTATUS(status)) {
    return __WEXITSTATUS(status);
}
pub inline fn WTERMSIG(status: anytype) @TypeOf(__WTERMSIG(status)) {
    return __WTERMSIG(status);
}
pub inline fn WSTOPSIG(status: anytype) @TypeOf(__WSTOPSIG(status)) {
    return __WSTOPSIG(status);
}
pub inline fn WIFEXITED(status: anytype) @TypeOf(__WIFEXITED(status)) {
    return __WIFEXITED(status);
}
pub inline fn WIFSIGNALED(status: anytype) @TypeOf(__WIFSIGNALED(status)) {
    return __WIFSIGNALED(status);
}
pub inline fn WIFSTOPPED(status: anytype) @TypeOf(__WIFSTOPPED(status)) {
    return __WIFSTOPPED(status);
}
pub inline fn WIFCONTINUED(status: anytype) @TypeOf(__WIFCONTINUED(status)) {
    return __WIFCONTINUED(status);
}
pub const _BITS_FLOATN_H = "";
pub const __HAVE_FLOAT128 = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT128 = @as(c_int, 0);
pub const __HAVE_FLOAT64X = @as(c_int, 1);
pub const __HAVE_FLOAT64X_LONG_DOUBLE = @as(c_int, 1);
pub const _BITS_FLOATN_COMMON_H = "";
pub const __HAVE_FLOAT16 = @as(c_int, 0);
pub const __HAVE_FLOAT32 = @as(c_int, 1);
pub const __HAVE_FLOAT64 = @as(c_int, 1);
pub const __HAVE_FLOAT32X = @as(c_int, 1);
pub const __HAVE_FLOAT128X = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT16 = __HAVE_FLOAT16;
pub const __HAVE_DISTINCT_FLOAT32 = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT64 = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT32X = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT64X = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT128X = __HAVE_FLOAT128X;
pub const __HAVE_FLOAT128_UNLIKE_LDBL = (__HAVE_DISTINCT_FLOAT128 != 0) and (__LDBL_MANT_DIG__ != @as(c_int, 113));
pub const __HAVE_FLOATN_NOT_TYPEDEF = @as(c_int, 0);
pub inline fn __f64(x: anytype) @TypeOf(x) {
    return x;
}
pub inline fn __f32x(x: anytype) @TypeOf(x) {
    return x;
}
pub inline fn __builtin_huge_valf32() @TypeOf(__builtin_huge_valf()) {
    return __builtin_huge_valf();
}
pub inline fn __builtin_inff32() @TypeOf(__builtin_inff()) {
    return __builtin_inff();
}
pub inline fn __builtin_nanf32(x: anytype) @TypeOf(__builtin_nanf(x)) {
    return __builtin_nanf(x);
}
pub const __ldiv_t_defined = @as(c_int, 1);
pub const __lldiv_t_defined = @as(c_int, 1);
pub const RAND_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const EXIT_FAILURE = @as(c_int, 1);
pub const EXIT_SUCCESS = @as(c_int, 0);
pub const MB_CUR_MAX = __ctype_get_mb_cur_max();
pub const _SYS_TYPES_H = @as(c_int, 1);
pub const __u_char_defined = "";
pub const __ino_t_defined = "";
pub const __dev_t_defined = "";
pub const __gid_t_defined = "";
pub const __mode_t_defined = "";
pub const __nlink_t_defined = "";
pub const __uid_t_defined = "";
pub const __off_t_defined = "";
pub const __id_t_defined = "";
pub const __ssize_t_defined = "";
pub const __daddr_t_defined = "";
pub const __key_t_defined = "";
pub const __BIT_TYPES_DEFINED__ = @as(c_int, 1);
pub const _ENDIAN_H = @as(c_int, 1);
pub const LITTLE_ENDIAN = __LITTLE_ENDIAN;
pub const BIG_ENDIAN = __BIG_ENDIAN;
pub const PDP_ENDIAN = __PDP_ENDIAN;
pub const BYTE_ORDER = __BYTE_ORDER;
pub const _BITS_BYTESWAP_H = @as(c_int, 1);
pub inline fn __bswap_constant_16(x: anytype) __uint16_t {
    return @import("std").zig.c_translation.cast(__uint16_t, ((x >> @as(c_int, 8)) & @as(c_int, 0xff)) | ((x & @as(c_int, 0xff)) << @as(c_int, 8)));
}
pub inline fn __bswap_constant_32(x: anytype) @TypeOf(((((x & @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0xff000000, .hexadecimal)) >> @as(c_int, 24)) | ((x & @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0x00ff0000, .hexadecimal)) >> @as(c_int, 8))) | ((x & @as(c_uint, 0x0000ff00)) << @as(c_int, 8))) | ((x & @as(c_uint, 0x000000ff)) << @as(c_int, 24))) {
    return ((((x & @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0xff000000, .hexadecimal)) >> @as(c_int, 24)) | ((x & @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0x00ff0000, .hexadecimal)) >> @as(c_int, 8))) | ((x & @as(c_uint, 0x0000ff00)) << @as(c_int, 8))) | ((x & @as(c_uint, 0x000000ff)) << @as(c_int, 24));
}
pub inline fn __bswap_constant_64(x: anytype) @TypeOf(((((((((x & @as(c_ulonglong, 0xff00000000000000)) >> @as(c_int, 56)) | ((x & @as(c_ulonglong, 0x00ff000000000000)) >> @as(c_int, 40))) | ((x & @as(c_ulonglong, 0x0000ff0000000000)) >> @as(c_int, 24))) | ((x & @as(c_ulonglong, 0x000000ff00000000)) >> @as(c_int, 8))) | ((x & @as(c_ulonglong, 0x00000000ff000000)) << @as(c_int, 8))) | ((x & @as(c_ulonglong, 0x0000000000ff0000)) << @as(c_int, 24))) | ((x & @as(c_ulonglong, 0x000000000000ff00)) << @as(c_int, 40))) | ((x & @as(c_ulonglong, 0x00000000000000ff)) << @as(c_int, 56))) {
    return ((((((((x & @as(c_ulonglong, 0xff00000000000000)) >> @as(c_int, 56)) | ((x & @as(c_ulonglong, 0x00ff000000000000)) >> @as(c_int, 40))) | ((x & @as(c_ulonglong, 0x0000ff0000000000)) >> @as(c_int, 24))) | ((x & @as(c_ulonglong, 0x000000ff00000000)) >> @as(c_int, 8))) | ((x & @as(c_ulonglong, 0x00000000ff000000)) << @as(c_int, 8))) | ((x & @as(c_ulonglong, 0x0000000000ff0000)) << @as(c_int, 24))) | ((x & @as(c_ulonglong, 0x000000000000ff00)) << @as(c_int, 40))) | ((x & @as(c_ulonglong, 0x00000000000000ff)) << @as(c_int, 56));
}
pub const _BITS_UINTN_IDENTITY_H = @as(c_int, 1);
pub inline fn htobe16(x: anytype) @TypeOf(__bswap_16(x)) {
    return __bswap_16(x);
}
pub inline fn htole16(x: anytype) @TypeOf(__uint16_identity(x)) {
    return __uint16_identity(x);
}
pub inline fn be16toh(x: anytype) @TypeOf(__bswap_16(x)) {
    return __bswap_16(x);
}
pub inline fn le16toh(x: anytype) @TypeOf(__uint16_identity(x)) {
    return __uint16_identity(x);
}
pub inline fn htobe32(x: anytype) @TypeOf(__bswap_32(x)) {
    return __bswap_32(x);
}
pub inline fn htole32(x: anytype) @TypeOf(__uint32_identity(x)) {
    return __uint32_identity(x);
}
pub inline fn be32toh(x: anytype) @TypeOf(__bswap_32(x)) {
    return __bswap_32(x);
}
pub inline fn le32toh(x: anytype) @TypeOf(__uint32_identity(x)) {
    return __uint32_identity(x);
}
pub inline fn htobe64(x: anytype) @TypeOf(__bswap_64(x)) {
    return __bswap_64(x);
}
pub inline fn htole64(x: anytype) @TypeOf(__uint64_identity(x)) {
    return __uint64_identity(x);
}
pub inline fn be64toh(x: anytype) @TypeOf(__bswap_64(x)) {
    return __bswap_64(x);
}
pub inline fn le64toh(x: anytype) @TypeOf(__uint64_identity(x)) {
    return __uint64_identity(x);
}
pub const __blksize_t_defined = "";
pub const __blkcnt_t_defined = "";
pub const __fsblkcnt_t_defined = "";
pub const __fsfilcnt_t_defined = "";
pub const _BITS_PTHREADTYPES_COMMON_H = @as(c_int, 1);
pub const _THREAD_SHARED_TYPES_H = @as(c_int, 1);
pub const _BITS_PTHREADTYPES_ARCH_H = @as(c_int, 1);
pub const __SIZEOF_PTHREAD_MUTEX_T = @as(c_int, 40);
pub const __SIZEOF_PTHREAD_ATTR_T = @as(c_int, 56);
pub const __SIZEOF_PTHREAD_RWLOCK_T = @as(c_int, 56);
pub const __SIZEOF_PTHREAD_BARRIER_T = @as(c_int, 32);
pub const __SIZEOF_PTHREAD_MUTEXATTR_T = @as(c_int, 4);
pub const __SIZEOF_PTHREAD_COND_T = @as(c_int, 48);
pub const __SIZEOF_PTHREAD_CONDATTR_T = @as(c_int, 4);
pub const __SIZEOF_PTHREAD_RWLOCKATTR_T = @as(c_int, 8);
pub const __SIZEOF_PTHREAD_BARRIERATTR_T = @as(c_int, 4);
pub const __LOCK_ALIGNMENT = "";
pub const __ONCE_ALIGNMENT = "";
pub const _THREAD_MUTEX_INTERNAL_H = @as(c_int, 1);
pub const __PTHREAD_MUTEX_HAVE_PREV = @as(c_int, 1);
pub const _RWLOCK_INTERNAL_H = "";
pub inline fn __PTHREAD_RWLOCK_INITIALIZER(__flags: anytype) @TypeOf(__flags) {
    return blk: {
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = __PTHREAD_RWLOCK_ELISION_EXTRA;
        _ = @as(c_int, 0);
        break :blk __flags;
    };
}
pub const __have_pthread_attr_t = @as(c_int, 1);
pub const _ALLOCA_H = @as(c_int, 1);
pub const __COMPAR_FN_T = "";
pub const SCM_HAVE_SYS_SELECT_H = @as(c_int, 1);
pub const SCM_HAVE_WINSOCK2_H = @as(c_int, 0);
pub const SCM_ENABLE_DEPRECATED = @as(c_int, 1);
pub const SCM_STACK_GROWS_UP = @as(c_int, 0);
pub const SCM_SIZEOF_CHAR = @as(c_int, 1);
pub const SCM_SIZEOF_UNSIGNED_CHAR = @as(c_int, 1);
pub const SCM_SIZEOF_SHORT = @as(c_int, 2);
pub const SCM_SIZEOF_UNSIGNED_SHORT = @as(c_int, 2);
pub const SCM_SIZEOF_LONG = @as(c_int, 8);
pub const SCM_SIZEOF_UNSIGNED_LONG = @as(c_int, 8);
pub const SCM_SIZEOF_INT = @as(c_int, 4);
pub const SCM_SIZEOF_UNSIGNED_INT = @as(c_int, 4);
pub const SCM_SIZEOF_SIZE_T = @as(c_int, 8);
pub const SCM_SIZEOF_LONG_LONG = @as(c_int, 8);
pub const SCM_SIZEOF_UNSIGNED_LONG_LONG = @as(c_int, 8);
pub const SCM_SIZEOF_INTMAX = @as(c_int, 8);
pub const SCM_SIZEOF_SCM_T_PTRDIFF = @as(c_int, 8);
pub const SCM_SIZEOF_INTPTR_T = @as(c_int, 8);
pub const SCM_SIZEOF_UINTPTR_T = @as(c_int, 8);
pub const SCM_USE_PTHREAD_THREADS = @as(c_int, 1);
pub const SCM_USE_NULL_THREADS = @as(c_int, 0);
pub const SCM_NEED_BRACES_ON_PTHREAD_ONCE_INIT = @as(c_int, 0);
pub const SCM_NEED_BRACES_ON_PTHREAD_MUTEX_INITIALIZER = @as(c_int, 0);
pub const SCM_HAVE_PTHREAD_SIGMASK = @as(c_int, 1);
pub const SCM_HAVE_GC_PTHREAD_CANCEL = @as(c_int, 1);
pub const SCM_HAVE_GC_PTHREAD_EXIT = @as(c_int, 1);
pub const SCM_HAVE_GC_PTHREAD_SIGMASK = @as(c_int, 1);
pub const SCM_HAVE_STRUCT_DIRENT64 = @as(c_int, 1);
pub const SCM_HAVE_READDIR64_R = @as(c_int, 0);
pub const SCM_T_OFF_MAX = INT64_MAX;
pub const SCM_T_OFF_MIN = INT64_MIN;
pub const SCM_HAVE_THREAD_STORAGE_CLASS = "";
pub const SCM_ICONVEH_ERROR = @as(c_int, 0);
pub const SCM_ICONVEH_QUESTION_MARK = @as(c_int, 1);
pub const SCM_ICONVEH_ESCAPE_SEQUENCE = @as(c_int, 2);
pub const SCM_HAVE_AUXILIARY_STACK = @as(c_int, 0);
pub const SCM_ENABLE_MINI_GMP = @as(c_int, 1);
pub const SCM_DEBUG = @as(c_int, 0);
pub const SCM_DEBUG_PAIR_ACCESSES = SCM_DEBUG;
pub const SCM_DEBUG_REST_ARGUMENT = SCM_DEBUG;
pub const SCM_DEBUG_TYPING_STRICTNESS = @as(c_int, 1);
pub const SCM_T_SIGNED_BITS_MAX = INTPTR_MAX;
pub const SCM_T_SIGNED_BITS_MIN = INTPTR_MIN;
pub const SCM_T_BITS_MAX = UINTPTR_MAX;
pub inline fn SCM_PACK(x: anytype) SCM {
    return @import("std").zig.c_translation.cast(SCM, x);
}
pub inline fn SCM_UNPACK_POINTER(x: anytype) [*c]scm_t_bits {
    return @import("std").zig.c_translation.cast([*c]scm_t_bits, SCM_UNPACK(x));
}
pub inline fn SCM_PACK_POINTER(x: anytype) @TypeOf(SCM_PACK(@import("std").zig.c_translation.cast(scm_t_bits, x))) {
    return SCM_PACK(@import("std").zig.c_translation.cast(scm_t_bits, x));
}
pub inline fn scm_is_eq(x: anytype, y: anytype) @TypeOf(SCM_UNPACK(x) == SCM_UNPACK(y)) {
    return SCM_UNPACK(x) == SCM_UNPACK(y);
}
pub inline fn SCM_IMP(x: anytype) @TypeOf(@as(c_int, 6) & SCM_UNPACK(x)) {
    return @as(c_int, 6) & SCM_UNPACK(x);
}
pub inline fn SCM_NIMP(x: anytype) @TypeOf(!(SCM_IMP(x) != 0)) {
    return !(SCM_IMP(x) != 0);
}
pub inline fn SCM_HEAP_OBJECT_P(x: anytype) @TypeOf(SCM_NIMP(x)) {
    return SCM_NIMP(x);
}
pub inline fn SCM_I_CONSP(x: anytype) @TypeOf(!(SCM_IMP(x) != 0) and ((@as(c_int, 1) & SCM_CELL_TYPE(x)) == @as(c_int, 0))) {
    return !(SCM_IMP(x) != 0) and ((@as(c_int, 1) & SCM_CELL_TYPE(x)) == @as(c_int, 0));
}
pub const scm_tc2_int = @as(c_int, 2);
pub inline fn SCM_ITAG3(x: anytype) @TypeOf(@as(c_int, 7) & SCM_UNPACK(x)) {
    return @as(c_int, 7) & SCM_UNPACK(x);
}
pub inline fn SCM_TYP3(x: anytype) @TypeOf(@as(c_int, 7) & SCM_CELL_TYPE(x)) {
    return @as(c_int, 7) & SCM_CELL_TYPE(x);
}
pub const scm_tc3_cons = @as(c_int, 0);
pub const scm_tc3_struct = @as(c_int, 1);
pub const scm_tc3_int_1 = scm_tc2_int + @as(c_int, 0);
pub const scm_tc3_unused = @as(c_int, 3);
pub const scm_tc3_imm24 = @as(c_int, 4);
pub const scm_tc3_tc7_1 = @as(c_int, 5);
pub const scm_tc3_int_2 = scm_tc2_int + @as(c_int, 4);
pub const scm_tc3_tc7_2 = @as(c_int, 7);
pub inline fn SCM_ITAG7(x: anytype) @TypeOf(@as(c_int, 0x7f) & SCM_UNPACK(x)) {
    return @as(c_int, 0x7f) & SCM_UNPACK(x);
}
pub inline fn SCM_TYP7(x: anytype) @TypeOf(@as(c_int, 0x7f) & SCM_CELL_TYPE(x)) {
    return @as(c_int, 0x7f) & SCM_CELL_TYPE(x);
}
pub inline fn SCM_HAS_HEAP_TYPE(x: anytype, @"type": anytype, tag: anytype) @TypeOf((SCM_NIMP(x) != 0) and (@"type"(x) == tag)) {
    return (SCM_NIMP(x) != 0) and (@"type"(x) == tag);
}
pub inline fn SCM_HAS_TYP7(x: anytype, tag: anytype) @TypeOf(SCM_HAS_HEAP_TYPE(x, SCM_TYP7, tag)) {
    return SCM_HAS_HEAP_TYPE(x, SCM_TYP7, tag);
}
pub const scm_tc7_symbol = @as(c_int, 0x05);
pub const scm_tc7_variable = @as(c_int, 0x07);
pub const scm_tc7_vector = @as(c_int, 0x0d);
pub const scm_tc7_wvect = @as(c_int, 0x0f);
pub const scm_tc7_string = @as(c_int, 0x15);
pub const scm_tc7_number = @as(c_int, 0x17);
pub const scm_tc7_hashtable = @as(c_int, 0x1d);
pub const scm_tc7_pointer = @as(c_int, 0x1f);
pub const scm_tc7_fluid = @as(c_int, 0x25);
pub const scm_tc7_stringbuf = @as(c_int, 0x27);
pub const scm_tc7_dynamic_state = @as(c_int, 0x2d);
pub const scm_tc7_frame = @as(c_int, 0x2f);
pub const scm_tc7_keyword = @as(c_int, 0x35);
pub const scm_tc7_atomic_box = @as(c_int, 0x37);
pub const scm_tc7_syntax = @as(c_int, 0x3d);
pub const scm_tc7_values = @as(c_int, 0x3f);
pub const scm_tc7_program = @as(c_int, 0x45);
pub const scm_tc7_vm_cont = @as(c_int, 0x47);
pub const scm_tc7_bytevector = @as(c_int, 0x4d);
pub const scm_tc7_unused_4f = @as(c_int, 0x4f);
pub const scm_tc7_weak_set = @as(c_int, 0x55);
pub const scm_tc7_weak_table = @as(c_int, 0x57);
pub const scm_tc7_array = @as(c_int, 0x5d);
pub const scm_tc7_bitvector = @as(c_int, 0x5f);
pub const scm_tc7_unused_65 = @as(c_int, 0x65);
pub const scm_tc7_unused_67 = @as(c_int, 0x67);
pub const scm_tc7_unused_6d = @as(c_int, 0x6d);
pub const scm_tc7_unused_6f = @as(c_int, 0x6f);
pub const scm_tc7_unused_75 = @as(c_int, 0x75);
pub const scm_tc7_smob = @as(c_int, 0x77);
pub const scm_tc7_port = @as(c_int, 0x7d);
pub const scm_tc7_unused_7f = @as(c_int, 0x7f);
pub inline fn SCM_TYP16(x: anytype) @TypeOf(@import("std").zig.c_translation.promoteIntLiteral(c_int, 0xffff, .hexadecimal) & SCM_CELL_TYPE(x)) {
    return @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xffff, .hexadecimal) & SCM_CELL_TYPE(x);
}
pub inline fn SCM_HAS_TYP16(x: anytype, tag: anytype) @TypeOf(SCM_HAS_HEAP_TYPE(x, SCM_TYP16, tag)) {
    return SCM_HAS_HEAP_TYPE(x, SCM_TYP16, tag);
}
pub inline fn SCM_TYP16_PREDICATE(tag: anytype, x: anytype) @TypeOf(SCM_HAS_TYP16(x, tag)) {
    return SCM_HAS_TYP16(x, tag);
}
pub inline fn SCM_ITAG8(X: anytype) @TypeOf(SCM_UNPACK(X) & @as(c_int, 0xff)) {
    return SCM_UNPACK(X) & @as(c_int, 0xff);
}
pub inline fn SCM_MAKE_ITAG8_BITS(X: anytype, TAG: anytype) @TypeOf((X << @as(c_int, 8)) + TAG) {
    return (X << @as(c_int, 8)) + TAG;
}
pub inline fn SCM_MAKE_ITAG8(X: anytype, TAG: anytype) @TypeOf(SCM_PACK(SCM_MAKE_ITAG8_BITS(X, TAG))) {
    return SCM_PACK(SCM_MAKE_ITAG8_BITS(X, TAG));
}
pub inline fn SCM_ITAG8_DATA(X: anytype) @TypeOf(SCM_UNPACK(X) >> @as(c_int, 8)) {
    return SCM_UNPACK(X) >> @as(c_int, 8);
}
pub inline fn SCM_IFLAGP(n: anytype) @TypeOf(SCM_ITAG8(n) == scm_tc8_flag) {
    return SCM_ITAG8(n) == scm_tc8_flag;
}
pub inline fn SCM_MAKIFLAG_BITS(n: anytype) @TypeOf(SCM_MAKE_ITAG8_BITS(n, scm_tc8_flag)) {
    return SCM_MAKE_ITAG8_BITS(n, scm_tc8_flag);
}
pub inline fn SCM_IFLAGNUM(n: anytype) @TypeOf(SCM_ITAG8_DATA(n)) {
    return SCM_ITAG8_DATA(n);
}
pub const SCM_BOOL_F_BITS = SCM_MAKIFLAG_BITS(@as(c_int, 0));
pub const SCM_ELISP_NIL_BITS = SCM_MAKIFLAG_BITS(@as(c_int, 1));
pub const SCM_BOOL_F = SCM_PACK(SCM_BOOL_F_BITS);
pub const SCM_ELISP_NIL = SCM_PACK(SCM_ELISP_NIL_BITS);
pub const SCM_EOL_BITS = SCM_MAKIFLAG_BITS(@as(c_int, 3));
pub const SCM_BOOL_T_BITS = SCM_MAKIFLAG_BITS(@as(c_int, 4));
pub const SCM_EOL = SCM_PACK(SCM_EOL_BITS);
pub const SCM_BOOL_T = SCM_PACK(SCM_BOOL_T_BITS);
pub const SCM_UNSPECIFIED_BITS = SCM_MAKIFLAG_BITS(@as(c_int, 8));
pub const SCM_UNDEFINED_BITS = SCM_MAKIFLAG_BITS(@as(c_int, 9));
pub const SCM_EOF_VAL_BITS = SCM_MAKIFLAG_BITS(@as(c_int, 10));
pub const SCM_UNSPECIFIED = SCM_PACK(SCM_UNSPECIFIED_BITS);
pub const SCM_UNDEFINED = SCM_PACK(SCM_UNDEFINED_BITS);
pub const SCM_EOF_VAL = SCM_PACK(SCM_EOF_VAL_BITS);
pub inline fn SCM_UNBNDP(x: anytype) @TypeOf(scm_is_eq(x, SCM_UNDEFINED)) {
    return scm_is_eq(x, SCM_UNDEFINED);
}
pub inline fn SCM_MATCHES_BITS_IN_COMMON(x: anytype, a: anytype, b: anytype) @TypeOf((SCM_UNPACK(x) & ~(SCM_UNPACK(a) ^ SCM_UNPACK(b))) == (SCM_UNPACK(a) & SCM_UNPACK(b))) {
    return (SCM_UNPACK(x) & ~(SCM_UNPACK(a) ^ SCM_UNPACK(b))) == (SCM_UNPACK(a) & SCM_UNPACK(b));
}
pub const SCM_EXPECT = __builtin_expect;
pub inline fn SCM_LIKELY(_expr: anytype) @TypeOf(SCM_EXPECT(_expr, @as(c_int, 1))) {
    return SCM_EXPECT(_expr, @as(c_int, 1));
}
pub inline fn SCM_UNLIKELY(_expr: anytype) @TypeOf(SCM_EXPECT(_expr, @as(c_int, 0))) {
    return SCM_EXPECT(_expr, @as(c_int, 0));
}
pub const SCM_CHAR_BIT = CHAR_BIT;
pub const SCM_LONG_BIT = SCM_SIZEOF_LONG * @as(c_int, 8);
pub inline fn SCM_STACK_PTR(ptr: anytype) [*c]SCM_STACKITEM {
    return @import("std").zig.c_translation.cast([*c]SCM_STACKITEM, @import("std").zig.c_translation.cast(?*anyopaque, ptr));
}
pub const SCM_ALIST_H = "";
pub const SCM_ERROR_H = "";
pub const SCM_ARGn = @as(c_int, 0);
pub const SCM_ARG1 = @as(c_int, 1);
pub const SCM_ARG2 = @as(c_int, 2);
pub const SCM_ARG3 = @as(c_int, 3);
pub const SCM_ARG4 = @as(c_int, 4);
pub const SCM_ARG5 = @as(c_int, 5);
pub const SCM_ARG6 = @as(c_int, 6);
pub const SCM_ARG7 = @as(c_int, 7);
pub const SCM_PAIRS_H = "";
pub const SCM_GC_H = "";
pub const SCM_INLINE_H = "";
pub const SCM_C_INLINE_KEYWORD = SCM_C_INLINE;
pub const SCM_CAN_INLINE = @as(c_int, 1);
pub const SCM_INLINE = SCM_C_EXTERN_INLINE;
pub const SCM_INLINE_IMPLEMENTATION = SCM_INLINE;
pub const SCM_CHOOKS_H = "";
pub inline fn PTR2SCM(x: anytype) @TypeOf(SCM_PACK_POINTER(x)) {
    return SCM_PACK_POINTER(x);
}
pub inline fn SCM2PTR(x: anytype) [*c]scm_t_cell {
    return @import("std").zig.c_translation.cast([*c]scm_t_cell, SCM_UNPACK_POINTER(x));
}
pub inline fn SCM_GC_CELL_OBJECT(x: anytype, n: anytype) @TypeOf(@import("std").zig.c_translation.cast([*c]SCM, SCM2PTR(x))[n]) {
    return @import("std").zig.c_translation.cast([*c]SCM, SCM2PTR(x))[n];
}
pub inline fn SCM_GC_CELL_WORD(x: anytype, n: anytype) @TypeOf(SCM_UNPACK(SCM_GC_CELL_OBJECT(x, n))) {
    return SCM_UNPACK(SCM_GC_CELL_OBJECT(x, n));
}
pub inline fn SCM_GC_SET_CELL_WORD(x: anytype, n: anytype, v: anytype) @TypeOf(SCM_GC_SET_CELL_OBJECT(x, n, SCM_PACK(v))) {
    return SCM_GC_SET_CELL_OBJECT(x, n, SCM_PACK(v));
}
pub inline fn SCM_GC_CELL_TYPE(x: anytype) @TypeOf(SCM_GC_CELL_OBJECT(x, @as(c_int, 0))) {
    return SCM_GC_CELL_OBJECT(x, @as(c_int, 0));
}
pub inline fn SCM_CELL_WORD(x: anytype, n: anytype) @TypeOf(SCM_GC_CELL_WORD(x, n)) {
    return SCM_GC_CELL_WORD(x, n);
}
pub inline fn SCM_CELL_WORD_0(x: anytype) @TypeOf(SCM_CELL_WORD(x, @as(c_int, 0))) {
    return SCM_CELL_WORD(x, @as(c_int, 0));
}
pub inline fn SCM_CELL_WORD_1(x: anytype) @TypeOf(SCM_CELL_WORD(x, @as(c_int, 1))) {
    return SCM_CELL_WORD(x, @as(c_int, 1));
}
pub inline fn SCM_CELL_WORD_2(x: anytype) @TypeOf(SCM_CELL_WORD(x, @as(c_int, 2))) {
    return SCM_CELL_WORD(x, @as(c_int, 2));
}
pub inline fn SCM_CELL_WORD_3(x: anytype) @TypeOf(SCM_CELL_WORD(x, @as(c_int, 3))) {
    return SCM_CELL_WORD(x, @as(c_int, 3));
}
pub inline fn SCM_CELL_OBJECT(x: anytype, n: anytype) @TypeOf(SCM_GC_CELL_OBJECT(x, n)) {
    return SCM_GC_CELL_OBJECT(x, n);
}
pub inline fn SCM_CELL_OBJECT_0(x: anytype) @TypeOf(SCM_CELL_OBJECT(x, @as(c_int, 0))) {
    return SCM_CELL_OBJECT(x, @as(c_int, 0));
}
pub inline fn SCM_CELL_OBJECT_1(x: anytype) @TypeOf(SCM_CELL_OBJECT(x, @as(c_int, 1))) {
    return SCM_CELL_OBJECT(x, @as(c_int, 1));
}
pub inline fn SCM_CELL_OBJECT_2(x: anytype) @TypeOf(SCM_CELL_OBJECT(x, @as(c_int, 2))) {
    return SCM_CELL_OBJECT(x, @as(c_int, 2));
}
pub inline fn SCM_CELL_OBJECT_3(x: anytype) @TypeOf(SCM_CELL_OBJECT(x, @as(c_int, 3))) {
    return SCM_CELL_OBJECT(x, @as(c_int, 3));
}
pub inline fn SCM_SET_CELL_WORD(x: anytype, n: anytype, v: anytype) @TypeOf(SCM_GC_SET_CELL_WORD(x, n, v)) {
    return SCM_GC_SET_CELL_WORD(x, n, v);
}
pub inline fn SCM_SET_CELL_WORD_0(x: anytype, v: anytype) @TypeOf(SCM_SET_CELL_WORD(x, @as(c_int, 0), v)) {
    return SCM_SET_CELL_WORD(x, @as(c_int, 0), v);
}
pub inline fn SCM_SET_CELL_WORD_1(x: anytype, v: anytype) @TypeOf(SCM_SET_CELL_WORD(x, @as(c_int, 1), v)) {
    return SCM_SET_CELL_WORD(x, @as(c_int, 1), v);
}
pub inline fn SCM_SET_CELL_WORD_2(x: anytype, v: anytype) @TypeOf(SCM_SET_CELL_WORD(x, @as(c_int, 2), v)) {
    return SCM_SET_CELL_WORD(x, @as(c_int, 2), v);
}
pub inline fn SCM_SET_CELL_WORD_3(x: anytype, v: anytype) @TypeOf(SCM_SET_CELL_WORD(x, @as(c_int, 3), v)) {
    return SCM_SET_CELL_WORD(x, @as(c_int, 3), v);
}
pub inline fn SCM_SET_CELL_OBJECT(x: anytype, n: anytype, v: anytype) @TypeOf(SCM_GC_SET_CELL_OBJECT(x, n, v)) {
    return SCM_GC_SET_CELL_OBJECT(x, n, v);
}
pub inline fn SCM_SET_CELL_OBJECT_0(x: anytype, v: anytype) @TypeOf(SCM_SET_CELL_OBJECT(x, @as(c_int, 0), v)) {
    return SCM_SET_CELL_OBJECT(x, @as(c_int, 0), v);
}
pub inline fn SCM_SET_CELL_OBJECT_1(x: anytype, v: anytype) @TypeOf(SCM_SET_CELL_OBJECT(x, @as(c_int, 1), v)) {
    return SCM_SET_CELL_OBJECT(x, @as(c_int, 1), v);
}
pub inline fn SCM_SET_CELL_OBJECT_2(x: anytype, v: anytype) @TypeOf(SCM_SET_CELL_OBJECT(x, @as(c_int, 2), v)) {
    return SCM_SET_CELL_OBJECT(x, @as(c_int, 2), v);
}
pub inline fn SCM_SET_CELL_OBJECT_3(x: anytype, v: anytype) @TypeOf(SCM_SET_CELL_OBJECT(x, @as(c_int, 3), v)) {
    return SCM_SET_CELL_OBJECT(x, @as(c_int, 3), v);
}
pub inline fn SCM_CELL_OBJECT_LOC(x: anytype, n: anytype) @TypeOf(&SCM_GC_CELL_OBJECT(x, n)) {
    return &SCM_GC_CELL_OBJECT(x, n);
}
pub inline fn SCM_CARLOC(x: anytype) @TypeOf(SCM_CELL_OBJECT_LOC(x, @as(c_int, 0))) {
    return SCM_CELL_OBJECT_LOC(x, @as(c_int, 0));
}
pub inline fn SCM_CDRLOC(x: anytype) @TypeOf(SCM_CELL_OBJECT_LOC(x, @as(c_int, 1))) {
    return SCM_CELL_OBJECT_LOC(x, @as(c_int, 1));
}
pub inline fn SCM_CELL_TYPE(x: anytype) @TypeOf(SCM_CELL_WORD_0(x)) {
    return SCM_CELL_WORD_0(x);
}
pub inline fn SCM_SET_CELL_TYPE(x: anytype, t: anytype) @TypeOf(SCM_SET_CELL_WORD_0(x, t)) {
    return SCM_SET_CELL_WORD_0(x, t);
}
pub inline fn SCM_GC_MALLOC(size: anytype) @TypeOf(scm_gc_malloc(size, NULL)) {
    return scm_gc_malloc(size, NULL);
}
pub inline fn SCM_GC_MALLOC_POINTERLESS(size: anytype) @TypeOf(scm_gc_malloc_pointerless(size, NULL)) {
    return scm_gc_malloc_pointerless(size, NULL);
}
pub inline fn scm_is_null_and_not_nil(x: anytype) @TypeOf(scm_is_eq(x, SCM_EOL)) {
    return scm_is_eq(x, SCM_EOL);
}
pub inline fn scm_is_null_assume_not_nil(x: anytype) @TypeOf(scm_is_eq(x, SCM_EOL)) {
    return scm_is_eq(x, SCM_EOL);
}
pub inline fn scm_is_null_or_nil(x: anytype) @TypeOf(SCM_MATCHES_BITS_IN_COMMON(x, SCM_ELISP_NIL, SCM_EOL)) {
    return SCM_MATCHES_BITS_IN_COMMON(x, SCM_ELISP_NIL, SCM_EOL);
}
pub inline fn SCM_NILP(x: anytype) @TypeOf(scm_is_eq(x, SCM_ELISP_NIL)) {
    return scm_is_eq(x, SCM_ELISP_NIL);
}
pub inline fn SCM_NULL_OR_NIL_P(x: anytype) @TypeOf(scm_is_null_or_nil(x)) {
    return scm_is_null_or_nil(x);
}
pub inline fn SCM_NULLP(x: anytype) @TypeOf(scm_is_null(x)) {
    return scm_is_null(x);
}
pub inline fn SCM_NNULLP(x: anytype) @TypeOf(!(scm_is_null(x) != 0)) {
    return !(scm_is_null(x) != 0);
}
pub inline fn SCM_CONSP(x: anytype) @TypeOf(scm_is_pair(x)) {
    return scm_is_pair(x);
}
pub inline fn SCM_NCONSP(x: anytype) @TypeOf(!(SCM_CONSP(x) != 0)) {
    return !(SCM_CONSP(x) != 0);
}
pub inline fn scm_is_null(x: anytype) @TypeOf(scm_is_null_or_nil(x)) {
    return scm_is_null_or_nil(x);
}
pub inline fn SCM_CAR(x: anytype) @TypeOf(SCM_VALIDATE_PAIR(x, SCM_CELL_OBJECT_0(x))) {
    return SCM_VALIDATE_PAIR(x, SCM_CELL_OBJECT_0(x));
}
pub inline fn SCM_CDR(x: anytype) @TypeOf(SCM_VALIDATE_PAIR(x, SCM_CELL_OBJECT_1(x))) {
    return SCM_VALIDATE_PAIR(x, SCM_CELL_OBJECT_1(x));
}
pub inline fn SCM_SETCAR(x: anytype, v: anytype) @TypeOf(SCM_VALIDATE_PAIR(x, SCM_SET_CELL_OBJECT_0(x, v))) {
    return SCM_VALIDATE_PAIR(x, SCM_SET_CELL_OBJECT_0(x, v));
}
pub inline fn SCM_SETCDR(x: anytype, v: anytype) @TypeOf(SCM_VALIDATE_PAIR(x, SCM_SET_CELL_OBJECT_1(x, v))) {
    return SCM_VALIDATE_PAIR(x, SCM_SET_CELL_OBJECT_1(x, v));
}
pub inline fn SCM_CAAR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CAR(OBJ))) {
    return SCM_CAR(SCM_CAR(OBJ));
}
pub inline fn SCM_CDAR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CAR(OBJ))) {
    return SCM_CDR(SCM_CAR(OBJ));
}
pub inline fn SCM_CADR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CDR(OBJ))) {
    return SCM_CAR(SCM_CDR(OBJ));
}
pub inline fn SCM_CDDR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CDR(OBJ))) {
    return SCM_CDR(SCM_CDR(OBJ));
}
pub inline fn SCM_CAAAR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CAR(SCM_CAR(OBJ)))) {
    return SCM_CAR(SCM_CAR(SCM_CAR(OBJ)));
}
pub inline fn SCM_CDAAR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CAR(SCM_CAR(OBJ)))) {
    return SCM_CDR(SCM_CAR(SCM_CAR(OBJ)));
}
pub inline fn SCM_CADAR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CDR(SCM_CAR(OBJ)))) {
    return SCM_CAR(SCM_CDR(SCM_CAR(OBJ)));
}
pub inline fn SCM_CDDAR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CDR(SCM_CAR(OBJ)))) {
    return SCM_CDR(SCM_CDR(SCM_CAR(OBJ)));
}
pub inline fn SCM_CAADR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CAR(SCM_CDR(OBJ)))) {
    return SCM_CAR(SCM_CAR(SCM_CDR(OBJ)));
}
pub inline fn SCM_CDADR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CAR(SCM_CDR(OBJ)))) {
    return SCM_CDR(SCM_CAR(SCM_CDR(OBJ)));
}
pub inline fn SCM_CADDR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CDR(SCM_CDR(OBJ)))) {
    return SCM_CAR(SCM_CDR(SCM_CDR(OBJ)));
}
pub inline fn SCM_CDDDR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CDR(SCM_CDR(OBJ)))) {
    return SCM_CDR(SCM_CDR(SCM_CDR(OBJ)));
}
pub inline fn SCM_CAAAAR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CAR(SCM_CAR(SCM_CAR(OBJ))))) {
    return SCM_CAR(SCM_CAR(SCM_CAR(SCM_CAR(OBJ))));
}
pub inline fn SCM_CDAAAR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CAR(SCM_CAR(SCM_CAR(OBJ))))) {
    return SCM_CDR(SCM_CAR(SCM_CAR(SCM_CAR(OBJ))));
}
pub inline fn SCM_CADAAR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CDR(SCM_CAR(SCM_CAR(OBJ))))) {
    return SCM_CAR(SCM_CDR(SCM_CAR(SCM_CAR(OBJ))));
}
pub inline fn SCM_CDDAAR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CDR(SCM_CAR(SCM_CAR(OBJ))))) {
    return SCM_CDR(SCM_CDR(SCM_CAR(SCM_CAR(OBJ))));
}
pub inline fn SCM_CAADAR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CAR(SCM_CDR(SCM_CAR(OBJ))))) {
    return SCM_CAR(SCM_CAR(SCM_CDR(SCM_CAR(OBJ))));
}
pub inline fn SCM_CDADAR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CAR(SCM_CDR(SCM_CAR(OBJ))))) {
    return SCM_CDR(SCM_CAR(SCM_CDR(SCM_CAR(OBJ))));
}
pub inline fn SCM_CADDAR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CDR(SCM_CDR(SCM_CAR(OBJ))))) {
    return SCM_CAR(SCM_CDR(SCM_CDR(SCM_CAR(OBJ))));
}
pub inline fn SCM_CDDDAR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CDR(SCM_CDR(SCM_CAR(OBJ))))) {
    return SCM_CDR(SCM_CDR(SCM_CDR(SCM_CAR(OBJ))));
}
pub inline fn SCM_CAAADR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CAR(SCM_CAR(SCM_CDR(OBJ))))) {
    return SCM_CAR(SCM_CAR(SCM_CAR(SCM_CDR(OBJ))));
}
pub inline fn SCM_CDAADR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CAR(SCM_CAR(SCM_CDR(OBJ))))) {
    return SCM_CDR(SCM_CAR(SCM_CAR(SCM_CDR(OBJ))));
}
pub inline fn SCM_CADADR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CDR(SCM_CAR(SCM_CDR(OBJ))))) {
    return SCM_CAR(SCM_CDR(SCM_CAR(SCM_CDR(OBJ))));
}
pub inline fn SCM_CDDADR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CDR(SCM_CAR(SCM_CDR(OBJ))))) {
    return SCM_CDR(SCM_CDR(SCM_CAR(SCM_CDR(OBJ))));
}
pub inline fn SCM_CAADDR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CAR(SCM_CDR(SCM_CDR(OBJ))))) {
    return SCM_CAR(SCM_CAR(SCM_CDR(SCM_CDR(OBJ))));
}
pub inline fn SCM_CDADDR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CAR(SCM_CDR(SCM_CDR(OBJ))))) {
    return SCM_CDR(SCM_CAR(SCM_CDR(SCM_CDR(OBJ))));
}
pub inline fn SCM_CADDDR(OBJ: anytype) @TypeOf(SCM_CAR(SCM_CDR(SCM_CDR(SCM_CDR(OBJ))))) {
    return SCM_CAR(SCM_CDR(SCM_CDR(SCM_CDR(OBJ))));
}
pub inline fn SCM_CDDDDR(OBJ: anytype) @TypeOf(SCM_CDR(SCM_CDR(SCM_CDR(SCM_CDR(OBJ))))) {
    return SCM_CDR(SCM_CDR(SCM_CDR(SCM_CDR(OBJ))));
}
pub inline fn SCM_VALIDATE_NULL(pos: anytype, scm: anytype) @TypeOf(SCM_I_MAKE_VALIDATE_MSG2(pos, scm, scm_is_null, "empty list")) {
    return SCM_I_MAKE_VALIDATE_MSG2(pos, scm, scm_is_null, "empty list");
}
pub inline fn SCM_VALIDATE_CONS(pos: anytype, scm: anytype) @TypeOf(SCM_I_MAKE_VALIDATE_MSG2(pos, scm, scm_is_pair, "pair")) {
    return SCM_I_MAKE_VALIDATE_MSG2(pos, scm, scm_is_pair, "pair");
}
pub inline fn SCM_VALIDATE_PAIR(cell: anytype, expr: anytype) @TypeOf(expr) {
    _ = cell;
    return expr;
}
pub const SCM_ARRAY_HANDLE_H = "";
pub const SCM_NUMBERS_H = "";
pub const SCM_PRINT_H = "";
pub const SCM_CHARS_H = "";
pub inline fn SCM_CHARP(x: anytype) @TypeOf(SCM_ITAG8(x) == scm_tc8_char) {
    return SCM_ITAG8(x) == scm_tc8_char;
}
pub inline fn SCM_CHAR(x: anytype) scm_t_wchar {
    return @import("std").zig.c_translation.cast(scm_t_wchar, SCM_ITAG8_DATA(x));
}
pub inline fn SCM_MAKE_CHAR(x: anytype) @TypeOf(if (x <= @as(c_int, 1)) SCM_MAKE_ITAG8(@import("std").zig.c_translation.cast(scm_t_bits, @import("std").zig.c_translation.cast(u8, x)), scm_tc8_char) else SCM_MAKE_ITAG8(@import("std").zig.c_translation.cast(scm_t_bits, x), scm_tc8_char)) {
    return if (x <= @as(c_int, 1)) SCM_MAKE_ITAG8(@import("std").zig.c_translation.cast(scm_t_bits, @import("std").zig.c_translation.cast(u8, x)), scm_tc8_char) else SCM_MAKE_ITAG8(@import("std").zig.c_translation.cast(scm_t_bits, x), scm_tc8_char);
}
pub const SCM_CODEPOINT_DOTTED_CIRCLE = @as(c_int, 0x25cc);
pub const SCM_CODEPOINT_SURROGATE_START = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xd800, .hexadecimal);
pub const SCM_CODEPOINT_SURROGATE_END = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xdfff, .hexadecimal);
pub const SCM_CODEPOINT_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x10ffff, .hexadecimal);
pub inline fn SCM_IS_UNICODE_CHAR(c: anytype) @TypeOf(((@import("std").zig.c_translation.cast(scm_t_wchar, c) >= @as(c_int, 0)) and (@import("std").zig.c_translation.cast(scm_t_wchar, c) < SCM_CODEPOINT_SURROGATE_START)) or ((@import("std").zig.c_translation.cast(scm_t_wchar, c) > SCM_CODEPOINT_SURROGATE_END) and (@import("std").zig.c_translation.cast(scm_t_wchar, c) <= SCM_CODEPOINT_MAX))) {
    return ((@import("std").zig.c_translation.cast(scm_t_wchar, c) >= @as(c_int, 0)) and (@import("std").zig.c_translation.cast(scm_t_wchar, c) < SCM_CODEPOINT_SURROGATE_START)) or ((@import("std").zig.c_translation.cast(scm_t_wchar, c) > SCM_CODEPOINT_SURROGATE_END) and (@import("std").zig.c_translation.cast(scm_t_wchar, c) <= SCM_CODEPOINT_MAX));
}
pub const SCM_OPTIONS_H = "";
pub const SCM_OPTION_BOOLEAN = @as(c_int, 0);
pub const SCM_OPTION_INTEGER = @as(c_int, 1);
pub const SCM_OPTION_SCM = @as(c_int, 2);
pub inline fn SCM_PRINT_STATE_P(obj: anytype) @TypeOf((SCM_STRUCTP(obj) != 0) and (scm_is_eq(SCM_STRUCT_VTABLE(obj), scm_print_state_vtable) != 0)) {
    return (SCM_STRUCTP(obj) != 0) and (scm_is_eq(SCM_STRUCT_VTABLE(obj), scm_print_state_vtable) != 0);
}
pub inline fn SCM_PRINT_STATE(obj: anytype) [*c]scm_print_state {
    return @import("std").zig.c_translation.cast([*c]scm_print_state, SCM_STRUCT_DATA(obj));
}
pub inline fn SCM_WRITINGP(pstate: anytype) @TypeOf(pstate.*.writingp) {
    return pstate.*.writingp;
}
pub inline fn SCM_PORT_WITH_PS_P(p: anytype) @TypeOf(SCM_TYP16_PREDICATE(scm_tc16_port_with_ps, p)) {
    return SCM_TYP16_PREDICATE(scm_tc16_port_with_ps, p);
}
pub inline fn SCM_PORT_WITH_PS_PORT(p: anytype) @TypeOf(SCM_CELL_OBJECT_1(p)) {
    return SCM_CELL_OBJECT_1(p);
}
pub inline fn SCM_PORT_WITH_PS_PS(p: anytype) @TypeOf(SCM_CELL_OBJECT_2(p)) {
    return SCM_CELL_OBJECT_2(p);
}
pub inline fn SCM_COERCE_OUTPORT(p: anytype) @TypeOf(if (SCM_PORT_WITH_PS_P(p)) SCM_PORT_WITH_PS_PORT(p) else p) {
    return if (SCM_PORT_WITH_PS_P(p)) SCM_PORT_WITH_PS_PORT(p) else p;
}
pub const SCM_PRINT_STATE_LAYOUT = "pwuwuwuwuwuwpwuwuwuwpwpw";
pub const SCM_I_FIXNUM_BIT = SCM_LONG_BIT - @as(c_int, 2);
pub const SCM_MOST_NEGATIVE_FIXNUM = -@as(c_long, 1) << (SCM_I_FIXNUM_BIT - @as(c_int, 1));
pub const SCM_MOST_POSITIVE_FIXNUM = -(SCM_MOST_NEGATIVE_FIXNUM + @as(c_int, 1));
pub inline fn SCM_SRS(x: anytype, y: anytype) @TypeOf(x >> y) {
    return x >> y;
}
pub inline fn SCM_I_INUM(x: anytype) @TypeOf(SCM_SRS(@import("std").zig.c_translation.cast(scm_t_inum, SCM_UNPACK(x)), @as(c_int, 2))) {
    return SCM_SRS(@import("std").zig.c_translation.cast(scm_t_inum, SCM_UNPACK(x)), @as(c_int, 2));
}
pub inline fn SCM_I_INUMP(x: anytype) @TypeOf(@as(c_int, 2) & SCM_UNPACK(x)) {
    return @as(c_int, 2) & SCM_UNPACK(x);
}
pub inline fn SCM_I_NINUMP(x: anytype) @TypeOf(!(SCM_I_INUMP(x) != 0)) {
    return !(SCM_I_INUMP(x) != 0);
}
pub inline fn SCM_I_MAKINUM(x: anytype) @TypeOf(SCM_PACK((@import("std").zig.c_translation.cast(scm_t_bits, x) << @as(c_int, 2)) + scm_tc2_int)) {
    return SCM_PACK((@import("std").zig.c_translation.cast(scm_t_bits, x) << @as(c_int, 2)) + scm_tc2_int);
}
pub inline fn SCM_POSFIXABLE(n: anytype) @TypeOf(n <= SCM_MOST_POSITIVE_FIXNUM) {
    return n <= SCM_MOST_POSITIVE_FIXNUM;
}
pub inline fn SCM_NEGFIXABLE(n: anytype) @TypeOf(n >= SCM_MOST_NEGATIVE_FIXNUM) {
    return n >= SCM_MOST_NEGATIVE_FIXNUM;
}
pub inline fn SCM_FIXABLE(n: anytype) @TypeOf((SCM_POSFIXABLE(n) != 0) and (SCM_NEGFIXABLE(n) != 0)) {
    return (SCM_POSFIXABLE(n) != 0) and (SCM_NEGFIXABLE(n) != 0);
}
pub const SCM_INUM0 = SCM_I_MAKINUM(@as(c_int, 0));
pub const SCM_INUM1 = SCM_I_MAKINUM(@as(c_int, 1));
pub const __CLANG_FLOAT_H = "";
pub const FLT_EVAL_METHOD = __FLT_EVAL_METHOD__;
pub const FLT_RADIX = __FLT_RADIX__;
pub const FLT_MANT_DIG = __FLT_MANT_DIG__;
pub const DBL_MANT_DIG = __DBL_MANT_DIG__;
pub const LDBL_MANT_DIG = __LDBL_MANT_DIG__;
pub const DECIMAL_DIG = __DECIMAL_DIG__;
pub const FLT_DIG = __FLT_DIG__;
pub const DBL_DIG = __DBL_DIG__;
pub const LDBL_DIG = __LDBL_DIG__;
pub const FLT_MIN_EXP = __FLT_MIN_EXP__;
pub const DBL_MIN_EXP = __DBL_MIN_EXP__;
pub const LDBL_MIN_EXP = __LDBL_MIN_EXP__;
pub const FLT_MIN_10_EXP = __FLT_MIN_10_EXP__;
pub const DBL_MIN_10_EXP = __DBL_MIN_10_EXP__;
pub const LDBL_MIN_10_EXP = __LDBL_MIN_10_EXP__;
pub const FLT_MAX_EXP = __FLT_MAX_EXP__;
pub const DBL_MAX_EXP = __DBL_MAX_EXP__;
pub const LDBL_MAX_EXP = __LDBL_MAX_EXP__;
pub const FLT_MAX_10_EXP = __FLT_MAX_10_EXP__;
pub const DBL_MAX_10_EXP = __DBL_MAX_10_EXP__;
pub const LDBL_MAX_10_EXP = __LDBL_MAX_10_EXP__;
pub const FLT_MAX = __FLT_MAX__;
pub const DBL_MAX = __DBL_MAX__;
pub const LDBL_MAX = __LDBL_MAX__;
pub const FLT_EPSILON = __FLT_EPSILON__;
pub const DBL_EPSILON = __DBL_EPSILON__;
pub const LDBL_EPSILON = __LDBL_EPSILON__;
pub const FLT_MIN = __FLT_MIN__;
pub const DBL_MIN = __DBL_MIN__;
pub const LDBL_MIN = __LDBL_MIN__;
pub const FLT_TRUE_MIN = __FLT_DENORM_MIN__;
pub const DBL_TRUE_MIN = __DBL_DENORM_MIN__;
pub const LDBL_TRUE_MIN = __LDBL_DENORM_MIN__;
pub const FLT_DECIMAL_DIG = __FLT_DECIMAL_DIG__;
pub const DBL_DECIMAL_DIG = __DBL_DECIMAL_DIG__;
pub const LDBL_DECIMAL_DIG = __LDBL_DECIMAL_DIG__;
pub const FLT_HAS_SUBNORM = __FLT_HAS_DENORM__;
pub const DBL_HAS_SUBNORM = __DBL_HAS_DENORM__;
pub const LDBL_HAS_SUBNORM = __LDBL_HAS_DENORM__;
pub const SCM_MAXEXP = DBL_MAX_10_EXP;
pub const SCM_FLTMAX = FLT_MAX;
pub const SCM_INTBUFLEN = @as(c_int, 5) + (SCM_CHAR_BIT * @import("std").zig.c_translation.sizeof(intmax_t));
pub const scm_tc16_big = scm_tc7_number + (@as(c_int, 1) * @as(c_long, 256));
pub const scm_tc16_real = scm_tc7_number + (@as(c_int, 2) * @as(c_long, 256));
pub const scm_tc16_complex = scm_tc7_number + (@as(c_int, 3) * @as(c_long, 256));
pub const scm_tc16_fraction = scm_tc7_number + (@as(c_int, 4) * @as(c_long, 256));
pub inline fn SCM_INEXACTP(x: anytype) @TypeOf(!(SCM_IMP(x) != 0) and ((@import("std").zig.c_translation.promoteIntLiteral(c_int, 0xfeff, .hexadecimal) & SCM_CELL_TYPE(x)) == scm_tc16_real)) {
    return !(SCM_IMP(x) != 0) and ((@import("std").zig.c_translation.promoteIntLiteral(c_int, 0xfeff, .hexadecimal) & SCM_CELL_TYPE(x)) == scm_tc16_real);
}
pub inline fn SCM_REALP(x: anytype) @TypeOf(SCM_HAS_TYP16(x, scm_tc16_real)) {
    return SCM_HAS_TYP16(x, scm_tc16_real);
}
pub inline fn SCM_COMPLEXP(x: anytype) @TypeOf(SCM_HAS_TYP16(x, scm_tc16_complex)) {
    return SCM_HAS_TYP16(x, scm_tc16_complex);
}
pub inline fn SCM_REAL_VALUE(x: anytype) @TypeOf(@import("std").zig.c_translation.cast([*c]scm_t_double, SCM2PTR(x)).*.real) {
    return @import("std").zig.c_translation.cast([*c]scm_t_double, SCM2PTR(x)).*.real;
}
pub inline fn SCM_COMPLEX_REAL(x: anytype) @TypeOf(@import("std").zig.c_translation.cast([*c]scm_t_complex, SCM2PTR(x)).*.real) {
    return @import("std").zig.c_translation.cast([*c]scm_t_complex, SCM2PTR(x)).*.real;
}
pub inline fn SCM_COMPLEX_IMAG(x: anytype) @TypeOf(@import("std").zig.c_translation.cast([*c]scm_t_complex, SCM2PTR(x)).*.imag) {
    return @import("std").zig.c_translation.cast([*c]scm_t_complex, SCM2PTR(x)).*.imag;
}
pub inline fn SCM_BIGP(x: anytype) @TypeOf(SCM_HAS_TYP16(x, scm_tc16_big)) {
    return SCM_HAS_TYP16(x, scm_tc16_big);
}
pub inline fn SCM_NUMBERP(x: anytype) @TypeOf((SCM_I_INUMP(x) != 0) or (SCM_NUMP(x) != 0)) {
    return (SCM_I_INUMP(x) != 0) or (SCM_NUMP(x) != 0);
}
pub inline fn SCM_NUMP(x: anytype) @TypeOf(SCM_HAS_TYP7(x, scm_tc7_number)) {
    return SCM_HAS_TYP7(x, scm_tc7_number);
}
pub inline fn SCM_FRACTIONP(x: anytype) @TypeOf(SCM_HAS_TYP16(x, scm_tc16_fraction)) {
    return SCM_HAS_TYP16(x, scm_tc16_fraction);
}
pub inline fn SCM_FRACTION_NUMERATOR(x: anytype) @TypeOf(SCM_CELL_OBJECT_1(x)) {
    return SCM_CELL_OBJECT_1(x);
}
pub inline fn SCM_FRACTION_DENOMINATOR(x: anytype) @TypeOf(SCM_CELL_OBJECT_2(x)) {
    return SCM_CELL_OBJECT_2(x);
}
pub const scm_to_schar = scm_to_int8;
pub const scm_from_schar = scm_from_int8;
pub const scm_to_uchar = scm_to_uint8;
pub const scm_from_uchar = scm_from_uint8;
pub const scm_to_char = scm_to_int8;
pub const scm_from_char = scm_from_int8;
pub const scm_to_short = scm_to_int16;
pub const scm_from_short = scm_from_int16;
pub const scm_to_ushort = scm_to_uint16;
pub const scm_from_ushort = scm_from_uint16;
pub const scm_to_int = scm_to_int32;
pub const scm_from_int = scm_from_int32;
pub const scm_to_uint = scm_to_uint32;
pub const scm_from_uint = scm_from_uint32;
pub const scm_to_long = scm_to_int64;
pub const scm_from_long = scm_from_int64;
pub const scm_to_ulong = scm_to_uint64;
pub const scm_from_ulong = scm_from_uint64;
pub const scm_to_intmax = scm_to_int64;
pub const scm_from_intmax = scm_from_int64;
pub const scm_to_uintmax = scm_to_uint64;
pub const scm_from_uintmax = scm_from_uint64;
pub const scm_to_long_long = scm_to_int64;
pub const scm_from_long_long = scm_from_int64;
pub const scm_to_ulong_long = scm_to_uint64;
pub const scm_from_ulong_long = scm_from_uint64;
pub const scm_to_ssize_t = scm_to_int64;
pub const scm_from_ssize_t = scm_from_int64;
pub const scm_to_size_t = scm_to_uint64;
pub const scm_from_size_t = scm_from_uint64;
pub const scm_to_ptrdiff_t = scm_to_int64;
pub const scm_from_ptrdiff_t = scm_from_int64;
pub const scm_to_intptr_t = scm_to_int64;
pub const scm_from_intptr_t = scm_from_int64;
pub const scm_to_uintptr_t = scm_to_uint64;
pub const scm_from_uintptr_t = scm_from_uint64;
pub inline fn SCM_NUM2SIZE(pos: anytype, arg: anytype) @TypeOf(scm_to_size_t(arg)) {
    _ = pos;
    return scm_to_size_t(arg);
}
pub inline fn SCM_NUM2SIZE_DEF(pos: anytype, arg: anytype, def: anytype) @TypeOf(if (SCM_UNBNDP(arg)) def else scm_to_size_t(arg)) {
    _ = pos;
    return if (SCM_UNBNDP(arg)) def else scm_to_size_t(arg);
}
pub inline fn SCM_NUM2PTRDIFF(pos: anytype, arg: anytype) @TypeOf(scm_to_ssize_t(arg)) {
    _ = pos;
    return scm_to_ssize_t(arg);
}
pub inline fn SCM_NUM2PTRDIFF_DEF(pos: anytype, arg: anytype, def: anytype) @TypeOf(if (SCM_UNBNDP(arg)) def else scm_to_ssize_t(arg)) {
    _ = pos;
    return if (SCM_UNBNDP(arg)) def else scm_to_ssize_t(arg);
}
pub inline fn SCM_NUM2SHORT(pos: anytype, arg: anytype) @TypeOf(scm_to_short(arg)) {
    _ = pos;
    return scm_to_short(arg);
}
pub inline fn SCM_NUM2SHORT_DEF(pos: anytype, arg: anytype, def: anytype) @TypeOf(if (SCM_UNBNDP(arg)) def else scm_to_short(arg)) {
    _ = pos;
    return if (SCM_UNBNDP(arg)) def else scm_to_short(arg);
}
pub inline fn SCM_NUM2USHORT(pos: anytype, arg: anytype) @TypeOf(scm_to_ushort(arg)) {
    _ = pos;
    return scm_to_ushort(arg);
}
pub inline fn SCM_NUM2USHORT_DEF(pos: anytype, arg: anytype, def: anytype) @TypeOf(if (SCM_UNBNDP(arg)) def else scm_to_ushort(arg)) {
    _ = pos;
    return if (SCM_UNBNDP(arg)) def else scm_to_ushort(arg);
}
pub inline fn SCM_NUM2INT(pos: anytype, arg: anytype) @TypeOf(scm_to_int(arg)) {
    _ = pos;
    return scm_to_int(arg);
}
pub inline fn SCM_NUM2INT_DEF(pos: anytype, arg: anytype, def: anytype) @TypeOf(if (SCM_UNBNDP(arg)) def else scm_to_int(arg)) {
    _ = pos;
    return if (SCM_UNBNDP(arg)) def else scm_to_int(arg);
}
pub inline fn SCM_NUM2UINT(pos: anytype, arg: anytype) @TypeOf(scm_to_uint(arg)) {
    _ = pos;
    return scm_to_uint(arg);
}
pub inline fn SCM_NUM2UINT_DEF(pos: anytype, arg: anytype, def: anytype) @TypeOf(if (SCM_UNBNDP(arg)) def else scm_to_uint(arg)) {
    _ = pos;
    return if (SCM_UNBNDP(arg)) def else scm_to_uint(arg);
}
pub inline fn SCM_NUM2ULONG(pos: anytype, arg: anytype) @TypeOf(scm_to_ulong(arg)) {
    _ = pos;
    return scm_to_ulong(arg);
}
pub inline fn SCM_NUM2ULONG_DEF(pos: anytype, arg: anytype, def: anytype) @TypeOf(if (SCM_UNBNDP(arg)) def else scm_to_ulong(arg)) {
    _ = pos;
    return if (SCM_UNBNDP(arg)) def else scm_to_ulong(arg);
}
pub inline fn SCM_NUM2LONG(pos: anytype, arg: anytype) @TypeOf(scm_to_long(arg)) {
    _ = pos;
    return scm_to_long(arg);
}
pub inline fn SCM_NUM2LONG_DEF(pos: anytype, arg: anytype, def: anytype) @TypeOf(if (SCM_UNBNDP(arg)) def else scm_to_long(arg)) {
    _ = pos;
    return if (SCM_UNBNDP(arg)) def else scm_to_long(arg);
}
pub inline fn SCM_NUM2LONG_LONG(pos: anytype, arg: anytype) @TypeOf(scm_to_long_long(arg)) {
    _ = pos;
    return scm_to_long_long(arg);
}
pub inline fn SCM_NUM2LONG_LONG_DEF(pos: anytype, arg: anytype, def: anytype) @TypeOf(if (SCM_UNBNDP(arg)) def else scm_to_long_long(arg)) {
    _ = pos;
    return if (SCM_UNBNDP(arg)) def else scm_to_long_long(arg);
}
pub inline fn SCM_NUM2ULONG_LONG(pos: anytype, arg: anytype) @TypeOf(scm_to_ulong_long(arg)) {
    _ = pos;
    return scm_to_ulong_long(arg);
}
pub inline fn SCM_NUM2ULONG_LONG_DEF(pos: anytype, arg: anytype, def: anytype) @TypeOf(if (SCM_UNBNDP(arg)) def else scm_to_ulong_long(arg)) {
    _ = pos;
    return if (SCM_UNBNDP(arg)) def else scm_to_ulong_long(arg);
}
pub inline fn SCM_NUM2FLOAT(pos: anytype, arg: anytype) f32 {
    _ = pos;
    return @import("std").zig.c_translation.cast(f32, scm_to_double(arg));
}
pub inline fn SCM_NUM2DOUBLE(pos: anytype, arg: anytype) @TypeOf(scm_to_double(arg)) {
    _ = pos;
    return scm_to_double(arg);
}
pub const SCM_ARRAY_H = "";
pub inline fn SCM_I_ARRAYP(a: anytype) @TypeOf(SCM_TYP16_PREDICATE(scm_tc7_array, a)) {
    return SCM_TYP16_PREDICATE(scm_tc7_array, a);
}
pub inline fn SCM_I_ARRAY_NDIM(x: anytype) usize {
    return @import("std").zig.c_translation.cast(usize, SCM_CELL_WORD_0(x) >> @as(c_int, 17));
}
pub inline fn SCM_I_ARRAY_V(a: anytype) @TypeOf(SCM_CELL_OBJECT_1(a)) {
    return SCM_CELL_OBJECT_1(a);
}
pub inline fn SCM_I_ARRAY_BASE(a: anytype) usize {
    return @import("std").zig.c_translation.cast(usize, SCM_CELL_WORD_2(a));
}
pub inline fn SCM_I_ARRAY_DIMS(a: anytype) [*c]scm_t_array_dim {
    return @import("std").zig.c_translation.cast([*c]scm_t_array_dim, SCM_CELL_OBJECT_LOC(a, @as(c_int, 3)));
}
pub inline fn SCM_I_ARRAY_SET_V(a: anytype, v: anytype) @TypeOf(SCM_SET_CELL_OBJECT_1(a, v)) {
    return SCM_SET_CELL_OBJECT_1(a, v);
}
pub inline fn SCM_I_ARRAY_SET_BASE(a: anytype, base: anytype) @TypeOf(SCM_SET_CELL_WORD_2(a, base)) {
    return SCM_SET_CELL_WORD_2(a, base);
}
pub inline fn scm_array_handle_rank(h: anytype) @TypeOf(h.*.ndims) {
    return h.*.ndims;
}
pub inline fn scm_array_handle_dims(h: anytype) @TypeOf(h.*.dims) {
    return h.*.dims;
}
pub const SCM_ARRAY_MAP_H = "";
pub const SCM_ASYNC_H = "";
pub const SCM_THREADS_H = "";
pub const SCM_PROCS_H = "";
pub const SCM_BOOLEAN_H = "";
pub inline fn scm_is_false_and_not_nil(x: anytype) @TypeOf(scm_is_eq(x, SCM_BOOL_F)) {
    return scm_is_eq(x, SCM_BOOL_F);
}
pub inline fn scm_is_true_or_nil(x: anytype) @TypeOf(!(scm_is_eq(x, SCM_BOOL_F) != 0)) {
    return !(scm_is_eq(x, SCM_BOOL_F) != 0);
}
pub inline fn scm_is_false_assume_not_nil(x: anytype) @TypeOf(scm_is_eq(x, SCM_BOOL_F)) {
    return scm_is_eq(x, SCM_BOOL_F);
}
pub inline fn scm_is_true_assume_not_nil(x: anytype) @TypeOf(!(scm_is_eq(x, SCM_BOOL_F) != 0)) {
    return !(scm_is_eq(x, SCM_BOOL_F) != 0);
}
pub inline fn scm_is_false_or_nil(x: anytype) @TypeOf(SCM_MATCHES_BITS_IN_COMMON(x, SCM_ELISP_NIL, SCM_BOOL_F)) {
    return SCM_MATCHES_BITS_IN_COMMON(x, SCM_ELISP_NIL, SCM_BOOL_F);
}
pub inline fn scm_is_true_and_not_nil(x: anytype) @TypeOf(!(scm_is_false_or_nil(x) != 0)) {
    return !(scm_is_false_or_nil(x) != 0);
}
pub inline fn scm_is_false(x: anytype) @TypeOf(scm_is_false_or_nil(x)) {
    return scm_is_false_or_nil(x);
}
pub inline fn scm_is_true(x: anytype) @TypeOf(!(scm_is_false(x) != 0)) {
    return !(scm_is_false(x) != 0);
}
pub inline fn scm_is_bool_or_nil(x: anytype) @TypeOf(SCM_MATCHES_BITS_IN_COMMON(x, SCM_BOOL_T, SCM_ELISP_NIL)) {
    return SCM_MATCHES_BITS_IN_COMMON(x, SCM_BOOL_T, SCM_ELISP_NIL);
}
pub inline fn scm_is_bool_and_not_nil(x: anytype) @TypeOf(SCM_MATCHES_BITS_IN_COMMON(x, SCM_BOOL_F, SCM_BOOL_T)) {
    return SCM_MATCHES_BITS_IN_COMMON(x, SCM_BOOL_F, SCM_BOOL_T);
}
pub inline fn scm_from_bool(x: anytype) @TypeOf(if (x) SCM_BOOL_T else SCM_BOOL_F) {
    return if (x) SCM_BOOL_T else SCM_BOOL_F;
}
pub inline fn SCM_FALSEP(x: anytype) @TypeOf(scm_is_false(x)) {
    return scm_is_false(x);
}
pub inline fn SCM_NFALSEP(x: anytype) @TypeOf(scm_is_true(x)) {
    return scm_is_true(x);
}
pub inline fn SCM_BOOLP(x: anytype) @TypeOf(scm_is_bool(x)) {
    return scm_is_bool(x);
}
pub inline fn SCM_BOOL(x: anytype) @TypeOf(scm_from_bool(x)) {
    return scm_from_bool(x);
}
pub inline fn SCM_NEGATE_BOOL(f: anytype) @TypeOf(scm_from_bool(!(f != 0))) {
    return scm_from_bool(!(f != 0));
}
pub inline fn SCM_BOOL_NOT(x: anytype) @TypeOf(scm_not(x)) {
    return scm_not(x);
}
pub inline fn scm_is_lisp_false(x: anytype) @TypeOf(SCM_MATCHES_BITS_IN_COMMON(x, SCM_BOOL_F, SCM_EOL)) {
    return SCM_MATCHES_BITS_IN_COMMON(x, SCM_BOOL_F, SCM_EOL);
}
pub const SCM_THROW_H = "";
pub const SCM_EXCEPTIONS_H = "";
pub const SCM_DYNSTACK_H = "";
pub const _SETJMP_H = @as(c_int, 1);
pub const _BITS_SETJMP_H = @as(c_int, 1);
pub const __jmp_buf_tag_defined = @as(c_int, 1);
pub inline fn sigsetjmp(env: anytype, savemask: anytype) @TypeOf(__sigsetjmp(env, savemask)) {
    return __sigsetjmp(env, savemask);
}
pub const _SIGNAL_H = "";
pub const _BITS_SIGNUM_GENERIC_H = @as(c_int, 1);
pub const SIG_ERR = @import("std").zig.c_translation.cast(__sighandler_t, -@as(c_int, 1));
pub const SIG_DFL = @import("std").zig.c_translation.cast(__sighandler_t, @as(c_int, 0));
pub const SIG_IGN = @import("std").zig.c_translation.cast(__sighandler_t, @as(c_int, 1));
pub const SIGINT = @as(c_int, 2);
pub const SIGILL = @as(c_int, 4);
pub const SIGABRT = @as(c_int, 6);
pub const SIGFPE = @as(c_int, 8);
pub const SIGSEGV = @as(c_int, 11);
pub const SIGTERM = @as(c_int, 15);
pub const SIGHUP = @as(c_int, 1);
pub const SIGQUIT = @as(c_int, 3);
pub const SIGTRAP = @as(c_int, 5);
pub const SIGKILL = @as(c_int, 9);
pub const SIGPIPE = @as(c_int, 13);
pub const SIGALRM = @as(c_int, 14);
pub const SIGIO = SIGPOLL;
pub const SIGIOT = SIGABRT;
pub const SIGCLD = SIGCHLD;
pub const _BITS_SIGNUM_ARCH_H = @as(c_int, 1);
pub const SIGSTKFLT = @as(c_int, 16);
pub const SIGPWR = @as(c_int, 30);
pub const SIGBUS = @as(c_int, 7);
pub const SIGSYS = @as(c_int, 31);
pub const SIGURG = @as(c_int, 23);
pub const SIGSTOP = @as(c_int, 19);
pub const SIGTSTP = @as(c_int, 20);
pub const SIGCONT = @as(c_int, 18);
pub const SIGCHLD = @as(c_int, 17);
pub const SIGTTIN = @as(c_int, 21);
pub const SIGTTOU = @as(c_int, 22);
pub const SIGPOLL = @as(c_int, 29);
pub const SIGXFSZ = @as(c_int, 25);
pub const SIGXCPU = @as(c_int, 24);
pub const SIGVTALRM = @as(c_int, 26);
pub const SIGPROF = @as(c_int, 27);
pub const SIGUSR1 = @as(c_int, 10);
pub const SIGUSR2 = @as(c_int, 12);
pub const SIGWINCH = @as(c_int, 28);
pub const __SIGRTMIN = @as(c_int, 32);
pub const __SIGRTMAX = @as(c_int, 64);
pub const _NSIG = __SIGRTMAX + @as(c_int, 1);
pub const __sig_atomic_t_defined = @as(c_int, 1);
pub const __siginfo_t_defined = @as(c_int, 1);
pub const ____sigval_t_defined = "";
pub const __SI_MAX_SIZE = @as(c_int, 128);
pub const __SI_PAD_SIZE = (__SI_MAX_SIZE / @import("std").zig.c_translation.sizeof(c_int)) - @as(c_int, 4);
pub const _BITS_SIGINFO_ARCH_H = @as(c_int, 1);
pub const __SI_ALIGNMENT = "";
pub const __SI_BAND_TYPE = c_long;
pub const __SI_CLOCK_T = __clock_t;
pub const __SI_ERRNO_THEN_CODE = @as(c_int, 1);
pub const __SI_HAVE_SIGSYS = @as(c_int, 1);
pub const __SI_SIGFAULT_ADDL = "";
pub const _BITS_SIGINFO_CONSTS_H = @as(c_int, 1);
pub const __SI_ASYNCIO_AFTER_SIGIO = @as(c_int, 1);
pub const __sigval_t_defined = "";
pub const __sigevent_t_defined = @as(c_int, 1);
pub const __SIGEV_MAX_SIZE = @as(c_int, 64);
pub const __SIGEV_PAD_SIZE = (__SIGEV_MAX_SIZE / @import("std").zig.c_translation.sizeof(c_int)) - @as(c_int, 4);
pub const _BITS_SIGEVENT_CONSTS_H = @as(c_int, 1);
pub inline fn sigmask(sig: anytype) @TypeOf(__glibc_macro_warning("sigmask is deprecated")(@import("std").zig.c_translation.cast(c_int, @as(c_uint, 1) << (sig - @as(c_int, 1))))) {
    return __glibc_macro_warning("sigmask is deprecated")(@import("std").zig.c_translation.cast(c_int, @as(c_uint, 1) << (sig - @as(c_int, 1))));
}
pub const NSIG = _NSIG;
pub const _BITS_SIGACTION_H = @as(c_int, 1);
pub const SA_NOCLDSTOP = @as(c_int, 1);
pub const SA_NOCLDWAIT = @as(c_int, 2);
pub const SA_SIGINFO = @as(c_int, 4);
pub const SA_ONSTACK = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x08000000, .hexadecimal);
pub const SA_RESTART = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x10000000, .hexadecimal);
pub const SA_NODEFER = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x40000000, .hexadecimal);
pub const SA_RESETHAND = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x80000000, .hexadecimal);
pub const SA_INTERRUPT = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x20000000, .hexadecimal);
pub const SA_NOMASK = SA_NODEFER;
pub const SA_ONESHOT = SA_RESETHAND;
pub const SA_STACK = SA_ONSTACK;
pub const SIG_BLOCK = @as(c_int, 0);
pub const SIG_UNBLOCK = @as(c_int, 1);
pub const SIG_SETMASK = @as(c_int, 2);
pub const _BITS_SIGCONTEXT_H = @as(c_int, 1);
pub const FP_XSTATE_MAGIC1 = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0x46505853, .hexadecimal);
pub const FP_XSTATE_MAGIC2 = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0x46505845, .hexadecimal);
pub const FP_XSTATE_MAGIC2_SIZE = @import("std").zig.c_translation.sizeof(FP_XSTATE_MAGIC2);
pub const __stack_t_defined = @as(c_int, 1);
pub const _SYS_UCONTEXT_H = @as(c_int, 1);
pub inline fn __ctx(fld: anytype) @TypeOf(fld) {
    return fld;
}
pub const __NGREG = @as(c_int, 23);
pub const NGREG = __NGREG;
pub const _BITS_SIGSTACK_H = @as(c_int, 1);
pub const MINSIGSTKSZ = @as(c_int, 2048);
pub const SIGSTKSZ = @as(c_int, 8192);
pub const _BITS_SS_FLAGS_H = @as(c_int, 1);
pub const __sigstack_defined = @as(c_int, 1);
pub const _BITS_SIGTHREAD_H = @as(c_int, 1);
pub const SIGRTMIN = __libc_current_sigrtmin();
pub const SIGRTMAX = __libc_current_sigrtmax();
pub const SCM_DYNSTACK_HEADER_LEN = @as(c_int, 2);
pub inline fn SCM_DYNSTACK_PREV_OFFSET(top: anytype) @TypeOf(top[-@as(c_int, 2)]) {
    return top[-@as(c_int, 2)];
}
pub inline fn SCM_DYNSTACK_TAG(top: anytype) @TypeOf(top[-@as(c_int, 1)]) {
    return top[-@as(c_int, 1)];
}
pub const SCM_DYNSTACK_TAG_TYPE_MASK = @as(c_int, 0xf);
pub const SCM_DYNSTACK_TAG_FLAGS_MASK = @as(c_int, 0xf0);
pub const SCM_DYNSTACK_TAG_FLAGS_SHIFT = @as(c_int, 4);
pub const SCM_DYNSTACK_TAG_LEN_SHIFT = @as(c_int, 8);
pub inline fn SCM_MAKE_DYNSTACK_TAG(@"type": anytype, flags: anytype, len: anytype) @TypeOf((@"type" | flags) | (len << SCM_DYNSTACK_TAG_LEN_SHIFT)) {
    return (@"type" | flags) | (len << SCM_DYNSTACK_TAG_LEN_SHIFT);
}
pub inline fn SCM_DYNSTACK_TAG_TYPE(tag: anytype) @TypeOf(tag & SCM_DYNSTACK_TAG_TYPE_MASK) {
    return tag & SCM_DYNSTACK_TAG_TYPE_MASK;
}
pub inline fn SCM_DYNSTACK_TAG_FLAGS(tag: anytype) @TypeOf(tag & SCM_DYNSTACK_TAG_FLAGS_MASK) {
    return tag & SCM_DYNSTACK_TAG_FLAGS_MASK;
}
pub inline fn SCM_DYNSTACK_TAG_LEN(tag: anytype) @TypeOf(tag >> SCM_DYNSTACK_TAG_LEN_SHIFT) {
    return tag >> SCM_DYNSTACK_TAG_LEN_SHIFT;
}
pub inline fn SCM_DYNSTACK_PREV(top: anytype) @TypeOf(if (SCM_DYNSTACK_PREV_OFFSET(top)) top - SCM_DYNSTACK_PREV_OFFSET(top) else NULL) {
    return if (SCM_DYNSTACK_PREV_OFFSET(top)) top - SCM_DYNSTACK_PREV_OFFSET(top) else NULL;
}
pub inline fn SCM_DYNSTACK_NEXT(top: anytype) @TypeOf(if (SCM_DYNSTACK_TAG(top)) (top + SCM_DYNSTACK_TAG_LEN(SCM_DYNSTACK_TAG(top))) + SCM_DYNSTACK_HEADER_LEN else NULL) {
    return if (SCM_DYNSTACK_TAG(top)) (top + SCM_DYNSTACK_TAG_LEN(SCM_DYNSTACK_TAG(top))) + SCM_DYNSTACK_HEADER_LEN else NULL;
}
pub inline fn SCM_DYNSTACK_FIRST(dynstack: anytype) @TypeOf(dynstack.*.base + SCM_DYNSTACK_HEADER_LEN) {
    return dynstack.*.base + SCM_DYNSTACK_HEADER_LEN;
}
pub inline fn SCM_DYNSTACK_CAPACITY(dynstack: anytype) @TypeOf(dynstack.*.limit - dynstack.*.base) {
    return dynstack.*.limit - dynstack.*.base;
}
pub inline fn SCM_DYNSTACK_SPACE(dynstack: anytype) @TypeOf(dynstack.*.limit - dynstack.*.top) {
    return dynstack.*.limit - dynstack.*.top;
}
pub inline fn SCM_DYNSTACK_HEIGHT(dynstack: anytype) @TypeOf(dynstack.*.top - dynstack.*.base) {
    return dynstack.*.top - dynstack.*.base;
}
pub inline fn SCM_DYNSTACK_HAS_SPACE(dynstack: anytype, n: anytype) @TypeOf(SCM_DYNSTACK_SPACE(dynstack) >= (n + SCM_DYNSTACK_HEADER_LEN)) {
    return SCM_DYNSTACK_SPACE(dynstack) >= (n + SCM_DYNSTACK_HEADER_LEN);
}
pub const SCM_ISELECT_H = "";
pub const SELECT_TYPE = fd_set;
pub const SCM_SMOB_H = "";
pub const SCM_SNARF_H = "";
pub const SCM_SUPPORT_STATIC_ALLOCATION = "";
pub inline fn SCM_SNARF_HERE(X: anytype) @TypeOf(X) {
    return X;
}
pub const SCM_SMOB_TYPE_MASK = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xffff, .hexadecimal);
pub inline fn SCM_SMOB_TYPE_BITS(tc: anytype) @TypeOf(tc) {
    return tc;
}
pub inline fn SCM_TC2SMOBNUM(x: anytype) @TypeOf(@as(c_int, 0x0ff) & (x >> @as(c_int, 8))) {
    return @as(c_int, 0x0ff) & (x >> @as(c_int, 8));
}
pub inline fn SCM_SMOBNUM(x: anytype) @TypeOf(SCM_TC2SMOBNUM(SCM_CELL_TYPE(x))) {
    return SCM_TC2SMOBNUM(SCM_CELL_TYPE(x));
}
pub inline fn SCM_SMOBNAME(smobnum: anytype) @TypeOf(scm_smobs[smobnum].name) {
    return scm_smobs[smobnum].name;
}
pub inline fn SCM_SMOB_PREDICATE(tag: anytype, obj: anytype) @TypeOf(SCM_HAS_TYP16(obj, tag)) {
    return SCM_HAS_TYP16(obj, tag);
}
pub inline fn SCM_SMOB_DESCRIPTOR(x: anytype) @TypeOf(scm_smobs[SCM_SMOBNUM(x)]) {
    return scm_smobs[SCM_SMOBNUM(x)];
}
pub inline fn SCM_SMOB_APPLICABLE_P(x: anytype) @TypeOf(SCM_SMOB_DESCRIPTOR(x).apply) {
    return SCM_SMOB_DESCRIPTOR(x).apply;
}
pub const SCM_I_MAX_SMOB_TYPE_COUNT = @as(c_int, 256);
pub inline fn SCM_SMOB_DATA_N(x: anytype, n: anytype) @TypeOf(SCM_CELL_WORD(x, n)) {
    return SCM_CELL_WORD(x, n);
}
pub inline fn SCM_SET_SMOB_DATA_N(x: anytype, n: anytype, data: anytype) @TypeOf(SCM_SET_CELL_WORD(x, n, data)) {
    return SCM_SET_CELL_WORD(x, n, data);
}
pub inline fn SCM_SMOB_DATA_0(x: anytype) @TypeOf(SCM_SMOB_DATA_N(x, @as(c_int, 0))) {
    return SCM_SMOB_DATA_N(x, @as(c_int, 0));
}
pub inline fn SCM_SMOB_DATA_1(x: anytype) @TypeOf(SCM_SMOB_DATA_N(x, @as(c_int, 1))) {
    return SCM_SMOB_DATA_N(x, @as(c_int, 1));
}
pub inline fn SCM_SMOB_DATA_2(x: anytype) @TypeOf(SCM_SMOB_DATA_N(x, @as(c_int, 2))) {
    return SCM_SMOB_DATA_N(x, @as(c_int, 2));
}
pub inline fn SCM_SMOB_DATA_3(x: anytype) @TypeOf(SCM_SMOB_DATA_N(x, @as(c_int, 3))) {
    return SCM_SMOB_DATA_N(x, @as(c_int, 3));
}
pub inline fn SCM_SET_SMOB_DATA_0(x: anytype, data: anytype) @TypeOf(SCM_SET_SMOB_DATA_N(x, @as(c_int, 0), data)) {
    return SCM_SET_SMOB_DATA_N(x, @as(c_int, 0), data);
}
pub inline fn SCM_SET_SMOB_DATA_1(x: anytype, data: anytype) @TypeOf(SCM_SET_SMOB_DATA_N(x, @as(c_int, 1), data)) {
    return SCM_SET_SMOB_DATA_N(x, @as(c_int, 1), data);
}
pub inline fn SCM_SET_SMOB_DATA_2(x: anytype, data: anytype) @TypeOf(SCM_SET_SMOB_DATA_N(x, @as(c_int, 2), data)) {
    return SCM_SET_SMOB_DATA_N(x, @as(c_int, 2), data);
}
pub inline fn SCM_SET_SMOB_DATA_3(x: anytype, data: anytype) @TypeOf(SCM_SET_SMOB_DATA_N(x, @as(c_int, 3), data)) {
    return SCM_SET_SMOB_DATA_N(x, @as(c_int, 3), data);
}
pub inline fn SCM_SMOB_FLAGS(x: anytype) @TypeOf(SCM_SMOB_DATA_0(x) >> @as(c_int, 16)) {
    return SCM_SMOB_DATA_0(x) >> @as(c_int, 16);
}
pub inline fn SCM_SMOB_DATA(x: anytype) @TypeOf(SCM_SMOB_DATA_1(x)) {
    return SCM_SMOB_DATA_1(x);
}
pub inline fn SCM_SET_SMOB_FLAGS(x: anytype, data: anytype) @TypeOf(SCM_SET_SMOB_DATA_0(x, (SCM_CELL_TYPE(x) & @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xffff, .hexadecimal)) | (data << @as(c_int, 16)))) {
    return SCM_SET_SMOB_DATA_0(x, (SCM_CELL_TYPE(x) & @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xffff, .hexadecimal)) | (data << @as(c_int, 16)));
}
pub inline fn SCM_SET_SMOB_DATA(x: anytype, data: anytype) @TypeOf(SCM_SET_SMOB_DATA_1(x, data)) {
    return SCM_SET_SMOB_DATA_1(x, data);
}
pub inline fn SCM_SMOB_OBJECT_N(x: anytype, n: anytype) @TypeOf(SCM_CELL_OBJECT(x, n)) {
    return SCM_CELL_OBJECT(x, n);
}
pub inline fn SCM_SET_SMOB_OBJECT_N(x: anytype, n: anytype, obj: anytype) @TypeOf(SCM_SET_CELL_OBJECT(x, n, obj)) {
    return SCM_SET_CELL_OBJECT(x, n, obj);
}
pub inline fn SCM_SMOB_OBJECT_N_LOC(x: anytype, n: anytype) @TypeOf(SCM_CELL_OBJECT_LOC(x, n)) {
    return SCM_CELL_OBJECT_LOC(x, n);
}
pub inline fn SCM_SMOB_OBJECT_1(x: anytype) @TypeOf(SCM_SMOB_OBJECT_N(x, @as(c_int, 1))) {
    return SCM_SMOB_OBJECT_N(x, @as(c_int, 1));
}
pub inline fn SCM_SMOB_OBJECT_2(x: anytype) @TypeOf(SCM_SMOB_OBJECT_N(x, @as(c_int, 2))) {
    return SCM_SMOB_OBJECT_N(x, @as(c_int, 2));
}
pub inline fn SCM_SMOB_OBJECT_3(x: anytype) @TypeOf(SCM_SMOB_OBJECT_N(x, @as(c_int, 3))) {
    return SCM_SMOB_OBJECT_N(x, @as(c_int, 3));
}
pub inline fn SCM_SET_SMOB_OBJECT_1(x: anytype, obj: anytype) @TypeOf(SCM_SET_SMOB_OBJECT_N(x, @as(c_int, 1), obj)) {
    return SCM_SET_SMOB_OBJECT_N(x, @as(c_int, 1), obj);
}
pub inline fn SCM_SET_SMOB_OBJECT_2(x: anytype, obj: anytype) @TypeOf(SCM_SET_SMOB_OBJECT_N(x, @as(c_int, 2), obj)) {
    return SCM_SET_SMOB_OBJECT_N(x, @as(c_int, 2), obj);
}
pub inline fn SCM_SET_SMOB_OBJECT_3(x: anytype, obj: anytype) @TypeOf(SCM_SET_SMOB_OBJECT_N(x, @as(c_int, 3), obj)) {
    return SCM_SET_SMOB_OBJECT_N(x, @as(c_int, 3), obj);
}
pub inline fn SCM_SMOB_OBJECT_0_LOC(x: anytype) @TypeOf(SCM_SMOB_OBJECT_N_LOC(x, @as(c_int, 0))) {
    return SCM_SMOB_OBJECT_N_LOC(x, @as(c_int, 0));
}
pub inline fn SCM_SMOB_OBJECT_1_LOC(x: anytype) @TypeOf(SCM_SMOB_OBJECT_N_LOC(x, @as(c_int, 1))) {
    return SCM_SMOB_OBJECT_N_LOC(x, @as(c_int, 1));
}
pub inline fn SCM_SMOB_OBJECT_2_LOC(x: anytype) @TypeOf(SCM_SMOB_OBJECT_N_LOC(x, @as(c_int, 2))) {
    return SCM_SMOB_OBJECT_N_LOC(x, @as(c_int, 2));
}
pub inline fn SCM_SMOB_OBJECT_3_LOC(x: anytype) @TypeOf(SCM_SMOB_OBJECT_N_LOC(x, @as(c_int, 3))) {
    return SCM_SMOB_OBJECT_N_LOC(x, @as(c_int, 3));
}
pub inline fn SCM_SMOB_OBJECT(x: anytype) @TypeOf(SCM_SMOB_OBJECT_1(x)) {
    return SCM_SMOB_OBJECT_1(x);
}
pub inline fn SCM_SET_SMOB_OBJECT(x: anytype, obj: anytype) @TypeOf(SCM_SET_SMOB_OBJECT_1(x, obj)) {
    return SCM_SET_SMOB_OBJECT_1(x, obj);
}
pub inline fn SCM_SMOB_OBJECT_LOC(x: anytype) @TypeOf(SCM_SMOB_OBJECT_1_LOC(x)) {
    return SCM_SMOB_OBJECT_1_LOC(x);
}
pub inline fn SCM_SMOB_APPLY_0(x: anytype) @TypeOf(scm_call_0(x)) {
    return scm_call_0(x);
}
pub inline fn SCM_SMOB_APPLY_1(x: anytype, a1: anytype) @TypeOf(scm_call_1(x, a1)) {
    return scm_call_1(x, a1);
}
pub inline fn SCM_SMOB_APPLY_2(x: anytype, a1: anytype, a2: anytype) @TypeOf(scm_call_2(x, a1, a2)) {
    return scm_call_2(x, a1, a2);
}
pub const _SCM_VM_H_ = "";
pub const _SCM_PROGRAMS_H_ = "";
pub inline fn SCM_PROGRAM_P(x: anytype) @TypeOf(SCM_HAS_TYP7(x, scm_tc7_program)) {
    return SCM_HAS_TYP7(x, scm_tc7_program);
}
pub inline fn SCM_PROGRAM_CODE(x: anytype) [*c]u32 {
    return @import("std").zig.c_translation.cast([*c]u32, SCM_CELL_WORD_1(x));
}
pub inline fn SCM_PROGRAM_FREE_VARIABLES(x: anytype) @TypeOf(SCM_CELL_OBJECT_LOC(x, @as(c_int, 2))) {
    return SCM_CELL_OBJECT_LOC(x, @as(c_int, 2));
}
pub inline fn SCM_PROGRAM_FREE_VARIABLE_REF(x: anytype, i: anytype) @TypeOf(SCM_PROGRAM_FREE_VARIABLES(x)[i]) {
    return SCM_PROGRAM_FREE_VARIABLES(x)[i];
}
pub inline fn SCM_PROGRAM_NUM_FREE_VARIABLES(x: anytype) @TypeOf(SCM_CELL_WORD_0(x) >> @as(c_int, 16)) {
    return SCM_CELL_WORD_0(x) >> @as(c_int, 16);
}
pub const SCM_F_PROGRAM_IS_BOOT = @as(c_int, 0x100);
pub const SCM_F_PROGRAM_IS_PRIMITIVE = @as(c_int, 0x200);
pub const SCM_F_PROGRAM_IS_PRIMITIVE_GENERIC = @as(c_int, 0x400);
pub const SCM_F_PROGRAM_IS_CONTINUATION = @as(c_int, 0x800);
pub const SCM_F_PROGRAM_IS_PARTIAL_CONTINUATION = @as(c_int, 0x1000);
pub const SCM_F_PROGRAM_IS_FOREIGN = @as(c_int, 0x2000);
pub inline fn SCM_PROGRAM_IS_BOOT(x: anytype) @TypeOf(SCM_CELL_WORD_0(x) & SCM_F_PROGRAM_IS_BOOT) {
    return SCM_CELL_WORD_0(x) & SCM_F_PROGRAM_IS_BOOT;
}
pub inline fn SCM_PROGRAM_IS_PRIMITIVE(x: anytype) @TypeOf(SCM_CELL_WORD_0(x) & SCM_F_PROGRAM_IS_PRIMITIVE) {
    return SCM_CELL_WORD_0(x) & SCM_F_PROGRAM_IS_PRIMITIVE;
}
pub inline fn SCM_PROGRAM_IS_PRIMITIVE_GENERIC(x: anytype) @TypeOf(SCM_CELL_WORD_0(x) & SCM_F_PROGRAM_IS_PRIMITIVE_GENERIC) {
    return SCM_CELL_WORD_0(x) & SCM_F_PROGRAM_IS_PRIMITIVE_GENERIC;
}
pub inline fn SCM_PROGRAM_IS_CONTINUATION(x: anytype) @TypeOf(SCM_CELL_WORD_0(x) & SCM_F_PROGRAM_IS_CONTINUATION) {
    return SCM_CELL_WORD_0(x) & SCM_F_PROGRAM_IS_CONTINUATION;
}
pub inline fn SCM_PROGRAM_IS_PARTIAL_CONTINUATION(x: anytype) @TypeOf(SCM_CELL_WORD_0(x) & SCM_F_PROGRAM_IS_PARTIAL_CONTINUATION) {
    return SCM_CELL_WORD_0(x) & SCM_F_PROGRAM_IS_PARTIAL_CONTINUATION;
}
pub inline fn SCM_PROGRAM_IS_FOREIGN(x: anytype) @TypeOf(SCM_CELL_WORD_0(x) & SCM_F_PROGRAM_IS_FOREIGN) {
    return SCM_CELL_WORD_0(x) & SCM_F_PROGRAM_IS_FOREIGN;
}
pub const SCM_VM_REGULAR_ENGINE = @as(c_int, 0);
pub const SCM_VM_DEBUG_ENGINE = @as(c_int, 1);
pub const SCM_VM_NUM_ENGINES = @as(c_int, 2);
pub const SCM_F_VM_CONT_PARTIAL = @as(c_int, 0x1);
pub const SCM_F_VM_CONT_REWINDABLE = @as(c_int, 0x2);
pub inline fn SCM_VM_CONT_P(OBJ: anytype) @TypeOf(SCM_HAS_TYP7(OBJ, scm_tc7_vm_cont)) {
    return SCM_HAS_TYP7(OBJ, scm_tc7_vm_cont);
}
pub inline fn SCM_VM_CONT_DATA(CONT: anytype) [*c]struct_scm_vm_cont {
    return @import("std").zig.c_translation.cast([*c]struct_scm_vm_cont, SCM_CELL_WORD_1(CONT));
}
pub inline fn SCM_VM_CONT_PARTIAL_P(CONT: anytype) @TypeOf(SCM_VM_CONT_DATA(CONT).*.flags & SCM_F_VM_CONT_PARTIAL) {
    return SCM_VM_CONT_DATA(CONT).*.flags & SCM_F_VM_CONT_PARTIAL;
}
pub inline fn SCM_VM_CONT_REWINDABLE_P(CONT: anytype) @TypeOf(SCM_VM_CONT_DATA(CONT).*.flags & SCM_F_VM_CONT_REWINDABLE) {
    return SCM_VM_CONT_DATA(CONT).*.flags & SCM_F_VM_CONT_REWINDABLE;
}
pub const SCM_PTHREADS_THREADS_H = "";
pub const _PTHREAD_H = @as(c_int, 1);
pub const _SCHED_H = @as(c_int, 1);
pub const _BITS_SCHED_H = @as(c_int, 1);
pub const SCHED_OTHER = @as(c_int, 0);
pub const SCHED_FIFO = @as(c_int, 1);
pub const SCHED_RR = @as(c_int, 2);
pub const _BITS_TYPES_STRUCT_SCHED_PARAM = @as(c_int, 1);
pub const _BITS_CPU_SET_H = @as(c_int, 1);
pub const __CPU_SETSIZE = @as(c_int, 1024);
pub const __NCPUBITS = @as(c_int, 8) * @import("std").zig.c_translation.sizeof(__cpu_mask);
pub inline fn __CPUELT(cpu: anytype) @TypeOf(cpu / __NCPUBITS) {
    return cpu / __NCPUBITS;
}
pub inline fn __CPUMASK(cpu: anytype) @TypeOf(@import("std").zig.c_translation.cast(__cpu_mask, @as(c_int, 1)) << (cpu % __NCPUBITS)) {
    return @import("std").zig.c_translation.cast(__cpu_mask, @as(c_int, 1)) << (cpu % __NCPUBITS);
}
pub inline fn __CPU_COUNT_S(setsize: anytype, cpusetp: anytype) @TypeOf(__sched_cpucount(setsize, cpusetp)) {
    return __sched_cpucount(setsize, cpusetp);
}
pub inline fn __CPU_ALLOC_SIZE(count: anytype) @TypeOf((((count + __NCPUBITS) - @as(c_int, 1)) / __NCPUBITS) * @import("std").zig.c_translation.sizeof(__cpu_mask)) {
    return (((count + __NCPUBITS) - @as(c_int, 1)) / __NCPUBITS) * @import("std").zig.c_translation.sizeof(__cpu_mask);
}
pub inline fn __CPU_ALLOC(count: anytype) @TypeOf(__sched_cpualloc(count)) {
    return __sched_cpualloc(count);
}
pub inline fn __CPU_FREE(cpuset: anytype) @TypeOf(__sched_cpufree(cpuset)) {
    return __sched_cpufree(cpuset);
}
// pub const __sched_priority = sched_priority;
pub const PTHREAD_CANCELED = @import("std").zig.c_translation.cast(?*anyopaque, -@as(c_int, 1));
pub const PTHREAD_ONCE_INIT = @as(c_int, 0);
pub const PTHREAD_BARRIER_SERIAL_THREAD = -@as(c_int, 1);
pub const __cleanup_fct_attribute = "";
pub inline fn __sigsetjmp_cancel(env: anytype, savemask: anytype) @TypeOf(__sigsetjmp(@import("std").zig.c_translation.cast([*c]struct___jmp_buf_tag, @import("std").zig.c_translation.cast(?*anyopaque, env)), savemask)) {
    return __sigsetjmp(@import("std").zig.c_translation.cast([*c]struct___jmp_buf_tag, @import("std").zig.c_translation.cast(?*anyopaque, env)), savemask);
}
pub const scm_i_pthread_t = pthread_t;
pub const scm_i_pthread_self = pthread_self;
pub const scm_i_pthread_create = pthread_create;
pub const scm_i_pthread_detach = pthread_detach;
pub const scm_i_pthread_exit = pthread_exit;
pub const scm_i_pthread_cancel = pthread_cancel;
pub const scm_i_pthread_cleanup_push = pthread_cleanup_push;
pub const scm_i_pthread_cleanup_pop = pthread_cleanup_pop;
pub const scm_i_sched_yield = sched_yield;
pub const scm_i_pthread_sigmask = pthread_sigmask;
pub const SCM_I_PTHREAD_MUTEX_INITIALIZER = PTHREAD_MUTEX_INITIALIZER;
pub const scm_i_pthread_mutex_t = pthread_mutex_t;
pub const scm_i_pthread_mutex_init = pthread_mutex_init;
pub const scm_i_pthread_mutex_destroy = pthread_mutex_destroy;
pub const scm_i_pthread_mutex_trylock = pthread_mutex_trylock;
pub const scm_i_pthread_mutex_lock = pthread_mutex_lock;
pub const scm_i_pthread_mutex_unlock = pthread_mutex_unlock;
pub const SCM_I_PTHREAD_COND_INITIALIZER = PTHREAD_COND_INITIALIZER;
pub const scm_i_pthread_cond_t = pthread_cond_t;
pub const scm_i_pthread_cond_init = pthread_cond_init;
pub const scm_i_pthread_cond_destroy = pthread_cond_destroy;
pub const scm_i_pthread_cond_signal = pthread_cond_signal;
pub const scm_i_pthread_cond_broadcast = pthread_cond_broadcast;
pub const scm_i_pthread_cond_wait = pthread_cond_wait;
pub const scm_i_pthread_cond_timedwait = pthread_cond_timedwait;
pub const scm_i_pthread_once_t = pthread_once_t;
pub const scm_i_pthread_once = pthread_once;
pub const SCM_I_PTHREAD_ONCE_INIT = PTHREAD_ONCE_INIT;
pub const scm_i_pthread_key_t = pthread_key_t;
pub const scm_i_pthread_key_create = pthread_key_create;
pub const scm_i_pthread_setspecific = pthread_setspecific;
pub const scm_i_pthread_getspecific = pthread_getspecific;
pub const scm_i_scm_pthread_mutex_lock = scm_pthread_mutex_lock;
pub const scm_i_dynwind_pthread_mutex_lock = scm_dynwind_pthread_mutex_lock;
pub const scm_i_scm_pthread_cond_wait = scm_pthread_cond_wait;
pub const scm_i_scm_pthread_cond_timedwait = scm_pthread_cond_timedwait;
pub const SCM_INLINE_GC_GRANULE_WORDS = @as(c_int, 2);
pub const SCM_INLINE_GC_GRANULE_BYTES = @import("std").zig.c_translation.sizeof(?*anyopaque) * SCM_INLINE_GC_GRANULE_WORDS;
pub const SCM_INLINE_GC_FREELIST_COUNT = @as(c_uint, 256) / SCM_INLINE_GC_GRANULE_BYTES;
pub inline fn SCM_I_IS_THREAD(x: anytype) @TypeOf(SCM_SMOB_PREDICATE(scm_tc16_thread, x)) {
    return SCM_SMOB_PREDICATE(scm_tc16_thread, x);
}
pub inline fn SCM_I_THREAD_DATA(x: anytype) [*c]scm_thread {
    return @import("std").zig.c_translation.cast([*c]scm_thread, SCM_SMOB_DATA(x));
}
pub inline fn SCM_VALIDATE_THREAD(pos: anytype, a: anytype) @TypeOf(scm_assert_smob_type(scm_tc16_thread, a)) {
    _ = pos;
    return scm_assert_smob_type(scm_tc16_thread, a);
}
pub inline fn SCM_VALIDATE_MUTEX(pos: anytype, a: anytype) @TypeOf(scm_assert_smob_type(scm_tc16_mutex, a)) {
    _ = pos;
    return scm_assert_smob_type(scm_tc16_mutex, a);
}
pub inline fn SCM_VALIDATE_CONDVAR(pos: anytype, a: anytype) @TypeOf(scm_assert_smob_type(scm_tc16_condvar, a)) {
    _ = pos;
    return scm_assert_smob_type(scm_tc16_condvar, a);
}
pub const SCM_TICK = scm_async_tick();
pub const SCM_ATOMIC_H = "";
pub const SCM_BITVECTORS_H = "";
pub const SCM_BYTEVECTORS_H = "";
pub const SCM_UNIFORM_H = "";
pub inline fn SCM_ARRAY_ELEMENT_TYPE_IS_UNBOXED(t: anytype) @TypeOf(scm_i_array_element_type_sizes[t] != @as(c_int, 0)) {
    return scm_i_array_element_type_sizes[t] != @as(c_int, 0);
}
pub const SCM_BYTEVECTOR_HEADER_SIZE = @as(c_uint, 4);
pub inline fn SCM_BYTEVECTOR_LENGTH(_bv: anytype) usize {
    return @import("std").zig.c_translation.cast(usize, SCM_CELL_WORD_1(_bv));
}
pub inline fn SCM_BYTEVECTOR_CONTENTS(_bv: anytype) [*c]i8 {
    return @import("std").zig.c_translation.cast([*c]i8, SCM_CELL_WORD_2(_bv));
}
pub inline fn SCM_BYTEVECTOR_PARENT(_bv: anytype) @TypeOf(SCM_CELL_OBJECT_3(_bv)) {
    return SCM_CELL_OBJECT_3(_bv);
}
pub inline fn SCM_BYTEVECTOR_P(x: anytype) @TypeOf(SCM_HAS_TYP7(x, scm_tc7_bytevector)) {
    return SCM_HAS_TYP7(x, scm_tc7_bytevector);
}
pub inline fn SCM_BYTEVECTOR_FLAGS(_bv: anytype) @TypeOf(SCM_CELL_TYPE(_bv) >> @as(c_ulong, 7)) {
    return SCM_CELL_TYPE(_bv) >> @as(c_ulong, 7);
}
pub inline fn SCM_SET_BYTEVECTOR_FLAGS(_bv: anytype, _f: anytype) @TypeOf(SCM_SET_CELL_TYPE(_bv, scm_tc7_bytevector | (@import("std").zig.c_translation.cast(scm_t_bits, _f) << @as(c_ulong, 7)))) {
    return SCM_SET_CELL_TYPE(_bv, scm_tc7_bytevector | (@import("std").zig.c_translation.cast(scm_t_bits, _f) << @as(c_ulong, 7)));
}
pub const SCM_F_BYTEVECTOR_CONTIGUOUS = @as(c_ulong, 0x100);
pub const SCM_F_BYTEVECTOR_IMMUTABLE = @as(c_ulong, 0x200);
pub inline fn SCM_MUTABLE_BYTEVECTOR_P(x: anytype) @TypeOf((SCM_NIMP(x) != 0) and ((SCM_CELL_TYPE(x) & (@as(c_ulong, 0x7f) | (SCM_F_BYTEVECTOR_IMMUTABLE << @as(c_ulong, 7)))) == scm_tc7_bytevector)) {
    return (SCM_NIMP(x) != 0) and ((SCM_CELL_TYPE(x) & (@as(c_ulong, 0x7f) | (SCM_F_BYTEVECTOR_IMMUTABLE << @as(c_ulong, 7)))) == scm_tc7_bytevector);
}
pub inline fn SCM_BYTEVECTOR_ELEMENT_TYPE(_bv: anytype) @TypeOf(SCM_BYTEVECTOR_FLAGS(_bv) & @as(c_ulong, 0xff)) {
    return SCM_BYTEVECTOR_FLAGS(_bv) & @as(c_ulong, 0xff);
}
pub inline fn SCM_BYTEVECTOR_CONTIGUOUS_P(_bv: anytype) @TypeOf(SCM_BYTEVECTOR_FLAGS(_bv) & SCM_F_BYTEVECTOR_CONTIGUOUS) {
    return SCM_BYTEVECTOR_FLAGS(_bv) & SCM_F_BYTEVECTOR_CONTIGUOUS;
}
pub inline fn SCM_BYTEVECTOR_TYPE_SIZE(@"var": anytype) @TypeOf(scm_i_array_element_type_sizes[SCM_BYTEVECTOR_ELEMENT_TYPE(@"var")] / @as(c_int, 8)) {
    return scm_i_array_element_type_sizes[SCM_BYTEVECTOR_ELEMENT_TYPE(@"var")] / @as(c_int, 8);
}
pub inline fn SCM_BYTEVECTOR_TYPED_LENGTH(@"var": anytype) @TypeOf(SCM_BYTEVECTOR_LENGTH(@"var") / SCM_BYTEVECTOR_TYPE_SIZE(@"var")) {
    return SCM_BYTEVECTOR_LENGTH(@"var") / SCM_BYTEVECTOR_TYPE_SIZE(@"var");
}
pub const SCM_GC_BYTEVECTOR = "bytevector";
pub const SCM_CONTINUATIONS_H = "";
pub inline fn SCM_CONTINUATIONP(x: anytype) @TypeOf((SCM_PROGRAM_P(x) != 0) and (SCM_PROGRAM_IS_CONTINUATION(x) != 0)) {
    return (SCM_PROGRAM_P(x) != 0) and (SCM_PROGRAM_IS_CONTINUATION(x) != 0);
}
pub const SCM_DYNL_H = "";
pub const SCM_DYNWIND_H = "";
pub const SCM_EQ_H = "";
pub inline fn SCM_EQ_P(x: anytype, y: anytype) @TypeOf(scm_is_eq(x, y)) {
    return scm_is_eq(x, y);
}
pub const SCM_EVAL_H = "";
pub const SCM_STRUCT_H = "";
pub const SCM_VTABLE_BASE_LAYOUT = "pw" ++ "uh" ++ "uh" ++ "pw" ++ "ph" ++ "uh" ++ "uh" ++ "uh";
pub const scm_vtable_index_layout = @as(c_int, 0);
pub const scm_vtable_index_flags = @as(c_int, 1);
pub const scm_vtable_index_instance_finalize = @as(c_int, 2);
pub const scm_vtable_index_instance_printer = @as(c_int, 3);
pub const scm_vtable_index_name = @as(c_int, 4);
pub const scm_vtable_index_size = @as(c_int, 5);
pub const scm_vtable_index_unboxed_fields = @as(c_int, 6);
pub const scm_vtable_index_reserved_7 = @as(c_int, 7);
pub const scm_vtable_offset_user = @as(c_int, 8);
pub const SCM_APPLICABLE_BASE_LAYOUT = "pw";
pub const SCM_APPLICABLE_WITH_SETTER_BASE_LAYOUT = "pw" ++ "pw";
pub const scm_applicable_struct_index_procedure = @as(c_int, 0);
pub const scm_applicable_struct_index_setter = @as(c_int, 1);
pub const SCM_VTABLE_FLAG_VALIDATED = @as(c_long, 1) << @as(c_int, 0);
pub const SCM_VTABLE_FLAG_VTABLE = @as(c_long, 1) << @as(c_int, 1);
pub const SCM_VTABLE_FLAG_APPLICABLE_VTABLE = @as(c_long, 1) << @as(c_int, 2);
pub const SCM_VTABLE_FLAG_APPLICABLE = @as(c_long, 1) << @as(c_int, 3);
pub const SCM_VTABLE_FLAG_SETTER_VTABLE = @as(c_long, 1) << @as(c_int, 4);
pub const SCM_VTABLE_FLAG_SETTER = @as(c_long, 1) << @as(c_int, 5);
pub const SCM_VTABLE_FLAG_RESERVED_0 = @as(c_long, 1) << @as(c_int, 6);
pub const SCM_VTABLE_FLAG_RESERVED_1 = @as(c_long, 1) << @as(c_int, 7);
pub const SCM_VTABLE_FLAG_SMOB_0 = @as(c_long, 1) << @as(c_int, 8);
pub const SCM_VTABLE_FLAG_GOOPS_0 = @as(c_long, 1) << @as(c_int, 9);
pub const SCM_VTABLE_FLAG_GOOPS_1 = @as(c_long, 1) << @as(c_int, 10);
pub const SCM_VTABLE_FLAG_GOOPS_2 = @as(c_long, 1) << @as(c_int, 11);
pub const SCM_VTABLE_FLAG_GOOPS_3 = @as(c_long, 1) << @as(c_int, 12);
pub const SCM_VTABLE_FLAG_GOOPS_4 = @as(c_long, 1) << @as(c_int, 13);
pub const SCM_VTABLE_FLAG_RESERVED_2 = @as(c_long, 1) << @as(c_int, 14);
pub const SCM_VTABLE_FLAG_RESERVED_3 = @as(c_long, 1) << @as(c_int, 15);
pub const SCM_VTABLE_USER_FLAG_SHIFT = @as(c_int, 16);
pub inline fn SCM_STRUCTP(X: anytype) @TypeOf(!(SCM_IMP(X) != 0) and (SCM_TYP3(X) == scm_tc3_struct)) {
    return !(SCM_IMP(X) != 0) and (SCM_TYP3(X) == scm_tc3_struct);
}
pub inline fn SCM_STRUCT_SLOTS(X: anytype) @TypeOf(SCM_CELL_OBJECT_LOC(X, @as(c_int, 1))) {
    return SCM_CELL_OBJECT_LOC(X, @as(c_int, 1));
}
pub inline fn SCM_STRUCT_SLOT_REF(X: anytype, I: anytype) @TypeOf(SCM_STRUCT_SLOTS(X)[I]) {
    return SCM_STRUCT_SLOTS(X)[I];
}
pub inline fn SCM_STRUCT_DATA(X: anytype) [*c]scm_t_bits {
    return @import("std").zig.c_translation.cast([*c]scm_t_bits, SCM_STRUCT_SLOTS(X));
}
pub inline fn SCM_STRUCT_DATA_REF(X: anytype, I: anytype) @TypeOf(SCM_STRUCT_DATA(X)[I]) {
    return SCM_STRUCT_DATA(X)[I];
}
pub inline fn SCM_VTABLE_LAYOUT(X: anytype) @TypeOf(SCM_STRUCT_SLOT_REF(X, scm_vtable_index_layout)) {
    return SCM_STRUCT_SLOT_REF(X, scm_vtable_index_layout);
}
pub inline fn SCM_SET_VTABLE_LAYOUT(X: anytype, L: anytype) @TypeOf(SCM_STRUCT_SLOT_SET(X, scm_vtable_index_layout, L)) {
    return SCM_STRUCT_SLOT_SET(X, scm_vtable_index_layout, L);
}
pub inline fn SCM_VTABLE_FLAGS(X: anytype) @TypeOf(SCM_STRUCT_DATA_REF(X, scm_vtable_index_flags)) {
    return SCM_STRUCT_DATA_REF(X, scm_vtable_index_flags);
}
pub inline fn SCM_VTABLE_FLAG_IS_SET(X: anytype, F: anytype) @TypeOf(SCM_STRUCT_DATA_REF(X, scm_vtable_index_flags) & F) {
    return SCM_STRUCT_DATA_REF(X, scm_vtable_index_flags) & F;
}
pub inline fn SCM_VTABLE_INSTANCE_FINALIZER(X: anytype) scm_t_struct_finalize {
    return @import("std").zig.c_translation.cast(scm_t_struct_finalize, SCM_STRUCT_DATA_REF(X, scm_vtable_index_instance_finalize));
}
pub inline fn SCM_SET_VTABLE_INSTANCE_FINALIZER(X: anytype, P: anytype) @TypeOf(SCM_STRUCT_DATA_SET(X, scm_vtable_index_instance_finalize, @import("std").zig.c_translation.cast(scm_t_bits, P))) {
    return SCM_STRUCT_DATA_SET(X, scm_vtable_index_instance_finalize, @import("std").zig.c_translation.cast(scm_t_bits, P));
}
pub inline fn SCM_VTABLE_INSTANCE_PRINTER(X: anytype) @TypeOf(SCM_STRUCT_SLOT_REF(X, scm_vtable_index_instance_printer)) {
    return SCM_STRUCT_SLOT_REF(X, scm_vtable_index_instance_printer);
}
pub inline fn SCM_SET_VTABLE_INSTANCE_PRINTER(X: anytype, P: anytype) @TypeOf(SCM_STRUCT_SLOT_SET(X, scm_vtable_index_instance_printer, P)) {
    return SCM_STRUCT_SLOT_SET(X, scm_vtable_index_instance_printer, P);
}
pub inline fn SCM_VTABLE_NAME(X: anytype) @TypeOf(SCM_STRUCT_SLOT_REF(X, scm_vtable_index_name)) {
    return SCM_STRUCT_SLOT_REF(X, scm_vtable_index_name);
}
pub inline fn SCM_SET_VTABLE_NAME(X: anytype, V: anytype) @TypeOf(SCM_STRUCT_SLOT_SET(X, scm_vtable_index_name, V)) {
    return SCM_STRUCT_SLOT_SET(X, scm_vtable_index_name, V);
}
pub inline fn SCM_VTABLE_SIZE(X: anytype) @TypeOf(SCM_STRUCT_DATA_REF(X, scm_vtable_index_size)) {
    return SCM_STRUCT_DATA_REF(X, scm_vtable_index_size);
}
pub inline fn SCM_VTABLE_UNBOXED_FIELDS(X: anytype) [*c]u32 {
    return @import("std").zig.c_translation.cast([*c]u32, SCM_STRUCT_DATA_REF(X, scm_vtable_index_unboxed_fields));
}
pub inline fn SCM_VTABLE_FIELD_IS_UNBOXED(X: anytype, F: anytype) @TypeOf(SCM_VTABLE_UNBOXED_FIELDS(X)[F >> @as(c_int, 5)] & (@as(c_uint, 1) << (F & @as(c_int, 31)))) {
    return SCM_VTABLE_UNBOXED_FIELDS(X)[F >> @as(c_int, 5)] & (@as(c_uint, 1) << (F & @as(c_int, 31)));
}
pub inline fn SCM_STRUCT_VTABLE(X: anytype) @TypeOf(SCM_PACK(SCM_CELL_WORD_0(X) - scm_tc3_struct)) {
    return SCM_PACK(SCM_CELL_WORD_0(X) - scm_tc3_struct);
}
pub inline fn SCM_STRUCT_LAYOUT(X: anytype) @TypeOf(SCM_VTABLE_LAYOUT(SCM_STRUCT_VTABLE(X))) {
    return SCM_VTABLE_LAYOUT(SCM_STRUCT_VTABLE(X));
}
pub inline fn SCM_STRUCT_SIZE(X: anytype) @TypeOf(SCM_VTABLE_SIZE(SCM_STRUCT_VTABLE(X))) {
    return SCM_VTABLE_SIZE(SCM_STRUCT_VTABLE(X));
}
pub inline fn SCM_STRUCT_PRINTER(X: anytype) @TypeOf(SCM_VTABLE_INSTANCE_PRINTER(SCM_STRUCT_VTABLE(X))) {
    return SCM_VTABLE_INSTANCE_PRINTER(SCM_STRUCT_VTABLE(X));
}
pub inline fn SCM_STRUCT_FINALIZER(X: anytype) @TypeOf(SCM_VTABLE_INSTANCE_FINALIZER(SCM_STRUCT_VTABLE(X))) {
    return SCM_VTABLE_INSTANCE_FINALIZER(SCM_STRUCT_VTABLE(X));
}
pub inline fn SCM_STRUCT_VTABLE_FLAGS(X: anytype) @TypeOf(SCM_VTABLE_FLAGS(SCM_STRUCT_VTABLE(X))) {
    return SCM_VTABLE_FLAGS(SCM_STRUCT_VTABLE(X));
}
pub inline fn SCM_STRUCT_VTABLE_FLAG_IS_SET(X: anytype, F: anytype) @TypeOf(SCM_VTABLE_FLAG_IS_SET(SCM_STRUCT_VTABLE(X), F)) {
    return SCM_VTABLE_FLAG_IS_SET(SCM_STRUCT_VTABLE(X), F);
}
pub inline fn SCM_STRUCT_FIELD_IS_UNBOXED(X: anytype, F: anytype) @TypeOf(SCM_VTABLE_FIELD_IS_UNBOXED(SCM_STRUCT_VTABLE(X), F)) {
    return SCM_VTABLE_FIELD_IS_UNBOXED(SCM_STRUCT_VTABLE(X), F);
}
pub inline fn SCM_STRUCT_APPLICABLE_P(X: anytype) @TypeOf(SCM_STRUCT_VTABLE_FLAG_IS_SET(X, SCM_VTABLE_FLAG_APPLICABLE)) {
    return SCM_STRUCT_VTABLE_FLAG_IS_SET(X, SCM_VTABLE_FLAG_APPLICABLE);
}
pub inline fn SCM_STRUCT_SETTER_P(X: anytype) @TypeOf(SCM_STRUCT_VTABLE_FLAG_IS_SET(X, SCM_VTABLE_FLAG_SETTER)) {
    return SCM_STRUCT_VTABLE_FLAG_IS_SET(X, SCM_VTABLE_FLAG_SETTER);
}
pub inline fn SCM_STRUCT_PROCEDURE(X: anytype) @TypeOf(SCM_STRUCT_SLOT_REF(X, scm_applicable_struct_index_procedure)) {
    return SCM_STRUCT_SLOT_REF(X, scm_applicable_struct_index_procedure);
}
pub inline fn SCM_SET_STRUCT_PROCEDURE(X: anytype, P: anytype) @TypeOf(SCM_STRUCT_SLOT_SET(X, scm_applicable_struct_index_procedure, P)) {
    return SCM_STRUCT_SLOT_SET(X, scm_applicable_struct_index_procedure, P);
}
pub inline fn SCM_STRUCT_SETTER(X: anytype) @TypeOf(SCM_STRUCT_SLOT_REF(X, scm_applicable_struct_index_setter)) {
    return SCM_STRUCT_SLOT_REF(X, scm_applicable_struct_index_setter);
}
pub inline fn SCM_SET_STRUCT_SETTER(X: anytype, P: anytype) @TypeOf(SCM_STRUCT_SLOT_SET(X, scm_applicable_struct_index_setter, P)) {
    return SCM_STRUCT_SLOT_SET(X, scm_applicable_struct_index_setter, P);
}
pub const SCM_MEMOIZE_H = "";
pub inline fn SCM_MEMOIZED_TAG(x: anytype) @TypeOf(scm_to_uint16(scm_car(x))) {
    return scm_to_uint16(scm_car(x));
}
pub inline fn SCM_MEMOIZED_ARGS(x: anytype) @TypeOf(scm_cdr(x)) {
    return scm_cdr(x);
}
pub const SCM_EXTEND_ENV = scm_acons;
pub inline fn scm_dapply(proc: anytype, arg1: anytype, args: anytype) @TypeOf(scm_apply(proc, arg1, args)) {
    return scm_apply(proc, arg1, args);
}
pub inline fn scm_primitive_eval_x(exp: anytype) @TypeOf(scm_primitive_eval(exp)) {
    return scm_primitive_eval(exp);
}
pub inline fn scm_eval_x(exp: anytype, module: anytype) @TypeOf(scm_eval(exp, module)) {
    return scm_eval(exp, module);
}
pub const SCM_EVALEXT_H = "";
pub const SCM_EXTENSIONS_H = "";
pub const SCM_FDES_FINALIZERS_H = "";
pub const SCM_FEATURE_H = "";
pub const SCM_FILESYS_H = "";
pub const SCM_DIR_FLAG_OPEN = @as(c_long, 1) << @as(c_int, 0);
pub inline fn SCM_DIRP(x: anytype) @TypeOf(SCM_HAS_TYP16(x, scm_tc16_dir)) {
    return SCM_HAS_TYP16(x, scm_tc16_dir);
}
pub inline fn SCM_DIR_OPEN_P(x: anytype) @TypeOf(SCM_SMOB_FLAGS(x) & SCM_DIR_FLAG_OPEN) {
    return SCM_SMOB_FLAGS(x) & SCM_DIR_FLAG_OPEN;
}
pub const SCM_FINALIZERS_H = "";
pub const SCM_FLUIDS_H = "";
pub const SCM_VECTORS_H = "";
pub inline fn SCM_SIMPLE_VECTOR_LENGTH(x: anytype) @TypeOf(SCM_I_VECTOR_LENGTH(x)) {
    return SCM_I_VECTOR_LENGTH(x);
}
pub inline fn SCM_SIMPLE_VECTOR_REF(x: anytype, idx: anytype) @TypeOf(SCM_I_VECTOR_ELTS(x)[idx]) {
    return SCM_I_VECTOR_ELTS(x)[idx];
}
pub const SCM_F_VECTOR_IMMUTABLE = @as(c_ulong, 0x80);
pub inline fn SCM_I_IS_MUTABLE_VECTOR(x: anytype) @TypeOf((SCM_NIMP(x) != 0) and ((SCM_CELL_TYPE(x) & (@as(c_int, 0x7f) | SCM_F_VECTOR_IMMUTABLE)) == scm_tc7_vector)) {
    return (SCM_NIMP(x) != 0) and ((SCM_CELL_TYPE(x) & (@as(c_int, 0x7f) | SCM_F_VECTOR_IMMUTABLE)) == scm_tc7_vector);
}
pub inline fn SCM_I_IS_VECTOR(x: anytype) @TypeOf(SCM_HAS_TYP7(x, scm_tc7_vector)) {
    return SCM_HAS_TYP7(x, scm_tc7_vector);
}
pub inline fn SCM_I_VECTOR_WELTS(x: anytype) @TypeOf(SCM_CELL_OBJECT_LOC(x, @as(c_int, 1))) {
    return SCM_CELL_OBJECT_LOC(x, @as(c_int, 1));
}
pub inline fn SCM_I_VECTOR_LENGTH(x: anytype) @TypeOf(@import("std").zig.c_translation.cast(usize, SCM_CELL_WORD_0(x)) >> @as(c_int, 8)) {
    return @import("std").zig.c_translation.cast(usize, SCM_CELL_WORD_0(x)) >> @as(c_int, 8);
}
pub inline fn SCM_FLUID_P(x: anytype) @TypeOf(SCM_HAS_TYP7(x, scm_tc7_fluid)) {
    return SCM_HAS_TYP7(x, scm_tc7_fluid);
}
pub inline fn SCM_VALIDATE_FLUID(pos: anytype, fluid: anytype) @TypeOf(SCM_I_MAKE_VALIDATE_MSG2(pos, fluid, scm_is_fluid, "fluid")) {
    return SCM_I_MAKE_VALIDATE_MSG2(pos, fluid, scm_is_fluid, "fluid");
}
pub const SCM_FOREIGN_H = "";
pub inline fn SCM_POINTER_P(x: anytype) @TypeOf(SCM_HAS_TYP7(x, scm_tc7_pointer)) {
    return SCM_HAS_TYP7(x, scm_tc7_pointer);
}
pub inline fn SCM_POINTER_VALUE(x: anytype) ?*anyopaque {
    return @import("std").zig.c_translation.cast(?*anyopaque, SCM_CELL_WORD_1(x));
}
pub inline fn SCM_IMMUTABLE_POINTER(c_name: anytype, ptr: anytype) @TypeOf(SCM_IMMUTABLE_CELL(c_name, scm_tc7_pointer, ptr)) {
    return SCM_IMMUTABLE_CELL(c_name, scm_tc7_pointer, ptr);
}
pub const SCM_FOREIGN_OBJECT_H = "";
pub const SCM_FPORTS_H = "";
pub const SCM_PORTS_H = "";
pub const SCM_STRINGS_H = "";
pub const scm_tc7_ro_string = scm_tc7_string + @as(c_int, 0x200);
pub const SCM_I_STRINGBUF_F_WIDE = @as(c_int, 0x400);
pub const SCM_I_STRINGBUF_F_MUTABLE = @as(c_int, 0x800);
pub inline fn SCM_EOF_OBJECT_P(x: anytype) @TypeOf(scm_is_eq(x, SCM_EOF_VAL)) {
    return scm_is_eq(x, SCM_EOF_VAL);
}
pub const SCM_OPN = @as(c_uint, 1) << @as(c_int, 8);
pub const SCM_RDNG = @as(c_uint, 1) << @as(c_int, 9);
pub const SCM_WRTNG = @as(c_uint, 1) << @as(c_int, 10);
pub const SCM_BUF0 = @as(c_uint, 1) << @as(c_int, 11);
pub const SCM_BUFLINE = @as(c_uint, 1) << @as(c_int, 12);
pub inline fn SCM_PORTP(x: anytype) @TypeOf(SCM_HAS_TYP7(x, scm_tc7_port)) {
    return SCM_HAS_TYP7(x, scm_tc7_port);
}
pub inline fn SCM_OPPORTP(x: anytype) @TypeOf((SCM_PORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_OPN) != 0)) {
    return (SCM_PORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_OPN) != 0);
}
pub inline fn SCM_INPUT_PORT_P(x: anytype) @TypeOf((SCM_PORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_RDNG) != 0)) {
    return (SCM_PORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_RDNG) != 0);
}
pub inline fn SCM_OUTPUT_PORT_P(x: anytype) @TypeOf((SCM_PORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_WRTNG) != 0)) {
    return (SCM_PORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_WRTNG) != 0);
}
pub inline fn SCM_OPINPORTP(x: anytype) @TypeOf((SCM_OPPORTP(x) != 0) and (SCM_INPUT_PORT_P(x) != 0)) {
    return (SCM_OPPORTP(x) != 0) and (SCM_INPUT_PORT_P(x) != 0);
}
pub inline fn SCM_OPOUTPORTP(x: anytype) @TypeOf((SCM_OPPORTP(x) != 0) and (SCM_OUTPUT_PORT_P(x) != 0)) {
    return (SCM_OPPORTP(x) != 0) and (SCM_OUTPUT_PORT_P(x) != 0);
}
pub inline fn SCM_OPENP(x: anytype) @TypeOf(SCM_OPPORTP(x)) {
    return SCM_OPPORTP(x);
}
pub inline fn SCM_CLOSEDP(x: anytype) @TypeOf(!(SCM_OPENP(x) != 0)) {
    return !(SCM_OPENP(x) != 0);
}
pub inline fn SCM_CLR_PORT_OPEN_FLAG(p: anytype) @TypeOf(SCM_SET_CELL_WORD_0(p, SCM_CELL_WORD_0(p) & ~SCM_OPN)) {
    return SCM_SET_CELL_WORD_0(p, SCM_CELL_WORD_0(p) & ~SCM_OPN);
}
pub inline fn SCM_STREAM(port: anytype) @TypeOf(SCM_CELL_WORD_1(port)) {
    return SCM_CELL_WORD_1(port);
}
pub inline fn SCM_SETSTREAM(port: anytype, stream: anytype) @TypeOf(SCM_SET_CELL_WORD_1(port, stream)) {
    return SCM_SET_CELL_WORD_1(port, stream);
}
pub inline fn SCM_PORT(x: anytype) [*c]scm_t_port {
    return @import("std").zig.c_translation.cast([*c]scm_t_port, SCM_CELL_WORD_2(x));
}
pub inline fn SCM_PORT_TYPE(port: anytype) [*c]scm_t_port_type {
    return @import("std").zig.c_translation.cast([*c]scm_t_port_type, SCM_CELL_WORD_3(port));
}
pub inline fn SCM_FSTREAM(x: anytype) [*c]scm_t_fport {
    return @import("std").zig.c_translation.cast([*c]scm_t_fport, SCM_STREAM(x));
}
pub inline fn SCM_FPORT_FDES(x: anytype) @TypeOf(SCM_FSTREAM(x).*.fdes) {
    return SCM_FSTREAM(x).*.fdes;
}
pub inline fn SCM_FPORTP(x: anytype) @TypeOf((SCM_PORTP(x) != 0) and (SCM_PORT_TYPE(x) == scm_file_port_type)) {
    return (SCM_PORTP(x) != 0) and (SCM_PORT_TYPE(x) == scm_file_port_type);
}
pub inline fn SCM_OPFPORTP(x: anytype) @TypeOf((SCM_FPORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_OPN) != 0)) {
    return (SCM_FPORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_OPN) != 0);
}
pub inline fn SCM_OPINFPORTP(x: anytype) @TypeOf((SCM_OPFPORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_RDNG) != 0)) {
    return (SCM_OPFPORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_RDNG) != 0);
}
pub inline fn SCM_OPOUTFPORTP(x: anytype) @TypeOf((SCM_OPFPORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_WRTNG) != 0)) {
    return (SCM_OPFPORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_WRTNG) != 0);
}
pub const _SCM_FRAMES_H_ = "";
pub inline fn SCM_FRAME_PREVIOUS_SP(fp: anytype) @TypeOf(fp + @as(c_int, 3)) {
    return fp + @as(c_int, 3);
}
pub inline fn SCM_FRAME_MACHINE_RETURN_ADDRESS(fp: anytype) @TypeOf(fp[@as(c_int, 0)].as_mcode) {
    return fp[@as(c_int, 0)].as_mcode;
}
pub inline fn SCM_FRAME_VIRTUAL_RETURN_ADDRESS(fp: anytype) @TypeOf(fp[@as(c_int, 1)].as_vcode) {
    return fp[@as(c_int, 1)].as_vcode;
}
pub inline fn SCM_FRAME_DYNAMIC_LINK(fp: anytype) @TypeOf(fp + fp[@as(c_int, 2)].as_uint) {
    return fp + fp[@as(c_int, 2)].as_uint;
}
pub inline fn SCM_FRAME_SLOT(fp: anytype, i: anytype) @TypeOf((fp - i) - @as(c_int, 1)) {
    return (fp - i) - @as(c_int, 1);
}
pub inline fn SCM_FRAME_LOCAL(fp: anytype, i: anytype) @TypeOf(SCM_FRAME_SLOT(fp, i).*.as_scm) {
    return SCM_FRAME_SLOT(fp, i).*.as_scm;
}
pub inline fn SCM_FRAME_NUM_LOCALS(fp: anytype, sp: anytype) @TypeOf(fp - sp) {
    return fp - sp;
}
pub const SCM_GENERALIZED_VECTORS_H = "";
pub inline fn SCM_VECTOR_IMPLEMENTATION(@"type": anytype, ctor: anytype) @TypeOf(SCM_SNARF_INIT(scm_i_register_vector_constructor(scm_i_array_element_types[@"type"], ctor))) {
    return SCM_SNARF_INIT(scm_i_register_vector_constructor(scm_i_array_element_types[@"type"], ctor));
}
pub const SCM_GOOPS_H = "";
pub const SCM_LIST_H = "";
pub const SCM_VTABLE_FLAG_GOOPS_CLASS = SCM_VTABLE_FLAG_GOOPS_0;
pub const SCM_VTABLE_FLAG_GOOPS_SLOT = SCM_VTABLE_FLAG_GOOPS_1;
pub const SCM_VTABLE_FLAG_GOOPS_STATIC_SLOT_ALLOCATION = SCM_VTABLE_FLAG_GOOPS_2;
pub const SCM_VTABLE_FLAG_GOOPS_INDIRECT = SCM_VTABLE_FLAG_GOOPS_3;
pub const SCM_VTABLE_FLAG_GOOPS_NEEDS_MIGRATION = SCM_VTABLE_FLAG_GOOPS_4;
pub inline fn SCM_CLASS_OF(x: anytype) @TypeOf(SCM_STRUCT_VTABLE(x)) {
    return SCM_STRUCT_VTABLE(x);
}
pub inline fn SCM_CLASS_FLAGS(class: anytype) @TypeOf(SCM_VTABLE_FLAGS(class)) {
    return SCM_VTABLE_FLAGS(class);
}
pub inline fn SCM_OBJ_CLASS_FLAGS(obj: anytype) @TypeOf(SCM_STRUCT_VTABLE_FLAGS(obj)) {
    return SCM_STRUCT_VTABLE_FLAGS(obj);
}
pub inline fn SCM_SET_CLASS_FLAGS(c: anytype, f: anytype) @TypeOf(SCM_SET_VTABLE_FLAGS(c, f)) {
    return SCM_SET_VTABLE_FLAGS(c, f);
}
pub inline fn SCM_CLEAR_CLASS_FLAGS(c: anytype, f: anytype) @TypeOf(SCM_CLEAR_VTABLE_FLAGS(c, f)) {
    return SCM_CLEAR_VTABLE_FLAGS(c, f);
}
pub const SCM_CLASSF_METACLASS = SCM_VTABLE_FLAG_GOOPS_CLASS | SCM_VTABLE_FLAG_VTABLE;
pub const SCM_CLASSF_GOOPS = SCM_VTABLE_FLAG_GOOPS_CLASS;
pub inline fn SCM_CLASSP(x: anytype) @TypeOf((SCM_STRUCTP(x) != 0) and ((SCM_STRUCT_VTABLE_FLAGS(x) & SCM_CLASSF_METACLASS) != 0)) {
    return (SCM_STRUCTP(x) != 0) and ((SCM_STRUCT_VTABLE_FLAGS(x) & SCM_CLASSF_METACLASS) != 0);
}
pub inline fn SCM_INSTANCEP(x: anytype) @TypeOf((SCM_STRUCTP(x) != 0) and ((SCM_STRUCT_VTABLE_FLAGS(x) & SCM_CLASSF_GOOPS) != 0)) {
    return (SCM_STRUCTP(x) != 0) and ((SCM_STRUCT_VTABLE_FLAGS(x) & SCM_CLASSF_GOOPS) != 0);
}
pub inline fn SCM_SLOT(x: anytype, i: anytype) @TypeOf(SCM_STRUCT_SLOT_REF(x, i)) {
    return SCM_STRUCT_SLOT_REF(x, i);
}
pub inline fn SCM_SET_SLOT(x: anytype, i: anytype, v: anytype) @TypeOf(SCM_STRUCT_SLOT_SET(x, i, v)) {
    return SCM_STRUCT_SLOT_SET(x, i, v);
}
pub inline fn SCM_SUBCLASSP(c1: anytype, c2: anytype) @TypeOf(scm_is_true(scm_c_memq(c2, scm_class_precedence_list(c1)))) {
    return scm_is_true(scm_c_memq(c2, scm_class_precedence_list(c1)));
}
pub inline fn SCM_IS_A_P(x: anytype, c: anytype) @TypeOf((SCM_INSTANCEP(x) != 0) and (SCM_SUBCLASSP(scm_class_of(x), c) != 0)) {
    return (SCM_INSTANCEP(x) != 0) and (SCM_SUBCLASSP(scm_class_of(x), c) != 0);
}
pub inline fn SCM_GENERICP(x: anytype) @TypeOf(scm_is_generic(x)) {
    return scm_is_generic(x);
}
pub inline fn SCM_METHODP(x: anytype) @TypeOf(scm_is_method(x)) {
    return scm_is_method(x);
}
pub const SCM_GSUBR_H = "";
pub const SCM_GSUBR_MAX = @as(c_int, 10);
pub inline fn SCM_PRIMITIVE_P(x: anytype) @TypeOf((SCM_PROGRAM_P(x) != 0) and (SCM_PROGRAM_IS_PRIMITIVE(x) != 0)) {
    return (SCM_PROGRAM_P(x) != 0) and (SCM_PROGRAM_IS_PRIMITIVE(x) != 0);
}
pub inline fn SCM_PRIMITIVE_GENERIC_P(x: anytype) @TypeOf((SCM_PROGRAM_P(x) != 0) and (SCM_PROGRAM_IS_PRIMITIVE_GENERIC(x) != 0)) {
    return (SCM_PROGRAM_P(x) != 0) and (SCM_PROGRAM_IS_PRIMITIVE_GENERIC(x) != 0);
}
pub inline fn SCM_SUBRF(x: anytype) @TypeOf(scm_subr_function(x)) {
    return scm_subr_function(x);
}
pub inline fn SCM_SUBR_NAME(x: anytype) @TypeOf(scm_subr_name(x)) {
    return scm_subr_name(x);
}
pub inline fn SCM_SUBR_GENERIC(x: anytype) [*c]SCM {
    return @import("std").zig.c_translation.cast([*c]SCM, SCM_POINTER_VALUE(SCM_PROGRAM_FREE_VARIABLE_REF(x, @as(c_int, 0))));
}
pub const SCM_FUNC_CAST_ARBITRARY_ARGS = scm_t_subr;
pub const SCM_DEFINE = SCM_DEFINE_GSUBR;
pub const SCM_GUARDIANS_H = "";
pub const SCM_HASH_H = "";
pub const SCM_HASHTAB_H = "";
pub inline fn SCM_HASHTABLE_P(x: anytype) @TypeOf(SCM_HAS_TYP7(x, scm_tc7_hashtable)) {
    return SCM_HAS_TYP7(x, scm_tc7_hashtable);
}
pub inline fn SCM_HASHTABLE_VECTOR(h: anytype) @TypeOf(SCM_CELL_OBJECT_1(h)) {
    return SCM_CELL_OBJECT_1(h);
}
pub inline fn SCM_SET_HASHTABLE_VECTOR(x: anytype, v: anytype) @TypeOf(SCM_SET_CELL_OBJECT_1(x, v)) {
    return SCM_SET_CELL_OBJECT_1(x, v);
}
pub inline fn SCM_HASHTABLE(x: anytype) [*c]scm_t_hashtable {
    return @import("std").zig.c_translation.cast([*c]scm_t_hashtable, SCM_CELL_WORD_2(x));
}
pub inline fn SCM_HASHTABLE_N_ITEMS(x: anytype) @TypeOf(SCM_HASHTABLE(x).*.n_items) {
    return SCM_HASHTABLE(x).*.n_items;
}
pub inline fn SCM_HASHTABLE_UPPER(x: anytype) @TypeOf(SCM_HASHTABLE(x).*.upper) {
    return SCM_HASHTABLE(x).*.upper;
}
pub inline fn SCM_HASHTABLE_LOWER(x: anytype) @TypeOf(SCM_HASHTABLE(x).*.lower) {
    return SCM_HASHTABLE(x).*.lower;
}
pub inline fn SCM_HASHTABLE_N_BUCKETS(h: anytype) @TypeOf(SCM_SIMPLE_VECTOR_LENGTH(SCM_HASHTABLE_VECTOR(h))) {
    return SCM_SIMPLE_VECTOR_LENGTH(SCM_HASHTABLE_VECTOR(h));
}
pub inline fn SCM_HASHTABLE_BUCKET(h: anytype, i: anytype) @TypeOf(SCM_SIMPLE_VECTOR_REF(SCM_HASHTABLE_VECTOR(h), i)) {
    return SCM_SIMPLE_VECTOR_REF(SCM_HASHTABLE_VECTOR(h), i);
}
pub inline fn SCM_SET_HASHTABLE_BUCKET(h: anytype, i: anytype, x: anytype) @TypeOf(SCM_SIMPLE_VECTOR_SET(SCM_HASHTABLE_VECTOR(h), i, x)) {
    return SCM_SIMPLE_VECTOR_SET(SCM_HASHTABLE_VECTOR(h), i, x);
}
pub const SCM_HOOKS_H = "";
pub inline fn SCM_HOOKP(x: anytype) @TypeOf(SCM_SMOB_PREDICATE(scm_tc16_hook, x)) {
    return SCM_SMOB_PREDICATE(scm_tc16_hook, x);
}
pub inline fn SCM_HOOK_ARITY(hook: anytype) @TypeOf(SCM_SMOB_FLAGS(hook)) {
    return SCM_SMOB_FLAGS(hook);
}
pub inline fn SCM_HOOK_PROCEDURES(hook: anytype) @TypeOf(SCM_SMOB_OBJECT(hook)) {
    return SCM_SMOB_OBJECT(hook);
}
pub inline fn SCM_SET_HOOK_PROCEDURES(hook: anytype, procs: anytype) @TypeOf(SCM_SET_SMOB_OBJECT(hook, procs)) {
    return SCM_SET_SMOB_OBJECT(hook, procs);
}
pub const SCM_I18N_H = "";
pub const SCM_INIT_H = "";
pub const SCM_IOEXT_H = "";
pub const SCM_RDELIM_H = "";
pub const SCM_RW_H = "";
pub const SCM_KEYWORDS_H = "";
pub inline fn SCM_I_KEYWORD_HASH(x: anytype) @TypeOf(scm_i_symbol_hash(SCM_CELL_OBJECT_1(x))) {
    return scm_i_symbol_hash(SCM_CELL_OBJECT_1(x));
}
pub const SCM_LOAD_H = "";
pub const SCM_MACROS_H = "";
pub const SCM_MALLOCS_H = "";
pub inline fn SCM_MALLOCP(X: anytype) @TypeOf(SCM_SMOB_PREDICATE(scm_tc16_malloc, X)) {
    return SCM_SMOB_PREDICATE(scm_tc16_malloc, X);
}
pub inline fn SCM_MALLOCDATA(obj: anytype) [*c]u8 {
    return @import("std").zig.c_translation.cast([*c]u8, SCM_SMOB_DATA(obj));
}
pub inline fn SCM_SETMALLOCDATA(obj: anytype, val: anytype) @TypeOf(SCM_SET_SMOB_DATA(obj, val)) {
    return SCM_SET_SMOB_DATA(obj, val);
}
pub const SCM_MODULES_H = "";
pub inline fn SCM_MODULEP(OBJ: anytype) @TypeOf(!(SCM_IMP(OBJ) != 0) and (SCM_CELL_TYPE(OBJ) == scm_module_tag)) {
    return !(SCM_IMP(OBJ) != 0) and (SCM_CELL_TYPE(OBJ) == scm_module_tag);
}
pub const scm_module_index_obarray = @as(c_int, 0);
pub const scm_module_index_uses = @as(c_int, 1);
pub const scm_module_index_binder = @as(c_int, 2);
pub const scm_module_index_eval_closure = @as(c_int, 3);
pub const scm_module_index_transformer = @as(c_int, 4);
pub const scm_module_index_duplicate_handlers = @as(c_int, 7);
pub const scm_module_index_import_obarray = @as(c_int, 8);
pub inline fn SCM_MODULE_OBARRAY(module: anytype) @TypeOf(SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_obarray])) {
    return SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_obarray]);
}
pub inline fn SCM_MODULE_USES(module: anytype) @TypeOf(SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_uses])) {
    return SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_uses]);
}
pub inline fn SCM_MODULE_BINDER(module: anytype) @TypeOf(SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_binder])) {
    return SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_binder]);
}
pub inline fn SCM_MODULE_EVAL_CLOSURE(module: anytype) @TypeOf(SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_eval_closure])) {
    return SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_eval_closure]);
}
pub inline fn SCM_MODULE_TRANSFORMER(module: anytype) @TypeOf(SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_transformer])) {
    return SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_transformer]);
}
pub inline fn SCM_MODULE_DUPLICATE_HANDLERS(module: anytype) @TypeOf(SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_duplicate_handlers])) {
    return SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_duplicate_handlers]);
}
pub inline fn SCM_MODULE_IMPORT_OBARRAY(module: anytype) @TypeOf(SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_import_obarray])) {
    return SCM_PACK(SCM_STRUCT_DATA(module)[scm_module_index_import_obarray]);
}
pub const SCM_NET_DB_H = "";
pub const SCM_OBJPROP_H = "";
pub const SCM_POSIX_H = "";
pub const SCM_PROCPROP_H = "";
pub const SCM_PROMISES_H = "";
pub const SCM_F_PROMISE_COMPUTED = @as(c_long, 1) << @as(c_int, 0);
pub inline fn SCM_PROMISE_COMPUTED_P(promise: anytype) @TypeOf(SCM_F_PROMISE_COMPUTED & SCM_SMOB_FLAGS(promise)) {
    return SCM_F_PROMISE_COMPUTED & SCM_SMOB_FLAGS(promise);
}
pub inline fn SCM_SET_PROMISE_COMPUTED(promise: anytype) @TypeOf(SCM_SET_SMOB_FLAGS(promise, SCM_F_PROMISE_COMPUTED)) {
    return SCM_SET_SMOB_FLAGS(promise, SCM_F_PROMISE_COMPUTED);
}
pub const SCM_PROMISE_MUTEX = SCM_SMOB_OBJECT_2;
pub const SCM_PROMISE_DATA = SCM_SMOB_OBJECT;
pub const SCM_SET_PROMISE_DATA = SCM_SET_SMOB_OBJECT;
pub const SCM_R6RS_PORTS_H = "";
pub const SCM_RANDOM_H = "";
pub inline fn scm_c_uniform32(RSTATE: anytype) @TypeOf(RSTATE.*.rng.*.random_bits(RSTATE)) {
    return RSTATE.*.rng.*.random_bits(RSTATE);
}
pub inline fn SCM_RSTATEP(obj: anytype) @TypeOf(SCM_SMOB_PREDICATE(scm_tc16_rstate, obj)) {
    return SCM_SMOB_PREDICATE(scm_tc16_rstate, obj);
}
pub inline fn SCM_RSTATE(obj: anytype) [*c]scm_t_rstate {
    return @import("std").zig.c_translation.cast([*c]scm_t_rstate, SCM_SMOB_DATA(obj));
}
pub const SCM_READ_H = "";
pub const SCM_LINE_INCREMENTORS = '\n';
pub const SCM_SCMSIGS_H = "";
pub const SCM_SCRIPT_H = "";
pub const SCM_SIMPOS_H = "";
pub const SCM_SOCKET_H = "";
pub const SCM_SORT_H = "";
pub const SCM_SRCPROP_H = "";
pub const SCM_STACKCHK_H = "";
pub const SCM_DEBUG_H = "";
pub const SCM_CHECK_STACK = "";
pub const SCM_STIME_H = "";
pub const SCM_TIME_UNITS_PER_SECOND = scm_c_time_units_per_second;
pub const SCM_SRFI_13_H = "";
pub const SCM_SRFI_14_H = "";
pub inline fn SCM_CHARSET_GET(cs: anytype, idx: anytype) @TypeOf(scm_i_charset_get(@import("std").zig.c_translation.cast([*c]scm_t_char_set, SCM_SMOB_DATA(cs)), idx)) {
    return scm_i_charset_get(@import("std").zig.c_translation.cast([*c]scm_t_char_set, SCM_SMOB_DATA(cs)), idx);
}
pub inline fn SCM_CHARSETP(x: anytype) @TypeOf(SCM_HAS_TYP16(x, scm_tc16_charset)) {
    return SCM_HAS_TYP16(x, scm_tc16_charset);
}
pub const SCM_STRORDER_H = "";
pub const SCM_STRPORTS_H = "";
pub inline fn SCM_STRPORTP(x: anytype) @TypeOf((SCM_PORTP(x) != 0) and (SCM_PORT_TYPE(x) == scm_string_port_type)) {
    return (SCM_PORTP(x) != 0) and (SCM_PORT_TYPE(x) == scm_string_port_type);
}
pub inline fn SCM_OPSTRPORTP(x: anytype) @TypeOf((SCM_STRPORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_OPN) != 0)) {
    return (SCM_STRPORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_OPN) != 0);
}
pub inline fn SCM_OPINSTRPORTP(x: anytype) @TypeOf((SCM_OPSTRPORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_RDNG) != 0)) {
    return (SCM_OPSTRPORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_RDNG) != 0);
}
pub inline fn SCM_OPOUTSTRPORTP(x: anytype) @TypeOf((SCM_OPSTRPORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_WRTNG) != 0)) {
    return (SCM_OPSTRPORTP(x) != 0) and ((SCM_CELL_WORD_0(x) & SCM_WRTNG) != 0);
}
pub const SCM_SYMBOLS_H = "";
pub inline fn scm_is_symbol(x: anytype) @TypeOf(SCM_HAS_TYP7(x, scm_tc7_symbol)) {
    return SCM_HAS_TYP7(x, scm_tc7_symbol);
}
pub inline fn scm_i_symbol_hash(x: anytype) c_ulong {
    return @import("std").zig.c_translation.cast(c_ulong, SCM_CELL_WORD_2(x));
}
pub inline fn scm_i_symbol_is_interned(x: anytype) @TypeOf(!((SCM_CELL_WORD_0(x) & SCM_I_F_SYMBOL_UNINTERNED) != 0)) {
    return !((SCM_CELL_WORD_0(x) & SCM_I_F_SYMBOL_UNINTERNED) != 0);
}
pub const SCM_I_F_SYMBOL_UNINTERNED = @as(c_int, 0x100);
pub inline fn SCM_SYMBOLP(x: anytype) @TypeOf(scm_is_symbol(x)) {
    return scm_is_symbol(x);
}
pub inline fn SCM_SYMBOL_HASH(x: anytype) @TypeOf(scm_i_symbol_hash(x)) {
    return scm_i_symbol_hash(x);
}
pub inline fn SCM_SYMBOL_INTERNED_P(x: anytype) @TypeOf(scm_i_symbol_is_interned(x)) {
    return scm_i_symbol_is_interned(x);
}
pub const SCM_VALUES_H = "";
pub inline fn SCM_VALUESP(x: anytype) @TypeOf(scm_is_values(x)) {
    return scm_is_values(x);
}
pub const SCM_VARIABLE_H = "";
pub inline fn SCM_VARIABLEP(X: anytype) @TypeOf(SCM_HAS_TYP7(X, scm_tc7_variable)) {
    return SCM_HAS_TYP7(X, scm_tc7_variable);
}
pub inline fn SCM_VARIABLE_REF(V: anytype) @TypeOf(SCM_CELL_OBJECT_1(V)) {
    return SCM_CELL_OBJECT_1(V);
}
pub inline fn SCM_VARIABLE_SET(V: anytype, X: anytype) @TypeOf(SCM_SET_CELL_OBJECT_1(V, X)) {
    return SCM_SET_CELL_OBJECT_1(V, X);
}
pub inline fn SCM_VARIABLE_LOC(V: anytype) @TypeOf(SCM_CELL_OBJECT_LOC(V, @as(c_int, 1))) {
    return SCM_CELL_OBJECT_LOC(V, @as(c_int, 1));
}
pub const SCM_SRFI_4_H = "";
pub const SCM_VERSION_H = "";
pub const SCM_MAJOR_VERSION = @as(c_int, 3);
pub const SCM_MINOR_VERSION = @as(c_int, 0);
pub const SCM_MICRO_VERSION = @as(c_int, 8);
pub const SCM_EFFECTIVE_VERSION = "3.0";
pub const SCM_VPORTS_H = "";
pub const SCM_WEAK_SET_H = "";
pub const SCM_WEAK_TABLE_H = "";
pub const SCM_WEAK_VECTOR_H = "";
pub inline fn SCM_I_WVECTP(x: anytype) @TypeOf(SCM_HAS_TYP7(x, scm_tc7_wvect)) {
    return SCM_HAS_TYP7(x, scm_tc7_wvect);
}
pub const SCM_BACKTRACE_H = "";
pub const SCM_STACKS_H = "";
pub const SCM_STACK_LAYOUT = "pw" ++ "pw" ++ "pw";
pub inline fn SCM_STACKP(obj: anytype) @TypeOf((SCM_STRUCTP(obj) != 0) and (scm_is_eq(SCM_STRUCT_VTABLE(obj), scm_stack_type) != 0)) {
    return (SCM_STRUCTP(obj) != 0) and (scm_is_eq(SCM_STRUCT_VTABLE(obj), scm_stack_type) != 0);
}
pub inline fn SCM_STACK_LENGTH(obj: anytype) @TypeOf(scm_to_long(SCM_STRUCT_SLOT_REF(obj, @as(c_int, 0)))) {
    return scm_to_long(SCM_STRUCT_SLOT_REF(obj, @as(c_int, 0)));
}
pub inline fn SCM_SET_STACK_LENGTH(obj: anytype, f: anytype) @TypeOf(SCM_STRUCT_SLOT_SET(obj, @as(c_int, 0), scm_from_long(f))) {
    return SCM_STRUCT_SLOT_SET(obj, @as(c_int, 0), scm_from_long(f));
}
pub inline fn SCM_STACK_ID(obj: anytype) @TypeOf(SCM_STRUCT_SLOT_REF(obj, @as(c_int, 1))) {
    return SCM_STRUCT_SLOT_REF(obj, @as(c_int, 1));
}
pub inline fn SCM_SET_STACK_ID(obj: anytype, f: anytype) @TypeOf(SCM_STRUCT_SLOT_SET(obj, @as(c_int, 1), f)) {
    return SCM_STRUCT_SLOT_SET(obj, @as(c_int, 1), f);
}
pub inline fn SCM_STACK_FRAME(obj: anytype) @TypeOf(SCM_STRUCT_SLOT_REF(obj, @as(c_int, 2))) {
    return SCM_STRUCT_SLOT_REF(obj, @as(c_int, 2));
}
pub inline fn SCM_SET_STACK_FRAME(obj: anytype, f: anytype) @TypeOf(SCM_STRUCT_SLOT_SET(obj, @as(c_int, 2), f)) {
    return SCM_STRUCT_SLOT_SET(obj, @as(c_int, 2), f);
}
pub const SCM_DEPRECATED_H = "";
pub inline fn SCM_GNUC_PREREQ(maj: anytype, min: anytype) @TypeOf(((__GNUC__ << @as(c_int, 16)) + __GNUC_MINOR__) >= ((maj << @as(c_int, 16)) + min)) {
    return ((__GNUC__ << @as(c_int, 16)) + __GNUC_MINOR__) >= ((maj << @as(c_int, 16)) + min);
}
pub const scm_gc_running_p = @as(c_int, 0);
pub inline fn SCM_I_UTYPE_MAX(@"type": anytype) @TypeOf(@"type" - @as(c_int, 1)) {
    return @"type" - @as(c_int, 1);
}
pub inline fn SCM_I_TYPE_MAX(@"type": anytype, umax: anytype) @TypeOf(@"type"(umax / @as(c_int, 2))) {
    return @"type"(umax / @as(c_int, 2));
}
pub inline fn SCM_I_TYPE_MIN(@"type": anytype, umax: anytype) @TypeOf(-@"type"(umax / @as(c_int, 2)) - @as(c_int, 1)) {
    return -@"type"(umax / @as(c_int, 2)) - @as(c_int, 1);
}
pub const SCM_T_UINT8_MAX = UINT8_MAX;
pub const SCM_T_INT8_MIN = INT8_MIN;
pub const SCM_T_INT8_MAX = INT8_MAX;
pub const SCM_T_UINT16_MAX = UINT16_MAX;
pub const SCM_T_INT16_MIN = INT16_MIN;
pub const SCM_T_INT16_MAX = INT16_MAX;
pub const SCM_T_UINT32_MAX = UINT32_MAX;
pub const SCM_T_INT32_MIN = INT32_MIN;
pub const SCM_T_INT32_MAX = INT32_MAX;
pub const SCM_T_UINT64_MAX = UINT64_MAX;
pub const SCM_T_INT64_MIN = INT64_MIN;
pub const SCM_T_INT64_MAX = INT64_MAX;
pub const SCM_T_UINTMAX_MAX = UINTMAX_MAX;
pub const SCM_T_INTMAX_MIN = INTMAX_MIN;
pub const SCM_T_INTMAX_MAX = INTMAX_MAX;
pub const SCM_T_UINTPTR_MAX = UINTPTR_MAX;
pub const SCM_T_INTPTR_MIN = INTPTR_MIN;
pub const SCM_T_INTPTR_MAX = INTPTR_MAX;
pub const SCM_HAVE_T_INT64 = @as(c_int, 1);
pub const SCM_HAVE_T_UINT64 = @as(c_int, 1);
pub const SCM_HAVE_ARRAYS = @as(c_int, 1);
pub const SCM_SOURCE_PROPERTY_FLAG_BREAK = @as(c_int, 1);
pub inline fn SCM_SYMBOL_FUNC(x: anytype) @TypeOf(scm_symbol_fref(x)) {
    return scm_symbol_fref(x);
}
pub inline fn SCM_SET_SYMBOL_FUNC(x: anytype, f: anytype) @TypeOf(scm_symbol_fset_x(x, f)) {
    return scm_symbol_fset_x(x, f);
}
pub inline fn SCM_SYMBOL_PROPS(x: anytype) @TypeOf(scm_symbol_pref(x)) {
    return scm_symbol_pref(x);
}
pub inline fn SCM_SET_SYMBOL_PROPS(x: anytype, p: anytype) @TypeOf(scm_symbol_pset_x(x, p)) {
    return scm_symbol_pset_x(x, p);
}
pub const timeval = struct_timeval;
pub const timespec = struct_timespec;
pub const __itimer_which = enum___itimer_which;
pub const itimerval = struct_itimerval;
pub const tm = struct_tm;
pub const itimerspec = struct_itimerspec;
pub const sigval = union_sigval;
pub const sigevent = struct_sigevent;
pub const __locale_data = struct___locale_data;
pub const __locale_struct = struct___locale_struct;
pub const __pthread_internal_list = struct___pthread_internal_list;
pub const __pthread_internal_slist = struct___pthread_internal_slist;
pub const __pthread_mutex_s = struct___pthread_mutex_s;
pub const __pthread_rwlock_arch_t = struct___pthread_rwlock_arch_t;
pub const __pthread_cond_s = struct___pthread_cond_s;
pub const random_data = struct_random_data;
pub const drand48_data = struct_drand48_data;
pub const scm_unused_struct = struct_scm_unused_struct;
pub const scm_tc8_tags = enum_scm_tc8_tags;
pub const scm_dynamic_state = struct_scm_dynamic_state;
pub const scm_dynstack = struct_scm_dynstack;
pub const scm_frame = struct_scm_frame;
pub const scm_vm_stack_element = union_scm_vm_stack_element;
pub const __jmp_buf_tag = struct___jmp_buf_tag;
pub const scm_vm = struct_scm_vm;
pub const scm_thread_wake_data = struct_scm_thread_wake_data;
pub const scm_jit_state = struct_scm_jit_state;
pub const scm_body_thunk_data = struct_scm_body_thunk_data;
pub const _fpx_sw_bytes = struct__fpx_sw_bytes;
pub const _fpreg = struct__fpreg;
pub const _fpxreg = struct__fpxreg;
pub const _xmmreg = struct__xmmreg;
pub const _fpstate = struct__fpstate;
pub const sigcontext = struct_sigcontext;
pub const _xsave_hdr = struct__xsave_hdr;
pub const _ymmh_state = struct__ymmh_state;
pub const _xstate = struct__xstate;
pub const _libc_fpxreg = struct__libc_fpxreg;
pub const _libc_xmmreg = struct__libc_xmmreg;
pub const _libc_fpstate = struct__libc_fpstate;
pub const scm_compare = enum_scm_compare;
pub const GC_ms_entry = struct_GC_ms_entry;
pub const scm_vm_cont = struct_scm_vm_cont;
pub const sched_param = struct_sched_param;
pub const _pthread_cleanup_buffer = struct__pthread_cleanup_buffer;
pub const __cancel_jmp_buf_tag = struct___cancel_jmp_buf_tag;
pub const __pthread_cleanup_frame = struct___pthread_cleanup_frame;
pub const scm_keyword_arguments_flags = enum_scm_keyword_arguments_flags;
pub const sockaddr = struct_sockaddr;
