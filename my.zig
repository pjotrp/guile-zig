//! This zig module provides functionality for binding guile against zig


const std = @import("std");
const mem = @import("std").mem;
const guile = @import("test-include.zig");
const SCM = guile.SCM;
const expectEqual = @import("std").testing.expectEqual;
const expect = @import("std").testing.expect;
const ArrayList = std.ArrayList;
const StringList = ArrayList([] const u8);
const Allocator = std.mem.Allocator;
const p = @import("std").debug.print;

const hello = "Hello World from Zig";

pub export fn ping_zig(i: SCM) SCM {
    return i;
}

export fn hello_zig() void {
    p("Hello world", .{});
}

fn hello_zig2(msg: [] const u8) []const u8 {
    const result = msg;
    return result;
}

export fn my_incrementing_zig_function(testi: SCM, flag: SCM) SCM {
    var result = testi;
    var vflag = guile.scm_to_bool(flag);
    if (vflag != 0)
        result = guile.scm_sum(testi, guile.scm_from_int (1))
    else
        result = testi;

    return result;
}

// Increment the first value that is 2 in a list
export fn my_increment_in_list_zig(lst: SCM) SCM {
    var lst2 = guile.scm_memv(guile.scm_from_int(2), lst);
    lst2 = guile.scm_list_set_x(lst2, guile.scm_from_int(0), guile.scm_from_int(3));
    return lst; // note original is updated!
}

// Increment the second value in the unboxed vector
export fn my_increment_in_f64vector_zig(vec: SCM) SCM {
    // _ = guile.scm_f64vector_set_x(vec, guile.scm_from_int(1), guile.scm_from_double(3.7));
    var handle: guile.scm_t_array_handle = undefined;
    var lenp: usize = undefined;
    var incp: isize = undefined;
    var data = guile.scm_f64vector_writable_elements(vec, &handle, &lenp, &incp);
    p("\nZig says {any},{any},{any}\n",.{data[0],data[1],data[2]});
    data[1] += 1.0;
    guile.scm_array_handle_release(&handle);
    return vec; // note original is updated!
}

test "hello zig" {
    try expectEqual(hello_zig2(hello),hello);
}

// ---- Guile

// pub const scm_t_subr = *anyopaque;

export fn init_module() void {
    // var t = @ptrCast(*anyopaque,&ping_zig);
    var c_ping_zig = @intToPtr(*anyopaque, @ptrToInt(&ping_zig));
    var c_incr_zig = @intToPtr(*anyopaque, @ptrToInt(&my_incrementing_zig_function));
    var c_incr_list_zig = @intToPtr(*anyopaque, @ptrToInt(&my_increment_in_list_zig));
    var c_incr_f64vector_zig = @intToPtr(*anyopaque, @ptrToInt(&my_increment_in_f64vector_zig));
    var res = guile.scm_c_define_gsubr("ping_zig", 1, 0, 0, c_ping_zig);
    res = guile.scm_c_define_gsubr("incr_zig", 2, 0, 0, c_incr_zig);
    res = guile.scm_c_define_gsubr("incr_list_zig", 1, 0, 0, c_incr_list_zig);
    res = guile.scm_c_define_gsubr("incr_f64vector_zig", 1, 0, 0, c_incr_f64vector_zig);
}
