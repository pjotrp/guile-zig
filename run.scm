(load-extension "libmy" "init_module")
; (define v #f64(3.1415 2.71 1.0)) <- not mutable
; (define v (make-f64vector 3 0))  <- init with zeroes
(define v (list->f64vector '(3.1415 2.71 1.0)))
(display "Guile says ")
(display v)
(incr_f64vector_zig v)
(display "Guile now says ")
(display v)
