// Testing guile bindings, see README.md and
// https://www.gnu.org/savannah-checkouts/gnu/guile/docs/docs-2.0/guile-ref/Dynamic-Types.html

#include <stdio.h>
#include <libguile.h>
#include <libguile/boolean.h>
#include <libguile/numbers.h>

extern void hello_zig();

extern SCM my_incrementing_zig_function (SCM a, SCM flag);

SCM my_incrementing_function (SCM a, SCM flag)
{
  SCM result;

  if (scm_is_true (flag))
    result = scm_sum (a, scm_from_int (1));
  else
    result = a;

  return result;
}


int main() {
    hello_zig();
    SCM test =  scm_from_int(3);
    SCM b = scm_from_bool(1);
    SCM result = my_incrementing_function(test,b);
    printf(" from %d to %d\n",scm_to_int(test),scm_to_int(result));
    hello_zig();
    SCM result2 = my_incrementing_zig_function(result,b);
    printf(" from %d to %d\n",scm_to_int(result),scm_to_int(result2));
    return 0;
}
